<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']                  = 'Auth_Controller';

$route['404_override']                        = 'Error_Controller';
$route['translate_uri_dashes']                = false;

$route['error']                               = 'Error_Controller';
$route['error/(:any)']                        = 'Error_Controller/$1';

// begin: menu umum
$route['dasbor']                              = 'app/Dasbor_Controller';
$route['dasbor/(:any)']                       = 'app/Dasbor_Controller/$1';

$route['appportal']                           = 'AppPortal_Controller';
$route['appportal/(:any)']                    = 'AppPortal_Controller/$1';

$route['aktivasi']                            = 'app/Aktivasi_Controller';
$route['aktivasi/(:any)']                     = 'app/Aktivasi_Controller/$1';

$route['dataumum']                            = 'app/DataUmum_Controller';
$route['dataumum/(:any)']                     = 'app/DataUmum_Controller/$1';

$route['profil']                              = 'app/ProfilPengguna_Controller';
$route['profil/(:any)']                       = 'app/ProfilPengguna_Controller/$1';
// end: menu umum

// begin: riwayat auditor
$route['pendidikan']                          = 'app/Pendidikan_Controller';
$route['pendidikan/(:any)']                   = 'app/Pendidikan_Controller/$1';

$route['diklatsertifikasijfa']                = 'app/DiklatSertifikasiJFA_Controller';
$route['diklatsertifikasijfa/(:any)']         = 'app/DiklatSertifikasiJFA_Controller/$1';

$route['diklatteknissubstantif']              = 'app/DiklatTeknisSubstantif_Controller';
$route['diklatteknissubstantif/(:any)']       = 'app/DiklatTeknisSubstantif_Controller/$1';

$route['diklatsertifikasiprofesi']            = 'app/DiklatSertifikasiProfesi_Controller';
$route['diklatsertifikasiprofesi/(:any)']     = 'app/DiklatSertifikasiProfesi_Controller/$1';

$route['angkakredit']                         = 'app/AngkaKredit_Controller';
$route['angkakredit/(:any)']                  = 'app/AngkaKredit_Controller/$1';

$route['jabatan']                             = 'app/Jabatan_Controller';
$route['jabatan/(:any)']                      = 'app/Jabatan_Controller/$1';

$route['pangkat']                             = 'app/Pangkat_Controller';
$route['pangkat/(:any)']                      = 'app/Pangkat_Controller/$1';

$route['golongan']                            = 'app/Golongan_Controller';
$route['golongan/(:any)']                     = 'app/Golongan_Controller/$1';

$route['jabpanggol']                          = 'app/JabPangGol_Controller';
$route['jabpanggol/(:any)']                   = 'app/JabPangGol_Controller/$1';
// end: riwayat auditor

// begin: menu unit kerja
$route['auditor']                             = 'app/Auditor_Controller';
$route['auditor/(:any)']                      = 'app/Auditor_Controller/$1';

$route['nonauditor']                          = 'app/NonAuditor_Controller';
$route['nonauditor/(:any)']                   = 'app/NonAuditor_Controller/$1';

$route['laporan']                             = 'app/LaporanAuditor_Controller';
$route['laporan/(:any)']                      = 'app/LaporanAuditor_Controller/$1';
// end: menu unit kerja

// begin: menu pusbin
$route['unitkerja']                           = 'app/UnitKerja_Controller';
$route['unitkerja/(:any)']                    = 'app/UnitKerja_Controller/$1';

$route['persetujuan']                           = 'app/Persetujuan_Controller';
$route['persetujuan/(:any)']                    = 'app/Persetujuan_Controller/$1';
// end: menu pusbin

// begin: referensi (master data)
$route['ref-golongan']                        = 'referensi/Golongan_Controller';
$route['ref-golongan/(:any)']                 = 'referensi/Golongan_Controller/$1';

$route['ref-jabatan']                         = 'referensi/Jabatan_Controller';
$route['ref-jabatan/(:any)']                  = 'referensi/Jabatan_Controller/$1';

$route['ref-jenjangjabatan']                  = 'referensi/JenjangJabatan_Controller';
$route['ref-jenjangjabatan/(:any)']           = 'referensi/JenjangJabatan_Controller/$1';

$route['ref-pangkat']                         = 'referensi/Pangkat_Controller';
$route['ref-pangkat/(:any)']                  = 'referensi/Pangkat_Controller/$1';

$route['ref-instansi']                        = 'referensi/Instansi_Controller';
$route['ref-instansi/(:any)']                 = 'referensi/Instansi_Controller/$1';

$route['ref-jenisinstansi']                   = 'referensi/JenisInstansi_Controller';
$route['ref-jenisinstansi/(:any)']            = 'referensi/JenisInstansi_Controller/$1';

$route['ref-struktural']                      = 'referensi/Struktural_Controller';
$route['ref-struktural/(:any)']               = 'referensi/Struktural_Controller/$1';

$route['ref-pendidikan']                      = 'referensi/Pendidikan_Controller';
$route['ref-pendidikan/(:any)']               = 'referensi/Pendidikan_Controller/$1';

$route['ref-unitkerja']                      = 'referensi/UnitKerja_Controller';
$route['ref-unitkerja/(:any)']               = 'referensi/UnitKerja_Controller/$1';

// end: referensi (master data)

// begin: _Auth
$route['auth']                                = 'Auth_Controller';
$route['auth/(:any)']                         = 'Auth_Controller/$1';

$route['login']                               = 'Login_Controller';
$route['login/(:any)']                        = 'Login_Controller/$1';
// end: _Auth

// begin : Pelaporan
$route['laporandataauditor']                      = 'app/LapDataAuditor_Controller';
$route['laporandataauditor/(:any)']               = 'app/LapDataAuditor_Controller/$1';

$route['laporankomposisiauditor']                 = 'app/LapKomposisiAuditor_Controller';
$route['laporankomposisiauditor/(:any)']          = 'app/LapKomposisiAuditor_Controller/$1';

$route['laporankomposisiauditorahli']             = 'app/LapKomposisiAuditorAhli_Controller';
$route['laporankomposisiauditorahli/(:any)']      = 'app/LapKomposisiAuditorAhli_Controller/$1';

$route['laporankomposisiauditorterampil']         = 'app/LapKomposisiAuditorTerampil_Controller';
$route['laporankomposisiauditorterampil/(:any)']  = 'app/LapKomposisiAuditorTerampil_Controller/$1';

$route['laporandaftarauditor']                    = 'app/LapDaftarAuditor_Controller';
$route['laporandaftarauditor/(:any)']             = 'app/LapDaftarAuditor_Controller/$1';

$route['laporanrekapdetail']                      = 'app/LapRekapDetail_Controller';
$route['laporanrekapdetail/(:any)']               = 'app/LapRekapDetail_Controller/$1';

$route['laporanrekapkomposisi']                   = 'app/LapRekapKomposisi_Controller';
$route['laporanrekapkomposisi/(:any)']            = 'app/LapRekapKomposisi_Controller/$1';
// end : Pelaporan
