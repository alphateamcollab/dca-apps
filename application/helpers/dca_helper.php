<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Helpher to verify auth login & menu access
 *
 * @package CodeIgniter
 * @category Helpers
 * @author Kafi Arifin (me@kafiarifin.web.id)
 * @link https://kafiarifin.web.id
 * @version 2.0
 */

 /**
  * Function check logged In or no authentication
  *
  * @param string NIP
  * @return string rerirect to other page (aktivasi, error)
  */
function is_login()
{
    $ci = get_instance();
    if (!$ci->session->userdata('NIP')) {
        redirect('auth');
    } else {
        $ci->db->where('NIP', $ci->session->userdata('NIP'));
        $users   = $ci->db->get('v_pengguna');
        $user    = $users->row_array();
        if ($user['Status'] == 0) {
            redirect('aktivasi');
            exit;
        } elseif ($user['Status'] == 1) {
            redirect('aktivasi/persetujuan');
            exit;
        } else {
            $modul = $ci->uri->segment(1);

            $RoleGroup_ID = $ci->session->userdata('RoleGroup_ID');
            // dapatkan id menu berdasarkan nama controller
            $menu = $ci->db->get_where('r_auth_menu', array('Menu_URL'=>$modul))->row_array();
            $Menu_ID = $menu['Menu_ID'];
            // chek apakah user ini boleh mengakses modul ini
            $hak_akses = $ci->db->get_where('r_auth_akses', array('Menu_ID'=>$Menu_ID,'RoleGroup_ID'=>$RoleGroup_ID));
            if ($hak_akses->num_rows()<1) {
                redirect('error/error403');
                exit;
            }
        }
    }
}

function is_login_global()
{
    // $ci = get_instance();
    // if (!$ci->session->userdata('NIP')) {
    //     redirect('auth');
    // } else {
    //     $ci->db->where('NIP', $ci->session->userdata('NIP'));
    //     $users   = $ci->db->get('v_pengguna');
    //     $user    = $users->row_array();
    //     if ($user['Status'] == 0) {
    //         redirect('aktivasi');
    //         exit;
    //     } elseif ($user['Status'] == 1) {
    //       redirect('aktivasi/persetujuan');
    //       exit;
    //     }
    // }
}

/**
 * Function to change active menu
 *
 * @param string uri at segment 1 or controller name
 * @return string class m-menu__item--active
 */
if (! function_exists('active_link')) {
    function activate_menu($controller)
    {
        // Get CI instance
        $CI = get_instance();
        // Fetch class name.
        $class = $CI->uri->segment(1);
        // Return class active as a string.
        return ($class == $controller) ? 'm-menu__item--active' : '';
    }
}


/**
 * Helpher to print the date in Indonesian language format
 *
 * @package CodeIgniter
 * @category Helpers
 * @author Kafi Arifin (me@kafiarifin.web.id)
 * @link https://kafiarifin.web.id
 * @version 3.0
 */

/**
 * Function to change the month
 * into the month of Indonesia
 * @param int month numbers, Date('MM')
 * @return string month names in Indonesian (ex: Desember)
 */
if (! function_exists('indonesian_month')) {
    function indonesian_month($month)
    {
        switch ($month) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }
}

/**
 * Function to change the month
 * into the Indonesian medium month
 * @param int month numbers, Date('M')
 * @return string 3 characters month names in Indonesian (ex: Des)
 */
if (! function_exists('indonesian_mediummonth')) {
    function indonesian_mediummonth($month)
    {
        switch ($month) {
            case 1:
                return "Jan";
                break;
            case 2:
                return "Feb";
                break;
            case 3:
                return "Mar";
                break;
            case 4:
                return "Apr";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Jun";
                break;
            case 7:
                return "Jul";
                break;
            case 8:
                return "Ags";
                break;
            case 9:
                return "Sep";
                break;
            case 10:
                return "Okt";
                break;
            case 11:
                return "Nov";
                break;
            case 12:
                return "Des";
                break;
        }
    }
}

/**
 * Function to change the month
 * into the Indonesian short number month
 * @param int month numbers, Date('M')
 * @return string short month names in Indonesian (ex: 12)
 */
if (! function_exists('shortmonth')) {
    function shortmonth($month)
    {
        switch ($month) {
            case 1:
                return "01";
                break;
            case 2:
                return "02";
                break;
            case 3:
                return "03";
                break;
            case 4:
                return "04";
                break;
            case 5:
                return "05";
                break;
            case 6:
                return "06";
                break;
            case 7:
                return "07";
                break;
            case 8:
                return "08";
                break;
            case 9:
                return "09";
                break;
            case 10:
                return "10";
                break;
            case 11:
                return "11";
                break;
            case 12:
                return "12";
                break;
        }
    }
}

/**
 * Function to change the date format
 * into the database date format
 * @param void
 * @return string date format (ex: 2015-12-31)
 */
if (!function_exists('database_date')) {
    function database_date($date)
    {
        $change       = gmdate($date, time()+60*60*8);
        $extract      = explode("/", $change);
        $currentdate  = $extract[0];
        $currentmonth = $extract[1];
        $currentyear  = $extract[2];
        $result       = $currentyear . "-" . $currentmonth . "-" . $currentdate;
        return $result;
    }
}

/**
 * Function to change the database date format
 * into the Indonesian date format
 * @param void
 * @return string date format (ex: 31 Desember 2015)
 */
if (! function_exists('indonesian_date')) {
    function indonesian_date($date)
    {
        $change       = gmdate($date, time()+60*60*8);
        $extract      = explode("-", $change);
        $currentdate  = $extract[2];
        $currentmonth = indonesian_month($extract[1]);
        $currentyear  = $extract[0];
        $result       = $currentdate.' '.$currentmonth.' '.$currentyear;
        return $result;
    }
}

/**
 * Function to change the database date format
 * into the Indonesian short date format
 * @param void
 * @return string date format (ex: 31/12/2015)
 */
if (! function_exists('indonesian_shortdate')) {
    function indonesian_shortdate($date)
    {
        $change       = gmdate($date, time()+60*60*8);
        $extract      = explode("-", $change);
        $currentdate  = $extract[2];
        $currentmonth = shortmonth($extract[1]);
        $currentyear  = $extract[0];
        $result       = $currentdate.'/'.$currentmonth.'/'.$currentyear;
        return $result;
    }
}

/**
 * Function to change the database date format
 * into the Indonesian medium date format
 * @param void
 * @return string date format (ex: 31-Des-2015)
 */
if (! function_exists('indonesian_mediumdate')) {
    function indonesian_mediumdate($date)
    {
        $change = gmdate($date, time()+60*60*8);
        $extract = explode("-", $change);
        $currentdate = $extract[2];
        $currentmonth = indonesian_mediummonth($extract[1]);
        $currentyear = $extract[0];
        $result       = $currentdate.'-'.$currentmonth.'-'.$currentyear;
        return $result;
    }
}

/**
 * Function to change the database date format
 * into the Indonesian long date format
 * @param void
 * @return string date format (ex: Senin, 31 Desember 2015)
 */
if (! function_exists('indonesian_longdate')) {
    function indonesian_longdate($date)
    {
        $change       = gmdate($date, time()+60*60*8);
        $extract      = explode("-", $change);
        $currentdate  = $extract[2];
        $currentmonth = $extract[1];
        $currentyear  = $extract[0];
        $indonesian_currentmonth = indonesian_month($extract[1]);

        $currentday   = date("l", mktime(0, 0, 0, $currentmonth, $currentdate, $currentyear));
        $indonesian_currentday = "";
        if ($currentday=="Sunday") {
            $indonesian_currentday="Minggu";
        } elseif ($currentday=="Monday") {
            $indonesian_currentday="Senin";
        } elseif ($currentday=="Tuesday") {
            $indonesian_currentday="Selasa";
        } elseif ($currentday=="Wednesday") {
            $indonesian_currentday="Rabu";
        } elseif ($currentday=="Thursday") {
            $indonesian_currentday="Kamis";
        } elseif ($currentday=="Friday") {
            $indonesian_currentday="Jumat";
        } elseif ($currentday=="Saturday") {
            $indonesian_currentday="Sabtu";
        }
        $result       = $indonesian_currentday.', '.$currentdate.' '.$indonesian_currentmonth.' '.$currentyear;
        return $result;
    }
}
