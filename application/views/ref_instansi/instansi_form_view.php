<div class="m-content">

  <div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                          </span>
          <h3 class="m-portlet__head-text">
                            Input Data Instansi
                          </h3>
        </div>
      </div>
    </div>
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right">
      <div class="m-portlet__body">
        <div class="row">
          <div class="col-md-6">                        <!--begin::Form-->
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Kode Instansi
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Kode Instansi" name="Instansi_Kode">
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Kode Jenis Instansi
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Kode Jenis Instansi" name="JenisInstansi_Kode">
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Nama Instansi
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Nama Instansi" name="Instansi_Nama">
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Kode Kabupaten
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Kode Kabupaten" name="Kabupaten_kode">
              </div>
            </div>
          </div>
          <div class="col-md-6">
                                  <!--begin::Form-->
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Kode Unit Eselon
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Kode Unit Eselon" name="UnitEs1X">
              </div>
            </div>

            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Nama Singkat Instansi
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Nama Singkat Instansi" name="Insatansi_NamaSingkat">
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Instansi Id
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Instansi Id" name="Instansi_Id">
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Instansi Non BPKP
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Instansi Non BPKP" name="Instansi_NonBPKP">
              </div>
            </div>

          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 m--align-left">
          <a href="<?php echo base_url('instansi')?>" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
						<span>
							<i class="la la-arrow-left"></i>
							&nbsp;&nbsp;
							<span>
								Batal
							</span>
						</span>
					</a>
        </div>
        <div class="col-lg-6 m--align-right">
          <a href="<?php echo base_url('instansi')?>" class="btn btn-info m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
						<span>
							<span>
								Simpan
							</span>
							&nbsp;&nbsp;

						</span>
					</a>
        </div>
      </div>
    </form>
  </div>

</div>
