<!-- <Begin : Konten> -->
<script>
function printDiv(elementId) {
    var a = document.getElementById('printing-css').value;
    var b = document.getElementById(elementId).innerHTML;
    window.frames["print_frame"].document.title = document.title;
    window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
    window.frames["print_frame"].window.focus();
    window.frames["print_frame"].window.print();
}
</script>
<div class="m-content">

  <div class="m-portlet__body">
    <!--begin: Search Form -->
    <div class="m-form m-form--label-align-right m--margin-top-10 m--margin-bottom-20">
      <div class="row align-items-center">
        <div class="col-3  order-2 order-xl-1">
          <h5 class="m--font-boldest">NIP / NRP</h5>
        </div>
          <div class="col-7  order-2 order-xl-1">
            <div class="form-group m-form__group row align-items-center">
              <!-- <div class="m-select2 m-select2--pill m-select2--air"> -->
                <input name="Auditor_NIP" class="form-control m-input m-input--air m-input--pill">
                </input >
              <!-- </div> -->
            </div>
          </div>
          <div class="col-2  order-2 order-xl-1 m--align-right">
            <a class="CariNIP no-print btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" href="#" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Cari NIP">
              <i class="la la-search"></i>
            </a>
          </div>
      </div>
    </div>
    <!--end: Search Form -->
  </div>
  <div class="m-portlet m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" id="print-area-2">
    <div class="m-portlet__body  m-portlet__body--no-padding">
        <div class="col-xl-12 order-1 order-xl-2 m--align-right">
          <div class="col-2  order-2 order-xl-1 m--align-right">
          </div>
          <a class="no-print btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" href="javascript:printDiv('print-area-2');" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Cetak Laporan">
            <i class="la la-print"></i>
          </a>
        </div>
        <div class="col-12">
          <div class="m-portlet__body">
            <div class="title">
              <center>
                  <h4 class="m--font-info">Data Umum</h4>
              </center>
                  <table border="1" solid="0" width="100%" border-style="solid" border-color="#FFFfff">
                    <thead>
                      <tr>
                        <th width="30%" align="center">
                          <div class="m-card-profile">
                            <div class="m-card-profile__pic">
                              <div class="m-card-profile__pic-wrapper">
                                <img height="80px"src="<?php echo base_url('assets/app/media/img/users/default.jpg')?>" alt="">
                              </div>
                            </div>
                            <div class="m-card-profile__details">
                              <span class="m-card-profile__name" > <h4> <span class="Pendidikan_GelarDepan"></span> <span class="Auditor_NamaLengkap"></span><span class="Pendidikan_GelarBelakang"></span> </h4> </span>
                              <p href="#" class="m-card-profile__email m-link">NIP. <span class="Auditor_NIP"></span></p>
                            </div>
                          </div>
                        </th>
                        <th width="70%" align="">
                          <div class="m-widget13">
                             <div class="m-widget13__item">
                               <span class="m-widget13__desc m--align-right">
                                 Jenis Kelamin   :
                               </span>
                               <span class="m-widget13__text m-widget13__text-bolder Auditor_JenisKelamin">
                               </span>
                             </div>
                             <div class="m-widget13__item">
                               <span class="m-widget13__desc m--align-right">
                                 Tempat Lahir   :
                               </span>
                               <span class="m-widget13__text m-widget13__text-bolder Auditor_TempatLlahir">
                               </span>
                             </div>
                             <div class="m-widget13__item">
                               <span class="m-widget13__desc m--align-right">
                                 Tanggal Lahir   :
                               </span>
                               <span class="m-widget13__text m-widget13__text-bolder Auditor_TglLahir">
                               </span>
                             </div>
                             <div class="m-widget13__item">
                               <span class="m-widget13__desc m--align-right">
                                 Alamat   :
                               </span>
                               <span class="m-widget13__text Auditor_Alamat">
                               </span>
                             </div>
                             <div class="m-widget13__item">
                               <span class="m-widget13__desc m--align-right">
                                 Surat Elektronik :
                               </span>
                               <span class="m-widget13__text Auditor__NoHP">
                               </span>
                             </div>
                             <div class="m-widget13__item">
                               <span class="m-widget13__desc m--align-right">
                                 Nomor HP  :
                               </span>
                               <span class="m-widget13__text m-widget13__number-bolder m--font-info Auditor_Surel">
                               </span>
                             </div>
                           </div>
                        </th>
                      </tr>
                    </thead>
                  </table>
            </div>
           <br>
           <div class="title">
               <center>
                   <h4 class="m--font-info">Data Unit Kerja</h4>
               </center>
               <div class="m-widget13">
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Jenis Lembaga :
                    </span>
                    <span class="m-widget13__text m-widget13__text-bolder JenisInstansi_Nama">
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Nama Instansi :
                    </span>
                    <span class="m-widget13__text m-widget13__text-bolder Instansi_Nama">
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Nama Unit Kerja :
                    </span>
                    <span class="m-widget13__text m-widget13__text-bolder UnitKerja_Nama">
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Alamat Kantor :
                    </span>
                    <span class="m-widget13__text UnitKerja_Alamat">
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Surat Elektronik :
                    </span>
                    <span class="m-widget13__text UnitKerja_Surel">
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Nomor Telepon Kantor :
                    </span>
                    <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoTlp">
                    </span>
                  </div>
                  <div class="m-widget13__item">
                    <span class="m-widget13__desc m--align-right">
                      Nomor Faximili Kantor :
                    </span>
                    <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoFax">
                    </span>
                  </div>
                </div>

            </div>
            <br>
            <div class="title">
                <center>
                    <h4 class="m--font-info">Data Detail Riwayat</h4>
                </center>
              </div>
            <h5 align="left"class="m--font-info">Pendidikan</h5>
            <table border="1" width="100%" style="border-collapse:collapse;" align="center" id="pendidikan">
              <thead class="table-info">
                <tr align="center">
                  <th rowspan="2" >Jenjang</th>
                  <th rowspan="2" >Lembaga Pendidikan</th>
                  <th rowspan="2" >Jurusan</th>
                  <th rowspan="2" >Tanggal Ijazah</th>
                </tr>
              </thead>
              <tbody>
                <tr align="center">
                  <td ></td>
                  <td ></td>
                  <td ></td>
                  <td ></td>
                </tr>
              </tbody>
            </table>
            <br>
            <h5 align="left"class="m--font-info">Pangkat & Golongan</h5>
            <table border="1" width="100%" style="border-collapse:collapse;" align="center">
              <thead class="table-info">
                <tr align="center">
                  <th rowspan="2" >Pangkat</th>
                  <th rowspan="2" >Golongan</th>
                  <th rowspan="2" >Nomor SK</th>
                  <th rowspan="2" >Tanggal SK</th>
                  <th rowspan="2" >TMT Pangkat</th>
                </tr>
              </thead>
              <tbody>
                <tr align="center">
                  <td ></td>
                  <td ></td>
                  <td ></td>
                  <td ></td>
                  <td ></td>
                </tr>
              </tbody>
            </table>
            <br>
            <h5 align="left"class="m--font-info">Jabatan</h5>
            <table border="1" width="100%" style="border-collapse:collapse;" align="center">
              <thead class="table-info">
                <tr align="center">
                  <th rowspan="2" >Jabatan</th>
                  <th rowspan="2" >Jenjang Jabatan</th>
                  <th rowspan="2" >Nomor SK</th>
                  <th rowspan="2" >Tanggal SK</th>
                  <th rowspan="2" >TMT Jabatan</th>
                </tr>
              </thead>
              <tbody>
                <tr align="center">
                  <td ></td>
                  <td ></td>
                  <td ></td>
                  <td ></td>
                  <td ></td>
                </tr>
              </tbody>
            </table>
            <br>
            <h5 align="left"class="m--font-info">Diklat Sertifikasi JFA</h5>
            <table border="1" width="100%" style="border-collapse:collapse;" align="center">
              <thead class="table-info">
                <tr align="center">
                  <th rowspan="2" >Nama Diklat</th>
                  <th rowspan="2" >Periode Diklat</th>
                  <th colspan="1" >STMPL</th>
                  <th colspan="1" >STMPL</th>
                </tr>
                <td align="center">Tanggal dan Nomor</td>
                <td align="center">Tanggal dan Nomor</td>

              </thead>
              <tbody>
                <tr align="center">
                  <td ></td>
                  <td ></td>
                  <td ></td>
                  <td ></td>
                </tr>
              </tbody>
            </table>
            <br>
            <h5 align="left"class="m--font-info">Diklat Teknis Substantif</h5>
            <table border="1" width="100%" style="border-collapse:collapse;" align="center">
              <thead class="table-info">
                <tr align="center">
                  <th >Nama Diklat</th>
                  <th >Periode Diklat</th>
                  <th >No.STMPL</th>
                  <th >Tgl.STMPL</th>
                </tr>
              </thead>
              <tbody>
                <tr align="center">
                  <td ></td>
                  <td ></td>
                  <td ></td>
                  <td ></td>
                </tr>
              </tbody>
            </table>
            <br>
            <h5 align="left"class="m--font-info">Diklat Sertifikasi Profesi</h5>
            <table border="1" width="100%" style="border-collapse:collapse;" align="center">
              <thead class="table-info">
                <tr align="center">
                  <th >Nama Diklat</th>
                  <th >Periode Diklat</th>
                  <th >No.STMPL</th>
                  <th >Tgl.STMPL</th>
                </tr>
              </thead>
              <tbody>
                <tr align="center">
                  <td ></td>
                  <td ></td>
                  <td ></td>
                  <td ></td>
                </tr>
              </tbody>
            </table>
            <br>
            <h5 align="left"class="m--font-info">Angka Kredit</h5>
            <table border="1" width="100%" style="border-collapse:collapse;" align="center">
              <thead class="table-info">
                <tr align="center">
                  <th >Nama Diklat</th>
                  <th >Periode Diklat</th>
                  <th >No.STMPL</th>
                  <th >Tgl.STMPL</th>
                </tr>
              </thead>
              <tbody>
                <tr align="center">
                  <td ></td>
                  <td ></td>
                  <td ></td>
                  <td ></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

</div>
<script>
/* Ajax Dropdown Instansi */
function getInstansi(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_instansi');?>",
    data: {value},
    success: function(data) {
      $("#KodeUnitKerja option:gt(0)").remove();
      $("#KodeInstansi").html(data);
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
};

/* Ajax Dropdown Unit Kerja */
function getUnitKerja(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_unitkerja');?>",
    data:{value},
    success: function(data) {
      $("#KodeUnitKerja").html(data);
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
}

/* Ajax Dropdown Prestasi Nilai */
function getPangkat(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_pangkat');?>",
    data: {value},
    success: function(data) {
      document.getElementById('pangkatgol').value = data;
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
}

/* Ajax Dropdown Jabatan */
function getJenjangJabatan(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_jenjangjabatan');?>",
    data: {value},
    success: function(data) {
      $("#jenjangjabatan").html(data);
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
};

//-- BEGIN: load data Ringkasan Profil & UnitKerja
$(document).ready(function(){
  getProfil();
});

// function getProfil(){
$(document).on('click', '.CariNIP' ,function(){
  // $('#CariNIP').on('click',function(){

    var id=$('[name="Auditor_NIP"]').val();
    // var id='$('[name="Auditor_NIP"]')val()';

    // memuat data dengan ajax
    $.ajax({
        type: "GET",
        url : "<?php echo site_url('auditor/ambil')?>",
        dataType: "JSON",
        data : {id:id},
        success: function(data)
        {
            $.each(data,function(NIP, isAuditor, NamaLengkap, GelarDepan, GelarBelakang, Pangkat_Nama, Golongan_Kode, JenjangJabatan_Nama, JenisInstansi_Nama, Instansi_Nama, UnitKerja_Nama, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax, UnitKerja_Web)
          {
            // ambil nilai input dari parameter JSON
            $('.Auditor_NIP').text(data[0].Auditor_NIP);
            if (data[0].IsAuditor == 'true') {
              $('.isAuditor').text('Aktif');
            }else {
              $('.isAuditor').text('Non-Aktif');
            }
            $('.Auditor_NamaLengkap').text(data[0].Auditor_NamaLengkap);
            $('.Pendidikan_GelarDepan').text(data[0].Pendidikan_GelarDepan);
            // $('.Auditor_JenisKelamin').text(data[0].Auditor_JenisKelamin);
            if (data[0].Auditor_JenisKelamin == 'L') {
              $('.Auditor_JenisKelamin').text('Laki-Laki');
            }else {
              $('.Auditor_JenisKelamin').text('Perempuan');
            }
            $('.Auditor_TempatLlahir').text(data[0].Auditor_TempatLlahir);
            $('.Auditor_TglLahir').text(data[0].Auditor_TglLahir);
            $('.Auditor_Alamat').text(data[0].Auditor_Alamat);
            $('.Auditor_Surel').text(data[0].Auditor_Surel);
            $('.Auditor_NoHP').text(data[0].Auditor_NoHP);
            $('.Pangkat_Nama').text(data[0].Pangkat_Nama);
            $('.Golongan_Kode').text(data[0].Golongan_Kode);
            $('.JenjangJabatan_Nama').text(data[0].JenjangJabatan_Nama);
            $('.JenisInstansi_Nama').text(data[0].JenisInstansi_Nama);
            $('.Instansi_Nama').text(data[0].Instansi_Nama);
            $('.UnitKerja_Nama').text(data[0].UnitKerja_Nama);
            $('.UnitKerja_Alamat').text(data[0].UnitKerja_Alamat);
            $('.UnitKerja_Surel').text(data[0].UnitKerja_Surel);
            $('.UnitKerja_NoTlp').text(data[0].UnitKerja_NoTlp);
            $('.UnitKerja_NoFax').text(data[0].UnitKerja_NoFax);
            $('.UnitKerja_Web').text(data[0].UnitKerja_Web);
          });
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax');
        }
    });
    return false;
});
//-- END: load data Ringkasan Profil & UnitKerja

//-- BEGIN: dataTable remote pendidikan
function getPendidikan(){
// $(document).on('click', '.CariNIP' ,function(){
  var id=$('[name="Auditor_NIP"]').val();
  $.ajax({
      type: "GET",
      url : "<?php echo site_url('auditor/pendidikan_detail')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data){
        var isiTabel='';
          $.each(data,function(Pendidikan_Jenjang, Pendidikan_Lembaga, Pendidikan_Jurusan, Pendidikan_TglIjazah)
          {isiTabel += '<tr><td>'+data[0]Pendidikan_Jenjang+'</td><td>'+data[0]Pendidikan_Lembaga+'</td><td>'+data[0]Pendidikan_Jurusan+'</td><td>'+data[0]Pendidikan_TglIjazah+'</td></tr>';}
            );
          $('#pendidikan').append(isiTabel);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax');
        }
    });
    return false;
};
//-- END: dataTable remote pendidikan

//-- BEGIN: dataTable remote jabatan
var DatatableJsonRemotePangkat = function () {

  //-- BEGIN: fungsi_data
  var pangkat = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/pangkat_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
			columns: [
      {
        field: "Pangkat_Nama",
        title: "Pangkat"
      }, {
        field: "GolRuang_Kode",
        title: "Golongan Ruang",
        textAlign: 'center',
      }, {
        field: "Pangkat_NoSuratKeputusan",
        title: "Nomor Surat Keputusan",
      },{
        field: "Pangkat_TglSuratKeputusan",
        title: "Tanggal Surat",
        textAlign: 'center'
      }, {
        field: "Pangkat_TglMulaiTugas",
        title: "TMT Pangkat",
        textAlign: 'center',
        sortable: 'desc'
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_pangkat').mDatatable(tabel_data);
        var key = $('[name="PangkatGol_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_pangkat').on('click', function() {
          $('#json_pangkat').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_pangkat').on('click', function() {
          // variable untuk menyimpan nilai input
          var id            = key.val();
          var nip           = '<?php echo $this->input->get('id');?>';
          var kode          = $('[name="Pangkat_Kode"]').val();
          var pangkat       = $('[name="Pangkat_Nama"]').val();
          var noSK          = $('[name="Pangkat_NoSuratKeputusan"]').val();
          var tglSK         = $('[name="Pangkat_TglSuratKeputusan"]').val();
          var tmt           = $('[name="Pangkat_TglMulaiTugas"]').val();
          var file          = $('[name="Pangkat_Sk"]').val();
          // ubah teks tombol
          $('#m_data_simpan_pangkat').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_pangkat').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/pangkat_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/pangkat_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajax({
              type: "POST",
              url : url,
              dataType: "JSON",
              data : {PangkatGol_ID:id, Auditor_NIP:nip, Pangkat_Kode:kode, Pangkat_Nama:pangkat, Pangkat_NoSuratKeputusan:noSK, Pangkat_TglSuratKeputusan:tglSK, Pangkat_TglMulaiTugas:tmt, Pangkat_Sk:file},
              success: function(data)
              {
                $('#modal_form_pangkat').modal('hide');
                $('#json_pangkat').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_pangkat').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_pangkat').attr('disabled',false);
                // panggil fungsi getProfil
                getProfil();
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function ()
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_pangkat').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_pangkat').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_pangkat').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_pangkat')[0].reset();
          // tampilkan modal
          $('#modal_form_pangkat').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Pangkat Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_pangkat', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_pangkat')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/pangkat_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(PangkatGol_ID, Auditor_NIP, Pangkat_Kode, Pangkat_Nama, Pangkat_NoSuratKeputusan, Pangkat_TglSuratKeputusan, Pangkat_TglMulaiTugas, Pangkat_Sk)
                {
                  // tampilkan modal
                  $('#modal_form_pangkat').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Pangkat');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].PangkatGol_ID);
                  $('[name="Auditor_NIP"]').val(data[0].Auditor_NIP);
                  $('[name="Pangkat_Kode"]').val(data[0].Pangkat_Kode);
                  $('[name="Pangkat_Nama"]').val(data[0].Pangkat_Nama);
                  $('[name="Pangkat_NoSuratKeputusan"]').val(data[0].Pangkat_NoSuratKeputusan);
                  $('[name="Pangkat_TglSuratKeputusan"]').datepicker('update', data[0].Pangkat_TglSuratKeputusan);
                  $('[name="Pangkat_TglMulaiTugas"]').datepicker('update', data[0].Pangkat_TglMulaiTugas);
                  $('[name="Pangkat_Sk"]').val(data[0].Pangkat_Sk);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_pangkat', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/pangkat_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_pangkat').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // panggil fungsi getProfil
                getProfil();
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      pangkat();
    }
  };
}();
//-- END: dataTable remote pangkat

//-- BEGIN: dataTable remote jabatan
var DatatableJsonRemoteJabatan = function () {

  //-- BEGIN: fungsi_data
  var jabatan = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/jabatan_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Jabatan_Nama",
        title: "Jabatan",
        // textAlign: 'center',
        sortable: false
      }, {
        field: "JenjangJabatan_Nama",
        title: "Jenjang Jabatan",
      }, {
        field: "Jabatan_NoSuratKeputusan",
        title: "Nomor Surat Keputusan",
      }, {
        field: "Jabatan_TglSuratKeputusan",
        title: "Tanggal Surat",
        textAlign: 'center',
      }, {
        field: "Jabatan_TglMulaiTugas",
        title: "TMT Jabatan",
        textAlign: 'center',
        // width: 80,
        sortable: 'desc'
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_jabatan').mDatatable(tabel_data);
        var key = $('[name="Jabatan_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_jabatan').on('click', function() {
          $('#json_jabatan').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_jabatan').on('click', function() {
          // variable untuk menyimpan nilai input
          var id            = key.val();
          var nip           = '<?php echo $this->input->get('id');?>';
          var kelompokjab   = $('[name="Jabatan_Kode"]').val();
          var jabatan       = $('[name="JenjangJabatan_Kode"]').val();
          var nomor         = $('[name="Jabatan_NoSuratKeputusan"]').val();
          var tgl           = $('[name="Jabatan_TglSuratKeputusan"]').val();
          var tmt           = $('[name="Jabatan_TglMulaiTugas"]').val();
          var sk            = $('[name="Jabatan_Sk"]').val();
          // ubah teks tombol
          $('#m_data_simpan_jabatan').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_jabatan').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/jabatan_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/jabatan_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajax({
              type: "POST",
              url : url,
              dataType: "JSON",
              data : {Jabatan_ID:id, Auditor_NIP:nip, Jabatan_Kode:kelompokjab, JenjangJabatan_Kode:jabatan, Jabatan_NoSuratKeputusan:nomor, Jabatan_TglSuratKeputusan:tgl, Jabatan_TglMulaiTugas:tmt, Jabatan_Sk:sk},
              success: function(data)
              {
                $('#modal_form_jabatan').modal('hide');
                $('#json_jabatan').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_jabatan').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_jabatan').attr('disabled',false);
                // panggil fungsi getProfil
                getProfil();
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function ()
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_jabatan').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_jabatan').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_jabatan').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form')[0].reset();
          // tampilkan modal
          $('#modal_form_jabatan').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Jabatan Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_jabatan', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/jabatan_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(Jabatan_Kode, JenjangJabatan_Kode, Jabatan_NoSuratKeputusan, Jabatan_TglSuratKeputusan, Jabatan_TglMulaiTugas, Jabatan_Sk)
                {
                  // tampilkan modal
                  $('#modal_form_jabatan').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Jabatan');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].Jabatan_ID);
                  $('[name="Jabatan_Kode"]').val(data[0].Jabatan_Kode);
                  $('[name="JenjangJabatan_Kode"]').val(data[0].JenjangJabatan_Kode);
                  $('[name="Jabatan_NoSuratKeputusan"]').val(data[0].Jabatan_NoSuratKeputusan);
                  $('[name="Jabatan_TglSuratKeputusan"]').datepicker('update', data[0].Jabatan_TglSuratKeputusan);
                  $('[name="Jabatan_TglMulaiTugas"]').datepicker('update', data[0].Jabatan_TglMulaiTugas);
                  $('[name="Jabatan_Sk"]').val(data[0].Jabatan_Sk);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_jabatan', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/jabatan_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_jabatan').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // panggil fungsi getProfil
                getProfil();
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      jabatan();
    }
  };
}();
//-- END: dataTable remote jabatan

//-- BEGIN: dataTable remote sertifikasijfa
var DatatableJsonRemoteDiklatJFA = function () {

  //-- BEGIN: fungsi_data
  var sertifikasijfa = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/sertifikasijfa_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Diklat_Nama",
        title: "Nama Diklat",
        // width: 180,
        // textAlign: 'center',
        sortable: false
      }, {
        field: "SertifikasiJFA_TglMulaiDiklat",
        title: "Periode Diklat",
        template: function (row) {
          // callback function support for column rendering
          var periode =
          '' + row.SertifikasiJFA_TglMulaiDiklat + '<br> <span class="m--font-bold"> s/d </span> <br> ' + row.SertifikasiJFA_TglAkhirDiklat + '';
          return periode;
        },
        // width: 80,
        textAlign: 'center'
      }, {
        field: "SertifikasiJFA_TglSTMPL",
        title: "Tgl. & No. STMPL",
        // width: 140,
        textAlign: 'center',
        sortable: 'desc',
        template: function (row) {
          // callback function support for column rendering
          var stmpl =
          '<span class="m--font-bold">Tgl.: </span>' + row.SertifikasiJFA_TglSTMPL + '<br>\
          <span class="m--font-bold">No.: </span>' + row.SertifikasiJFA_NoSTMPL + '';
          return stmpl;
        }
      }, {
        field: "SertifikasiJFA_TglSTTPL",
        title: "Tgl. & No. STTPL",
        // width: 140,
        textAlign: 'center',
        template: function (row) {
          // callback function support for column rendering
          var sttpl =
          '<span class="m--font-bold">Tgl.: </span>' + row.SertifikasiJFA_TglSTTPL + '<br>\
          <span class="m--font-bold">No.: </span>' + row.SertifikasiJFA_NoSTTPL + '';
          return sttpl;
        }
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_sertifikasijfa').mDatatable(tabel_data);
        var key = $('[name="SertifikasiJFA_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_sertifikasijfa').on('click', function() {
          $('#json_sertifikasijfa').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_sertifikasijfa').on('click', function() {
          // variable untuk menyimpan nilai input
          var id        = key.val();
          var nip       = '<?php echo $this->input->get('id');?>';
          var kode      = $('[name="Diklat_Kode_JFA"]').val();
          var mulai     = $('[name="SertifikasiJFA_TglMulaiDiklat"]').val();
          var akhir     = $('[name="SertifikasiJFA_TglAkhirDiklat"]').val();
          var nostmpl   = $('[name="SertifikasiJFA_NoSTMPL"]').val();
          var tglstmpl  = $('[name="SertifikasiJFA_TglSTMPL"]').val();
          var nosttpl   = $('[name="SertifikasiJFA_NoSTTPL"]').val();
          var tglsttpl  = $('[name="SertifikasiJFA_TglSTTPL"]').val();
          var file      = $('[name="SertifikasiJFA_Sertifikat"]').val();
          // ubah teks tombol
          $('#m_data_simpan_sertifikasijfa').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_sertifikasijfa').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/sertifikasijfa_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/sertifikasijfa_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajax({
              type: "POST",
              url : url,
              dataType: "JSON",
              data : {SertifikasiJFA_ID:id, Auditor_NIP:nip, Diklat_Kode_JFA:kode, SertifikasiJFA_TglMulaiDiklat:mulai, SertifikasiJFA_TglAkhirDiklat:akhir, SertifikasiJFA_NoSTMPL:nostmpl, SertifikasiJFA_TglSTMPL:tglstmpl, SertifikasiJFA_NoSTTPL:nosttpl, SertifikasiJFA_TglSTTPL:tglsttpl, SertifikasiJFA_Sertifikat:file},
              success: function(data)
              {
                $('#modal_form_sertifikasijfa').modal('hide');
                $('#json_sertifikasijfa').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_sertifikasijfa').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_sertifikasijfa').attr('disabled',false);
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function ()
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_sertifikasijfa').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_sertifikasijfa').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_sertifikasijfa').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_sertifikasijfa')[0].reset();
          // tampilkan modal
          $('#modal_form_sertifikasijfa').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Sertifikasi JFA Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_sertifikasijfa', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/sertifikasijfa_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(Diklat_Kode, SertifikasiJFA_TglMulaiDiklat, SertifikasiJFA_TglAkhirDiklat, SertifikasiJFA_NoSTMPL, SertifikasiJFA_TglSTMPL, SertifikasiJFA_NoSTTPL, SertifikasiJFA_TglSTTPL, SertifikasiJFA_Sertifikat )
                {
                  // tampilkan modal
                  $('#modal_form_sertifikasijfa').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Sertifikasi JFA');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].SertifikasiJFA_ID);
                  $('[name="Diklat_Kode_JFA"]').val(data[0].Diklat_Kode);
                  $('[name="SertifikasiJFA_TglMulaiDiklat"]').datepicker('update', data[0].SertifikasiJFA_TglMulaiDiklat);
                  $('[name="SertifikasiJFA_TglAkhirDiklat"]').datepicker('update', data[0].SertifikasiJFA_TglAkhirDiklat);
                  $('[name="SertifikasiJFA_NoSTMPL"]').val(data[0].SertifikasiJFA_NoSTMPL);
                  $('[name="SertifikasiJFA_TglSTMPL"]').datepicker('update', data[0].SertifikasiJFA_TglSTMPL);
                  $('[name="SertifikasiJFA_NoSTTPL"]').val(data[0].SertifikasiJFA_NoSTTPL);
                  $('[name="SertifikasiJFA_TglSTTPL"]').datepicker('update', data[0].SertifikasiJFA_TglSTTPL);
                  $('[name="SertifikasiJFA_Sertifikat"]').val(data[0].SertifikasiJFA_Sertifikat);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_sertifikasijfa', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/sertifikasijfa_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_sertifikasijfa').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      sertifikasijfa();
    }
  };
}();
//-- END: dataTable remote sertifikasijfa

//-- BEGIN: dataTable remote diklatteknissubstantif
var DatatableJsonRemoteDiklatSubstantif = function () {

  //-- BEGIN: fungsi_data
  var diklatteknissubstantif = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/teknissubstantif_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Diklat_Nama",
        title: "Nama Diklat",
        width: 180,
        // textAlign: 'center',
        sortable: false
      }, {
        field: "TeknisSubstantif_TglMulai",
        title: "Periode Diklat",
        template: function (row) {
          // callback function support for column rendering
          var periode =
          '' + row.TeknisSubstantif_TglMulai + '<br> <span class="m--font-bold"> s/d </span> <br> ' + row.TeknisSubstantif_TglAkhir + '';
          return periode;
        },
        width: 80,
        textAlign: 'center'
      }, {
        field: "TeknisSubstantif_NoSTMPL",
        title: "Tgl. & No. STMPL",
        width: 140,
        textAlign: 'center',
        sortable: 'desc',
        template: function (row) {
          // callback function support for column rendering
          var stmpl =
          '<span class="m--font-bold">Tgl.: </span>' + row.TeknisSubstantif_TglSTMPL + '<br>\
          <span class="m--font-bold">No.: </span>' + row.TeknisSubstantif_NoSTMPL + '';
          return stmpl;
        }
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_diklatteknissubstantif').mDatatable(tabel_data);
        var key = $('[name="TeknisSubstantif_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_diklatteknissubstantif').on('click', function() {
          $('#json_diklatteknissubstantif').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_teknissubstantif').on('click', function() {
          // variable untuk menyimpan nilai input
          var id         = key.val();
          var nip        = '<?php echo $this->input->get('id');?>';
          var kode       = $('[name="Diklat_Kode_TekSub"]').val();
          var mulai      = $('[name="TeknisSubstantif_TglMulai"]').val();
          var akhir      = $('[name="TeknisSubstantif_TglAkhir"]').val();
          var nostmpl    = $('[name="TeknisSubstantif_NoSTMPL"]').val();
          var tglstmpl   = $('[name="TeknisSubstantif_TglSTMPL"]').val();
          var file       = $('[name="TeknisSubstantif_Sertifikat"]').val();
          // ubah teks tombol
          $('#m_data_simpan_teknissubstantif').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_teknissubstantif').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/teknissubstantif_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/teknissubstantif_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajax({
              type: "POST",
              url : url,
              dataType: "JSON",
              data : {TeknisSubstantif_ID:id, Auditor_NIP:nip, Diklat_Kode_TekSub:kode, TeknisSubstantif_TglMulai:mulai, TeknisSubstantif_TglAkhir:akhir, TeknisSubstantif_NoSTMPL:nostmpl, TeknisSubstantif_TglSTMPL:tglstmpl, TeknisSubstantif_Sertifikat:file},
              success: function(data)
              {
                $('#modal_form_teknissubstantif').modal('hide');
                $('#json_diklatteknissubstantif').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_teknissubstantif').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_teknissubstantif').attr('disabled',false);
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function ()
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_teknissubstantif').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_teknissubstantif').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_diklatteknissubstantif').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_teknissubstantif')[0].reset();
          // tampilkan modal
          $('#modal_form_teknissubstantif').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Diklat Teknis Substantif Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_diklatteknissubstantif', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_teknissubstantif')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/teknissubstantif_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(Diklat_Kode, TeknisSubstantif_TglMulai, TeknisSubstantif_TglAkhir, TeknisSubstantif_NoSTMPL, TeknisSubstantif_TglSTMPL, TeknisSubstantif_Sertifikat)
                {
                  // tampilkan modal
                  $('#modal_form_teknissubstantif').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Diklat Teknis Substantif');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].TeknisSubstantif_ID);
                  $('[name="Diklat_Kode_TekSub"]').val(data[0].Diklat_Kode);
                  $('[name="TeknisSubstantif_TglMulai"]').datepicker('update', data[0].TeknisSubstantif_TglMulai);
                  $('[name="TeknisSubstantif_TglAkhir"]').datepicker('update', data[0].TeknisSubstantif_TglAkhir);
                  $('[name="TeknisSubstantif_NoSTMPL"]').val(data[0].TeknisSubstantif_NoSTMPL);
                  $('[name="TeknisSubstantif_TglSTMPL"]').datepicker('update', data[0].TeknisSubstantif_TglSTMPL);
                  $('[name="TeknisSubstantif_Sertifikat"]').val(data[0].TeknisSubstantif_Sertifikat);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_diklatteknissubstantif', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/teknissubstantif_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_diklatteknissubstantif').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      diklatteknissubstantif();
    }
  };
}();
//-- END: dataTable remote diklatteknissubstantif

//-- BEGIN: dataTable remote diklatsertifikasiprofesi
var DatatableJsonRemoteDiklatProfesi = function () {

  //-- BEGIN: fungsi_data
  var diklatsertifikasiprofesi = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/sertifikasiprofesi_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "SertifikasiProfesi_Nama",
        title: "Nama Diklat",
        // width: 180,
        // textAlign: 'center',
        sortable: false
      }, {
        field: "SertifikasiProfesi_Gelar",
        title: "Gelar Profesi",
        responsive: {visible: 'lg'}
      }, {
        field: "SertifikasiProfesi_TglMulai",
        title: "Periode Diklat",
        template: function (row) {
          // callback function support for column rendering
          var periode =
          '' + row.SertifikasiProfesi_TglMulai + '<br> <span class="m--font-bold"> s/d </span> <br> ' + row.SertifikasiProfesi_TglAkhir + '';
          return periode;
        },
        // width: 80,
        textAlign: 'center'
      }, {
        field: "SertifikasiProfesi_TglSTMPL",
        title: "Tgl. & No. STMPL",
        // width: 140,
        textAlign: 'center',
        sortable: 'desc',
        template: function (row) {
          // callback function support for column rendering
          var stmpl =
          '<span class="m--font-bold">Tgl.: </span>' + row.SertifikasiProfesi_TglSTMPL + '<br>\
          <span class="m--font-bold">No.: </span>' + row.SertifikasiProfesi_NoSTMPL + '';
          return stmpl;
        }
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_sertifikasiprofesi').mDatatable(tabel_data);
        var key = $('[name="SertifikasiProfesi_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_sertifikasiprofesi').on('click', function() {
          $('#json_sertifikasiprofesi').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_sertifikasiprofesi').on('click', function() {
          // variable untuk menyimpan nilai input
          var id         = key.val();
          var nip        = '<?php echo $this->input->get('id');?>';
          var nama       = $('[name="SertifikasiProfesi_Nama"]').val();
          var lembaga    = $('[name="SertifikasiProfesi_Lembaga"]').val();
          var gelar      = $('[name="SertifikasiProfesi_Gelar"]').val();
          var mulai      = $('[name="SertifikasiProfesi_TglMulai"]').val();
          var akhir      = $('[name="SertifikasiProfesi_TglAkhir"]').val();
          var nostmpl    = $('[name="SertifikasiProfesi_NoSTMPL"]').val();
          var tglstmpl   = $('[name="SertifikasiProfesi_TglSTMPL"]').val();
          var file       = $('[name="SertifikasiProfesi_Sertifikat"]').val();
          // ubah teks tombol
          $('#m_data_simpan_sertifikasiprofesi').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_sertifikasiprofesi').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/sertifikasiprofesi_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/sertifikasiprofesi_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajax({
              type: "POST",
              url : url,
              dataType: "JSON",
              data : {SertifikasiProfesi_ID:id, Auditor_NIP:nip, SertifikasiProfesi_Nama:nama, SertifikasiProfesi_Lembaga:lembaga, SertifikasiProfesi_Gelar:gelar, SertifikasiProfesi_TglMulai:mulai, SertifikasiProfesi_TglAkhir:akhir, SertifikasiProfesi_NoSTMPL:nostmpl, SertifikasiProfesi_TglSTMPL:tglstmpl, SertifikasiProfesi_Sertifikat:file},
              success: function(data)
              {
                $('#modal_form_sertifikasiprofesi').modal('hide');
                $('#json_sertifikasiprofesi').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_sertifikasiprofesi').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_sertifikasiprofesi').attr('disabled',false);
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function ()
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_sertifikasiprofesi').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_sertifikasiprofesi').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_sertifikasiprofesi').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_sertifikasiprofesi')[0].reset();
          // tampilkan modal
          $('#modal_form_sertifikasiprofesi').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_sertifikasiprofesi', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_sertifikasiprofesi')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/sertifikasiprofesi_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(SertifikasiProfesi_Nama, SertifikasiProfesi_Lembaga, SertifikasiProfesi_Gelar, SertifikasiProfesi_TglMulai, SertifikasiProfesi_TglAkhir, SertifikasiProfesi_NoSTMPL, SertifikasiProfesi_TglSTMPL, SertifikasiProfesi_Sertifikat )
                {
                  // tampilkan modal
                  $('#modal_form_sertifikasiprofesi').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].SertifikasiProfesi_ID);
                  $('[name="SertifikasiProfesi_Nama"]').val(data[0].SertifikasiProfesi_Nama);
                  $('[name="SertifikasiProfesi_Lembaga"]').val(data[0].SertifikasiProfesi_Lembaga);
                  $('[name="SertifikasiProfesi_Gelar"]').val(data[0].SertifikasiProfesi_Gelar);
                  $('[name="SertifikasiProfesi_TglMulai"]').datepicker('update', data[0].SertifikasiProfesi_TglMulai);
                  $('[name="SertifikasiProfesi_TglAkhir"]').datepicker('update', data[0].SertifikasiProfesi_TglAkhir);
                  $('[name="SertifikasiProfesi_NoSTMPL"]').val(data[0].SertifikasiProfesi_NoSTMPL);
                  $('[name="SertifikasiProfesi_TglSTMPL"]').datepicker('update', data[0].SertifikasiProfesi_TglSTMPL);
                  $('[name="SertifikasiProfesi_Sertifikat"]').val(data[0].SertifikasiProfesi_Sertifikat);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_sertifikasiprofesi', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/sertifikasiprofesi_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_sertifikasiprofesi').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      diklatsertifikasiprofesi();
    }
  };
}();
//-- END: dataTable remote diklatsertifikasiprofesi

//-- BEGIN: dataTable remote angkakredit
var DatatableJsonRemoteAngkaKredit = function () {

  //-- BEGIN: fungsi_data
  var angkakredit = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/angkakredit_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "AngkaKredit_NoPAK",
        title: "Nomor PAK",
        // width: 60,
        // textAlign: 'center',
        sortable: false
      }, {
        field: "AngkaKredit_TglPAK",
        title: "Tanggal PAK",
        // width: 160,
        sortable: 'desc'
      }, {
        field: "AngkaKredit_Nilai",
        title: "Nilai Angka Kredit",
        // width: 80
      }, {
        field: "AngkaKredit_TglMulai",
        title: "Masa Penilaian",
        template: function (row) {
          // callback function support for column rendering
          var periode =
          '' + row.AngkaKredit_TglMulai + '<br> <span class="m--font-bold"> s/d </span> <br> ' + row.AngkaKredit_TglAkhir + '';
          return periode;
        },
        // width: 80,
        textAlign: 'center'
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_angkakredit').mDatatable(tabel_data);
        var key = $('[name="AngkaKredit_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_angkakredit').on('click', function() {
          $('#json_angkakredit').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_angkakredit').on('click', function() {
          // variable untuk menyimpan nilai input
          var id         = key.val();
          var nip        = '<?php echo $this->input->get('id');?>';
          var nopak      = $('[name="AngkaKredit_NoPAK"]').val();
          var tglpak     = $('[name="AngkaKredit_TglPAK"]').val();
          var mulai      = $('[name="AngkaKredit_TglMulai"]').val();
          var akhir      = $('[name="AngkaKredit_TglAkhir"]').val();
          var nilai      = $('[name="AngkaKredit_Nilai"]').val();
          var file       = $('[name="AngkaKredit_PAK"]').val();
          // ubah teks tombol
          $('#m_data_simpan_angkakredit').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_angkakredit').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/angkakredit_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/angkakredit_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajax({
              type: "POST",
              url : url,
              dataType: "JSON",
                data : {AngkaKredit_ID:id, Auditor_NIP:nip, AngkaKredit_NoPAK:nopak, AngkaKredit_TglPAK:tglpak, AngkaKredit_TglMulai:mulai, AngkaKredit_TglAkhir:akhir, AngkaKredit_Nilai:nilai, AngkaKredit_PAK:file},
              success: function(data)
              {
                $('#modal_form_angkakredit').modal('hide');
                $('#json_angkakredit').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_angkakredit').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_angkakredit').attr('disabled',false);
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function ()
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_angkakredit').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_angkakredit').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_angkakredit').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_angkakredit')[0].reset();
          // tampilkan modal
          $('#modal_form_angkakredit').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_angkakredit', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_angkakredit')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/angkakredit_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                  $.each(data,function( AngkaKredit_Nilai, AngkaKredit_NoPAK, AngkaKredit_TglPAK, AngkaKredit_TglMulai, AngkaKredit_TglAkhir, AngkaKredit_PAK)
                {
                  // tampilkan modal
                  $('#modal_form_angkakredit').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].AngkaKredit_ID);
                  $('[name="AngkaKredit_Nilai"]').val(data[0].AngkaKredit_Nilai);
                  $('[name="AngkaKredit_NoPAK"]').val(data[0].AngkaKredit_NoPAK);
                  $('[name="AngkaKredit_TglPAK"]').datepicker('update',data[0].AngkaKredit_TglPAK);
                  $('[name="AngkaKredit_TglMulai"]').datepicker('update',data[0].AngkaKredit_TglMulai);
                  $('[name="AngkaKredit_TglAkhir"]').datepicker('update',data[0].AngkaKredit_TglAkhir);
                  $('[name="AngkaKredit_PAK"]').val(data[0].AngkaKredit_PAK);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_angkakredit', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/angkakredit_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_angkakredit').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      angkakredit();
    }
  };
}();
//-- END: dataTable remote angkakredit

var MaxlengthCharacters = function () {

    //== Private functions
    var data = function () {
        // tahun 4 karakter
        $('.m_maxlength_tahun').maxlength({
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--info m-badge--rounded m-badge--wide",
            placement: 'bottom-left',
            appendToParent: true
        });

        // teks 50 karakter
        $('.m_maxlength_50').maxlength({
            warningClass: "m-badge m-badge--info m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--warning m-badge--rounded m-badge--wide",
            placement: 'bottom-left',
            appendToParent: true
        });

        // teks 10 karakter
        $('.m_maxlength_10').maxlength({
            warningClass: "m-badge m-badge--info m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--warning m-badge--rounded m-badge--wide",
            placement: 'bottom-left',
            appendToParent: true
        });

        // always show
        $('#m_maxlength_3').maxlength({
            alwaysShow: true,
            threshold: 5,
            warningClass: "m-badge m-badge--primary m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        // custom text
        $('#m_maxlength_4').maxlength({
            threshold: 3,
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            separator: ' of ',
            preText: 'You have ',
            postText: ' chars remaining.',
            validate: true
        });

        // textarea example
        $('#m_maxlength_5').maxlength({
            threshold: 5,
            warningClass: "m-badge m-badge--primary m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        // position examples
        $('#m_maxlength_6_1').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'top-left',
            warningClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        $('#m_maxlength_6_2').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'top-right',
            warningClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        $('#m_maxlength_6_3').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'bottom-left',
            warningClass: "m-badge m-badge--warning m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        $('#m_maxlength_6_4').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'bottom-right',
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        //== Modal Examples

        // minimum setup
        $('#m_maxlength_1_modal').maxlength({
            warningClass: "m-badge m-badge--warning m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            appendToParent: true
        });

        // threshold value
        $('#m_maxlength_2_modal').maxlength({
            threshold: 5,
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            appendToParent: true
        });

        // always show
        // textarea example
        $('#m_maxlength_5_modal').maxlength({
            threshold: 5,
            warningClass: "m-badge m-badge--primary m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide",
            appendToParent: true
        });

        // custom text
        $('#m_maxlength_4_modal').maxlength({
            threshold: 3,
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            appendToParent: true,
            separator: ' of ',
            preText: 'You have ',
            postText: ' chars remaining.',
            validate: true
        });
    }

    return {
        // public functions
        init: function() {
            data();
        }
    };
}();

jQuery(document).ready(function () {
  // DatatableJsonRemoteUnitKerja.init();
  // DatatableJsonRemotePendidikan.init();
  DatatableJsonRemotePangkat.init();
  DatatableJsonRemoteJabatan.init();
  DatatableJsonRemoteDiklatJFA.init();
  DatatableJsonRemoteDiklatSubstantif.init();
  DatatableJsonRemoteDiklatProfesi.init();
  DatatableJsonRemoteAngkaKredit.init();
  MaxlengthCharacters.init();
 });
</script>




<textarea id="printing-css" style="display:none;">html,body,div,span,applet,object,iframe,h1,h2,h3,h5,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font:normal normal .8125em/1.4 Arial,Sans-Serif;background-color:white;color:#333}strong,b{font-weight:bold}cite,em,i{font-style:italic}a{text-decoration:none}a:hover{text-decoration:underline}a img{border-color : #303240; border-color: 100px;}abbr,acronym{border-bottom:1px dotted;cursor:help}sup,sub{vertical-align:baseline;position:relative;top:-.4em;font-size:86%}sub{top:.4em}small{font-size:86%}kbd{font-size:80%;border:1px solid #999;padding:2px 5px;border-bottom-width:2px;border-radius:3px}mark{background-color:#ffce00;color:black}p,blockquote,pre,table,figure,hr,form,ol,ul,dl{margin:1.5em 0}hr{height:1px;border:none;background-color:#666}h1,h2,h3,h5,h5,h6{font-weight:bold;line-height:normal;margin:1.5em 0 0}h1{font-size:200%}h2{font-size:180%}h3{font-size:160%}h5{font-size:140%}h5{font-size:120%}h6{font-size:100%}ol,ul,dl{margin-left:3em}ol{list-style:decimal outside}ul{list-style:disc outside}li{margin:.5em 0}dt{font-weight:bold}dd{margin:0 0 .5em 2em}input,button,select,textarea{font:inherit;font-size:100%;line-height:normal;vertical-align:baseline}textarea{display:block;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}pre,code{font-family:"Courier New",Courier,Monospace;color:inherit}pre{white-space:pre;word-wrap:normal;overflow:auto}blockquote{margin-left:2em;margin-right:2em;border-left:4px solid #ccc;padding-left:1em;font-style:italic}table[border="1"] th,table[border="1"] td,table[border="1"] caption{border:1px solid;padding:.5em 1em;text-align:left;vertical-align:top}th{font-weight:bold}table[border="1"] caption{border:none;font-style:italic}.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
