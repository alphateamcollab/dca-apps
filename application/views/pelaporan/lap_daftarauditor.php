<!-- <Begin : Konten> -->
<script>
function printDiv(elementId) {
    var a = document.getElementById('printing-css').value;
    var b = document.getElementById(elementId).innerHTML;
    window.frames["print_frame"].document.title = document.title;
    window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
    window.frames["print_frame"].window.focus();
    window.frames["print_frame"].window.print();
}
</script>
<div class="m-content">

  <div class="m-portlet__body">
    <!--begin: Search Form -->
    <div class="m-form m-form--label-align-right m--margin-top-10 m--margin-bottom-20">
      <div class="row align-items-center">
        <div class="col-3  order-2 order-xl-1">
          <h5 class="m--font-boldest">
          Unit Kerja
        </h5>
        </div>
        <div class="col-7  order-2 order-xl-1">
          <div class="form-group m-form__group row align-items-center">
            <!-- <div class="m-select2 m-select2--pill m-select2--air"> -->
              <select name="Pengguna_UnitKerja" class="form-control m-input m-input--air m-input--pill" id="KodeUnitKerja">
                <option value="">Silahkan Pilih Unit Kerja</option>
              </select>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-2  order-2 order-xl-1 m--align-right">
          <a class="no-print btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" href="javascript:printDiv('print-area-2');" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Cetak Laporan">
            <i class="la la-print"></i>
          </a>
        </div>
      </div>
    </div>
    <!--end: Search Form -->
  </div>
  <div class="m-portlet m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" id="print-area-2">
    <div class="m-portlet__body  m-portlet__body--no-padding">
        <div class="col-xl-12 order-1 order-xl-2 m--align-right">
        </div>
        <div class="col-12">
          <div class="m-portlet__body">
              <div class="title">
                <center>
                    <h4 class="m--font-info">DAFTAR AUDITOR</h4>
                    <br>
                    <h5 align="left"class="m--font-info">UNIT KERJA : </h5>
                </center>
              </div>
             <br>
        <table border="1" width="100%" style="border-collapse:collapse;" align="center">
          <thead class="table-info">
            <tr align="center">
              <th rowspan="2" class="m--icon-font-size-lg2">NIP/NRP</th>
              <th rowspan="2" class="m--icon-font-size-lg2">Nama</th>
              <th rowspan="2" class="m--icon-font-size-lg2">Jenis Kelamin</th>
              <th rowspan="2" class="m--icon-font-size-lg2">Pendidikan Terakhir</th>
              <th colspan="2" class="m--icon-font-size-lg2">Pangkat & Golongan</th>
              <th colspan="2" class="m--icon-font-size-lg2">Jabatan</th>
              <th rowspan="2" class="m--icon-font-size-lg2">Angka Kredit</th>
            </tr>
            <td align="center">NO.SK</td>
            <td align="center">Pangkat</td>
            <td align="center">NO.SK</td>
            <td align="center">Jabatan</td>
          </thead>
          <tbody>
            <tr align="center">
              <td class="m--icon-font-size-lg2"></td>
              <td class="m--icon-font-size-lg2"></td>
              <td class="m--icon-font-size-lg2"></td>
              <td class="m--icon-font-size-lg2"></td>
              <td class="m--icon-font-size-lg2"></td>
              <td class="m--icon-font-size-lg2"></td>
              <td class="m--icon-font-size-lg2"></td>
              <td class="m--icon-font-size-lg2"></td>
              <td class="m--icon-font-size-lg2"></td>

            </tr>

          </tbody>
          <tfoot class="table-info">
            <tr align="center">
              <!-- <td class="m--icon-font-size-lg2 m--font-boldest">Total</td> -->
            </tr>
          </tfoot>
        </table>
          </div>
        </div>
      </div>
    </div>

</div>
<!-- end::Content -->
<textarea id="printing-css" style="display:none;">html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font:normal normal .8125em/1.4 Arial,Sans-Serif;background-color:white;color:#333}strong,b{font-weight:bold}cite,em,i{font-style:italic}a{text-decoration:none}a:hover{text-decoration:underline}a img{border:none}abbr,acronym{border-bottom:1px dotted;cursor:help}sup,sub{vertical-align:baseline;position:relative;top:-.4em;font-size:86%}sub{top:.4em}small{font-size:86%}kbd{font-size:80%;border:1px solid #999;padding:2px 5px;border-bottom-width:2px;border-radius:3px}mark{background-color:#ffce00;color:black}p,blockquote,pre,table,figure,hr,form,ol,ul,dl{margin:1.5em 0}hr{height:1px;border:none;background-color:#666}h1,h2,h3,h4,h5,h6{font-weight:bold;line-height:normal;margin:1.5em 0 0}h1{font-size:200%}h2{font-size:180%}h3{font-size:160%}h4{font-size:140%}h5{font-size:120%}h6{font-size:100%}ol,ul,dl{margin-left:3em}ol{list-style:decimal outside}ul{list-style:disc outside}li{margin:.5em 0}dt{font-weight:bold}dd{margin:0 0 .5em 2em}input,button,select,textarea{font:inherit;font-size:100%;line-height:normal;vertical-align:baseline}textarea{display:block;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}pre,code{font-family:"Courier New",Courier,Monospace;color:inherit}pre{white-space:pre;word-wrap:normal;overflow:auto}blockquote{margin-left:2em;margin-right:2em;border-left:4px solid #ccc;padding-left:1em;font-style:italic}table[border="1"] th,table[border="1"] td,table[border="1"] caption{border:1px solid;padding:.5em 1em;text-align:left;vertical-align:top}th{font-weight:bold}table[border="1"] caption{border:none;font-style:italic}.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
