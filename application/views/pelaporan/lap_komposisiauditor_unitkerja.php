<script>
function printDiv(elementId) {
    var a = document.getElementById('printing-css').value;
    var b = document.getElementById(elementId).innerHTML;
    window.frames["print_frame"].document.title = document.title;
    window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
    window.frames["print_frame"].window.focus();
    window.frames["print_frame"].window.print();
}
</script>
<!-- <Begin : Konten> -->


<div class="m-content">
  <div class="print-area m-portlet m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" id="print-area-2">
    <div class="m-portlet__body  m-portlet__body--no-padding">
      <div class="col-xl-12 order-1 order-xl-2 m--align-right">
        <a class="no-print btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" href="javascript:printDiv('print-area-2');" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Cetak Laporan">
          <i class="la la-print"></i>
        </a>
        <!-- Tittle Print -->
          <div class="title m--hide">
              <center>
                  <div class="m-stack__item m-stack__item--middle m-stack__item--center m-brand__logo">
                    <a href="<?php echo base_url('appportal')?>" class="m-brand__logo-wrapper">
                        <img alt="" src="<?php echo base_url('assets/vendors/media/img/logo/logooo.png')?>"/>
                    </a>
                  </div>
                  <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                    <span class="m-footer__copyright">
                          <a href="http://pusbinjfa.bpkp.go.id/" class="m-link">
                            Pusbin JFA BPKP
                          </a>
                        </span>
                  </div>
                  <h4 class="m--font-info">KOMPOSISI AUDITOR</h4>
                  <h4 class="m--font-info">Nama Unit Kerja</h4>
              </center>
            </div>
        <!-- Tittle Print -->

        <div class="m-separator m-separator--dashed d-xl-none"></div>
      </div>
      <div class="row m-row--no-padding m-row--col-separator-xl">
        <!-- <div class="row"> -->
        <div class="col-md-4"></div>
        <div class="m-separator m-separator--dashed d-xl-none"></div>

        <div class="col-md-4">
          <div class="m-portlet__body">
            <div class="m-widget4 m--font-info" >
              <div id="a_komposisiauditor" style="height: 350px;"></div>
            </div>
          </div>
        </div>
        <div class="m-separator m-separator--dashed d-xl-none"></div>

        <div class="col-md-4"></div>
    <!-- </div> -->
        <div class="col-12">
          <div class="m-portlet__body">
            <table border="1" width="100%" style="border-collapse:collapse;" align="center">
                <thead class="table-info">
                  <tr align="center">
                    <th class="m--icon-font-size-lg2 m--font-boldest">Kelompok Jabatan</th>
                    <th class="m--icon-font-size-lg2 m--font-boldest">Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  <tr align="center">
                    <td class="m--icon-font-size-lg2">Auditor Ahli</td>
                    <td class="m--icon-font-size-lg2 "><?php echo $auditor_ahli;?></td>
                  </tr>
                  <tr align="center">
                    <td class="m--icon-font-size-lg2">Auditor Terampil</td>
                    <td class="m--icon-font-size-lg2 "><?php echo $auditor_terampil;?></td>
                  </tr>
                </tbody>
                <tfoot class="table-info">
                  <tr align="center">
                    <td class="m--icon-font-size-lg2 m--font-boldest">Total</td>
                    <td class="m--icon-font-size-lg2 m--font-boldest "><?php echo $jumlah_auditor;?></td>
                  </tr>
                </tfoot>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end::Content -->
<script>

var amChartsChartsAuditorKomposisi = function() {
    var komposisiauditor = function() {
      var chart = AmCharts.makeChart("a_komposisiauditor", {
          "type": "serial",
          "theme": "light",
          "dataProvider": [{
          "country": "Auditor Ahli",
          "utama": <?php echo $auditor_ahli;?>,
        }, {
          "country": "Auditor Terampil",
          "pertama": <?php echo $auditor_terampil;?>,
        }],
        "valueAxes": [{
          "stackType": "regular",
          "title": "Komposisi Auditor",
          "axisAlpha": 0,
          "gridAlpha": 0
           }],
        "startDuration": 1,
        "graphs": [{
          "balloonText": " [[category]] : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Utama",
          "type": "column",
          "valueField": "utama"
        }, {
          "balloonText": " [[category]] : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Pertama",
          "type": "column",
          "valueField": "pertama"
        }],
          "plotAreaFillAlphas": 0.1,
          "depth3D": 10,
          "angle": 35,
          "categoryField": "country",
          "categoryAxis": {
          "gridPosition": "start"
        },
        //   "legend": {
        //   "horizontalGap": 10,
        //   "useGraphSettings": true,
        //   "markerSize": 10
        // },

        "export": {
          "enabled": false
        },
      });
    }

    return {
      // public functions
      init: function() {
        komposisiauditor();
      }
    };
  }();
  //-- BEGIN: dataTable remote
jQuery(document).ready(function() {
amChartsChartsAuditorKomposisi.init();
});
</script>


<textarea id="printing-css" style="display:none;">html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font:normal normal .8125em/1.4 Arial,Sans-Serif;background-color:white;color:#333}strong,b{font-weight:bold}cite,em,i{font-style:italic}a{text-decoration:none}a:hover{text-decoration:underline}a img{border:none}abbr,acronym{border-bottom:1px dotted;cursor:help}sup,sub{vertical-align:baseline;position:relative;top:-.4em;font-size:86%}sub{top:.4em}small{font-size:86%}kbd{font-size:80%;border:1px solid #999;padding:2px 5px;border-bottom-width:2px;border-radius:3px}mark{background-color:#ffce00;color:black}p,blockquote,pre,table,figure,hr,form,ol,ul,dl{margin:1.5em 0}hr{height:1px;border:none;background-color:#666}h1,h2,h3,h4,h5,h6{font-weight:bold;line-height:normal;margin:1.5em 0 0}h1{font-size:200%}h2{font-size:180%}h3{font-size:160%}h4{font-size:140%}h5{font-size:120%}h6{font-size:100%}ol,ul,dl{margin-left:3em}ol{list-style:decimal outside}ul{list-style:disc outside}li{margin:.5em 0}dt{font-weight:bold}dd{margin:0 0 .5em 2em}input,button,select,textarea{font:inherit;font-size:100%;line-height:normal;vertical-align:baseline}textarea{display:block;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}pre,code{font-family:"Courier New",Courier,Monospace;color:inherit}pre{white-space:pre;word-wrap:normal;overflow:auto}blockquote{margin-left:2em;margin-right:2em;border-left:4px solid #ccc;padding-left:1em;font-style:italic}table[border="1"] th,table[border="1"] td,table[border="1"] caption{border:1px solid;padding:.5em 1em;text-align:left;vertical-align:top}th{font-weight:bold}table[border="1"] caption{border:none;font-style:italic}.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
