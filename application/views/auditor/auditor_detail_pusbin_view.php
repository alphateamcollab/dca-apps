<!-- begin::Content -->
<div class="m-content">
  <div class="row">
    <div class="col-xl-4 col-lg-4">
      <div class="m-portlet m-portlet--tabs m-portlet--info m-portlet--head-solid-bg m-portlet--bordered m-portlet--head-sm">
        <div class="m-portlet__head">
          <div class="m-portlet__head-tools">
            <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line-danger" role="tablist">
              <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#profil" role="tab">
                  <i class="flaticon-profile"></i>
                  Ringkasan
                </a>
              </li>
              <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link" data-toggle="tab" href="#unitkerja" role="tab">
                  <i class="flaticon-suitcase"></i>
                  Unit Kerja
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="m-portlet__body">
          <div class="tab-content">
            <div class="tab-pane active" id="profil">
              <div class="m--align-right">
                <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Ubah Status Auditor" id="m_data_ubah_status">
                  <i class="flaticon-user"></i>
                </button>
                <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Atur Ulang Kata Sandi" id="m_data_aturulang_katasandi">
                  <i class="flaticon-lock"></i>
                </button>
                <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Ubah Data Non-Kedinasan" id="m_data_ubah_profil">
                  <i class="flaticon-edit-1"></i>
                </button>
              </div>
              <div class="m-card-profile">
                <div class="m-card-profile__title m--hide">
                  Profile Saya
                </div>
                <div class="m-card-profile__pic">
                  <div class="m-card-profile__pic-wrapper">
                    <img src="<?php echo base_url('assets/app/media/img/users/default.jpg')?>" alt="">
                  </div>
                </div>
                <div class="m-card-profile__details">
                  <span class="m-card-profile__name" > <h4> <span class="Pendidikan_GelarDepan"></span> <span class="Auditor_NamaLengkapGelar"></span><span class="Pendidikan_GelarBelakang"></span> </h4> </span>
                  <a href="#" class="m-card-profile__email m-link">NIP. <span class="Auditor_NIP"></span></a>
                </div>
              </div>
              <div class="m-widget1 m-widget1--paddingless">
                  <div class="m-widget1__item">
                    <div class="row m-row--no-padding align-items-center">
                      <div class="col">
                        <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-user"></i> Status Auditor</h3>
                        <span class="m-widget1__desc isAuditor"></span>
                      </div>
                    </div>
                  </div>
                  <div class="m-widget1__item">
                    <div class="row m-row--no-padding align-items-center">
                      <div class="col">
                        <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-map"></i> Pangkat & Golongan</h3>
                        <span class="m-widget1__desc"><span class="Pangkat_Nama"></span>
                        <!-- , Golongan Ruang <span class="Golongan_Kode"></span></span> -->
                      </div>
                    </div>
                  </div>
                  <div class="m-widget1__item">
                    <div class="row m-row--no-padding align-items-center">
                      <div class="col">
                        <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-suitcase"></i> Jabatan</h3>
                        <span class="m-widget1__desc JenjangJabatan_Nama"></span>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="tab-pane" id="unitkerja" role="tabpanel">
              <div class="m-portlet__head-caption">
                <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                    <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Ubah Data" id="m_data_ubah_unit">
                      <i class="flaticon-edit-1"></i>
                    </button>
                </ul>
              </div>
               <div class="m-form m-form--fit m-form--label-align-right">
                <div class="m-portlet__body">
                  <div class="m-widget13">
                    <div class="m-widget13__item">
                      <span class="m-widget13__desc m--align-right">
                        Jenis Lembaga :
                      </span>
                      <span class="m-widget13__text m-widget13__text-bolder JenisInstansi_Nama">
                      </span>
                    </div>
                    <div class="m-widget13__item">
                      <span class="m-widget13__desc m--align-right">
                        Nama Instansi :
                      </span>
                      <span class="m-widget13__text m-widget13__text-bolder Instansi_Nama">
                      </span>
                    </div>
                    <div class="m-widget13__item">
                      <span class="m-widget13__desc m--align-right">
                        Nama Unit Kerja :
                      </span>
                      <span class="m-widget13__text m-widget13__text-bolder UnitKerja_Nama">
                      </span>
                    </div>
                    <div class="m-widget13__item">
                      <span class="m-widget13__desc m--align-right">
                        Alamat Kantor :
                      </span>
                      <span class="m-widget13__text UnitKerja_Alamat">
                      </span>
                    </div>
                    <div class="m-widget13__item">
                      <span class="m-widget13__desc m--align-right">
                        Surat Elektronik :
                      </span>
                      <span class="m-widget13__text UnitKerja_Surel">
                      </span>
                    </div>
                    <div class="m-widget13__item">
                      <span class="m-widget13__desc m--align-right">
                        Nomor Telepon Kantor :
                      </span>
                      <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoTlp">
                      </span>
                    </div>
                    <div class="m-widget13__item">
                      <span class="m-widget13__desc m--align-right">
                        Nomor Faximili Kantor :
                      </span>
                      <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoFax">
                      </span>
                    </div>
                  </div>
                </div>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-8 col-lg-8">
      <div class="m-portlet m-portlet--tabs m-portlet--info m-portlet--head-solid-bg m-portlet--bordered m-portlet--head-sm">
          <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
              <ul class="nav nav-tabs m-tabs-line" role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active" data-toggle="tab" href="#pendidikan" role="tab">
                    <i class="flaticon-list-1"></i>
                    Pendidikan
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#pangkat" role="tab">
                    <i class="flaticon-map"></i>
                    Pangkat & Gol.
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#jabatan" role="tab">
                    <i class="flaticon-suitcase"></i>
                    Jabatan
                  </a>
                </li>
                <li class="nav-item dropdown m-tabs__item">
                  <a class="nav-link m-tabs__link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="flaticon-open-box"></i>
                    Diklat
                  </a>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" data-toggle="tab" href="#diklat_jfa"> Sertifikasi JFA </a>
                    <a class="dropdown-item" data-toggle="tab" href="#diklat_substantif"> Teknis Substantif </a>
                    <a class="dropdown-item" data-toggle="tab" href="#diklat_profesi">Sertifikasi Profesi </a>
                  </div>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#angka_kredit" role="tab">
                    <i class="flaticon-puzzle"></i>
                    Angka Kredit
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="m-portlet__body">
            <div class="tab-content">
              <div class="tab-pane active" id="pendidikan" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_pendidikan">
                          <i class="la la-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Tambah Data" id="m_data_tambah_pendidikan">
                          <i class="la la-plus"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Pendidikan
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_pendidikan"></div>
                  <!--end: Datatable -->
              </div>
              <div class="tab-pane" id="pangkat" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_pangkat">
                          <i class="la la-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Tambah Data" id="m_data_tambah_pangkat">
                          <i class="la la-plus"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Pangkat & Golongan
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_pangkat"></div>
                  <!--end: Datatable -->
              </div>
              <div class="tab-pane" id="jabatan" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_jabatan">
                          <i class="la la-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Tambah Data" id="m_data_tambah_jabatan">
                          <i class="la la-plus"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Jabatan
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_jabatan"></div>
                  <!--end: Datatable -->
              </div>
              <div class="tab-pane" id="diklat_jfa" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_sertifikasijfa">
                          <i class="la la-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Tambah Data" id="m_data_tambah_sertifikasijfa">
                          <i class="la la-plus"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Diklat Sertifikasi JFA
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_sertifikasijfa"></div>
                  <!--end: Datatable -->
              </div>
              <div class="tab-pane" id="diklat_substantif" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_diklatteknissubstantif">
                          <i class="la la-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Tambah Data" id="m_data_tambah_diklatteknissubstantif">
                          <i class="la la-plus"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Diklat Teknis Substantif
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_diklatteknissubstantif"></div>
                  <!--end: Datatable -->
              </div>
              <div class="tab-pane" id="diklat_profesi" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_sertifikasiprofesi">
                          <i class="la la-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Tambah Data" id="m_data_tambah_sertifikasiprofesi">
                          <i class="la la-plus"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Diklat Sertifikasi Profesi
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_sertifikasiprofesi"></div>
                  <!--end: Datatable -->
              </div>
              <div class="tab-pane" id="angka_kredit" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_angkakredit">
                          <i class="la la-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Tambah Data" id="m_data_tambah_angkakredit">
                          <i class="la la-plus"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Angka Kredit
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_angkakredit"></div>
                  <!--end: Datatable -->
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<!-- begin: Modal Profil -->
<div class="modal fade" id="modal_form_profil" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
          Tambah Data
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_profil">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="420">
            <div class="m-portlet__body">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <span class="m-widget1__desc">
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          NIP/NRP
                        </label>
                        <div class="col-7">
                          <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="NIP/NRP Auditor" name="Auditor_NIP">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Nama Lengkap
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input" placeholder="Nama Lengkap" name="Auditor_NamaLengkap">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Surat Elektronik
                        </label>
                        <div class="col-7">
                          <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Surat Elektronik (email)" name="Pengguna_Surel">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Nomor Kontak Telp. / HP
                        </label>
                        <div class="col-7">
                          <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Kontak Telp. / HP" name="Pengguna_NoHP">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Alamat Tempat Tinggal
                        </label>
                        <div class="col-7">
                          <textarea class="form-control m-input m-input--air m-input--pill" placeholder="Alamat tinggal saat ini" name="Pengguna_Alamat" rows="4"></textarea>
                        </div>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_profil">Simpan</button>
       </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal Profil -->

<!-- begin: Modal Status Auditor -->
<div class="modal fade" id="modal_form_status" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
          Tambah Data
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_status">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="420">
            <div class="m-portlet__body">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h5 class="m-widget1__title"><i class="m-nav__link-icon flaticon-user"></i>Data Umum</h5>
                    <span class="m-widget1__desc">
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Peran Admin
                        </label>
                        <div class="col-7 m-form__group-sub">
                          <select type="text" class="form-control m-input m-input--air m-input--pill" name="Pengguna_RoleID">
                            <option value="false">
                              Non Aktif Sebagai Admin
                            </option>
                            <option value="true">
                              Aktif Sebagai Admin
                            </option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Status Auditor
                        </label>
                        <div class="col-7 m-form__group-sub">
                          <select type="text" class="form-control m-input m-input--air m-input--pill" name="Status_Auditor">
                            <option value="3">
                              Pengangkatan Pertama
                            </option>
                            <option value="4">
                              Kenaikan Jabatan
                            </option>
                            <option value="5">
                              Inpassing
                            </option>
                            <option value="2">
                              Pembebasan Sementara
                            </option>
                            <option value="6">
                              Pengangkatan Kembali
                            </option>
                            <option value="0">
                              Pembebasan Tetap (Berhenti/Pensiun)
                            </option>
                          </select>
                        </div>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan">Simpan</button>
       </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal  Status Auditor-->

<!-- begin: Modal unitkerja-->
<div class="modal fade" id="modal_form_unit" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Ubah Data Unit Kerja
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_unitkerja" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m--hide">
                <label for="example-text-input" class="col-5 col-form-label">
                  NIP / NRP
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" readonly placeholder="000000000000000000" name="Auditor_NIP" value="<?php echo $this->input->get('id')?>">
                </div>
              </div>
              <h4>Unit Kerja Sebelumnya</h4>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nama Jenis Instansi
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Unit Kerja" name="JenisInstansi_Nama" disabled>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nama Instansi
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Instansi" name="Instansi_Nama" disabled>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nama Unit Kerja
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Unit Kerja" name="UnitKerja_Nama" disabled>
                </div>
              </div>
              <div class="m-form__heading">
                <h3 class="m-form__heading-title">
                  Pindah Ke :
                  <small class="m--font-danger">
                    <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon la la-asterisk" title="Wajib Diisi"></i>Wajib Diisi
                  </small>
                </h3>
              </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-12">
                    <!-- <div class="m-select2 m-select2--pill"> -->
                      <select name="JenisInstansi" class="form-control m-input m-input--air m-input--pill" id="JenisInstansi" onchange="getInstansi(this.value)" required >
                          <option value="NULL">Pilih Jenis Instansi</option>
                          <?php foreach ($dd_jenisinstansi as $row): ?>
                            <option value="<?php echo $row->JenisInstansi_Kode; ?>"><?php echo $row->JenisInstansi_Nama; ?></option>
                          <?php endforeach; ?>
                      </select>
                    <!-- </div> -->
                </div>
              </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-12">
                  <div class="m-select2 m-select2--pill m-select2--air">
                    <select name="Pengguna_Instansi" class="form-control  m-select2" id="KodeInstansi" onchange="getUnitKerja(this.value)" required>
                      <option value="">Silahkan Pilih</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-12">
                  <div class="m-select2 m-select2--pill m-select2--air">
                    <select name="UnitKerja_Kode" class="form-control m-select2" id="KodeUnitKerja" class="form-control" >
                      <option value="">Silahkan Pilih</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_unit">Simpan</button>
        </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal unitkerja-->

<!-- begin: Modal Pendidikan-->
<div class="modal fade" id="modal_form_pendidikan" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Tambah Data Pendidikan
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_pendidikan" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
                <div class="form-group m-form__group row m--hide">
                  <label for="example-text-input" class="col-5 col-form-label">
                    ID Pendidikan
                  </label>
                  <div class="col-7">
                    <input type="text" class="form-control m-input m-input--air m-input--pill" name="Pendidikan_ID">
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    NIP / NRP
                  </label>
                  <div class="col-7">
                    <input type="text" class="form-control m-input m-input--air m-input--pill" readonly placeholder="000000000000000000" name="Auditor_NIP" value="<?php echo $this->input->get('id')?>">
                  </div>
                </div>
                <div class="form-group m-form__group row m--hide">
                  <label for="example-text-input" class="col-5 col-form-label">
                    No. PAK
                  </label>
                  <div class="col-7">
                    <input class="form-control m-input m-input--air m-input--pill m_maxlength_50" type='text' maxlength="50" type="text" placeholder="Nama Lembaga Pendidikan" name="No_PAK" >
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Tingkat Pendidikan Akhir
                  </label>
                  <div class="col-7">
                      <select class="m-input--pill form-control" id="Pendidikan_Kode" name="Pendidikan_Kode" required>
                        <option value="">Strata Pendidikan</option>
                        <?php foreach ($dd_jenjangpendidikan as $row): ?>
                          <option value="<?php echo $row->Pendidikan_Kode;?>"><?php echo $row->Pendidikan_Jenjang; ?></option>
                        <?php endforeach; ?>
                      <select/>
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Lembaga Pendidikan
                  </label>
                  <div class="col-7">
                    <input class="form-control m-input m-input--air m-input--pill m_maxlength_50" type='text' maxlength="50" type="text" placeholder="Nama Lembaga Pendidikan" name="Pendidikan_Lembaga" >
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Fakultas
                  </label>
                  <div class="col-7">
                    <input class="form-control m-input m-input--air m-input--pill m_maxlength_50" type='text' maxlength="50" type="text" placeholder="Fakultas" name="Pendidikan_Fakultas" >
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Jurusan
                  </label>
                  <div class="col-7">
                    <input class="form-control m-input m-input--air m-input--pill m_maxlength_50" type='text' maxlength="50" type="text" placeholder="Jurusan" name="Pendidikan_Jurusan" >
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Gelar Yang diterima
                  </label>
                  <div class="col-3">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_maxlength_10" type='text' maxlength="10" type="text" placeholder="Gelar Depan" name="Pendidikan_GelarDepan">
                    <span class="m-form__help">
                      Contoh: Prof.
                    </span>
                  </div>
                  <label class="col-1 col-form-label">
                      Atau
                  </label>
                  <div class="col-3">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_maxlength_10" type='text' maxlength="10" type="text" placeholder="Gelar Belakang" name="Pendidikan_GelarBelakang">
                    <span class="m-form__help">
                      Contoh: S.Pd.
                    </span>
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Nomor Seri Ijazah
                  </label>
                  <div class="col-7">
                    <input class="form-control m-input m-input--air m-input--pill m_maxlength_tahun" type='text' maxlength="30" type="text" placeholder="Contoh Nomor Seri: 1602/H17.5/S1/2010" name="Pendidikan_NoIjazah">
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Tanggal Terbit Ijazah
                  </label>
                  <div class="col-7">
                    <div class="input-group date">
                      <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly""  placeholder="30/05/1983" name="Pendidikan_TglIjazah">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="la la-calendar"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group m-form__group row">
                   <label for="example-text-input" class="col-5 col-form-label">
                      Unggah File Ijazah
                   </label>
                   <div class="col-7">
                        <!-- <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" name="Pendidikan_Ijazah" id="m-dropzone-one">
                          <div class="m-dropzone__msg dz-message needsclick">
                                  <h3 class="m-dropzone__msg-title">
                                    Klik / Seret File kesini untuk mengunggah
                                  </h3>
                                  <span class="m-dropzone__msg-desc">
                                   tipe file dengan ekstensi : PDF, JPG
                                  </span>
                          </div>
                        </div>
                        <span class="m-form__help ">
                          tipe file dengan ekstensi : PDF, JPG
                        </span> -->
                        <input class="custom-file-input m-input m-input--air m-input--pill m--hide" name="Pendidikan_Ijazah_Lama" type="text">
                        <div class="custom-file">
                          <input class="custom-file-input m-input m-input--air m-input--pill" id="Pendidikan_Ijazah" name="Pendidikan_Ijazah" type="file">
                          <label class="custom-file-label Pendidikan_Ijazah" for="customFile">
                            Pilih File
                          </label>
                        </div>
                        <span class="m-form__help">
                          tipe file dengan ekstensi : PDF, JPG
                        </span>
                   </div>
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_pendidikan">Simpan</button>
        </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal Pendidikan-->

<!-- begin: Modal Pangkat-->
<div class="modal fade" id="modal_form_pangkat" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Tambah Data Riwayat Pangkat & Golongan
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_pangkat" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m--hide">
                <label for="example-text-input" class="col-5 col-form-label">
                  ID Pangkat
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" name="PangkatGol_ID">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  NIP / NRP
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" readonly placeholder="000000000000000000" name="Auditor_NIP" value="<?php echo $this->input->get('id');?>">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Golongan Ruang
                </label>
                <div class="col-7">
                  <div class="col-xlg-7 m-form__group-sub">
                    <select class="form-control m-input m-input--air m-input--pill" name="Pangkat_Kode" id="golongan" onchange="getPangkat(this.value)">
                      <option value="">Silahkan Pilih</option>
                      <?php foreach ($dd_golongan as $row): ?>
                        <option value="<?php echo $row->Pangkat_Kode; ?>"><?php echo $row->GolRuang_Kode; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Pangkat
                </label>
                <div class="col-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Pangkat" name="Pangkat_Nama" id="pangkatgol" readonly>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nomor SK
                </label>
                <div class="col-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Nomor SK" name="Pangkat_NoSuratKeputusan">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Tanggal SK
                </label>
                <div class="col-7">
                  <div class="input-group date">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly"" placeholder="30/05/1983" name="Pangkat_TglSuratKeputusan">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="la la-calendar"></i>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  TMT Pangkat/Golongan
                </label>
                <div class="col-7">
                  <div class="input-group date">
                      <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly"" placeholder="30/05/1983" name="Pangkat_TglMulaiTugas">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="la la-calendar"></i>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Unggah File SK Pangkat
                </label>
                <div class="col-7">
                  <!-- <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" name="Pangkat_SK" id="m-dropzone-one">
                    <div class="m-dropzone__msg dz-message needsclick">
                      <h3 class="m-dropzone__msg-title">
                        klik atau Seret File kesini untuk mengunggah
                      </h3>
                      <span class="m-dropzone__msg-desc">
                        tipe file dengan ekstensi : PDF, JPG
                      </span>
                    </div>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span> -->
                  <input class="custom-file-input m-input m-input--air m-input--pill m--hide" name="Pangkat_SK_Lama" type="text">
                  <div class="custom-file">
                    <input class="custom-file-input m-input m-input--air m-input--pill" id="Pangkat_SK" name="Pangkat_SK" type="file">
                    <label class="custom-file-label Pangkat_SK" for="customFile">
                      Pilih File
                    </label>
                  </div>
                  <span class="m-form__help">
                    tipe file dengan ekstensi : PDF, JPG
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_pangkat">Simpan</button>
        </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal Pangkat-->

<!-- begin: Modal Jabatan-->
<div class="modal fade" id="modal_form_jabatan" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Tambah Data
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m--hide">
                <label for="example-text-input" class="col-5 col-form-label">
                  ID Jabatan
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" name="Jabatan_ID">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  NIP / NRP
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" readonly placeholder="000000000000000000" name="Auditor_NIP" value="<?php echo $this->input->get('id');?>">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Kelompok Jabatan
                </label>
                <div class="col-7">
                  <div class="col-xlg-7 m-form__group-sub">
                    <select class="form-control m-input m-input--air m-input--pill" name="Jabatan_Kode" id="jabatan" onchange="getJenjangJabatan(this.value)">
                      <option value="">Pilih Kelompok Jabatan</option>
                      <?php foreach ($dd_jabatan as $row): ?>
                        <option value="<?php echo $row->Jabatan_Kode;?>">
                          <?php echo $row->Jabatan_Nama; ?>
                        </option>
                      <?php endforeach; ?>
                    <select/>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Jenjang Jabatan
                </label>
                <div class="col-7">
                  <div class="col-xlg-7 m-form__group-sub">
                    <select class="form-control m-input m-input--air m-input--pill" name="JenjangJabatan_Kode" id="jenjangjabatan" class="form-control" required>
                      <option value="">Silahkan Pilih</option>
                      <?php foreach ($dd_jenjangjabatn as $row): ?>
                        <option value="<?php echo $row->JenjangJabatan_Kode; ?>"
                          <?php if ($row->JenjangJabatan_Kode == $JenjangJabatan_Kode): ?>
                            selected="selected"
                          <?php endif; ?>>
                          <?php echo $row->JenjangJabatan_Nama; ?>
                        </option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nomor SK
                </label>
                <div class="col-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Nomor SK" name="Jabatan_NoSuratKeputusan">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Tanggal SK
                </label>
                <div class="col-7">
                  <div class="input-group date">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly"" placeholder="30/05/1983" name="Jabatan_TglSuratKeputusan">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="la la-calendar"></i>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  TMT Jabatan
                </label>
                <div class="col-7">
                  <div class="input-group date">
                      <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly"" placeholder="30/05/1983" name="Jabatan_TglMulaiTugas">
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="la la-calendar"></i>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Unggah File SK Jabatan
                </label>
                <div class="col-7">
                  <!-- <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" name="Jabatan_SK" id="m-dropzone-one">
                    <div class="m-dropzone__msg dz-message needsclick">
                      <h3 class="m-dropzone__msg-title">
                        klik atau Seret File kesini untuk mengunggah
                      </h3>
                      <span class="m-dropzone__msg-desc">
                        tipe file dengan ekstensi : PDF, JPG
                      </span>
                    </div>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span> -->
                  <input class="custom-file-input m-input m-input--air m-input--pill m--hide" name="Jabatan_SK_Lama" type="text">
                  <div class="custom-file">
                    <input class="custom-file-input m-input m-input--air m-input--pill" id="Jabatan_SK" name="Jabatan_SK" type="file">
                    <label class="custom-file-label Jabatan_SK" for="customFile">
                      Pilih File
                    </label>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_jabatan">Simpan</button>
        </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal Jabatan-->

<!-- begin: Modal Sertifikasi JFA-->
<div class="modal fade" id="modal_form_sertifikasijfa" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Tambah Data
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_sertifikasijfa" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m--hide">
                <label for="example-text-input" class="col-5 col-form-label">
                  ID Sertifikasi JFA
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" name="SertifikasiJFA_ID">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  NIP / NRP
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" readonly placeholder="000000000000000000" name="Auditor_NIP" value="<?php echo $this->input->get('id');?>">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nama Diklat
                </label>
                <div class="col-md-7">
                  <select class="form-control m-input m-input--air m-input--pill" name="Diklat_Kode_JFA">
                    <option value="">Silahkan Pilih</option>
                    <?php foreach ($dd_diklatjfa as $row): ?>
                      <option value="<?php echo $row->Diklat_Kode;?>"><?php echo $row->Diklat_Nama; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-md-5 col-form-label">
                    Periode Diklat
                  </label>
                  <div class="col-md-3">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly""  placeholder="Tanggal Mulai " name="SertifikasiJFA_TglMulaiDiklat">
                  </div>
                    <label class="col-form-label col-md-1">
                        s/d
                    </label>
                  <div class="col-md-3">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly""  placeholder="Tanggal Akhir " name="SertifikasiJFA_TglAkhirDiklat">
                  </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nomor STMPL
                </label>
                <div class="col-md-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nomor STMPL" name="SertifikasiJFA_NoSTMPL" >
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Tanggal STMPL
                </label>
                <div class="col-md-7">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly"" placeholder="30/05/1983" name="SertifikasiJFA_TglSTMPL">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nomor STTPL
                </label>
                <div class="col-md-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Nomor STTPL" name="SertifikasiJFA_NoSTTPL">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Tanggal STTPL
                </label>
                <div class="col-md-7">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly"" placeholder="30/05/1983" name="SertifikasiJFA_TglSTTPL">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Unggah Lampiran Sertifikat
                </label>
                <div class="col-md-7">
                  <!-- <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" name="SertifikasiJFA_Sertifikat" id="m-dropzone-one">
                    <div class="m-dropzone__msg dz-message needsclick">
                      <h3 class="m-dropzone__msg-title">
                        Klik atau seret file kesini untuk mengunggah
                      </h3>
                      <span class="m-dropzone__msg-desc">
                        tipe file dengan ekstensi : PDF, JPG
                      </span>
                    </div>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span> -->
                  <input class="custom-file-input m-input m-input--air m-input--pill m--hide" type="text" name="SertifikasiJFA_Sertifikat_Lama">
                  <div class="custom-file">
                    <input class="custom-file-input m-input m-input--air m-input--pill" id="SertifikasiJFA_Sertifikat" type="file" name="SertifikasiJFA_Sertifikat">
                    <label class="custom-file-label SertifikasiJFA_Sertifikat" for="customFile">
                      Pilih File
                    </label>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_sertifikasijfa">Simpan</button>
        </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal Sertifikasi JFA-->

<!-- begin: Modal Teknis Substantif-->
<div class="modal fade" id="modal_form_teknissubstantif" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Tambah Data Diklat Teknis Substantif
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_teknissubstantif" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m--hide">
                <label for="example-text-input" class="col-5 col-form-label">
                  ID Teknis Substantif
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" name="TeknisSubstantif_ID">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  NIP / NRP
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" readonly placeholder="000000000000000000" name="Auditor_NIP" value="<?php echo $this->input->get('id');?>">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nama Diklat
                </label>
                <div class="col-md-7">
                  <select class="form-control m-input m-input--air m-input--pill" name="Diklat_Kode_TekSub">
                    <option value="">Silahkan Pilih</option>
                    <?php foreach ($dd_diklatteknissubstantif as $row): ?>
                      <option value="<?php echo $row->Diklat_Kode;?>"><?php echo $row->Diklat_Nama; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Periode Diklat
                </label>
                <div class="col-md-3">
                  <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly""  placeholder="Tanggal Mulai " name="TeknisSubstantif_TglMulai">
                </div>
                  <label class="col-form-label col-md-1">
                      Hingga
                  </label>
                <div class="col-md-3">
                  <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly""  placeholder="Tanggal Akhir " name="TeknisSubstantif_TglAkhir">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nomor STMPL
                </label>
                <div class="col-md-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Nomor STMPL" name="TeknisSubstantif_NoSTMPL">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Tanggal STMPL
                </label>
                <div class="col-md-7">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly"" placeholder="30/05/1983" name="TeknisSubstantif_TglSTMPL">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                 Unggah File Sertifikat
                </label>
                <div class="col-md-7">
                  <!-- <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" name="Diklat_Sertifikat" id="m-dropzone-one">
                    <div class="m-dropzone__msg dz-message needsclick">
                      <h3 class="m-dropzone__msg-title">
                        Klik atau seret file kesini untuk mengunggah
                      </h3>
                      <span class="m-dropzone__msg-desc">
                        tipe file dengan ekstensi : PDF, JPG
                      </span>
                    </div>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span> -->
                  <input class="custom-file-input m-input m-input--air m-input--pill m--hide" name="TeknisSubstantif_Sertifikat_Lama" type="text">
                  <div class="custom-file">
                    <input class="custom-file-input m-input m-input--air m-input--pill" id="TeknisSubstantif_Sertifikat" name="TeknisSubstantif_Sertifikat" type="file">
                    <label class="custom-file-label TeknisSubstantif_Sertifikat" for="customFile">
                      Pilih File
                    </label>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_teknissubstantif">Simpan</button>
        </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal Teknis Substantif-->

<!-- begin: Modal Sertifikasi Profesi-->
<div class="modal fade" id="modal_form_sertifikasiprofesi" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Tambah Data Diklat Teknis Substantif
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_sertifikasiprofesi" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m--hide">
                <label for="example-text-input" class="col-5 col-form-label">
                  ID Sertifikasi Profesi
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" name="SertifikasiProfesi_ID">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  NIP / NRP
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" readonly placeholder="000000000000000000" name="Auditor_NIP" value="<?php echo $this->input->get('id');?>">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nama Lembaga Pelaksana Diklat
                </label>
                <div class="col-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Nama Lembaga Pelaksana" name="SertifikasiProfesi_Lembaga">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nama Diklat
                </label>
                <div class="col-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Nama Diklat" name="SertifikasiProfesi_Nama">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Gelar Profesi
                </label>
                <div class="col-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Gelar Profesi" name="SertifikasiProfesi_Gelar">
                </div>
              </div>
              <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-md-5 col-form-label">
                    Periode Diklat
                  </label>
                  <div class="col-md-3">
                        <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly""  placeholder="Tanggal Mulai " name="SertifikasiProfesi_TglMulai">
                  </div>
                      <label class="col-form-label col-md-1">
                          S/D
                      </label>
                  <div class="col-md-3">
                        <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly""  placeholder="Tanggal Akhir " name="SertifikasiProfesi_TglAkhir">
                  </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nomor STMPL
                </label>
                <div class="col-md-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Nomor STMPL" name="SertifikasiProfesi_NoSTMPL" >
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Tanggal STMPL
                </label>
                <div class="col-md-7">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly"" placeholder="30/05/1983" name="SertifikasiProfesi_TglSTMPL">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Unggah File Sertifikat
                </label>
                <div class="col-md-7">
                  <!-- <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" name="SertifikasiProfesi_Sertifikat" id="m-dropzone-one">
                    <div class="m-dropzone__msg dz-message needsclick">
                      <h3 class="m-dropzone__msg-title">
                        Klik atau seret file kesini untuk mengunggah
                      </h3>
                      <span class="m-dropzone__msg-desc">
                        tipe file dengan ekstensi : PDF, JPG
                      </span>
                    </div>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span> -->
                  <input class="custom-file-input m-input m-input--air m-input--pill m--hide" name="SertifikasiProfesi_Sertifikat_Lama" type="text">
                  <div class="custom-file">
                    <input class="custom-file-input m-input m-input--air m-input--pill" id="SertifikasiProfesi_Sertifikat" name="SertifikasiProfesi_Sertifikat" type="file">
                    <label class="custom-file-label SertifikasiProfesi_Sertifikat" for="customFile">
                      Pilih File
                    </label>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_sertifikasiprofesi">Simpan</button>
        </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal Sertifikasi Profesi-->

<!-- begin: Modal Angka Kredit-->
<div class="modal fade" id="modal_form_angkakredit" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Tambah Data Nilai Angka Kredit
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_angkakredit" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m--hide">
                <label for="example-text-input" class="col-5 col-form-label">
                  ID Angka Kredit
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" name="AngkaKredit_ID">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  NIP / NRP
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" readonly placeholder="000000000000000000" name="Auditor_NIP" value="<?php echo $this->input->get('id');?>">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Masa Penilaian
                </label>
                <div class="col-md-3">
                  <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly""  placeholder="Tanggal Mulai " name="AngkaKredit_TglMulai">
                </div>
                <label class="col-form-label col-md-1">
                    S/D
                </label>
                <div class="col-md-3">
                  <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly""  placeholder="Tanggal Akhir " name="AngkaKredit_TglAkhir">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nomor PAK
                </label>
                <div class="col-md-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Nomor SK" name="AngkaKredit_NoPAK" >
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                    Tanggal PAK
                </label>
                <div class="col-md-7">
                    <input type="datepicker" class="form-control m-input m-input--air m-input--pill m_datepicker_atas" readonly"" placeholder="30/05/1983" name="AngkaKredit_TglPAK">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nilai Angka Kredit
                </label>
                <div class="col-md-7">
                  <input class="form-control m-input m-input--air m-input--pill" type="number" placeholder="Nilai Angka Kredit" name="AngkaKredit_Nilai" >
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Lampiran PAK
                </label>
                <div class="col-md-7">
                  <!-- <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" name="AngkaKredit_PAK" id="m-dropzone-one">
                    <div class="m-dropzone__msg dz-message needsclick">
                      <h3 class="m-dropzone__msg-title">
                        klik atau Seret File kesini untuk mengunggah
                      </h3>
                      <span class="m-dropzone__msg-desc">
                        tipe file dengan ekstensi : PDF, JPG
                      </span>
                    </div>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG
                  </span> -->
                  <input class="custom-file-input m-input m-input--air m-input--pill m--hide" name="AngkaKredit_PAK_Lama" type="text">
                  <div class="custom-file">
                    <input class="custom-file-input m-input m-input--air m-input--pill" id="AngkaKredit_PAK" name="AngkaKredit_PAK" type="file">
                    <label class="custom-file-label AngkaKredit_PAK" for="customFile">
                      Pilih File
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_angkakredit">Simpan</button>
        </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal Angka Kredit-->

<script>
/* Ajax Dropdown Instansi */
function getInstansi(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_instansi');?>",
    data: {value},
    success: function(data) {
      $("#KodeUnitKerja option:gt(0)").remove();
      $("#KodeInstansi").html(data);
      $("#KodeInstansi").select2();
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
};

/* Ajax Dropdown Unit Kerja */
function getUnitKerja(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_unitkerja');?>",
    data:{value},
    success: function(data) {
      $("#KodeUnitKerja").html(data);
      $("#KodeUnitKerja").select2();
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
}

/* Ajax Dropdown Prestasi Nilai */
function getPangkat(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_pangkat');?>",
    data: {value},
    success: function(data) {
      document.getElementById('pangkatgol').value = data;
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
}

/* Ajax Dropdown Jabatan */
function getJenjangJabatan(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_jenjangjabatan');?>",
    data: {value},
    success: function(data) {
      $("#jenjangjabatan").html(data);
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
};

//-- BEGIN: load data Ringkasan Profil & UnitKerja
$(document).ready(function(){
  getProfil();
});

function getProfil(){
    var id='<?php echo $this->input->get('id');?>';
    // memuat data dengan ajax
    $.ajax({
        type: "GET",
        url : "<?php echo site_url('auditor/ambil')?>",
        dataType: "JSON",
        data : {id:id},
        success: function(data)
        {
            $.each(data,function(NIP, isAuditor, NamaLengkap, NamaLengkapGelar, GelarDepan, GelarBelakang, Pangkat_Nama, Golongan_Kode, JenjangJabatan_Nama, JenisInstansi_Nama, Instansi_Nama, UnitKerja_Nama, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax, UnitKerja_Web)
          {
            // ambil nilai input dari parameter JSON
            $('.Auditor_NIP').text(data[0].Auditor_NIP);
            if (data[0].IsAuditor == 'true') {
              $('.isAuditor').text('Aktif');
            }else {
              $('.isAuditor').text('Non-Aktif');
            }
            $('.Auditor_NamaLengkap').text(data[0].Auditor_NamaLengkap);
            $('.Auditor_NamaLengkapGelar').text(data[0].Auditor_NamaLengkapGelar);
            $('.Pendidikan_GelarDepan').text(data[0].Pendidikan_GelarDepan);
            $('.Pendidikan_GelarBelakang').text(data[0].Pendidikan_GelarBelakang);
            $('.Pangkat_Nama').text(data[0].Pangkat_Nama);
            $('.Golongan_Kode').text(data[0].Golongan_Kode);
            $('.JenjangJabatan_Nama').text(data[0].JenjangJabatan_Nama);
            $('.JenisInstansi_Nama').text(data[0].JenisInstansi_Nama);
            $('.Instansi_Nama').text(data[0].Instansi_Nama);
            $('.UnitKerja_Nama').text(data[0].UnitKerja_Nama);
            $('.UnitKerja_Alamat').text(data[0].UnitKerja_Alamat);
            $('.UnitKerja_Surel').text(data[0].UnitKerja_Surel);
            $('.UnitKerja_NoTlp').text(data[0].UnitKerja_NoTlp);
            $('.UnitKerja_NoFax').text(data[0].UnitKerja_NoFax);
            $('.UnitKerja_Web').text(data[0].UnitKerja_Web);
          });
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data from ajax');
        }
    });
    return false;
};
//-- END: load data Ringkasan Profil & UnitKerja

//-- BEGIN: variabel global
var id;
//-- END: variabel global

//-- BEGIN: fungsi simpan data Ringkasan Profil
$('#m_data_simpan_profil').on('click', function() {
  // variable untuk menyimpan nilai input
  var nip         = $('[name="Auditor_NIP"]').val();
  var nama        = $('[name="Auditor_NamaLengkap"]').val();
  var surel       = $('[name="Pengguna_Surel"]').val();
  var nohp        = $('[name="Pengguna_NoHP"]').val();
  var alamat      = $('[name="Pengguna_Alamat"]').val();
  // ubah teks tombol
  $('#m_data_simpan_profil').text('Menyimpan...');
  // nonaktifkan tombol
  $('#m_data_simpan_profil').attr('disabled',true);
  // variable untuk menyimpan url ajax
  var url = "<?php echo site_url('auditor/auditorprofil_ubah')?>";
  // menambahkan data ke ajax dengan ajax
  $.ajax({
      type: "POST",
      url : url,
      dataType: "JSON",
      data : {Auditor_NIP:nip, Auditor_NamaLengkap:nama, Pengguna_Surel:surel, Pengguna_NoHP:nohp, Pengguna_Alamat:alamat},
      success: function(data)
      {
        $('#modal_form_profil').modal('hide');
        // ubah teks tombol
        $('#m_data_simpan_profil').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan_profil').attr('disabled',false);
        // panggil fungsi getProfil
        getProfil();
        // pesan penambahan data berhasil
        swal({
            title: 'Berhasil!',
            text: "Data telah tersimpan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      },
      error: function ()
      {
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
        // alert('Error adding / update data');
        // ubah teks tombol
        $('#m_data_simpan_profil').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan_profil').attr('disabled',false);
      }
  });
  return false;
});
//-- END: fungsi simpan data Ringkasan Profil

//-- BEGIN: fungsi ubah data Ringkasan Profil
$('#m_data_ubah_profil').on('click', function() {
  metode_simpan = 'ubah';
  var id = '<?php echo $this->input->get('id');?>';
  // reset isi form di modal
  $('#form_profil')[0].reset();
  // memuat data dengan ajax
  $.ajax({
      type: "GET",
      url : "<?php echo site_url('auditor/auditorprofil_ambil')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
        $.each(data,function(Auditor_NIP, Auditor_NamaLengkap, Auditor_Surel, isAuditor, Auditor_NoHP, Auditor_Alamat, Instansi_Nama, UnitKerja_Nama, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax, UnitKerja_Web)
        {
          // tampilkan modal
          $('#modal_form_profil').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Ubah Data Non-Kedinasan');
          // nonaktifkan inputan primary key / foreign key
          $('[name="Auditor_NIP"]').attr('disabled',true);
          // ambil nilai input dari parameter JSON
          $('[name="Auditor_NIP"]').val(data[0].NIP);
          $('[name="Auditor_NamaLengkap"]').val(data[0].NamaLengkap);
          $('[name="Pengguna_Surel"]').val(data[0].Surel);
          $('[name="Pengguna_NoHP"]').val(data[0].NoHP);
          $('[name="Pengguna_Alamat"]').val(data[0].Alamat);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // alert('Error get data from ajax');
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      }
  });
  return false;
});
//-- END: fungsi ubah data Ringkasan Profil

//-- BEGIN: fungsi simpan data Status Auditor
$('#m_data_ubah_status').on('click', function() {
  // variable untuk menyimpan nilai input
  var nip         = $('[name="Auditor_NIP"]').val();
  var status      = $('[name="Status_Auditor"]').val();
  var peran       = $('[name="Pengguna_RoleID"]').val();
  // ubah teks tombol
  $('#m_data_simpan_profil').text('Menyimpan...');
  // nonaktifkan tombol
  $('#m_data_simpan_profil').attr('disabled',true);
  // variable untuk menyimpan url ajax
  var url = "<?php echo site_url('auditor/auditorprofil_ubah')?>";
  // menambahkan data ke ajax dengan ajax
  $.ajax({
      type: "POST",
      url : url,
      dataType: "JSON",
      data : {Auditor_NIP:nip, Status_Auditor:status, Pengguna_RoleID:peran},
      success: function(data)
      {
        $('#modal_form_status').modal('show');
        // ubah teks tombol
        $('#m_data_simpan_status').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan_status').attr('disabled',false);
        // panggil fungsi getProfil
        getProfil();
        // pesan penambahan data berhasil
        swal({
            title: 'Berhasil!',
            text: "Data telah tersimpan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      },
      error: function ()
      {
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
        // alert('Error adding / update data');
        // ubah teks tombol
        $('#m_data_simpan_profil').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan_profil').attr('disabled',false);
      }
  });
  return false;
});
//-- END: fungsi simpan data Status Auditor

//-- BEGIN: fungsi ubah data Status Auditor
$('#m_data_simpan_status').on('click', function() {
  metode_simpan = 'ubah';
  var id = '<?php echo $this->input->get('id');?>';
  // reset isi form di modal
  $('#form_profil')[0].reset();
  // memuat data dengan ajax
  $.ajax({
      type: "GET",
      url : "<?php echo site_url('auditor/auditorprofil_ambil')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
        $.each(data,function(Auditor_NIP, Auditor_NamaLengkap, Auditor_Surel, isAuditor, Auditor_NoHP, Auditor_Alamat, Instansi_Nama, UnitKerja_Nama, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax, UnitKerja_Web)
        {
          // tampilkan modal
          $('#modal_form_profil').modal('hide');
          // menetapkan judul di modal
          $('.modal-title').text('Ubah Data Non-Kedinasan');
          // nonaktifkan inputan primary key / foreign key
          $('[name="Auditor_NIP"]').attr('disabled',true);
          // ambil nilai input dari parameter JSON
          $('[name="Auditor_NIP"]').val(data[0].NIP);
          $('[name="Auditor_NamaLengkap"]').val(data[0].NamaLengkap);
          $('[name="Pengguna_Surel"]').val(data[0].Surel);
          $('[name="Pengguna_NoHP"]').val(data[0].NoHP);
          $('[name="Pengguna_Alamat"]').val(data[0].Alamat);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // alert('Error get data from ajax');
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      }
  });
  return false;
});
//-- END: fungsi ubah data Status Auditor

//-- BEGIN: fungsi atur ulang katasandi
$('#m_data_aturulang_katasandi').on('click', function() {
  // variable id untuk menyimpan nilai primary key atau foreign key,
  // nilai didapat dari action button
  var id = '<?php echo $this->input->get('id');?>'
  swal({
      title: 'Apakah anda yakin mengatur ulang Kata Sandi?',
      text: "Kata Sandi yang telah diubah tidak dapat dikembalikan!, sistem akan mengirimkan Kata Sandi baru ke surat elektronik pemilik akun ini.",
      type: 'question',
      showCancelButton: true,
      confirmButtonText: "<span><i class='la la-check'></i><span>Ya, atur ulang!</span></span>",
      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
      cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
      cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
      reverseButtons: true,
      animation: false,
      customClass: 'animated bounceIn'
  }).then(function(result){
      if (result.value) {
        // memuat data dengan ajax
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('auditor/katasandi_ubah')?>",
          dataType : "JSON",
            data : {id: id},
            success: function(data){
            // panggil fungsi getProfil
            getProfil();
            }
        });
        swal({
            title: 'Kata Sandi Di Atur Ulang!',
            text: "Kata Sandi berhasil di atur ulang, surel berhasil di kirimkan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            customClass: 'animated bounceIn'
        })
        // panggil fungsi getProfil
        getProfil();
        // result.dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
      } else if (result.dismiss === 'cancel') {
          swal({
              title: 'Dibatalkan',
              text: "Proses atur ulang Kata Sandi dibatalkan",
              type: 'error',
              confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
              customClass: 'animated bounceIn'
          }
          )
      }
  });
  return false;
});
//-- END:  fungsi atur ulang katasandi

//-- BEGIN: fungsi ubah data Unit Kerja
$('#m_data_ubah_unit').on('click', function() {
  metode_simpan = 'ubah unitkerja';
  var id='<?php echo $this->input->get('id');?>';
  // reset isi form di modal
  $('#form_unitkerja')[0].reset();
  // memuat data dengan ajax
  $.ajax({
      type: "GET",
      url : "<?php echo site_url('auditor/auditorunitkerja_ambil')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
        $.each(data,function(UnitKerja_Kode, UnitKerja_NamaUnitInstansi, Instansi_Kode, Instansi_Nama, JenisInstansi_Kode, JenisInstansi)
        {
          // tampilkan modal
          $('#modal_form_unit').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Perpindahan Unit Kerja');
          // nonaktifkan inputan primary key / foreign key
          // key.attr('disabled',true);
          // ambil nilai input dari parameter JSON
          // key.val(data[0].Auditor_Kode);
          $('[name="UnitKerja_Kode"]').val(data[0].UnitKerja_Kode);
          $('[name="UnitKerja_Nama"]').val(data[0].UnitKerja_NamaUnitInstansi);
          $('[name="Instansi_Kode"]').val(data[0].Instansi_Kode);
          $('[name="Instansi_Nama"]').val(data[0].Instansi_Nama);
          $('[name="JenisInstansi_Kode"]').val(data[0].JenisInstansi_Kode);
          $('[name="JenisInstansi_Nama"]').val(data[0].JenisInstansi);


          // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // alert('Error get data from ajax');
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      }
  });
  return false;
});
//-- END: fungsi ubah data Unit Kerja

//-- BEGIN: fungsi simpan data unitkerja
$('#m_data_simpan_unit').on('click', function() {
  // variable untuk menyimpan nilai input
  var nip    = '<?php echo $this->input->get('id');?>';
  var kodeunit    = $('[name="UnitKerja_Kode"]').val();

  // ubah teks tombol
  $('#m_data_simpan').text('Menyimpan...');
  // nonaktifkan tombol
  $('#m_data_simpan').attr('disabled',true);
  // variable untuk menyimpan url ajax
  url = "<?php echo site_url('auditor/auditorunitkerja_ubah')?>";
  // menambahkan data ke ajax dengan ajax
  $.ajax({
      type: "POST",
      url : url,
      dataType: "JSON",
      data : {Auditor_NIP:nip, UnitKerja_Kode:kodeunit},
      success: function(data)
      {
        $('#modal_form_unit').modal('hide');
        getProfil();
        // $('.m_datatable').mDatatable('reload');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
        // pesan penambahan data berhasil
        swal({
            title: 'Berhasil!',
            text: "Data telah tersimpan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      },
      error: function ()
      {
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
        // alert('Error adding / update data');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
      }
  });
  return false;
});
//-- END: fungsi simpan data unitkerja

//-- BEGIN: dataTable remote pendidikan
var DatatableJsonRemotePendidikan = function () {

  //-- BEGIN: fungsi_data
  var pendidikan = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/pendidikan_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Pendidikan_Jenjang",
        title: "Jenjang",
        sortable: false
      }, {
        field: "Pendidikan_Lembaga",
        title: "Lembaga Pendidikan",
        width: 120,
        textAlign: 'center',
        sortable: false
      }, {
        field: "Pendidikan_Jurusan",
        title: "Jurusan",
        sortable: false
      }, {
        field: "Pendidikan_TglIjazah",
        title: "Tgl Terbit Ijazah",
        width: 80,
        textAlign: 'center',
        sortable: 'desc'
      }, {
        field: "Pendidikan_Ijazah",
        title: "File Ijazah",
        width: 60,
        textAlign: 'center',
        template: function (row) {
          // callback function support for column rendering
          var file = row.Pendidikan_Ijazah == '' ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.Pendidikan_Ijazah + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="'+row.Pendidikan_Ijazah+'" title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        }
      }, {
        field: "Aksi",
        width: 90,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_ubah_pendidikan" data="'+row.Pendidikan_ID+'" title="Ubah Data">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_pendidikan" data="'+row.Pendidikan_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_pendidikan').mDatatable(tabel_data);
        var key = $('[name="Pendidikan_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_pendidikan').on('click', function() {
          $('#json_pendidikan').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_pendidikan').on('click', function() {
          // variable untuk menyimpan nilai input
          var id         = key.val();
          var nip        = '<?php echo $this->input->get('id');?>';
          var kode       = $('[name="Pendidikan_Kode"]').val();
          var lembaga    = $('[name="Pendidikan_Lembaga"]').val();
          var fakultas   = $('[name="Pendidikan_Fakultas"]').val();
          var jurusan    = $('[name="Pendidikan_Jurusan"]').val();
          var gelardepan = $('[name="Pendidikan_GelarDepan"]').val();
          var gelarbelakang= $('[name="Pendidikan_GelarBelakang"]').val();
          var noijazah   = $('[name="Pendidikan_NoIjazah"]').val();
          var tglijazah  = $('[name="Pendidikan_TglIjazah"]').val();
          var file       = $('[name="Pendidikan_Ijazah"]').val();
          var filelama   = $('[name="Pendidikan_Ijazah_Lama"]').val();
          // ubah teks tombol
          $('#m_data_simpan_pendidikan').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_pendidikan').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/pendidikan_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/pendidikan_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajaxFileUpload({
              type: "POST",
              url : url,
              secureuri: true,
              fileElementId : 'Pendidikan_Ijazah',
              dataType: "JSON",
              data : {Pendidikan_ID:id, Auditor_NIP:nip, Pendidikan_Kode:kode, Pendidikan_Lembaga:lembaga, Pendidikan_Fakultas:fakultas, Pendidikan_Jurusan:jurusan, Pendidikan_GelarDepan:gelardepan, Pendidikan_GelarBelakang:gelarbelakang, Pendidikan_NoIjazah:noijazah, Pendidikan_TglIjazah:tglijazah, Pendidikan_Ijazah_Lama:filelama},
              success: function(data, status)
              {
                $('#modal_form_pendidikan').modal('hide');
                $('#json_pendidikan').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_pendidikan').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_pendidikan').attr('disabled',false);
                // panggil fungsi getProfil
                getProfil();
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function(status)
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_pendidikan').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_pendidikan').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_pendidikan').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_pendidikan')[0].reset();
          // tampilkan modal
          $('#modal_form_pendidikan').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Pendidikan Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_pendidikan', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_pendidikan')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/pendidikan_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(Pendidikan_Kode, Pendidikan_Lembaga, Pendidikan_Fakultas, Pendidikan_Jurusan, Pendidikan_GelarDepan, Pendidikan_GelarBelakang, Pendidikan_NoIjazah, Pendidikan_TglIjazah, Pendidikan_Ijazah)
                {
                  // tampilkan modal
                  $('#modal_form_pendidikan').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Pendidikan');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].Pendidikan_ID);
                  $('[name="Pendidikan_Kode"]').val(data[0].Pendidikan_Kode);
                  $('[name="Pendidikan_Jenjang"]').val(data[0].Pendidikan_Jenjang);
                  $('[name="Pendidikan_Lembaga"]').val(data[0].Pendidikan_Lembaga);
                  $('[name="Pendidikan_Fakultas"]').val(data[0].Pendidikan_Fakultas);
                  $('[name="Pendidikan_Jurusan"]').val(data[0].Pendidikan_Jurusan);
                  $('[name="Pendidikan_GelarDepan"]').val(data[0].Pendidikan_GelarDepan);
                  $('[name="Pendidikan_GelarBelakang"]').val(data[0].Pendidikan_GelarBelakang);
                  $('[name="Pendidikan_NoIjazah"]').val(data[0].Pendidikan_NoIjazah);
                  $('[name="Pendidikan_TglIjazah"]').datepicker('update',data[0].Pendidikan_TglIjazah);
                  $('[name="Pendidikan_Ijazah_Lama"]').val(data[0].Pendidikan_Ijazah);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_pendidikan', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/pendidikan_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_pendidikan').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // panggil fungsi getProfil
                getProfil();
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      pendidikan();
    }
  };
}();
//-- END: dataTable remote pendidikan

//-- BEGIN: dataTable remote jabatan
var DatatableJsonRemotePangkat = function () {

  //-- BEGIN: fungsi_data
  var pangkat = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/pangkat_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Pangkat_Nama",
        title: "Pangkat"
      }, {
        field: "GolRuang_Kode",
        title: "Golongan Ruang",
        textAlign: 'center',
        width: 80
      }, {
        field: "Pangkat_NoSuratKeputusan",
        title: "Nomor Surat Keputusan",
        width: 160
      },{
        field: "Pangkat_TglSuratKeputusan",
        title: "Tanggal Surat",
        width: 80,
        textAlign: 'center'
      }, {
        field: "Pangkat_TglMulaiTugas",
        title: "TMT Pangkat",
        width: 80,
        textAlign: 'center',
        sortable: 'desc'
      }, {
        field: "Pangkat_SK",
        title: "File Surat",
        width: 60,
        textAlign: 'center',
        template: function (row) {
          // callback function support for column rendering
          var file = row.Pangkat_SK == null ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.Pangkat_SK + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="" title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        }
      }, {
        field: "Aksi",
        width: 80,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_ubah_pangkat" data="'+row.PangkatGol_ID+'" title="Ubah Data">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_pangkat" data="'+row.PangkatGol_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_pangkat').mDatatable(tabel_data);
        var key = $('[name="PangkatGol_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_pangkat').on('click', function() {
          $('#json_pangkat').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_pangkat').on('click', function() {
          // variable untuk menyimpan nilai input
          var id            = key.val();
          var nip           = '<?php echo $this->input->get('id');?>';
          var kode          = $('[name="Pangkat_Kode"]').val();
          var pangkat       = $('[name="Pangkat_Nama"]').val();
          var noSK          = $('[name="Pangkat_NoSuratKeputusan"]').val();
          var tglSK         = $('[name="Pangkat_TglSuratKeputusan"]').val();
          var tmt           = $('[name="Pangkat_TglMulaiTugas"]').val();
          var file          = $('[name="Pangkat_SK"]').val();
          var filelama      = $('[name="Pangkat_SK_Lama"]').val();
          // ubah teks tombol
          $('#m_data_simpan_pangkat').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_pangkat').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/pangkat_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/pangkat_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajaxFileUpload({
              type: "POST",
              url : url,
              secureuri: true,
              fileElementId : 'Pangkat_SK',
              dataType: "JSON",
              data : {PangkatGol_ID:id, Auditor_NIP:nip, Pangkat_Kode:kode, Pangkat_Nama:pangkat, Pangkat_NoSuratKeputusan:noSK, Pangkat_TglSuratKeputusan:tglSK, Pangkat_TglMulaiTugas:tmt, Pangkat_SK_Lama:filelama},
              success: function(data, status)
              {
                $('#modal_form_pangkat').modal('hide');
                $('#json_pangkat').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_pangkat').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_pangkat').attr('disabled',false);
                // panggil fungsi getProfil
                getProfil();
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function(status)
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_pangkat').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_pangkat').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_pangkat').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_pangkat')[0].reset();
          // tampilkan modal
          $('#modal_form_pangkat').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Pangkat Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_pangkat', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_pangkat')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/pangkat_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(PangkatGol_ID, Auditor_NIP, Pangkat_Kode, Pangkat_Nama, Pangkat_NoSuratKeputusan, Pangkat_TglSuratKeputusan, Pangkat_TglMulaiTugas, Pangkat_SK)
                {
                  // tampilkan modal
                  $('#modal_form_pangkat').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Pangkat');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].PangkatGol_ID);
                  $('[name="Auditor_NIP"]').val(data[0].Auditor_NIP);
                  $('[name="Pangkat_Kode"]').val(data[0].Pangkat_Kode);
                  $('[name="Pangkat_Nama"]').val(data[0].Pangkat_Nama);
                  $('[name="Pangkat_NoSuratKeputusan"]').val(data[0].Pangkat_NoSuratKeputusan);
                  $('[name="Pangkat_TglSuratKeputusan"]').datepicker('update', data[0].Pangkat_TglSuratKeputusan);
                  $('[name="Pangkat_TglMulaiTugas"]').datepicker('update', data[0].Pangkat_TglMulaiTugas);
                  $('[name="Pangkat_SK_Lama"]').val(data[0].Pangkat_SK);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_pangkat', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/pangkat_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_pangkat').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // panggil fungsi getProfil
                getProfil();
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      pangkat();
    }
  };
}();
//-- END: dataTable remote pangkat

//-- BEGIN: dataTable remote jabatan
var DatatableJsonRemoteJabatan = function () {

  //-- BEGIN: fungsi_data
  var jabatan = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/jabatan_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Jabatan_Nama",
        title: "Jenjang Jabatan",
        width: 80,
        textAlign: 'center',
        sortable: false
      }, {
        field: "JenjangJabatan_Nama",
        title: "Jabatan",
        width: 120
      }, {
        field: "Jabatan_NoSuratKeputusan",
        title: "Nomor Surat Keputusan",
        width: 160
      }, {
        field: "Jabatan_TglSuratKeputusan",
        title: "Tanggal Surat",
        textAlign: 'center',
        width: 80
      }, {
        field: "Jabatan_TglMulaiTugas",
        title: "TMT Jabatan",
        textAlign: 'center',
        width: 80,
        sortable: 'desc'
      }, {
        field: "Jabatan_SK",
        title: "File SK Jabatan",
        width: 60,
        textAlign: 'center',
        template: function (row) {
          // callback function support for column rendering
          var file = row.Jabatan_SK == null ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.Jabatan_SK + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="" title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        }
      }, {
        field: "Aksi",
        width: 80,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="'+row.Jabatan_ID+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_ubah_jabatan" data="'+row.Jabatan_ID+'" title="Ubah Data">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_jabatan" data="'+row.Jabatan_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_jabatan').mDatatable(tabel_data);
        var key = $('[name="Jabatan_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_jabatan').on('click', function() {
          $('#json_jabatan').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_jabatan').on('click', function() {
          // variable untuk menyimpan nilai input
          var id            = key.val();
          var nip           = '<?php echo $this->input->get('id');?>';
          var kelompokjab   = $('[name="Jabatan_Kode"]').val();
          var jabatan       = $('[name="JenjangJabatan_Kode"]').val();
          var nomor         = $('[name="Jabatan_NoSuratKeputusan"]').val();
          var tgl           = $('[name="Jabatan_TglSuratKeputusan"]').val();
          var tmt           = $('[name="Jabatan_TglMulaiTugas"]').val();
          var file          = $('[name="Jabatan_SK"]').val();
          var filelama      = $('[name="Jabatan_SK_Lama"]').val();
          // ubah teks tombol
          $('#m_data_simpan_jabatan').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_jabatan').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/jabatan_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/jabatan_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajaxFileUpload({
              type: "POST",
              url : url,
              secureuri: true,
              fileElementId : 'Jabatan_SK',
              dataType: "JSON",
              data : {Jabatan_ID:id, Auditor_NIP:nip, Jabatan_Kode:kelompokjab, JenjangJabatan_Kode:jabatan, Jabatan_NoSuratKeputusan:nomor, Jabatan_TglSuratKeputusan:tgl, Jabatan_TglMulaiTugas:tmt, Jabatan_SK_Lama:filelama},
              success: function(data, status)
              {
                $('#modal_form_jabatan').modal('hide');
                $('#json_jabatan').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_jabatan').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_jabatan').attr('disabled',false);
                // panggil fungsi getProfil
                getProfil();
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function(status)
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_jabatan').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_jabatan').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_jabatan').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form')[0].reset();
          // tampilkan modal
          $('#modal_form_jabatan').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Jabatan Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_jabatan', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/jabatan_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(Jabatan_Kode, JenjangJabatan_Kode, Jabatan_NoSuratKeputusan, Jabatan_TglSuratKeputusan, Jabatan_TglMulaiTugas, Jabatan_SK)
                {
                  // tampilkan modal
                  $('#modal_form_jabatan').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Jabatan');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].Jabatan_ID);
                  $('[name="Jabatan_Kode"]').val(data[0].Jabatan_Kode);
                  $('[name="JenjangJabatan_Kode"]').val(data[0].JenjangJabatan_Kode);
                  $('[name="Jabatan_NoSuratKeputusan"]').val(data[0].Jabatan_NoSuratKeputusan);
                  $('[name="Jabatan_TglSuratKeputusan"]').datepicker('update', data[0].Jabatan_TglSuratKeputusan);
                  $('[name="Jabatan_TglMulaiTugas"]').datepicker('update', data[0].Jabatan_TglMulaiTugas);
                  $('[name="Jabatan_SK_Lama"]').val(data[0].Jabatan_SK);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_jabatan', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/jabatan_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_jabatan').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // panggil fungsi getProfil
                getProfil();
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      jabatan();
    }
  };
}();
//-- END: dataTable remote jabatan

//-- BEGIN: dataTable remote sertifikasijfa
var DatatableJsonRemoteDiklatJFA = function () {

  //-- BEGIN: fungsi_data
  var sertifikasijfa = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/sertifikasijfa_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Diklat_Nama",
        title: "Nama Diklat",
        width: 180,
        textAlign: 'center',
        sortable: false
      }, {
        field: "SertifikasiJFA_TglMulaiDiklat",
        title: "Periode Diklat",
        template: function (row) {
          // callback function support for column rendering
          var periode =
          '' + row.SertifikasiJFA_TglMulaiDiklat + '<br> <span class="m--font-bold"> s/d </span> <br> ' + row.SertifikasiJFA_TglAkhirDiklat + '';
          return periode;
        },
        width: 80,
        textAlign: 'center'
      }, {
        field: "SertifikasiJFA_TglSTMPL",
        title: "Tgl. & No. STMPL",
        width: 140,
        textAlign: 'center',
        sortable: 'desc',
        template: function (row) {
          // callback function support for column rendering
          var stmpl =
          '<span class="m--font-bold">Tgl.: </span>' + row.SertifikasiJFA_TglSTMPL + '<br>\
          <span class="m--font-bold">No.: </span>' + row.SertifikasiJFA_NoSTMPL + '';
          return stmpl;
        }
      }, {
        field: "SertifikasiJFA_TglSTTPL",
        title: "Tgl. & No. STTPL",
        width: 140,
        textAlign: 'center',
        template: function (row) {
          // callback function support for column rendering
          var sttpl =
          '<span class="m--font-bold">Tgl.: </span>' + row.SertifikasiJFA_TglSTTPL + '<br>\
          <span class="m--font-bold">No.: </span>' + row.SertifikasiJFA_NoSTTPL + '';
          return sttpl;
        }
      }, {
        field: "SertifikasiJFA_Sertifikat",
        title: "File Sertifikat",
        template: function (row) {
          // callback function support for column rendering
          var file = row.SertifikasiJFA_Sertifikat == null ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.SertifikasiJFA_Sertifikat + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="" title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        },
        width: 60,
        textAlign: 'center'
      }, {
        field: "Aksi",
        width: 80,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_ubah_sertifikasijfa" data="'+row.SertifikasiJFA_ID+'" title="Ubah Data">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_sertifikasijfa" data="'+row.SertifikasiJFA_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_sertifikasijfa').mDatatable(tabel_data);
        var key = $('[name="SertifikasiJFA_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_sertifikasijfa').on('click', function() {
          $('#json_sertifikasijfa').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_sertifikasijfa').on('click', function() {
          // variable untuk menyimpan nilai input
          var id        = key.val();
          var nip       = '<?php echo $this->input->get('id');?>';
          var kode      = $('[name="Diklat_Kode_JFA"]').val();
          var mulai     = $('[name="SertifikasiJFA_TglMulaiDiklat"]').val();
          var akhir     = $('[name="SertifikasiJFA_TglAkhirDiklat"]').val();
          var nostmpl   = $('[name="SertifikasiJFA_NoSTMPL"]').val();
          var tglstmpl  = $('[name="SertifikasiJFA_TglSTMPL"]').val();
          var nosttpl   = $('[name="SertifikasiJFA_NoSTTPL"]').val();
          var tglsttpl  = $('[name="SertifikasiJFA_TglSTTPL"]').val();
          var file      = $('[name="SertifikasiJFA_Sertifikat"]').val();
          var filelama  = $('[name="SertifikasiJFA_Sertifikat_Lama"]').val();
          // ubah teks tombol
          $('#m_data_simpan_sertifikasijfa').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_sertifikasijfa').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/sertifikasijfa_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/sertifikasijfa_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajaxFileUpload({
              type: "POST",
              url : url,
              secureuri: true,
              fileElementId : 'SertifikasiJFA_Sertifikat',
              dataType: "JSON",
              data : {SertifikasiJFA_ID:id, Auditor_NIP:nip, Diklat_Kode_JFA:kode, SertifikasiJFA_TglMulaiDiklat:mulai, SertifikasiJFA_TglAkhirDiklat:akhir, SertifikasiJFA_NoSTMPL:nostmpl, SertifikasiJFA_TglSTMPL:tglstmpl, SertifikasiJFA_NoSTTPL:nosttpl, SertifikasiJFA_TglSTTPL:tglsttpl, SertifikasiJFA_Sertifikat_Lama:filelama},
              success: function(data, status)
              {
                $('#modal_form_sertifikasijfa').modal('hide');
                $('#json_sertifikasijfa').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_sertifikasijfa').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_sertifikasijfa').attr('disabled',false);
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function(status)
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_sertifikasijfa').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_sertifikasijfa').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_sertifikasijfa').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_sertifikasijfa')[0].reset();
          // tampilkan modal
          $('#modal_form_sertifikasijfa').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Sertifikasi JFA Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_sertifikasijfa', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/sertifikasijfa_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(Diklat_Kode, SertifikasiJFA_TglMulaiDiklat, SertifikasiJFA_TglAkhirDiklat, SertifikasiJFA_NoSTMPL, SertifikasiJFA_TglSTMPL, SertifikasiJFA_NoSTTPL, SertifikasiJFA_TglSTTPL, SertifikasiJFA_Sertifikat )
                {
                  // tampilkan modal
                  $('#modal_form_sertifikasijfa').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Sertifikasi JFA');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].SertifikasiJFA_ID);
                  $('[name="Diklat_Kode_JFA"]').val(data[0].Diklat_Kode);
                  $('[name="SertifikasiJFA_TglMulaiDiklat"]').datepicker('update', data[0].SertifikasiJFA_TglMulaiDiklat);
                  $('[name="SertifikasiJFA_TglAkhirDiklat"]').datepicker('update', data[0].SertifikasiJFA_TglAkhirDiklat);
                  $('[name="SertifikasiJFA_NoSTMPL"]').val(data[0].SertifikasiJFA_NoSTMPL);
                  $('[name="SertifikasiJFA_TglSTMPL"]').datepicker('update', data[0].SertifikasiJFA_TglSTMPL);
                  $('[name="SertifikasiJFA_NoSTTPL"]').val(data[0].SertifikasiJFA_NoSTTPL);
                  $('[name="SertifikasiJFA_TglSTTPL"]').datepicker('update', data[0].SertifikasiJFA_TglSTTPL);
                  $('[name="SertifikasiJFA_Sertifikat_Lama"]').val(data[0].SertifikasiJFA_Sertifikat);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_sertifikasijfa', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/sertifikasijfa_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_sertifikasijfa').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      sertifikasijfa();
    }
  };
}();
//-- END: dataTable remote sertifikasijfa

//-- BEGIN: dataTable remote diklatteknissubstantif
var DatatableJsonRemoteDiklatSubstantif = function () {

  //-- BEGIN: fungsi_data
  var diklatteknissubstantif = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/teknissubstantif_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Diklat_Nama",
        title: "Nama Diklat",
        width: 180,
        textAlign: 'center',
        sortable: false
      }, {
        field: "TeknisSubstantif_TglMulai",
        title: "Periode Diklat",
        template: function (row) {
          // callback function support for column rendering
          var periode =
          '' + row.TeknisSubstantif_TglMulai + '<br> <span class="m--font-bold"> s/d </span> <br> ' + row.TeknisSubstantif_TglAkhir + '';
          return periode;
        },
        width: 80,
        textAlign: 'center'
      }, {
        field: "TeknisSubstantif_NoSTMPL",
        title: "Tgl. & No. STMPL",
        width: 140,
        textAlign: 'center',
        sortable: 'desc',
        template: function (row) {
          // callback function support for column rendering
          var stmpl =
          '<span class="m--font-bold">Tgl.: </span>' + row.TeknisSubstantif_TglSTMPL + '<br>\
          <span class="m--font-bold">No.: </span>' + row.TeknisSubstantif_NoSTMPL + '';
          return stmpl;
        }
      }, {
        field: "TeknisSubstantif_Sertifikat",
        title: "File Sertifikat",
        template: function (row) {
          // callback function support for column rendering
          var file = row.TeknisSubstantif_Sertifikat == '' ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.TeknisSubstantif_Sertifikat + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"   title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        },
        width: 60,
        textAlign: 'center'
      }, {
        field: "Aksi",
        width: 80,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_ubah_diklatteknissubstantif" data="'+row.TeknisSubstantif_ID+'" title="Ubah Data">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_diklatteknissubstantif" data="'+row.TeknisSubstantif_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_diklatteknissubstantif').mDatatable(tabel_data);
        var key = $('[name="TeknisSubstantif_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_diklatteknissubstantif').on('click', function() {
          $('#json_diklatteknissubstantif').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_teknissubstantif').on('click', function() {
          // variable untuk menyimpan nilai input
          var id         = key.val();
          var nip        = '<?php echo $this->input->get('id');?>';
          var kode       = $('[name="Diklat_Kode_TekSub"]').val();
          var mulai      = $('[name="TeknisSubstantif_TglMulai"]').val();
          var akhir      = $('[name="TeknisSubstantif_TglAkhir"]').val();
          var nostmpl    = $('[name="TeknisSubstantif_NoSTMPL"]').val();
          var tglstmpl   = $('[name="TeknisSubstantif_TglSTMPL"]').val();
          var file       = $('[name="TeknisSubstantif_Sertifikat"]').val();
          var filelama   = $('[name="TeknisSubstantif_Sertifikat_Lama"]').val();
          // ubah teks tombol
          $('#m_data_simpan_teknissubstantif').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_teknissubstantif').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/teknissubstantif_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/teknissubstantif_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajaxFileUpload({
              type: "POST",
              url : url,
              secureuri: true,
              fileElementId : 'TeknisSubstantif_Sertifikat',
              dataType: "JSON",
              data : {TeknisSubstantif_ID:id, Auditor_NIP:nip, Diklat_Kode_TekSub:kode, TeknisSubstantif_TglMulai:mulai, TeknisSubstantif_TglAkhir:akhir, TeknisSubstantif_NoSTMPL:nostmpl, TeknisSubstantif_TglSTMPL:tglstmpl, TeknisSubstantif_Sertifikat_Lama:filelama},
              success: function(data, status)
              {
                $('#modal_form_teknissubstantif').modal('hide');
                $('#json_diklatteknissubstantif').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_teknissubstantif').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_teknissubstantif').attr('disabled',false);
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function(status)
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_teknissubstantif').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_teknissubstantif').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_diklatteknissubstantif').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_teknissubstantif')[0].reset();
          // tampilkan modal
          $('#modal_form_teknissubstantif').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Diklat Teknis Substantif Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_diklatteknissubstantif', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_teknissubstantif')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/teknissubstantif_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(Diklat_Kode, TeknisSubstantif_TglMulai, TeknisSubstantif_TglAkhir, TeknisSubstantif_NoSTMPL, TeknisSubstantif_TglSTMPL, TeknisSubstantif_Sertifikat)
                {
                  // tampilkan modal
                  $('#modal_form_teknissubstantif').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Diklat Teknis Substantif');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].TeknisSubstantif_ID);
                  $('[name="Diklat_Kode_TekSub"]').val(data[0].Diklat_Kode);
                  $('[name="TeknisSubstantif_TglMulai"]').datepicker('update', data[0].TeknisSubstantif_TglMulai);
                  $('[name="TeknisSubstantif_TglAkhir"]').datepicker('update', data[0].TeknisSubstantif_TglAkhir);
                  $('[name="TeknisSubstantif_NoSTMPL"]').val(data[0].TeknisSubstantif_NoSTMPL);
                  $('[name="TeknisSubstantif_TglSTMPL"]').datepicker('update', data[0].TeknisSubstantif_TglSTMPL);
                  $('[name="TeknisSubstantif_Sertifikat_Lama"]').val(data[0].TeknisSubstantif_Sertifikat);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_diklatteknissubstantif', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/teknissubstantif_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_diklatteknissubstantif').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      diklatteknissubstantif();
    }
  };
}();
//-- END: dataTable remote diklatteknissubstantif

//-- BEGIN: dataTable remote diklatsertifikasiprofesi
var DatatableJsonRemoteDiklatProfesi = function () {

  //-- BEGIN: fungsi_data
  var diklatsertifikasiprofesi = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/sertifikasiprofesi_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "SertifikasiProfesi_Nama",
        title: "Nama Diklat",
        width: 180,
        textAlign: 'center',
        sortable: false
      }, {
        field: "SertifikasiProfesi_Gelar",
        title: "Gelar Profesi",
        responsive: {visible: 'lg'}
      }, {
        field: "SertifikasiProfesi_TglMulai",
        title: "Periode Diklat",
        template: function (row) {
          // callback function support for column rendering
          var periode =
          '' + row.SertifikasiProfesi_TglMulai + '<br> <span class="m--font-bold"> s/d </span> <br> ' + row.SertifikasiProfesi_TglAkhir + '';
          return periode;
        },
        width: 80,
        textAlign: 'center'
      }, {
        field: "SertifikasiProfesi_TglSTMPL",
        title: "Tgl. & No. STMPL",
        width: 140,
        textAlign: 'center',
        sortable: 'desc',
        template: function (row) {
          // callback function support for column rendering
          var stmpl =
          '<span class="m--font-bold">Tgl.: </span>' + row.SertifikasiProfesi_TglSTMPL + '<br>\
          <span class="m--font-bold">No.: </span>' + row.SertifikasiProfesi_NoSTMPL + '';
          return stmpl;
        }
      }, {
        field: "SertifikasiProfesi_Sertifikat",
        title: "File Sertifikat",
        template: function (row) {
          // callback function support for column rendering
          var file = row.SertifikasiProfesi_Sertifikat == null ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.SertifikasiProfesi_Sertifikat + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="" title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        },
        width: 60,
        textAlign: 'center'
      }, {
        field: "Aksi",
        width: 80,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_ubah_sertifikasiprofesi" data="'+row.SertifikasiProfesi_ID+'" title="Ubah Data">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_sertifikasiprofesi" data="'+row.SertifikasiProfesi_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_sertifikasiprofesi').mDatatable(tabel_data);
        var key = $('[name="SertifikasiProfesi_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_sertifikasiprofesi').on('click', function() {
          $('#json_sertifikasiprofesi').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_sertifikasiprofesi').on('click', function() {
          // variable untuk menyimpan nilai input
          var id         = key.val();
          var nip        = '<?php echo $this->input->get('id');?>';
          var nama       = $('[name="SertifikasiProfesi_Nama"]').val();
          var lembaga    = $('[name="SertifikasiProfesi_Lembaga"]').val();
          var gelar      = $('[name="SertifikasiProfesi_Gelar"]').val();
          var mulai      = $('[name="SertifikasiProfesi_TglMulai"]').val();
          var akhir      = $('[name="SertifikasiProfesi_TglAkhir"]').val();
          var nostmpl    = $('[name="SertifikasiProfesi_NoSTMPL"]').val();
          var tglstmpl   = $('[name="SertifikasiProfesi_TglSTMPL"]').val();
          var file       = $('[name="SertifikasiProfesi_Sertifikat"]').val();
          var filelama   = $('[name="SertifikasiProfesi_Sertifikat_Lama"]').val();
          // ubah teks tombol
          $('#m_data_simpan_sertifikasiprofesi').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_sertifikasiprofesi').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/sertifikasiprofesi_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/sertifikasiprofesi_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajaxFileUpload({
              type: "POST",
              url : url,
              secureuri: true,
              fileElementId : 'SertifikasiProfesi_Sertifikat',
              dataType: "JSON",
              data : {SertifikasiProfesi_ID:id, Auditor_NIP:nip, SertifikasiProfesi_Nama:nama, SertifikasiProfesi_Lembaga:lembaga, SertifikasiProfesi_Gelar:gelar, SertifikasiProfesi_TglMulai:mulai, SertifikasiProfesi_TglAkhir:akhir, SertifikasiProfesi_NoSTMPL:nostmpl, SertifikasiProfesi_TglSTMPL:tglstmpl, SertifikasiProfesi_Sertifikat_Lama:filelama},
              success: function(data, status)
              {
                $('#modal_form_sertifikasiprofesi').modal('hide');
                $('#json_sertifikasiprofesi').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_sertifikasiprofesi').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_sertifikasiprofesi').attr('disabled',false);
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function(status)
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_sertifikasiprofesi').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_sertifikasiprofesi').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_sertifikasiprofesi').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_sertifikasiprofesi')[0].reset();
          // tampilkan modal
          $('#modal_form_sertifikasiprofesi').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_sertifikasiprofesi', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_sertifikasiprofesi')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/sertifikasiprofesi_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(SertifikasiProfesi_Nama, SertifikasiProfesi_Lembaga, SertifikasiProfesi_Gelar, SertifikasiProfesi_TglMulai, SertifikasiProfesi_TglAkhir, SertifikasiProfesi_NoSTMPL, SertifikasiProfesi_TglSTMPL, SertifikasiProfesi_Sertifikat )
                {
                  // tampilkan modal
                  $('#modal_form_sertifikasiprofesi').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].SertifikasiProfesi_ID);
                  $('[name="SertifikasiProfesi_Nama"]').val(data[0].SertifikasiProfesi_Nama);
                  $('[name="SertifikasiProfesi_Lembaga"]').val(data[0].SertifikasiProfesi_Lembaga);
                  $('[name="SertifikasiProfesi_Gelar"]').val(data[0].SertifikasiProfesi_Gelar);
                  $('[name="SertifikasiProfesi_TglMulai"]').datepicker('update', data[0].SertifikasiProfesi_TglMulai);
                  $('[name="SertifikasiProfesi_TglAkhir"]').datepicker('update', data[0].SertifikasiProfesi_TglAkhir);
                  $('[name="SertifikasiProfesi_NoSTMPL"]').val(data[0].SertifikasiProfesi_NoSTMPL);
                  $('[name="SertifikasiProfesi_TglSTMPL"]').datepicker('update', data[0].SertifikasiProfesi_TglSTMPL);
                  $('[name="SertifikasiProfesi_Sertifikat_Lama"]').val(data[0].SertifikasiProfesi_Sertifikat);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_sertifikasiprofesi', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/sertifikasiprofesi_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_sertifikasiprofesi').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      diklatsertifikasiprofesi();
    }
  };
}();
//-- END: dataTable remote diklatsertifikasiprofesi

//-- BEGIN: dataTable remote angkakredit
var DatatableJsonRemoteAngkaKredit = function () {

  //-- BEGIN: fungsi_data
  var angkakredit = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('auditor/angkakredit_detail')?>?id=<?php echo $this->input->get('id');?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "AngkaKredit_NoPAK",
        title: "Nomor PAK",
        width: 60,
        textAlign: 'center',
        sortable: false
      }, {
        field: "AngkaKredit_TglPAK",
        title: "Tanggal PAK",
        width: 160,
        sortable: 'desc'
      }, {
        field: "AngkaKredit_Nilai",
        title: "Nilai Angka Kredit",
        width: 80
      }, {
        field: "AngkaKredit_TglMulai",
        title: "Masa Penilaian",
        template: function (row) {
          // callback function support for column rendering
          var periode =
          '' + row.AngkaKredit_TglMulai + '<br> <span class="m--font-bold"> s/d </span> <br> ' + row.AngkaKredit_TglAkhir + '';
          return periode;
        },
        width: 80,
        textAlign: 'center'
      }, {
        field: "AngkaKredit_PAK",
        title: "File PAK",
        template: function (row) {
          // callback function support for column rendering
          var file = row.AngkaKredit_PAK == null ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.AngkaKredit_PAK + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="" title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        },
        width: 60,
        textAlign: 'center'
      }, {
        field: "Aksi",
        width: 110,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_ubah_angkakredit" data="'+row.AngkaKredit_ID+'" title="Ubah Data">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_angkakredit" data="'+row.AngkaKredit_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_angkakredit').mDatatable(tabel_data);
        var key = $('[name="AngkaKredit_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_angkakredit').on('click', function() {
          $('#json_angkakredit').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_angkakredit').on('click', function() {
          // variable untuk menyimpan nilai input
          var id         = key.val();
          var nip        = '<?php echo $this->input->get('id');?>';
          var nopak      = $('[name="AngkaKredit_NoPAK"]').val();
          var tglpak     = $('[name="AngkaKredit_TglPAK"]').val();
          var mulai      = $('[name="AngkaKredit_TglMulai"]').val();
          var akhir      = $('[name="AngkaKredit_TglAkhir"]').val();
          var nilai      = $('[name="AngkaKredit_Nilai"]').val();
          var file       = $('[name="AngkaKredit_PAK"]').val();
          var filelama   = $('[name="AngkaKredit_PAK_Lama"]').val();
          // ubah teks tombol
          $('#m_data_simpan_angkakredit').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_angkakredit').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('auditor/angkakredit_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('auditor/angkakredit_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajaxFileUpload({
              type: "POST",
              url : url,
              secureuri: true,
              fileElementId : 'AngkaKredit_PAK',
              dataType: "JSON",
              data : {AngkaKredit_ID:id, Auditor_NIP:nip, AngkaKredit_NoPAK:nopak, AngkaKredit_TglPAK:tglpak, AngkaKredit_TglMulai:mulai, AngkaKredit_TglAkhir:akhir, AngkaKredit_Nilai:nilai, AngkaKredit_PAK_Lama:filelama},
              success: function(data, status)
              {
                $('#modal_form_angkakredit').modal('hide');
                $('#json_angkakredit').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_angkakredit').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_angkakredit').attr('disabled',false);
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function(status)
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_angkakredit').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_angkakredit').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_angkakredit').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form_angkakredit')[0].reset();
          // tampilkan modal
          $('#modal_form_angkakredit').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_angkakredit', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_angkakredit')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('auditor/angkakredit_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                  $.each(data,function( AngkaKredit_Nilai, AngkaKredit_NoPAK, AngkaKredit_TglPAK, AngkaKredit_TglMulai, AngkaKredit_TglAkhir, AngkaKredit_PAK)
                {
                  // tampilkan modal
                  $('#modal_form_angkakredit').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].AngkaKredit_ID);
                  $('[name="AngkaKredit_Nilai"]').val(data[0].AngkaKredit_Nilai);
                  $('[name="AngkaKredit_NoPAK"]').val(data[0].AngkaKredit_NoPAK);
                  $('[name="AngkaKredit_TglPAK"]').datepicker('update',data[0].AngkaKredit_TglPAK);
                  $('[name="AngkaKredit_TglMulai"]').datepicker('update',data[0].AngkaKredit_TglMulai);
                  $('[name="AngkaKredit_TglAkhir"]').datepicker('update',data[0].AngkaKredit_TglAkhir);
                  $('[name="AngkaKredit_PAK_Lama"]').val(data[0].AngkaKredit_PAK);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_angkakredit', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('auditor/angkakredit_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_angkakredit').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      angkakredit();
    }
  };
}();
//-- END: dataTable remote angkakredit

var MaxlengthCharacters = function () {

    //== Private functions
    var data = function () {
        // tahun 4 karakter
        $('.m_maxlength_tahun').maxlength({
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--info m-badge--rounded m-badge--wide",
            placement: 'bottom-left',
            appendToParent: true
        });

        // teks 50 karakter
        $('.m_maxlength_50').maxlength({
            warningClass: "m-badge m-badge--info m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--warning m-badge--rounded m-badge--wide",
            placement: 'bottom-left',
            appendToParent: true
        });

        // teks 10 karakter
        $('.m_maxlength_10').maxlength({
            warningClass: "m-badge m-badge--info m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--warning m-badge--rounded m-badge--wide",
            placement: 'bottom-left',
            appendToParent: true
        });

        // always show
        $('#m_maxlength_3').maxlength({
            alwaysShow: true,
            threshold: 5,
            warningClass: "m-badge m-badge--primary m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        // custom text
        $('#m_maxlength_4').maxlength({
            threshold: 3,
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            separator: ' of ',
            preText: 'You have ',
            postText: ' chars remaining.',
            validate: true
        });

        // textarea example
        $('#m_maxlength_5').maxlength({
            threshold: 5,
            warningClass: "m-badge m-badge--primary m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        // position examples
        $('#m_maxlength_6_1').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'top-left',
            warningClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        $('#m_maxlength_6_2').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'top-right',
            warningClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        $('#m_maxlength_6_3').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'bottom-left',
            warningClass: "m-badge m-badge--warning m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        $('#m_maxlength_6_4').maxlength({
            alwaysShow: true,
            threshold: 5,
            placement: 'bottom-right',
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide"
        });

        //== Modal Examples

        // minimum setup
        $('#m_maxlength_1_modal').maxlength({
            warningClass: "m-badge m-badge--warning m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            appendToParent: true
        });

        // threshold value
        $('#m_maxlength_2_modal').maxlength({
            threshold: 5,
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            appendToParent: true
        });

        // always show
        // textarea example
        $('#m_maxlength_5_modal').maxlength({
            threshold: 5,
            warningClass: "m-badge m-badge--primary m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--brand m-badge--rounded m-badge--wide",
            appendToParent: true
        });

        // custom text
        $('#m_maxlength_4_modal').maxlength({
            threshold: 3,
            warningClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--success m-badge--rounded m-badge--wide",
            appendToParent: true,
            separator: ' of ',
            preText: 'You have ',
            postText: ' chars remaining.',
            validate: true
        });
    }

    return {
        // public functions
        init: function() {
            data();
        }
    };
}();

jQuery(document).ready(function () {
  // DatatableJsonRemoteUnitKerja.init();
  DatatableJsonRemotePendidikan.init();
  DatatableJsonRemotePangkat.init();
  DatatableJsonRemoteJabatan.init();
  DatatableJsonRemoteDiklatJFA.init();
  DatatableJsonRemoteDiklatSubstantif.init();
  DatatableJsonRemoteDiklatProfesi.init();
  DatatableJsonRemoteAngkaKredit.init();
  MaxlengthCharacters.init();
 });

 $('.m_datepicker_atas').datepicker({
     orientation: "top left",
     todayHighlight: true,
     changeMonth: true,
     changeYear: true,
     autoclose: true,
     format: "dd/mm/yyyy",
     templates: {
         leftArrow: '<i class="la la-angle-left"></i>',
         rightArrow: '<i class="la la-angle-right"></i>'
     }
 });

 // Dropzone.options.mDropzoneOne = {
 //     paramName: "file", // The name that will be used to transfer the file
 //     maxFiles: 1,
 //     maxFilesize: 5, // MB
 //     addRemoveLinks: true,
 //     accept: function(file, done) {
 //         if (file.name == "justinbieber.jpg") {
 //             done("Naha, you don't.");
 //         } else {
 //             done();
 //         }
 //     }
 // };
</script>
