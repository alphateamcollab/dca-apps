<!-- begin::Content -->
<div class="m-content">
  <div class="row"><div class="col-xl-12 col-lg-12">
      <div class="m-portlet m-portlet--tabs m-portlet--info m-portlet--head-solid-bg m-portlet--bordered m-portlet--head-sm">
          <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
              <ul class="nav nav-tabs m-tabs-line" role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active" data-toggle="tab" href="#auditor" role="tab">
                    <i class="flaticon-puzzle"></i>
                    Auditor Baru
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#pangkat" role="tab">
                    <i class="flaticon-map"></i>
                    Riwayat Pangkat & Gol.
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#jabatan" role="tab">
                    <i class="flaticon-suitcase"></i>
                    Riwayat Jabatan
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#pendidikan" role="tab">
                    <i class="flaticon-list-1"></i>
                    Riwayat Pendidikan
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="m-portlet__body">
            <div class="tab-content">
              <div class="tab-pane active" id="auditor" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_auditor">
                          <i class="la la-refresh"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Auditor Baru
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_auditor"></div>
                  <!--end: Datatable -->
              </div>
              <div class="tab-pane" id="pangkat" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_pangkat">
                          <i class="la la-refresh"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Pangkat & Golongan
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_pangkat"></div>
                  <!--end: Datatable -->
              </div>
              <div class="tab-pane" id="jabatan" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_jabatan">
                          <i class="la la-refresh"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Jabatan
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_jabatan"></div>
                  <!--end: Datatable -->
              </div>
              <div class="tab-pane" id="pendidikan" role="tabpanel">
                  <div class="m-portlet__head-caption">
                    <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                        <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Perbaharui" id="m_datatable_reload_pendidikan">
                          <i class="la la-refresh"></i>
                        </button>
                    </ul>
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                      </span>
                      <h4 class="m-portlet__head-text">
                        Riwayat Pendidikan
                      </h4>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!--begin: Datatable -->
                   <div class="m_datatable" id="json_pendidikan"></div>
                  <!--end: Datatable -->
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<script>
/* Ajax Dropdown Instansi */
function getInstansi(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('persetujuan/get_instansi');?>",
    data: {value},
    success: function(data) {
      $("#KodeUnitKerja option:gt(0)").remove();
      $("#KodeInstansi").html(data);
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
};

/* Ajax Dropdown Unit Kerja */
function getUnitKerja(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('persetujuan/get_unitkerja');?>",
    data:{value},
    success: function(data) {
      $("#KodeUnitKerja").html(data);
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
}

/* Ajax Dropdown Prestasi Nilai */
function getPangkat(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('persetujuan/get_pangkat');?>",
    data: {value},
    success: function(data) {
      document.getElementById('pangkatgol').value = data;
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
}

/* Ajax Dropdown Jabatan */
function getJenjangJabatan(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('persetujuan/get_jenjangjabatan');?>",
    data: {value},
    success: function(data) {
      $("#jenjangjabatan").html(data);
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
};

//-- BEGIN: load data Ringkasan Profil & UnitKerja
$(document).ready(function(){
  // getProfil();
});

//-- BEGIN: variabel global
var id;
//-- END: variabel global

//-- BEGIN: dataTable remote jabatan
var DatatableJsonRemotePangkat = function () {

  //-- BEGIN: fungsi_data
  var pangkat = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('persetujuan/pangkat_data')?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
			columns: [
      {
        field: "Auditor_NIP",
        title: "NIP / NRP",
        sortable: false
      }, {
          field: "NamaLengkap",
          title: "Nama Lengkap",
          width: 200,
          sortable: 'asc',
      }, {
        field: "Pangkat_Nama",
        title: "Pangkat"
      }, {
        field: "GolRuang_Kode",
        title: "Golongan Ruang",
        textAlign: 'center',
        width: 80
      }, {
        field: "Pangkat_NoSuratKeputusan",
        title: "Nomor Surat Keputusan",
        width: 160
      },{
        field: "Pangkat_TglSuratKeputusan",
        title: "Tanggal Surat",
        width: 80,
        textAlign: 'center'
      }, {
        field: "Pangkat_TglMulaiTugas",
        title: "TMT Pangkat",
        width: 80,
        textAlign: 'center',
        sortable: 'desc'
      }, {
        field: "Pangkat_SK",
        title: "File Surat",
        width: 60,
        textAlign: 'center',
        template: function (row) {
          // callback function support for column rendering
          var file = row.Pangkat_SK == null ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.Pangkat_SK + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="" title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        }
      }, {
        field: "Aksi",
        width: 80,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m_data_persetujuan_pangkat" data="'+row.PangkatGol_ID+'" title="Terima Persetujuan">\
              <i class="la la-check-circle"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_penolakan_pangkat" data="'+row.PangkatGol_ID+'" title="Tolak Persetujuan">\
              <i class="la la-times-circle"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_pangkat" data="'+row.PangkatGol_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_pangkat').mDatatable(tabel_data);
        var key = $('[name="PangkatGol_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_pangkat').on('click', function() {
          $('#json_pangkat').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_pangkat', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_pangkat')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('persetujuan/pangkat_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(PangkatGol_ID, Auditor_NIP, Pangkat_Kode, Pangkat_Nama, Pangkat_NoSuratKeputusan, Pangkat_TglSuratKeputusan, Pangkat_TglMulaiTugas, Pangkat_Sk)
                {
                  // tampilkan modal
                  $('#modal_form_pangkat').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Pangkat');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].PangkatGol_ID);
                  $('[name="Auditor_NIP"]').val(data[0].Auditor_NIP);
                  $('[name="Pangkat_Kode"]').val(data[0].Pangkat_Kode);
                  $('[name="Pangkat_Nama"]').val(data[0].Pangkat_Nama);
                  $('[name="Pangkat_NoSuratKeputusan"]').val(data[0].Pangkat_NoSuratKeputusan);
                  $('[name="Pangkat_TglSuratKeputusan"]').datepicker('update', data[0].Pangkat_TglSuratKeputusan);
                  $('[name="Pangkat_TglMulaiTugas"]').datepicker('update', data[0].Pangkat_TglMulaiTugas);
                  $('[name="Pangkat_Sk"]').val(data[0].Pangkat_Sk);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_pangkat', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/pangkat_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_pangkat').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // panggil fungsi getProfil
                getProfil();
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        //-- BEGIN: fungsi persetujuan data pangkat
        $(document).on('click','.m_data_persetujuan_pangkat', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          var status=$(this).attr('status');
          swal({
              title: 'Apakah anda yakin?',
              text: "Status persetujuan akan diubah!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, lanjutkan!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/pangkat_setuju')?>",
                  dataType : "JSON",
                    data : {id: id, status: status},
                    success: function(data){
                      $('#json_pangkat').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Status Berubah!',
                    text: "Persetujuan berhasil diubah.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses ubah persetujuan dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi persetujuan data pangkat

        //-- BEGIN: fungsi penolakan data pangkat
        $(document).on('click','.m_data_penolakan_pangkat', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          var status=$(this).attr('status');
          swal({
              title: 'Apakah anda yakin untuk menolak?',
              text: "Status persetujuan akan diubah!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, lanjutkan!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/pangkat_tolak')?>",
                  dataType : "JSON",
                    data : {id: id, status: status},
                    success: function(data){
                      $('#json_pangkat').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Status Berubah!',
                    text: "Persetujuan berhasil diubah.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses ubah persetujuan dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi penolakan data pangkat

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      pangkat();
    }
  };
}();
//-- END: dataTable remote pangkat

//-- BEGIN: dataTable remote jabatan
var DatatableJsonRemoteJabatan = function () {

  //-- BEGIN: fungsi_data
  var jabatan = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('persetujuan/jabatan_data')?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Auditor_NIP",
        title: "NIP / NRP",
        sortable: false
      }, {
          field: "NamaLengkap",
          title: "Nama Lengkap",
          width: 200,
          sortable: 'asc',
      }, {
        field: "Jabatan_Nama",
        title: "Jenjang Jabatan",
        width: 120,
        textAlign: 'center',
        sortable: false
      }, {
        field: "JenjangJabatan_Nama",
        title: "Jabatan",
        width: 80
      }, {
        field: "Jabatan_NoSuratKeputusan",
        title: "Nomor Surat Keputusan",
        width: 160
      }, {
        field: "Jabatan_TglSuratKeputusan",
        title: "Tanggal Surat",
        textAlign: 'center',
        width: 80
      }, {
        field: "Jabatan_TglMulaiTugas",
        title: "TMT Jabatan",
        textAlign: 'center',
        width: 80,
        sortable: 'desc'
      }, {
        field: "Jabatan_Sk",
        title: "File SK Jabatan",
        width: 60,
        textAlign: 'center',
        template: function (row) {
          // callback function support for column rendering
          var file = row.Jabatan_SK == null ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.Jabatan_SK + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="" title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        }
      }, {
        field: "Aksi",
        width: 80,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m_data_persetujuan_jabatan" data="'+row.Jabatan_ID+'" title="Terima Persetujuan">\
              <i class="la la-check-circle"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_penolakan_jabatan" data="'+row.Jabatan_ID+'" title="Tolak Persetujuan">\
              <i class="la la-times-circle"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_jabatan" data="'+row.Jabatan_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_jabatan').mDatatable(tabel_data);
        var key = $('[name="Jabatan_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_jabatan').on('click', function() {
          $('#json_jabatan').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi simpan data
        $('#m_data_simpan_jabatan').on('click', function() {
          // variable untuk menyimpan nilai input
          var id            = key.val();
          var nip           = '<?php echo $this->input->get('id');?>';
          var kelompokjab   = $('[name="Jabatan_Kode"]').val();
          var jabatan       = $('[name="JenjangJabatan_Kode"]').val();
          var nomor         = $('[name="Jabatan_NoSuratKeputusan"]').val();
          var tgl           = $('[name="Jabatan_TglSuratKeputusan"]').val();
          var tmt           = $('[name="Jabatan_TglMulaiTugas"]').val();
          var sk            = $('[name="Jabatan_Sk"]').val();
          // ubah teks tombol
          $('#m_data_simpan_jabatan').text('Menyimpan...');
          // nonaktifkan tombol
          $('#m_data_simpan_jabatan').attr('disabled',true);
          // variable untuk menyimpan url ajax
          var url;
          if(metode_simpan == 'tambah') {
              url = "<?php echo site_url('persetujuan/jabatan_tambah')?>";
          } else if (metode_simpan == 'ubah') {
              url = "<?php echo site_url('persetujuan/jabatan_ubah')?>";
          }
          // menambahkan data ke ajax dengan ajax
          $.ajax({
              type: "POST",
              url : url,
              dataType: "JSON",
              data : {Jabatan_ID:id, Auditor_NIP:nip, Jabatan_Kode:kelompokjab, JenjangJabatan_Kode:jabatan, Jabatan_NoSuratKeputusan:nomor, Jabatan_TglSuratKeputusan:tgl, Jabatan_TglMulaiTugas:tmt, Jabatan_Sk:sk},
              success: function(data)
              {
                $('#modal_form_jabatan').modal('hide');
                $('#json_jabatan').mDatatable('reload');
                // ubah teks tombol
                $('#m_data_simpan_jabatan').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_jabatan').attr('disabled',false);
                // panggil fungsi getProfil
                getProfil();
                // pesan penambahan data berhasil
                swal({
                    title: 'Berhasil!',
                    text: "Data telah tersimpan.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              },
              error: function ()
              {
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
                // alert('Error adding / update data');
                // ubah teks tombol
                $('#m_data_simpan_jabatan').text('Simpan');
                // aktifkan tombol
                $('#m_data_simpan_jabatan').attr('disabled',false);
              }
          });
          return false;
        });
        //-- END: fungsi simpan data

        //-- BEGIN: fungsi tambah data
        $('#m_data_tambah_jabatan').on('click', function() {
          metode_simpan = 'tambah';
          // aktifkan inputan primary key / foreign key
          key.attr('disabled',false);
          // reset isi form di modal
          $('#form')[0].reset();
          // tampilkan modal
          $('#modal_form_jabatan').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Tambah Data Jabatan Baru');
        });
        //-- END: fungsi tambah data

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_jabatan', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('persetujuan/jabatan_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(Jabatan_Kode, JenjangJabatan_Kode, Jabatan_NoSuratKeputusan, Jabatan_TglSuratKeputusan, Jabatan_TglMulaiTugas, Jabatan_Sk)
                {
                  // tampilkan modal
                  $('#modal_form_jabatan').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Jabatan');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].Jabatan_ID);
                  $('[name="Jabatan_Kode"]').val(data[0].Jabatan_Kode);
                  $('[name="JenjangJabatan_Kode"]').val(data[0].JenjangJabatan_Kode);
                  $('[name="Jabatan_NoSuratKeputusan"]').val(data[0].Jabatan_NoSuratKeputusan);
                  $('[name="Jabatan_TglSuratKeputusan"]').datepicker('update', data[0].Jabatan_TglSuratKeputusan);
                  $('[name="Jabatan_TglMulaiTugas"]').datepicker('update', data[0].Jabatan_TglMulaiTugas);
                  $('[name="Jabatan_Sk"]').val(data[0].Jabatan_Sk);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_jabatan', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/jabatan_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_jabatan').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // panggil fungsi getProfil
                getProfil();
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        //-- BEGIN: fungsi persetujuan data jabatan
        $(document).on('click','.m_data_persetujuan_jabatan', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          var status=$(this).attr('status');
          swal({
              title: 'Apakah anda yakin?',
              text: "Status persetujuan akan diubah!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, lanjutkan!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/jabatan_setuju')?>",
                  dataType : "JSON",
                    data : {id: id, status: status},
                    success: function(data){
                      $('#json_jabatan').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Status Berubah!',
                    text: "Persetujuan berhasil diubah.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses ubah persetujuan dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi persetujuan data jabatan

        //-- BEGIN: fungsi penolakan data jabatan
        $(document).on('click','.m_data_penolakan_jabatan', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          var status=$(this).attr('status');
          swal({
              title: 'Apakah anda yakin untuk menolak?',
              text: "Status persetujuan akan diubah!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, lanjutkan!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/jabatan_tolak')?>",
                  dataType : "JSON",
                    data : {id: id, status: status},
                    success: function(data){
                      $('#json_jabatan').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Status Berubah!',
                    text: "Persetujuan berhasil diubah.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses ubah persetujuan dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi penolakan data jabatan

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      jabatan();
    }
  };
}();
//-- END: dataTable remote jabatan

//-- BEGIN: dataTable remote auditor
var DatatableJsonRemoteAuditor = function () {

  //-- BEGIN: fungsi_data
  var auditor = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('persetujuan/auditor_data')?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Auditor_NIP",
        title: "NIP / NRP",
        sortable: false
      }, {
        field: "Auditor_NamaLengkap",
        title: "Nama Lengkap",
        width: 160,
        sortable: false
      }, {
        field: "Auditor_TempatLlahir",
        title: "Tempat Lahir",
        width: 160,
        sortable: 'desc'
      }, {
        field: "Auditor_TglLahir",
        title: "Tanggal Lahir",
        width: 80
      }, {
        field: "Aksi",
        width: 110,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m_data_persetujuan_auditor" data="'+row.Auditor_ID+'" title="Terima Persetujuan">\
              <i class="la la-check-circle"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_penolakan_auditor" data="'+row.Auditor_ID+'" title="Tolak Persetujuan">\
              <i class="la la-times-circle"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_auditor" data="'+row.Auditor_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        }
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_auditor').mDatatable(tabel_data);
        var key = $('[name="AngkaKredit_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_auditor').on('click', function() {
          $('#json_auditor').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_auditor', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_auditor')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('persetujuan/auditor_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                  $.each(data,function( AngkaKredit_Nilai, AngkaKredit_NoPAK, AngkaKredit_TglPAK, AngkaKredit_TglMulai, AngkaKredit_TglAkhir, AngkaKredit_PAK)
                {
                  // tampilkan modal
                  $('#modal_form_auditor').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].AngkaKredit_ID);
                  $('[name="AngkaKredit_Nilai"]').val(data[0].AngkaKredit_Nilai);
                  $('[name="AngkaKredit_NoPAK"]').val(data[0].AngkaKredit_NoPAK);
                  $('[name="AngkaKredit_TglPAK"]').datepicker('update',data[0].AngkaKredit_TglPAK);
                  $('[name="AngkaKredit_TglMulai"]').datepicker('update',data[0].AngkaKredit_TglMulai);
                  $('[name="AngkaKredit_TglAkhir"]').datepicker('update',data[0].AngkaKredit_TglAkhir);
                  $('[name="AngkaKredit_PAK"]').val(data[0].AngkaKredit_PAK);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_auditor', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/auditor_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_auditor').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        //-- BEGIN: fungsi persetujuan data auditor
        $(document).on('click','.m_data_persetujuan_auditor', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          var status=$(this).attr('status');
          swal({
              title: 'Apakah anda yakin?',
              text: "Status persetujuan akan diubah!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, lanjutkan!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/auditor_setuju')?>",
                  dataType : "JSON",
                    data : {id: id, status: status},
                    success: function(data){
                      $('#json_auditor').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Status Berubah!',
                    text: "Persetujuan berhasil diubah.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses ubah persetujuan dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi persetujuan data auditor

        //-- BEGIN: fungsi penolakan data auditor
        $(document).on('click','.m_data_penolakan_auditor', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          var status=$(this).attr('status');
          swal({
              title: 'Apakah anda yakin untuk menolak?',
              text: "Status persetujuan akan diubah!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, lanjutkan!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/auditor_tolak')?>",
                  dataType : "JSON",
                    data : {id: id, status: status},
                    success: function(data){
                      $('#json_auditor').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Status Berubah!',
                    text: "Persetujuan berhasil diubah.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses ubah persetujuan dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi penolakan data jabatan

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      auditor();
    }
  };
}();
//-- END: dataTable remote auditor


//-- BEGIN: dataTable remote pendidikan
var DatatableJsonRemotePendidikan = function () {

  //-- BEGIN: fungsi_data
  var pendidikan = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('persetujuan/pendidikan_data')?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
      {
        field: "Auditor_NIP",
        title: "NIP / NRP",
        sortable: false
      }, {
          field: "NamaLengkap",
          title: "Nama Lengkap",
          width: 200,
          sortable: 'asc',
      }, {
        field: "Pendidikan_Jenjang",
        title: "Jenjang",
        sortable: false
      }, {
        field: "Pendidikan_Lembaga",
        title: "Lembaga Pendidikan",
        width: 120,
        textAlign: 'center',
        sortable: false
      }, {
        field: "Pendidikan_Jurusan",
        title: "Jurusan",
        sortable: false
      }, {
        field: "Pendidikan_TglIjazah",
        title: "Tgl Terbit Ijazah",
        width: 80,
        textAlign: 'center',
        sortable: 'desc'
      }, {
        field: "Pendidikan_Ijazah",
        title: "File Ijazah",
        width: 60,
        textAlign: 'center',
        template: function (row) {
          // callback function support for column rendering
          var file = row.Pendidikan_Ijazah == '' ?
          '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
          '<a target="_blank" href="<?php echo base_url() ?>' + row.Pendidikan_Ijazah + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="'+row.Pendidikan_Ijazah+'" title="Lihat File">\
            <i class="flaticon-attachment"></i>\
          </a>';
          return file;
        }
      }, {
        field: "Aksi",
        width: 120,
        title: "Aksi",
        textAlign: 'center',
        sortable: false,
        overflow: 'visible',
        template: function (row, index, datatable) {
          return '\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill m_data_persetujuan_pendidikan" data="'+row.Pendidikan_ID+'" title="Terima Persetujuan">\
              <i class="la la-check-circle"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_penolakan_pendidikan" data="'+row.Pendidikan_ID+'" title="Tolak Persetujuan">\
              <i class="la la-times-circle"></i>\
            </a>\
            <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus_pendidikan" data="'+row.Pendidikan_ID+'" title="Hapus Data">\
              <i class="la la-trash"></i>\
            </a>\
          ';
        },
      }],
      //-- END: columns definition

    };
    //-- END: tabel data

        /*
        | -------------------------------------------------------------------------
        | BOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        | # Reload Table
        | # Save Data:
        |   - Create Data
        |   - Update Data
        | # Delete Data
        */

        //-- BEGIN: variabel global
        var datatable = $('#json_pendidikan').mDatatable(tabel_data);
        var key = $('[name="Pendidikan_ID"]');
        var metode_simpan;
        var id;
        //-- END: variabel global

        //-- BEGIN: fungsi refresh mDatatable
        $('#m_datatable_reload_pendidikan').on('click', function() {
          $('#json_pendidikan').mDatatable('reload');
        });
        //-- END: fungsi refresh mDatatable

        //-- BEGIN: fungsi ubah data
        $(document).on('click','.m_data_ubah_pendidikan', function() {
          metode_simpan = 'ubah';
          var id=$(this).attr('data');
          // reset isi form di modal
          $('#form_pendidikan')[0].reset();
          // memuat data dengan ajax
          $.ajax({
              type: "GET",
              url : "<?php echo site_url('persetujuan/pendidikan_ambil')?>",
              dataType: "JSON",
              data : {id:id},
              success: function(data)
              {
                $.each(data,function(Pendidikan_Kode, Pendidikan_Lembaga, Pendidikan_Fakultas, Pendidikan_Jurusan, Pendidikan_GelarDepan, Pendidikan_GelarBelakang, Pendidikan_NoIjazah, Pendidikan_TglIjazah, Pendidikan_Ijazah)
                {
                  // tampilkan modal
                  $('#modal_form_pendidikan').modal('show');
                  // menetapkan judul di modal
                  $('.modal-title').text('Ubah Data Pendidikan');
                  // nonaktifkan inputan primary key / foreign key
                  key.attr('disabled',true);
                  // ambil nilai input dari parameter JSON
                  key.val(data[0].Pendidikan_ID);
                  $('[name="Pendidikan_Kode"]').val(data[0].Pendidikan_Kode);
                  $('[name="Pendidikan_Jenjang"]').val(data[0].Pendidikan_Jenjang);
                  $('[name="Pendidikan_Lembaga"]').val(data[0].Pendidikan_Lembaga);
                  $('[name="Pendidikan_Fakultas"]').val(data[0].Pendidikan_Fakultas);
                  $('[name="Pendidikan_Jurusan"]').val(data[0].Pendidikan_Jurusan);
                  $('[name="Pendidikan_GelarDepan"]').val(data[0].Pendidikan_GelarDepan);
                  $('[name="Pendidikan_GelarBelakang"]').val(data[0].Pendidikan_GelarBelakang);
                  $('[name="Pendidikan_NoIjazah"]').val(data[0].Pendidikan_NoIjazah);
                  $('[name="Pendidikan_TglIjazah"]').datepicker('update',data[0].Pendidikan_TglIjazah);
                  $('[name="Pendidikan_Ijazah"]').val(data[0].Pendidikan_Ijazah);
                  // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
                });
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                // alert('Error get data from ajax');
                swal({
                    title: 'Proses Simpan Gagal!',
                    text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                    type: 'error',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    animation: false,
                    customClass: 'animated bounceIn'
                });
              }
          });
          return false;
        });
        //-- END: fungsi ubah data

        //-- BEGIN: fungsi hapus data
        $(document).on('click','.m_data_hapus_pendidikan', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          swal({
              title: 'Apakah anda yakin?',
              text: "Data yang telah dihapus tidak dapat dikembalikan!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/pendidikan_hapus')?>",
                  dataType : "JSON",
                    data : {id: id},
                    success: function(data){
                      $('#json_pendidikan').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Dihapus!',
                    text: "Data telah dihapus.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // panggil fungsi getProfil
                getProfil();
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses hapus dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi hapus data

        //-- BEGIN: fungsi persetujuan data pendidikan
        $(document).on('click','.m_data_persetujuan_pendidikan', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          var status=$(this).attr('status');
          swal({
              title: 'Apakah anda yakin?',
              text: "Status persetujuan akan diubah!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, lanjutkan!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/pendidikan_setuju')?>",
                  dataType : "JSON",
                    data : {id: id, status: status},
                    success: function(data){
                      $('.m_datatable').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Status Berubah!',
                    text: "Persetujuan berhasil diubah.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses ubah persetujuan dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi persetujuan data pendidikan

        //-- BEGIN: fungsi penolakan data pendidikan
        $(document).on('click','.m_data_penolakan_pendidikan', function() {
          // variable id untuk menyimpan nilai primary key atau foreign key,
          // nilai didapat dari action button
          var id=$(this).attr('data');
          var status=$(this).attr('status');
          swal({
              title: 'Apakah anda yakin untuk menolak?',
              text: "Status persetujuan akan diubah!",
              type: 'question',
              showCancelButton: true,
              confirmButtonText: "<span><i class='la la-check'></i><span>Ya, lanjutkan!</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
              cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
              cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
              reverseButtons: true,
              animation: false,
              customClass: 'animated bounceIn'
          }).then(function(result){
              if (result.value) {
                // memuat data dengan ajax
                $.ajax({
                  type : "POST",
                  url  : "<?php echo base_url('persetujuan/pendidikan_tolak')?>",
                  dataType : "JSON",
                    data : {id: id, status: status},
                    success: function(data){
                      $('.m_datatable').mDatatable('reload');
                    }
                });
                swal({
                    title: 'Status Berubah!',
                    text: "Persetujuan berhasil diubah.",
                    type: 'success',
                    confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                    confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                    customClass: 'animated bounceIn'
                })
                // result.dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
              } else if (result.dismiss === 'cancel') {
                  swal({
                      title: 'Dibatalkan',
                      text: "Proses ubah persetujuan dibatalkan",
                      type: 'error',
                      confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                      customClass: 'animated bounceIn'
                  }
                  )
              }
          });
          return false;
        });
        //-- END:  fungsi penolakan data pendidikan

        /*
        | -------------------------------------------------------------------------
        | EOF: ACTION BUTTON DEFINITION
        | -------------------------------------------------------------------------
        */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      pendidikan();
    }
  };
}();
//-- END: dataTable remote pendidikan

jQuery(document).ready(function () {
  // DatatableJsonRemoteUnitKerja.init();
  DatatableJsonRemotePangkat.init();
  DatatableJsonRemoteJabatan.init();
  DatatableJsonRemoteAuditor.init();
  DatatableJsonRemotePendidikan.init();
 });

 $('.m_datepicker_atas').datepicker({
     orientation: "top left",
     todayHighlight: true,
     changeMonth: true,
     changeYear: true,
     autoclose: true,
     format: "dd/mm/yyyy",
     templates: {
         leftArrow: '<i class="la la-angle-left"></i>',
         rightArrow: '<i class="la la-angle-right"></i>'
     }
 });

 Dropzone.options.mDropzoneOne = {
     paramName: "file", // The name that will be used to transfer the file
     maxFiles: 1,
     maxFilesize: 5, // MB
     addRemoveLinks: true,
     accept: function(file, done) {
         if (file.name == "justinbieber.jpg") {
             done("Naha, you don't.");
         } else {
             done();
         }
     }
 };
</script>
