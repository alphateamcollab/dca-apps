<div class="m-content">
  <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            Tabel Riwayat Diklat Teknis Substantif
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href=""  data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" id="m_datatable_reload">
              <i class="la la-refresh"></i>
            </a>
          </li>
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
              <i class="la la-expand"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="m-portlet__body">
      <!--begin: Search Form -->
      <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <div class="row align-items-center">
          <div class="col-xl-8 order-2 order-xl-1">
            <div class="form-group m-form__group row align-items-center">
              <div class="col-md-4">
                <div class="m-input-icon m-input-icon--left">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Cari..." id="generalSearch">
                  <span class="m-input-icon__icon m-input-icon__icon--left">
                    <span>
                      <i class="la la-search"></i>
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--end: Search Form -->

      <!--begin: Datatable -->
      <div class="m_datatable" id="json_data"></div>
      <!--end: Datatable -->
    </div>
  </div>
</div>

<script>
//-- BEGIN: dataTable remote
var DatatableJsonRemoteDiklatTeknisSubstantif = function () {

  //-- BEGIN: fungsi_data
  var diklatteknissubstantif = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('diklatteknissubstantif/data')?>',
          },
        },
        pageSize: 5,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

          //-- BEGIN: columns definition
          columns: [
          {
            field: "Diklat_Nama",
            title: "Nama Diklat",
            // width: 180,
            // textAlign: 'center',
            sortable: false
          }, {
            field: "TeknisSubstantif_TglMulai",
            title: "Periode Diklat",
            template: function (row) {
              // callback function support for column rendering
              var periode =
              '' + row.TeknisSubstantif_TglMulai + ' <span class="m--font-bold"> s/d </span>  ' + row.TeknisSubstantif_TglAkhir + '';
              return periode;
            },
            // width: 80,
            textAlign: 'center'
          }, {
            field: "TeknisSubstantif_NoSTMPL",
            title: "Tgl. & No. STMPL",
            width: 140,
            textAlign: 'center',
            sortable: 'desc',
            template: function (row) {
              // callback function support for column rendering
              var stmpl =
              '<span class="m--font-bold">Tgl.: </span>' + row.TeknisSubstantif_TglSTMPL + '<br>\
              <span class="m--font-bold">No.: </span>' + row.TeknisSubstantif_NoSTMPL + '';
              return stmpl;
            }
          }, {
            field: "TeknisSubstantif_Sertifikat",
            title: "File Sertifikat",
            template: function (row) {
              // callback function support for column rendering
              var file = row.TeknisSubstantif_Sertifikat == '' ?
              '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
              '<a target="_blank" href="<?php echo base_url() ?>' + row.TeknisSubstantif_Sertifikat + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"   title="Lihat File">\
                <i class="flaticon-attachment"></i>\
              </a>';
              return file;
            },
            width: 60,
            textAlign: 'center'
          }],
        //-- END: columns definition

    };

    //-- BEGIN: variabel global
    var datatable = $('.m_datatable').mDatatable(tabel_data);
    //-- END: variabel global

      //-- BEGIN: fungsi refresh mDatatable
      $('#m_datatable_reload').on('click', function() {
        $('.m_datatable').mDatatable('reload');
      });
      //-- END: fungsi refresh mDatatable
    };
    //-- END: fungsi_data

    return {
      // public functions
      init: function () {
        diklatteknissubstantif();
      }
    };
    }();
    //-- END: dataTable remote

//-- BEGIN: jQuery init
jQuery(document).ready(function () {
DatatableJsonRemoteDiklatTeknisSubstantif.init();
});
//-- END: jQuery init
</script>
