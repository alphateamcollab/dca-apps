<div class="m-content">
  <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            Tabel Riwayat Angka Kredit
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href=""  data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" id="m_datatable_reload">
              <i class="la la-refresh"></i>
            </a>
          </li>
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
              <i class="la la-expand"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="m-portlet__body">
      <!--begin: Search Form -->
      <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <div class="row align-items-center">
          <div class="col-xl-8 order-2 order-xl-1">
            <div class="form-group m-form__group row align-items-center">
              <div class="col-md-4">
                <div class="m-input-icon m-input-icon--left">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Cari..." id="generalSearch">
                  <span class="m-input-icon__icon m-input-icon__icon--left">
                    <span>
                      <i class="la la-search"></i>
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--end: Search Form -->

  <!--begin: Datatable -->
  <div class="m_datatable" id="json_data"></div>
      <!--end: Datatable -->
    </div>
  </div>
</div>
<!-- modal form -->
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Tambah Data Baru
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  NIP / NRP
                </label>
                <div class="col-md-7">
                <input type="number" class="m-input--pill form-control m-input" readonly="" placeholder="000000000000000000" name="Auditor_NIP">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nama Lengkap
                </label>
                <div class="col-md-7"m-form__group row">
                    <label for="example-text-input" class="col-md-5 col-form-label">
                      Periode Penilaian
                    </label>
                        <div class="col>
                  <input class="m-input--pill form-control m-input" type="text" readonly="" placeholder="Nama Lengkap Serta Gelar" name="Auditor_NamaLengkap">
                </div>
              </div>
              <div class="form-group m-form__group row">
                        <input type="datepicker" class="m-input--pill form-control m-input" readonly""  placeholder="Tanggal Mulai " name="AngkaKrededit_PenilaianSejak" id="m_datepicker_atas_1"/>
                      </div>
                      <label class="col-form-label col-md-1">
                          S/D
                      </label>
                      <div class="col-md-3">
                        <input type="datepicker" class="m-input--pill form-control m-input" readonly""  placeholder="Tanggal Akhir " name="AngkaKredit_PenilaianSampai" id="m_datepicker_atas_2"/>
                      </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nilai Angka Kredit
                </label>
                <div class="col-md-7">
                  <input class="m-input--pill form-control m-input" type="number" placeholder="Nilai Angka Kredit" name="AngkaKredit_Nilai" >
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Nomor PAK
                </label>
                <div class="col-md-7">
                  <input class="m-input--pill form-control m-input" type="text" placeholder="Nomor SK" name="AngkaKredit_NoSKPak" >
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                    Tanggal PAK
                </label>
                <div class="col-md-7">
                    <input type="datepicker" class="m-input--pill form-control m-input" readonly="" placeholder="hh/bb/tttt" name="AngkaKredit_TglSKPak" id="m_datepicker_atas_3">
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-md-5 col-form-label">
                  Unggah File SK Angka Kredit
                </label>
                <div class="col-md-7">
                  <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" name="AngkaKredit_SK" id="m-dropzone-one">
                    <div class="m-dropzone__msg dz-message needsclick">
                      <h3 class="m-dropzone__msg-title">
                        klik atau Seret File kesini untuk mengunggah
                      </h3>
                      <span class="m-dropzone__msg-desc">
                        tipe file dengan ekstensi : PDF, JPG)
                      </span>
                    </div>
                  </div>
                  <span class="m-form__help ">
                    tipe file dengan ekstensi : PDF, JPG)
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn m-btn--pill m-btn--air btn-metal" data-dismiss="modal">Batal</button>
        <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="btnSimpan" onclick="simpan_data()">Simpan</button>
      </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- modal form -->
<script>

  //
  // $('#m_datepicker_atas_1').datepicker({
  //     orientation: "top left",
  //     todayHighlight: true,
  //     templates: {
  //         leftArrow: '<i class="la la-angle-left"></i>',
  //         rightArrow: '<i class="la la-angle-right"></i>'
  //     }
  // });
  //
  // $('#m_datepicker_atas_2').datepicker({
  //     orientation: "top left",
  //     todayHighlight: true,
  //     templates: {
  //         leftArrow: '<i class="la la-angle-left"></i>',
  //         rightArrow: '<i class="la la-angle-right"></i>'
  //     }
  // });
  //
  // $('#m_datepicker_atas_3').datepicker({
  //     orientation: "top left",
  //     todayHighlight: true,
  //     templates: {
  //         leftArrow: '<i class="la la-angle-left"></i>',
  //         rightArrow: '<i class="la la-angle-right"></i>'
  //     }
  // });
  //
  // $('#m_datepicker_atas_4').datepicker({
  //     orientation: "top left",
  //     todayHighlight: true,
  //     templates: {
  //         leftArrow: '<i class="la la-angle-left"></i>',
  //         rightArrow: '<i class="la la-angle-right"></i>'
  //     }
  // });
  //-- BEGIN: dataTable remote
  var DatatableJsonRemoteAngkaKredit = function () {

  	//-- BEGIN: fungsi_data
  	var angkakredit = function () {

      //-- BEGIN: tabel data
  		var tabel_data = {

        //-- BEGIN: datasource definition
  			data: {
  				type: 'remote',
  				source: {
            read: {
              url: '<?php echo base_url('angkakredit/riwayat')?>',
            },
          },
  				pageSize: 5,
          serverPaging: true,
  				serverFiltering: false,
  				serverSorting: false,
  			},
        //-- END: datasource definition

  			//-- BEGIN: layout definition
  			layout: {
  				theme: 'default', // datatable theme
  				class: '', // custom wrapper class
          spinner: {
            state: 'info'
          },
  				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
  				footer: false // display/hide footer
  			},
        //-- END: layout definition

        //-- BEGIN: Translate definition
        translate:{
          records: {
            processing: 'Memuat data',
            noRecords: 'Data masih kosong'
          },
          toolbar: {
            pagination: {
              items: {
                info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
              }
            }
          }
        },
        //-- END: Translate definition

        //-- BEGIN: Toolbar definition
        toolbar: {
          items: {
            pagination: {
              pages: {
                desktop: {
                  layout: 'default',
                  pagesNumber: 6
                },
                tablet: {
                  layout: 'default',
                  pagesNumber: 3
                },
                mobile: {
                  layout: 'compact'
                }
              },
              pageSizeSelect: [5, 10, 20, 30, 50, 100]
            }
          },
        },
        //-- END: Toolbar definition

  			//-- BEGIN: column properties
  			sortable: true,
  			pagination: true,
  			search: {
  				input: $('#generalSearch')
  			},
        //-- END: column properties

  			//-- BEGIN: columns definition
  			columns: [
          {
    				field: "NIP/NRP",
    				title: "Nomor PAK",
    				width: 60,
    				textAlign: 'center',
    				sortable: false
    			}, {
    				field: "AngkaKredit_NoSKPak",
    				title: "Nomor PAK",
    				width: 60,
    				textAlign: 'center',
    				sortable: false
    			}, {
    				field: "AngkaKredit_TglSKPak",
    				title: "Tanggal PAK",
    				width: 160,
            sortable: 'asc'
    			}, {
    				field: "",
    				title: "Periode Penilaian",
            responsive: {visible: 'lg'}
    			}, {
    				field: "AngkaKredit_Nilai",
    				title: "Nilai Angka Kredit",
            responsive: {visible: 'lg'}
    			}, {
    				field: "AngkaKredit_SK",
    				title: "File PAK",
            responsive: {visible: 'lg'}
    			}
          // , {
    			// 	field: "Aksi",
    			// 	width: 110,
    			// 	title: "Aksi",
    			// 	textAlign: 'center',
    			// 	sortable: false,
    			// 	overflow: 'visible',
    			// 	template: function (row, index, datatable) {
    			// 		return '\
          //       <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_ubah" data="'+row.AngkaKredit_Kode+'" title="Ubah Data">\
    			// 				<i class="la la-edit"></i>\
    			// 			</a>\
    			// 			<a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus" data="'+row.AngkaKredit_Kode+'" title="Hapus Data">\
    			// 				<i class="la la-trash"></i>\
    			// 			</a>\
    			// 		';
    			// 	},
    			// }
        ],
        //-- END: columns definition
    	};
      //-- END: tabel data
      /*
      | -------------------------------------------------------------------------
      | BOF: ACTION BUTTON DEFINITION
      | -------------------------------------------------------------------------
      | # Reload Table
      | # Save Data:
      |   - Create Data
      |   - Update Data
      | # Delete Data
      */

      //-- BEGIN: variabel global
      var datatable = $('.m_datatable').mDatatable(tabel_data);
      var key = $('[name="AngkaKredit_Kode"]');
      var metode_simpan;
      var id;
      //-- END: variabel global

      //-- BEGIN: fungsi refresh mDatatable
      $('#m_datatable_reload').on('click', function() {
        $('.m_datatable').mDatatable('reload');
      });
      //-- END: fungsi refresh mDatatable
      //
      // //-- BEGIN: fungsi simpan data
      // $('#m_data_simpan').on('click', function() {
      //   // variable untuk menyimpan nilai input
      //   var nomor = $('[name="AngkaKredit_NoSKPak"]').val();
      //   var tgl   = $('[name="AngkaKredit_TglSKPak"]').val();
      //   var nilai = $('[name="AngkaKredit_Nilai"]').val();
      //   // var masa = $('[name="MASAPENILAIAN"]').val();
      //   var file = $('[name="AngkaKredit_SK"]').val();
      //
      //
      //   // ubah teks tombol
      //   $('#m_data_simpan').text('Menyimpan...');
      //   // nonaktifkan tombol
      //   $('#m_data_simpan').attr('disabled',true);
      //   // variable untuk menyimpan url ajax
      //   var url;
      //   if(metode_simpan == 'tambah') {
      //       url = "<?php echo site_url('angkakredit/tambah')?>";
      //   } else if (metode_simpan == 'ubah') {
      //       url = "<?php echo site_url('angkakredit/ubah')?>";
      //   }
      //   // menambahkan data ke ajax dengan ajax
      //   $.ajax({
      //       type: "POST",
      //       url : url,
      //       dataType: "JSON",
      //       data : {AngkaKredit_NoSKPak:nomor, AngkaKredit_TglSKPak:tgl, AngkaKredit_Nilai:nilai, AngkaKredit_SK:file},
      //       success: function(data)
      //       {
      //         $('#modal_form').modal('hide');
      //         $('.m_datatable').mDatatable('reload');
      //         // ubah teks tombol
      //         $('#m_data_simpan').text('Simpan');
      //         // aktifkan tombol
      //         $('#m_data_simpan').attr('disabled',false);
      //         // pesan penambahan data berhasil
      //         swal({
      //             title: 'Berhasil!',
      //             text: "Data telah tersimpan.",
      //             type: 'success',
      //             confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
      //             confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
      //             animation: false,
      //             customClass: 'animated bounceIn'
      //         });
      //       },
      //       error: function ()
      //       {
      //         swal({
      //             title: 'Proses Simpan Gagal!',
      //             text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
      //             type: 'error',
      //             confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
      //             confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
      //             animation: false,
      //             customClass: 'animated bounceIn'
      //         });
      //         // alert('Error adding / update data');
      //         // ubah teks tombol
      //         $('#m_data_simpan').text('Simpan');
      //         // aktifkan tombol
      //         $('#m_data_simpan').attr('disabled',false);
      //       }
      //   });
      //   return false;
      // });
      // //-- END: fungsi simpan data
      //
      // //-- BEGIN: fungsi tambah data
      // $('#m_data_tambah').on('click', function() {
      //   metode_simpan = 'tambah';
      //   // aktifkan inputan primary key / foreign key
      //   key.attr('disabled',false);
      //   // reset isi form di modal
      //   $('#form')[0].reset();
      //   // tampilkan modal
      //   $('#modal_form').modal('show');
      //   // menetapkan judul di modal
      //   $('.modal-title').text('Tambah Data Baru');
      // });
      // //-- END: fungsi tambah data
      //
      // //-- BEGIN: fungsi ubah data
      // $(document).on('click','.m_data_ubah', function() {
      //   metode_simpan = 'ubah';
      //   var id=$(this).attr('data');
      //   // reset isi form di modal
      //   $('#form')[0].reset();
      //   // memuat data dengan ajax
      //   $.ajax({
      //       type: "GET",
      //       url : "<?php echo site_url('angkakredit/ambil')?>",
      //       dataType: "JSON",
      //       data : {id:id},
      //       success: function(data)
      //       {
      //         $.each(data,function(AngkaKredit_NoSKPak, AngkaKredit_TglSKPak, AngkaKredit_Nilai, AngkaKredit_SK)
      //         {
      //           // tampilkan modal
      //           $('#modal_form').modal('show');
      //           // menetapkan judul di modal
      //           $('.modal-title').text('Ubah Data');
      //           // nonaktifkan inputan primary key / foreign key
      //           key.attr('disabled',true);
      //           // ambil nilai input dari parameter JSON
      //           key.val(data[0].AngkaKredit_Kode);
      //           $('[name="AngkaKredit_NoSKPak"]').val(data[0].AngkaKredit_NoSKPak);
      //           $('[name="AngkaKredit_TglSKPak"]').val(data[0].AngkaKredit_TglSKPak);
      //           $('[name="AngkaKredit_Nilai"]').val(data[0].AngkaKredit_Nilai);
      //           $('[name="AngkaKredit_SK"]').val(data[0].AngkaKredit_SK);
      //
      //
      //
      //           // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
      //         });
      //       },
      //       error: function (jqXHR, textStatus, errorThrown)
      //       {
      //         // alert('Error get data from ajax');
      //         swal({
      //             title: 'Proses Simpan Gagal!',
      //             text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
      //             type: 'error',
      //             confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
      //             confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
      //             animation: false,
      //             customClass: 'animated bounceIn'
      //         });
      //       }
      //   });
      //   return false;
      // });
      // //-- END: fungsi ubah data
      //
      // //-- BEGIN: fungsi hapus data
      // $(document).on('click','.m_data_hapus', function() {
      //   // variable id untuk menyimpan nilai primary key atau foreign key,
      //   // nilai didapat dari action button
      //   var id=$(this).attr('data');
      //   swal({
      //       title: 'Apakah anda yakin?',
      //       text: "Data yang telah dihapus tidak dapat dikembalikan!",
      //       type: 'question',
      //       showCancelButton: true,
      //       confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
      //       confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
      //       cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
      //       cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
      //       reverseButtons: true,
      //       animation: false,
      //       customClass: 'animated bounceIn'
      //   }).then(function(result){
      //       if (result.value) {
      //         // memuat data dengan ajax
      //         $.ajax({
      //           type : "POST",
      //           url  : "<?php echo base_url('angkakredit/hapus')?>",
      //           dataType : "JSON",
      //             data : {id: id},
      //             success: function(data){
      //               $('.m_datatable').mDatatable('reload');
      //             }
      //         });
      //         swal({
      //             title: 'Dihapus!',
      //             text: "Data telah dihapus.",
      //             type: 'success',
      //             confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
      //             confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
      //             customClass: 'animated bounceIn'
      //         })
      //         // result.dismiss can be 'cancel', 'overlay',
      //         // 'close', and 'timer'
      //       } else if (result.dismiss === 'cancel') {
      //           swal({
      //               title: 'Dibatalkan',
      //               text: "Proses hapus dibatalkan",
      //               type: 'error',
      //               confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
      //               confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
      //               customClass: 'animated bounceIn'
      //           }
      //           )
      //       }
      //   });
      //   return false;
      // });
      //-- END:  fungsi hapus data

      /*
      | -------------------------------------------------------------------------
      | EOF: ACTION BUTTON DEFINITION
      | -------------------------------------------------------------------------
      */

    };
    //-- END: fungsi_data

  	return {
  		// public functions
  		init: function () {
  			angkakredit();
  		}
  	};
  }();
  //-- END: dataTable remote

  //-- BEGIN: jQuery init
  jQuery(document).ready(function () {
  	DatatableJsonRemoteAngkaKredit.init();
  });
  //-- END: jQuery init

  Dropzone.options.mDropzoneOne = {
      paramName: "file", // The name that will be used to transfer the file
      maxFiles: 1,
      maxFilesize: 5, // MB
      addRemoveLinks: true,
      accept: function(file, done) {
          if (file.name == "justinbieber.jpg") {
              done("Naha, you don't.");
          } else {
              done();
          }
      }
  };
</script>
