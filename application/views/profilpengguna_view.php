<!-- begin::Content -->
<style>
.m-dropzone {
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  -ms-border-radius: 4px;
  -o-border-radius: 4px;
  border-radius: 4px;
  padding: 0px;
  text-align: center;
  cursor: pointer; }
  .m-dropzone .m-dropzone__msg-title {
    margin: 0 0 5px 0;
    padding: 0;
    font-weight: 400;
    font-size: 1.1rem; }
  .m-dropzone .m-dropzone__msg-desc {
    font-size: 0.85rem; }
  .m-dropzone .dz-preview .dz-image {
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    -ms-border-radius: 6px;
    -o-border-radius: 6px;
    border-radius: 6px; }

.m-dropzone {
  border: 0px dashed #ebedf2; }

</style>
<div class="m-content">
  <div class="row">
    <div class="col-xl-4 col-lg-4">
      <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
        <div class="m-portlet__body">
          <div class="m--align-right">
            <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" id="m_data_ubah_profil">
              <i class="flaticon-edit-1"></i>
            </button>
          </div>
          <div class="m-card-profile">
            <div class="m-card-profile__title m--hide">
              Profile Saya
            </div>
            <div class="m-card-profile__pic" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Unggah Foto Pengguna">
              <!-- <div class="m-card-profile__pic-wrapper"> -->
                <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" id="m-dropzone-one" name="AuditoFoto_Data">
                  <div class="m-dropzone__msg dz-message needsclick">
                    <div class="container">
                      <div class="upload-btn-wrapper">
                        <img src="<?php echo base_url('assets/app/media/img/users/default.jpg')?>" alt="" >
                      </div>
                    </div>
                  </div>
                </div>
              <!-- </div> -->
            </div>
            <div class="m-card-profile__details">
              <span class="m-card-profile__name" >
                <h4>
                  <span class="GelarDepan"></span>
                  <span class="NamaLengkap"></span>
                  <span class="GelarBelakang"></span>
                </h4>
              </span>
              <a href="#" class="m-card-profile__email m-link">NIP. <span class="Auditor_NIP"></span></a>
            </div>
          </div>
          <div class="m-widget1 m-widget1--paddingless">
            <div class="m-portlet__body">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-placeholder"></i> Alamat Tempat Tinggal</h3>
                    <span class="m-widget1__desc Alamat"></span>
                  </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-email"></i> Surat Elektronik</h3>
                    <span class="m-widget1__desc"><span class="Surel"></span></span>
                  </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-suitcase"></i> Kontak Telepon / HP</h3>
                    <span class="m-widget1__desc NoHP"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-8 col-lg-8 ">
      <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <span class="m-portlet__head-icon">
                <i class="flaticon-suitcase"></i>
              </span>
              <h3 class="m-portlet__head-text">
                Instansi & Unit Kerja
              </h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item m--hide">
                <a href="" data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Reload">
                  <i class="la la-refresh"></i>
                </a>
              </li>
              <li class="m-portlet__nav-item">
                <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                  <i class="la la-expand"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="m-portlet__body">
          <div class="m--align-right">
            <?php
              $role = $this->session->userdata('RoleCode');
              $isadmin = $this->session->userdata('isAdmin');

              if ($isadmin = 1 || $role == 'AdminUnitKerja') {
                  echo '
                <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" id="m_data_ubah_unit">
                  <i class="flaticon-edit-1"></i>
                </button>';
              }
            ?>
          </div>
          <div class="form-group m-form__group m--margin-top-10 m--hide">
            <div class="alert m-alert m-alert--default" role="alert">
              Data Unit Kerja berikt ini di tetapkan oleh Admin Unit Kerja, jika anda tidak lagi di Unit Kerja berikut atau data tidak sesuai silahkan laporkan kepada Admin Unit Kerja terkait. Terimakasih.
            </div>
          </div>
          <br>
          <div class="m-widget13">
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Jenis Lembaga :
              </span>
              <span class="m-widget13__text m-widget13__text-bolder JenisInstansi_Nama">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Nama Instansi :
              </span>
              <span class="m-widget13__text m-widget13__text-bolder Instansi_Nama">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Nama Unit Kerja :
              </span>
              <span class="m-widget13__text m-widget13__text-bolder UnitKerja_Nama">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Alamat Kantor :
              </span>
              <span class="m-widget13__text UnitKerja_Alamat">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Surat Elektronik :
              </span>
              <span class="m-widget13__text UnitKerja_Surel">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Nomor Telepon Kantor :
              </span>
              <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoTlp">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Nomor Faximili Kantor :
              </span>
              <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoFax">
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end::Content -->

<!-- begin: Modal -->
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
          Tambah Data
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="420">
            <div class="m-portlet__body">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <span class="m-widget1__desc">
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          NIP/NRP
                        </label>
                        <div class="col-7">
                          <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="NIP/NRP Auditor" name="Auditor_NIP" disabled>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Nama Lengkap
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input" placeholder="Nama Lengkap" name="Auditor_NamaLengkap">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Surat Elektronik
                        </label>
                        <div class="col-7">
                          <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Surat Elektronik (email)" name="Pengguna_Surel">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Nomor Kontak Telp. / HP
                        </label>
                        <div class="col-7">
                          <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Kontak Telp. / HP" name="Pengguna_NoHP">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Alamat Tempat Tinggal
                        </label>
                        <div class="col-7">
                          <textarea class="form-control m-input m-input--air m-input--pill" placeholder="Alamat tinggal saat ini" name="Pengguna_Alamat" rows="4"></textarea>
                        </div>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan">Simpan</button>
       </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal -->

<!-- begin: Modal -->
<div class="modal fade" id="modal_form_unit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
          Tambah Data
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="420">
            <div class="m-portlet__body">
              <!-- <div class="m-widget1__item"> -->
                <!-- <div class="row m-row--no-padding align-items-center"> -->
                  <div class="col">
                      <div class="form-group m-form__group row m--hide">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Jenis Lembaga
                        </label>
                        <div class="col-7">
                          <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Jenis Lembaga" name="JenisInstansi_Nama" value="" disabled>
                        </div>
                      </div>
                      <div class="form-group m-form__group row m--hide">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Nama Instansi
                        </label>
                        <div class="col-7">
                          <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Instansi" name="Instansi_Nama" disabled>
                        </div>
                      </div>
                      <div class="form-group m-form__group row m--hide">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Kode Unit Kerja
                        </label>
                        <div class="col-7">
                          <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Unit Kerja" name="UnitKerja_Kode" disabled>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Nama Unit Kerja
                        </label>
                        <div class="col-7">
                          <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Unit Kerja" name="UnitKerja_Nama" disabled>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Alamat Kantor
                        </label>
                        <div class="col-7">
                          <textarea class="form-control m-input m-input--air m-input--pill" placeholder="Alamat tinggal saat ini" name="UnitKerja_Alamat" rows="4"></textarea>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Surat Elektronik Kantor
                        </label>
                        <div class="col-7">
                          <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Surat Elektronik (email)" name="UnitKerja_Surel">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Nomor Telp.
                        </label>
                        <div class="col-7">
                          <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Kontak Telp." name="UnitKerja_NoTlp">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Nomor Fax.
                        </label>
                        <div class="col-7">
                          <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Kontak Telp." name="UnitKerja_NoFax">
                        </div>
                      </div>
                  </div>
                <!-- </div> -->
              <!-- </div> -->
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_unit">Simpan</button>
       </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal -->


<script>

$(document).ready(function() {
  getUnitKerja();
  getProfil();
});

function getUnitKerja() {

  var id="<?php echo $this->session->userdata('NIP'); ?>";

  $.ajax({
      type: "GET",
      url : "<?php echo site_url('dasbor/profil')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
          $.each(data,function(Auditor_NIP, isAuditor, Auditor_NamaLengkap, Auditor_GelarDepan, Auditor_GelarBelakang, Pangkat_Nama, Golongan_Kode, JenjangJabatan_Nama, JenisInstansi_Nama, Instansi_Nama, UnitKerja_Nama, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax, UnitKerja_Web)
        {
          // ambil nilai input dari parameter JSON
          // $('.Auditor_NIP').text(data[0].Auditor_NIP);
          // if (data[0].IsAuditor == 'true') {
          //   $('.isAuditor').text('Aktif');
          // }else {
          //   $('.isAuditor').text('Non-Aktif');
          // }
          // $('.Auditor_NamaLengkap').text(data[0].Auditor_NamaLengkap);
          // $('.Auditor_GelarDepan').text(data[0].Auditor_GelarDepan);
          // $('.Auditor_GelarBelakang').text(data[0].Auditor_GelarBelakang);
          // $('.Pangkat_Nama').text(data[0].Pangkat_Nama);
          // $('.Golongan_Kode').text(data[0].Golongan_Kode);
          // $('.JenjangJabatan_Nama').text(data[0].JenjangJabatan_Nama);
          $('.JenisInstansi_Nama').text(data[0].JenisInstansi_Nama);
          $('.Instansi_Nama').text(data[0].Instansi_Nama);
          $('.UnitKerja_Nama').text(data[0].UnitKerja_Nama);
          $('.UnitKerja_Alamat').text(data[0].UnitKerja_Alamat);
          $('.UnitKerja_Surel').text(data[0].UnitKerja_Surel);
          $('.UnitKerja_NoTlp').text(data[0].UnitKerja_NoTlp);
          $('.UnitKerja_NoFax').text(data[0].UnitKerja_NoFax);
          $('.UnitKerja_Web').text(data[0].UnitKerja_Web);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // alert('Error get data from ajax');
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      }
  });
  return false;
}

//-- BEGIN: fungsi ubah data Unit Kerja
$('#m_data_ubah_unit').on('click', function() {
  metode_simpan = 'ubah unitkerja';
  var id='<?php echo $kodeunit?>';
  // reset isi form di modal
  $('#form')[0].reset();
  // memuat data dengan ajax
  $.ajax({
      type: "GET",
      url : "<?php echo site_url('profil/ambil_unit')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
        $.each(data,function(UnitKerja_Kode, UnitKerja_NamaUnitInstansi, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax)
        {
          // tampilkan modal
          $('#modal_form_unit').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Ubah Data Unit Kerja');
          // nonaktifkan inputan primary key / foreign key
          // key.attr('disabled',true);
          // ambil nilai input dari parameter JSON
          // key.val(data[0].Auditor_Kode);
          $('[name="UnitKerja_Kode"]').val(data[0].UnitKerja_Kode);
          $('[name="UnitKerja_Nama"]').val(data[0].UnitKerja_NamaUnitInstansi);
          $('[name="UnitKerja_Alamat"]').val(data[0].UnitKerja_Alamat);
          $('[name="UnitKerja_Surel"]').val(data[0].UnitKerja_Surel);
          $('[name="UnitKerja_NoTlp"]').val(data[0].UnitKerja_NoTlp);
          $('[name="UnitKerja_NoFax"]').val(data[0].UnitKerja_NoFax);


          // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // alert('Error get data from ajax');
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      }
  });
  return false;
});
//-- END: fungsi ubah data Unit Kerja

//-- BEGIN: fungsi simpan data unitkerja
$('#m_data_simpan_unit').on('click', function() {
  // variable untuk menyimpan nilai input
  var kodeunit    = $('[name="UnitKerja_Kode"]').val();
  var namaunit    = $('[name="UnitKerja_Nama"]').val();
  var surel       = $('[name="UnitKerja_Surel"]').val();
  var notlp       = $('[name="UnitKerja_NoTlp"]').val();
  var nofax       = $('[name="UnitKerja_NoFax"]').val();
  var alamat      = $('[name="UnitKerja_Alamat"]').val();
  // var sandi       = $('[name="Pengguna_KataSandi"]').val();

  // ubah teks tombol
  $('#m_data_simpan').text('Menyimpan...');
  // nonaktifkan tombol
  $('#m_data_simpan').attr('disabled',true);
  // variable untuk menyimpan url ajax
  url = "<?php echo site_url('profil/ubah_unit')?>";
  // menambahkan data ke ajax dengan ajax
  $.ajax({
      type: "POST",
      url : url,
      dataType: "JSON",
      data : {UnitKerja_Kode:kodeunit, UnitKerja_Nama:namaunit, UnitKerja_Surel:surel, UnitKerja_NoTlp:notlp, UnitKerja_NoFax:nofax, UnitKerja_Alamat:alamat},
      success: function(data)
      {
        $('#modal_form_unit').modal('hide');
        getUnitKerja();
        // $('.m_datatable').mDatatable('reload');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
        // pesan penambahan data berhasil
        swal({
            title: 'Berhasil!',
            text: "Data telah tersimpan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      },
      error: function ()
      {
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
        // alert('Error adding / update data');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
      }
  });
  return false;
});
//-- END: fungsi simpan data unitkerja

//-- BEGIN: load data profil
function getProfil() {

  var id="<?php echo $nip; ?>";

  $.ajax({
      type: "GET",
      url : "<?php echo site_url('profil/ambil')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
          $.each(data,function(NIP, isAuditor, NamaLengkap, GelarDepan, GelarBelakang, Surel, NoHP, Alamat, NamaJenisInstansi, NamaInstansi, NamaUnitKerja, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax, UnitKerja_Web)
        {
          // ambil nilai input dari parameter JSON
          $('.Auditor_NIP').text(data[0].NIP);
          if (data[0].IsAuditor == 'true') {
            $('.isAuditor').text('Aktif');
          }else {
            $('.isAuditor').text('Non-Aktif');
          }
          $('.NamaLengkap').text(data[0].NamaLengkap);
          $('.GelarDepan').text(data[0].GelarDepan);
          $('.GelarBelakang').text(data[0].GelarBelakang);
          $('.Surel').text(data[0].Surel);
          $('.NoHP').text(data[0].NoHP);
          $('.Alamat').text(data[0].Alamat);
          $('.NamaJenisInstansi').text(data[0].NamaJenisInstansi);
          $('.NamaInstansi').text(data[0].NamaInstansi);
          $('.NamaUnitKerja').text(data[0].NamaUnitKerja);
          $('.UnitKerja_Alamat').text(data[0].UnitKerja_Alamat);
          $('.UnitKerja_Surel').text(data[0].UnitKerja_Surel);
          $('.UnitKerja_NoTlp').text(data[0].UnitKerja_NoTlp);
          $('.UnitKerja_NoFax').text(data[0].UnitKerja_NoFax);
          $('.UnitKerja_Web').text(data[0].UnitKerja_Web);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // alert('Error get data from ajax');
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      }
  });
  return false;
};
//-- END: load data profil

//-- BEGIN: fungsi ubah data Profil
$('#m_data_ubah_profil').on('click', function() {
  metode_simpan = 'ubah profil';
  var id='<?php echo $nip?>';
  // reset isi form di modal
  $('#form')[0].reset();
  // memuat data dengan ajax
  $.ajax({
      type: "GET",
      url : "<?php echo site_url('profil/ambil')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
        $.each(data,function(NIP, Surel, NoHP, Alamat)
        {
          // tampilkan modal
          $('#modal_form').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Ubah Data Profil');
          // nonaktifkan inputan primary key / foreign key
          // key.attr('disabled',true);
          // ambil nilai input dari parameter JSON
          // key.val(data[0].Auditor_Kode);
          $('[name="Auditor_NIP"]').val(data[0].NIP);
          $('[name="Auditor_NamaLengkap"]').val(data[0].NamaLengkap);
          $('[name="Pengguna_Surel"]').val(data[0].Surel);
          $('[name="Pengguna_NoHP"]').val(data[0].NoHP);
          $('[name="Pengguna_Alamat"]').val(data[0].Alamat);


          // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // alert('Error get data from ajax');
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      }
  });
  return false;
});
//-- END: fungsi ubah data Profil

//-- BEGIN: fungsi simpan data profil
$('#m_data_simpan').on('click', function() {
  // variable untuk menyimpan nilai input
  var nip         = $('[name="Auditor_NIP"]').val();
  var nama        = $('[name="Auditor_NamaLengkap"]').val();
  var surel       = $('[name="Pengguna_Surel"]').val();
  var nohp        = $('[name="Pengguna_NoHP"]').val();
  var alamat      = $('[name="Pengguna_Alamat"]').val();
  // var sandi       = $('[name="Pengguna_KataSandi"]').val();

  // ubah teks tombol
  $('#m_data_simpan').text('Menyimpan...');
  // nonaktifkan tombol
  $('#m_data_simpan').attr('disabled',true);
  // variable untuk menyimpan url ajax
  url = "<?php echo site_url('profil/ubah')?>";
  // menambahkan data ke ajax dengan ajax
  $.ajax({
      type: "POST",
      url : url,
      dataType: "JSON",
      data : {Auditor_NIP:nip, Auditor_NamaLengkap:nama, Pengguna_Surel:surel, Pengguna_NoHP:nohp, Pengguna_Alamat:alamat},
      success: function(data)
      {
        $('#modal_form').modal('hide');
        getProfil();
        // $('.m_datatable').mDatatable('reload');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
        // pesan penambahan data berhasil
        swal({
            title: 'Berhasil!',
            text: "Data telah tersimpan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      },
      error: function ()
      {
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
        // alert('Error adding / update data');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
      }
  });
  return false;
});
//-- END: fungsi simpan data profil
</script>
