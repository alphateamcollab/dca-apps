<!-- begin: m_content -->
<div class="m-content">
  <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            Tabel Referensi Golongan Ruang
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href=""  data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" id="m_datatable_reload">
              <i class="la la-refresh"></i>
            </a>
          </li>
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
              <i class="la la-expand"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="m-portlet__body">
      <!--begin: Search Form -->
      <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <div class="row align-items-center">
          <div class="col-xl-8 order-2 order-xl-1">
            <div class="form-group m-form__group row align-items-center">
              <div class="col-md-4">
                <div class="m-input-icon m-input-icon--left">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Cari..." id="generalSearch">
                  <span class="m-input-icon__icon m-input-icon__icon--left">
                    <span>
                      <i class="la la-search"></i>
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 order-1 order-xl-2 m--align-right">
            <!-- <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" id="m_datatable_reload">
              <i class="la la-refresh"></i>
            </button> -->
            <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" id="m_data_tambah">
              <i class="la la-plus"></i>
            </button>
            <div class="m-separator m-separator--dashed d-xl-none"></div>
          </div>
        </div>
      </div>
      <!--end: Search Form -->

      <!--begin: Datatable -->
      <div class="m_datatable" id="json_data"></div>
      <!--end: Datatable -->
    </div>
  </div>

  <!-- begin: Modal -->
  <div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <!-- begin: modal header -->
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">
            Tambah Data
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
              &times;
            </span>
          </button>
        </div>
        <!-- end: modal header -->
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right" id="form">
          <!-- begin: modal body -->
          <div class="modal-body">
            <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
              <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Kode Golongan
                  </label>
                  <div class="col-7">
                    <input type="text" class="m-input--pill form-control m-input" placeholder="Kode Golongan" name="Golongan_Kode">
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Nama Golongan
                  </label>
                  <div class="col-7">
                    <input type="text" class="m-input--pill form-control m-input" placeholder="Nama Golongan" name="Golongan_Nama">
                  </div>
                </div>
                <div class="form-group m-form__group row">
                  <label for="example-text-input" class="col-5 col-form-label">
                    Deskripsi Golongan
                  </label>
                  <div class="col-7">
                    <textarea class="form-control m-input m-input--air m-input--pill" placeholder="Deskripsi Golongan" name="Golongan_Deskripsi" rows="4"></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- end: modal body -->
          <!-- begin: meodal-footer -->
          <div class="modal-footer">
            <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
            <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan">Simpan</button>
          </div>
          <!-- end: modal footer -->
        </form>
        <!-- end::Form -->
      </div>
    </div>
  </div>
  <!-- end: Modal -->

</div>
<!-- end: m_content -->

<script>

//-- BEGIN: dataTable remote
var DatatableJsonRemoteGolongan = function () {

	//-- BEGIN: fungsi_data
	var golongan = function () {

    //-- BEGIN: tabel data
		var tabel_data = {

      //-- BEGIN: datasource definition
			data: {
				type: 'remote',
				source: {
          read: {
            url: '<?php echo base_url('ref-golongan/data')?>',
          },
        },
				pageSize: 5,
        serverPaging: true,
				serverFiltering: false,
				serverSorting: false,
			},
      //-- END: datasource definition

			//-- BEGIN: layout definition
			layout: {
				theme: 'default', // datatable theme
				class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

			//-- BEGIN: column properties
			sortable: true,
			pagination: true,
			search: {
				input: $('#generalSearch')
			},
      //-- END: column properties

			//-- BEGIN: columns definition
			columns: [
        {
  				field: "Golongan_Kode",
  				title: "Kode",
  				width: 60,
  				textAlign: 'center',
  				sortable: false
  			}, {
  				field: "Golongan_Nama",
  				title: "Nama Golongan",
  				width: 160,
          sortable: 'desc'
  			}, {
  				field: "Golongan_Deskripsi",
  				title: "Deskirpsi",
          responsive: {visible: 'lg'}
  			}, {
  				field: "DibuatOleh",
  				title: "Dibuat Oleh",
  				width: 150,
  				textAlign: 'center',
  				template: function (row) {
            var dibuat = {
                1: {'title': 'Administrator', 'class': 'm-badge--primary'},
                2: {'title': 'Admin Pusbin', 'class': ' m-badge--info'},
              };
  					// callback function support for column rendering
            if (row.DibuatOleh == null) {
              ''
            } else {
              var dibuatoleh = row.DibuatOleh <= 2 ?
              '<span class="m-badge ' + dibuat[row.DibuatOleh].class + ' m-badge--wide">' + dibuat[row.DibuatOleh].title + '</span> <br>' + row.DibuatTgl :
              '<span class="m-badge m-badge--warning m-badge--wide">' + row.DibuatOleh + '</span> <br>' + row.DibuatTgl;
            }
            return dibuatoleh;
  				},
          responsive: {visible: 'lg'}
  			}, {
  				field: "DiubahOleh",
  				title: "Diubah Oleh",
  				width: 150,
  				textAlign: 'center',
  				template: function (row) {
            var diubah = {
                1: {'title': 'Administrator', 'class': 'm-badge--primary'},
                2: {'title': 'Admin Pusbin', 'class': ' m-badge--info'},
              };
  					// callback function support for column rendering
            if (row.DiubahOleh == null || row.DiubahOleh == '') {
              ''
            } else {
                var diubaholeh = row.DiubahOleh <= 2 ?
                '<span class="m-badge ' + diubah[row.DiubahOleh].class + ' m-badge--wide">' + diubah[row.DiubahOleh].title + '</span> <br>' + row.DiubahTgl :
                '<span class="m-badge m-badge--warning m-badge--wide">' + row.DiubahOleh + '</span> <br>' + row.DiubahTgl;
            }
            return diubaholeh;
  				},
          responsive: {visible: 'lg'}
  			}, {
  				field: "Aksi",
  				width: 110,
  				title: "Aksi",
  				textAlign: 'center',
  				sortable: false,
  				overflow: 'visible',
  				template: function (row, index, datatable) {
  					return '\
              <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill m_data_ubah" data="'+row.Golongan_Kode+'" title="Ubah Data">\
  							<i class="la la-edit"></i>\
  						</a>\
  						<a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus" data="'+row.Golongan_Kode+'" title="Hapus Data">\
  							<i class="la la-trash"></i>\
  						</a>\
  					';
  				},
  			}],
      //-- END: columns definition

  	};
    //-- END: tabel data

    /*
    | -------------------------------------------------------------------------
    | BOF: ACTION BUTTON DEFINITION
    | -------------------------------------------------------------------------
    | # Reload Table
    | # Save Data:
    |   - Create Data
    |   - Update Data
    | # Delete Data
    */

    //-- BEGIN: variabel global
    var datatable = $('.m_datatable').mDatatable(tabel_data);
    var key = $('[name="Golongan_Kode"]');
    var metode_simpan;
    var id;
    //-- END: variabel global

    //-- BEGIN: fungsi refresh mDatatable
    $('#m_datatable_reload').on('click', function() {
      $('.m_datatable').mDatatable('reload');
    });
    //-- END: fungsi refresh mDatatable

    //-- BEGIN: fungsi simpan data
    $('#m_data_simpan').on('click', function() {
      // variable untuk menyimpan nilai input
      var kode = $('[name="Golongan_Kode"]').val();
      var nama = $('[name="Golongan_Nama"]').val();
      var deskripsi = $('[name="Golongan_Deskripsi"]').val();
      // ubah teks tombol
      $('#m_data_simpan').text('Menyimpan...');
      // nonaktifkan tombol
      $('#m_data_simpan').attr('disabled',true);
      // variable untuk menyimpan url ajax
      var url;
      if(metode_simpan == 'tambah') {
          url = "<?php echo site_url('ref-golongan/tambah')?>";
      } else if (metode_simpan == 'ubah') {
          url = "<?php echo site_url('ref-golongan/ubah')?>";
      }
      // menambahkan data ke ajax dengan ajax
      $.ajax({
          type: "POST",
          url : url,
          dataType: "JSON",
          data : {Golongan_Kode:kode, Golongan_Nama:nama, Golongan_Deskripsi:deskripsi},
          success: function(data)
          {
            $('#modal_form').modal('hide');
            $('.m_datatable').mDatatable('reload');
            // ubah teks tombol
            $('#m_data_simpan').text('Simpan');
            // aktifkan tombol
            $('#m_data_simpan').attr('disabled',false);
            // pesan penambahan data berhasil
            swal({
                title: 'Berhasil!',
                text: "Data telah tersimpan.",
                type: 'success',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                animation: false,
                customClass: 'animated bounceIn'
            });
          },
          error: function ()
          {
            swal({
                title: 'Proses Simpan Gagal!',
                text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                type: 'error',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                animation: false,
                customClass: 'animated bounceIn'
            });
            // alert('Error adding / update data');
            // ubah teks tombol
            $('#m_data_simpan').text('Simpan');
            // aktifkan tombol
            $('#m_data_simpan').attr('disabled',false);
          }
      });
      return false;
    });
    //-- END: fungsi simpan data

    //-- BEGIN: fungsi tambah data
    $('#m_data_tambah').on('click', function() {
      metode_simpan = 'tambah';
      // aktifkan inputan primary key / foreign key
      key.attr('disabled',false);
      // reset isi form di modal
      $('#form')[0].reset();
      // tampilkan modal
      $('#modal_form').modal('show');
      // menetapkan judul di modal
      $('.modal-title').text('Tambah Data Baru');
    });
    //-- END: fungsi tambah data

    //-- BEGIN: fungsi ubah data
    $(document).on('click','.m_data_ubah', function() {
      metode_simpan = 'ubah';
      var id=$(this).attr('data');
      // reset isi form di modal
      $('#form')[0].reset();
      // memuat data dengan ajax
      $.ajax({
          type: "GET",
          url : "<?php echo site_url('ref-golongan/ambil')?>",
          dataType: "JSON",
          data : {id:id},
          success: function(data)
          {
            $.each(data,function(Golongan_Kode, Golongan_Nama, Golongan_Deskripsi)
            {
              // tampilkan modal
              $('#modal_form').modal('show');
              // menetapkan judul di modal
              $('.modal-title').text('Ubah Data');
              // nonaktifkan inputan primary key / foreign key
              key.attr('disabled',true);
              // ambil nilai input dari parameter JSON
              key.val(data[0].Golongan_Kode);
              $('[name="Golongan_Nama"]').val(data[0].Golongan_Nama);
              $('[name="Golongan_Deskripsi"]').val(data[0].Golongan_Deskripsi);
              // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
            });
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            // alert('Error get data from ajax');
            swal({
                title: 'Proses Simpan Gagal!',
                text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                type: 'error',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                animation: false,
                customClass: 'animated bounceIn'
            });
          }
      });
      return false;
    });
    //-- END: fungsi ubah data

    //-- BEGIN: fungsi hapus data
    $(document).on('click','.m_data_hapus', function() {
      // variable id untuk menyimpan nilai primary key atau foreign key,
      // nilai didapat dari action button
      var id=$(this).attr('data');
      swal({
          title: 'Apakah anda yakin?',
          text: "Data yang telah dihapus tidak dapat dikembalikan!",
          type: 'question',
          showCancelButton: true,
          confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
          confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
          cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
          cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
          reverseButtons: true,
          animation: false,
          customClass: 'animated bounceIn'
      }).then(function(result){
          if (result.value) {
            // memuat data dengan ajax
            $.ajax({
              type : "POST",
              url  : "<?php echo base_url('ref-golongan/hapus')?>",
              dataType : "JSON",
                data : {id: id},
                success: function(data){
                  $('.m_datatable').mDatatable('reload');
                }
            });
            swal({
                title: 'Dihapus!',
                text: "Data telah dihapus.",
                type: 'success',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                customClass: 'animated bounceIn'
            })
            // result.dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
          } else if (result.dismiss === 'cancel') {
              swal({
                  title: 'Dibatalkan',
                  text: "Proses hapus dibatalkan",
                  type: 'error',
                  confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                  confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                  customClass: 'animated bounceIn'
              }
              )
          }
      });
      return false;
    });
    //-- END:  fungsi hapus data

    /*
    | -------------------------------------------------------------------------
    | EOF: ACTION BUTTON DEFINITION
    | -------------------------------------------------------------------------
    */

  };
  //-- END: fungsi_data

	return {
		// public functions
		init: function () {
			golongan();
		}
	};
}();
//-- END: dataTable remote

//-- BEGIN: jQuery init
jQuery(document).ready(function () {
	DatatableJsonRemoteGolongan.init();
});
//-- END: jQuery init

</script>
