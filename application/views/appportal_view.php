<!DOCTYPE html>
<html lang="id">
<!-- begin::Head -->

<head>
  <?php
      $this->load->view('layout/head');
      $this->load->view('layout/js');
  ?>

  <style>
  #menu_icon
  {
  /* border-radius: 50%; */
  position: relative;
  /* color:#fff; */
  width: auto; */
  height: 10%;
  /* background-color:darkblue  ; */
  margin:200px auto 3px auto;
  /* border: 2.5px dotted LightGray   ; */
  }

  .sibijak
  {
   /* position: relative; */
   width: 50%;
   /* height:auto; */
   text-align: center;
   margin:0 auto 0 auto;

  }

  .proud
  {
   /* position: relative; */
   width: 36%;
   /* height:auto; */
   text-align: center;
   margin:15px auto 0 5px;

  }
  .btn-map
  {
   position: relative;
   margin: auto;
   width: 150px;
   height: auto;
  }

  #map
  {
  position: relative;
  height: auto;
  margin: auto;
  }


  .wcircle-menu
  {
  width: 100%;
  height: auto;
  position: relative;
  display: block;


  }

  .cp
   {
     width: 135px;
     height: 135px;
     position: absolute;
    }
  .regol
     {
      width: 135px;
      height: 135px;
      position: absolute;
      }
  .sertifikasi
     {
       width: 135px;
       height: 135px;
       position: absolute;
      }
  .bank_soal
     {
       width: 135px;
       height: 135px;
       position: absolute;
      }
  .dca
     {
       width: 135px;
       height: 135px;
       position: absolute;
      }
  .wcircle-icon
  {
  position: absolute;
  top:auto;
  height: auto;
  width: 100%;
  text-align: center;
  padding: auto;
  opacity: 1;
  }
</style>
</head>
<!-- end::Head -->
<!--begin::Body -->

<body class="m-page--wide m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default">

  <!-- begin:: m-Page -->
  <div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- begin:: m-Header -->
    <header class="m-grid__item m-header" data-minimize-offset="200" data-minimize-mobile-offset="200">
      <img class="proud" src="<?php echo base_url('assets/vendors/media/img/logo/proud.png')?>">

    </header>
    <!-- end: m-Header -->

    <!-- begin:: m-Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
      <!-- begin:: m-Wrapper -->
      <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url(<?php echo base_url('assets/vendors/media/bg-fix.jpg')?>);">
        <!-- begin: Content -->
        <!-- <div class="m-content" > -->
          <div id="menu_icon" >
              <!-- <div class="wcircle-icon ">
                <i class="fa fa-sitemap " style="font-size:40px;color:white;"></i>
              </div> -->
            <div class="wcircle-menu">
              <div class="regol">
                <a href="http://pusdiklatwas.bpkp.go.id:8099/registrasi/">
                  <img  class="regol"src="<?php echo base_url('assets/vendors/media/img/logo/regol.png')?>">
                </a>
               </div>
              <div class="cp">
                <a href="http://163.53.185.91:8083/sibijak/ppak">
                  <img  class="cp"src="<?php echo base_url('assets/vendors/media/img/logo/cp.png')?>">
               </a>
              </div>
              <div class="dca">
                <a href="<?php echo base_url('dasbor'); ?>">
                  <img  class="dca"src="<?php echo base_url('assets/vendors/media/img/logo/dca.png')?>">
                </a>
              </div>
              <div class="sertifikasi">
                <a href="http://163.53.185.91:8083/sibijak/sertifikasi">
                  <img  class="sertifikasi"src="<?php echo base_url('assets/vendors/media/img/logo/sertifikasi.png')?>">
                </a>
              </div>
              <div class="bank_soal">
                <a href="http://163.53.185.91:8083/sibijak/sertifikasi/ujian-sertifikasi-jfa">
                  <img  class="bank_soal"src="<?php echo base_url('assets/vendors/media/img/logo/bank-soal.png')?>">
                </a>
              </div>
            </div>
          </div>
          <!-- <div> -->

          <!-- Logo sibijak -->
          <img class="sibijak" src="<?php echo base_url('assets/vendors/media/img/logo/sibijak-logo.png')?>">
          <!-- Logo sibijak -->

          <!-- </div> -->
          <div id="map">
            <a target="_blank" href="http://118.97.51.140:10001/map">
              <img  class="btn-map"src="<?php echo base_url('assets/vendors/media/img/logo/btn-map.png')?>">
						</a>
          </div>
        <!-- </div> -->
        <!-- end: m-content -->
      </div>
      <!-- end:: m-Wrapper -->
    </div>
    <!-- end:: m-Body -->

    <!-- begin::Footer -->
    <footer class="m-grid__item  m-footer ">
      <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
            <span class="m-footer__copyright">
              Data Center Auditor &copy; 2017 - <?php echo date('Y');?> by
              <a href="http://pusbinjfa.bpkp.go.id/" class="m-link">
                Pusbin JFA BPKP
              </a>
            </span>
          </div>
          <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
            <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
              <li class="m-nav__item">
                <a href="#" class="m-nav__link">
                  <span class="m-nav__link-text">
                    Tentang DCA
                  </span>
                </a>
              </li>
              <li class="m-nav__item m-nav__item">
                <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Panduan Penggunaan" data-placement="left">
                  <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- end::Footer -->
  </div>
  <!-- end:: m-Page -->
</body>
</html>
<script>
$(document).ready(function(){
  $('#menu_icon').trigger('click');
});

  $('#menu_icon').WCircleMenu({
    width: '140px',
    height: '140px',
    angle_start : -Math.PI/0.20,
    delay: 75,
    distance: 195,
    angle_interval: Math.PI/4,
    easingFuncShow:"easeOutBack",
      // easingFuncHide:"easeOuttBack",
    step:45,
    openCallback:false,
      // closeCallback:true,
    itemRotation:0,
    iconRotation:0,
  });

</script>
