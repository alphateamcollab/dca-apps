<!DOCTYPE html>
<html lang="id">
<!-- begin::Head -->

<head>
  <?php
      $this->load->view('layout/head');
      $this->load->view('layout/js');
  ?>

  <style>
  #menu_icon
  {
  border-radius: 50%;
  position: relative;
  color:#fff;
  width: 150px;
  height: 150px;
  background-color:FireBrick  ;
  margin:200px auto 50px auto;
  border: 2.5px dotted LightGray   ;
  }

  #menu_icon1
  {
  border-radius: 200%;
  position: relative;
  color:#fff;
  width: 120px;
  height: 60px;
  /* background-color: #e74c3c; */
  margin:10px auto 10px auto;


  }


    .wcircle-icon1
    {
    position: absolute;
    top: 0px;
    height: 70px;
    width: 70px;
    text-align: center;
    padding: 20px;
    display: block;
    transform: translate3d(0px, 0px, 0px) rotate(0deg);
    opacity: 1;
    }

  .wcircle-menu
  {
  width: 135px;
  height: 135px;
  position: relative;
  display: block;


  }

  .dupak_auditor
   {
    background-color: rgb(41, 128, 185);
    border-radius: 50%;
    padding: 20px;
    width: 140px;
    height: 140px;
    text-align: center;
    position: absolute;
    top: 0px; left: 0px;
    opacity: 3;
    transform: translate3d(70px, -70px, 0px) rotate(0deg);
    border: 6px double LightGray ;
    background-color:   RoyalBlue ;
    }
  .pendaftaran_diklat
     {
      background-color: rgb(41, 128, 185);
      border-radius: 50%;
      padding: 15px;
      width: 140px;
      height: 140px;
      text-align: center;
      position: absolute;
      top: 0px; left: 0px;
      opacity: 3;
      transform: translate3d(70px, -70px, 0px) rotate(0deg);
      border: 6px double LightGray ;
      background-color: DodgerBlue  ;
      }
  .sertifikasi
     {
      background-color: rgb(41, 128, 185);
      border-radius: 50%;
      padding: 20px;
      width: 140px;
      height: 140px;
      text-align: center;
      position: absolute;
      top: 0px; left: 0px;
      opacity: 3;
      transform: translate3d(70px, -70px, 0px) rotate(0deg);
      border: 6px double LightGray ;
      background-color: blue;
      }
  .bank_soal
     {
      background-color: rgb(41, 128, 185);
      border-radius: 50%;
      padding: 20px;
      width: 140px;
      height: 140px;
      text-align: center;
      position: absolute;
      top: 0px; left: 0px;
      opacity: 3;
      transform: translate3d(70px, -70px, 0px) rotate(0deg);
      border: 6px double LightGray ;
      background-color:darkblue   ;
      }

  .wcircle-icon
  {
  position: absolute;
  top: 0px;
  height: 70px;
  width: 70px;
  text-align: center;
  padding: 20px;
  display: block;
  transform: translate3d(0px, 0px, 0px) rotate(0deg);
  opacity: 1;
  }
</style>
</head>
<!-- end::Head -->
<!--begin::Body -->

<body class="m-page--wide m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default">

  <!-- begin:: m-Page -->
  <div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- begin:: m-Header -->
    <header class="m-grid__item m-header " data-minimize-offset="200" data-minimize-mobile-offset="200">
        <?php $this->load->view('layout/header'); ?>
    </header>
    <!-- end: m-Header -->

    <!-- begin:: m-Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
      <!-- begin:: m-Wrapper -->
      <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" style="background-image: url(<?php echo base_url ('assets/vendors/media/bg-0.jpg')?>);">
        <!-- begin: Content -->
        <div class="m-content" >
          <div id="menu_icon">
              <div class="wcircle-icon">
                <a class="m-link m-link--light" href="<?php echo base_url('dasbor'); ?>">
                <i class="flaticon-user" style="font-size: 3rem;"></i>
                <h6>
                  DATA AUDITOR
  							</h6>
              </a>
              </div>
            <div class="wcircle-menu">
              <div class="pendaftaran_diklat">
                <a class="m-link m-link--light" href="<?php echo base_url('login'); ?>">
                  <i class="flaticon-technology" style="font-size: 3rem;"></i>
                   <h6>
                    PENDAFTARAN DIKLAT
                  </h6>
                 </a>
               </div>
              <div class="dupak_auditor">
                <a class="m-link m-link--light" href="<?php echo base_url('login'); ?>">
                  <i class="flaticon-puzzle" style="font-size: 3rem;"></i>
                  <h6>
                    DUPAK AUDITOR
                  </h6>
               </a>
              </div>
              <div class="sertifikasi">
                <a class="m-link m-link--light" href="<?php echo base_url('login'); ?>">
                  <i class="flaticon-open-box" style="font-size: 3rem;"></i>
                  <h6>
                    SERTIFIKASI JFA
                  </h6>
                 </a>
              </div>
              <div class="bank_soal">
                <a class="m-link m-link--light" href="<?php echo base_url('login'); ?>">
                  <i class="flaticon-list-1" style="font-size: 3rem;"></i>
                  <h6>
                    UJIAN JFA & BANK SOAL
                  </h6>
                </a>
              </div>
            </div>
          </div>
          <div id="menu_icon1">
            <a href="<?php echo base_url('login'); ?>" class="m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air btn m-btn m-btn--pill m-btn--gradient-from-info m-btn--gradient-to-warning">
							<strong>
									MAP BPKP
							</ssttrroonngg>
						</a>
          </div>
        </div>
        <!-- end: m-content -->
      </div>
      <!-- end:: m-Wrapper -->
    </div>
    <!-- end:: m-Body -->

    <!-- begin::Footer -->
    <footer class="m-grid__item  m-footer ">
      <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
            <span class="m-footer__copyright">
                  Data Center Auditor &copy; 2017 - <?php echo date('Y');?> by
                  <a href="http://pusbinjfa.bpkp.go.id/" class="m-link">
                    Pusbin JFA BPKP
                  </a>
                </span>
          </div>
          <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
            <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
              <li class="m-nav__item">
                <a href="#" class="m-nav__link">
                  <span class="m-nav__link-text">
                    Tentang DCA
                  </span>
                </a>
              </li>
              <li class="m-nav__item m-nav__item">
                <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Panduan Penggunaan" data-placement="left">
                  <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- end::Footer -->
  </div>
  <!-- end:: m-Page -->
</body>
</html>
<script>
$(document).ready(function(){
  $('#menu_icon').trigger('click');
});

  $('#menu_icon').WCircleMenu({
    width: '140px',
    height: '140px',
    angle_start : -Math.PI/0.20,
    delay: 150,
    distance: 180,
    angle_interval: Math.PI/3,
    easingFuncShow:"easeOutBack",
    easingFuncHide:"easeInBack",
    step:30,
    openCallback:true,
    closeCallback:true,
    itemRotation:0,
    iconRotation:0,
  });

</script>
