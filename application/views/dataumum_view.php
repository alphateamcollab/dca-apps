<div class="m-content">
  <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <span class="m-portlet__head-icon m--hide">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="m-portlet__head-text">
                    Input Data Umum
                  </h3>
                </div>
              </div>
            </div>
            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right">
            <div class="m-portlet__body">
              <div class="row">
                    <div class="col-md-6">                  <!--begin::Form-->
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                          KLP
                          </label>
                          <div class="col-7">
  													<div class="col-xlg-7 m-form__group-sub">
                              <select class="m-input--pill form-control m-input" name="hirarki_Pemerintahan">
          												<option value="">
          													--Kementrian dan Lembaga--
          												</option>
          												<option>
          													Pemerintah Pusat
          												</option>
                                  <option>
          													Pemerintah Daerah
          												</option>
          											</select>
  													</div>
                            <div class="col-xlg-7 m-form__group-sub">

                              <select class="m-input--pill form-control m-input" name="pemerintah_daerah">
          												<option value="">
          													--Pilih Pemerintahan Daerah--
          												</option>
          												<option>
          													Pemerintah prov
          												</option>
          												<option>
          													Pemerintah kabupaten
          												</option>
                                  <option>
          													Pemerintah kota
          												</option>
          											</select>
  													</div>
                          </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Instansi
                          </label>
                          <div class="col-7">

                            <div class="col-xlg-7 m-form__group-sub">

                                          <select class="m-input--pill form-control m-input" name="instansi">
                                              <option value="">
                                                --Pilih Instansi--
                                              </option>
                                              <option>
                                                Kepolisian Negara Republik Indonesia
                                              </option>
                                              <option>
                                                Badan Intelelijen Negara
                                              </option>
                                              <option>
                                                Lembaga Sandi Negara
                                              </option>
                                            </select>
                            </div>
                                    <div class="col-xlg-7 m-form__group-sub">

                                      <div class="input-group date">
                                        <input class="m-input--pill m-input--pill form-control m-input" type="number" readonly="" placeholder="Kode Instansi" name="kode_instansi" id="example-text-input">

                                      </div>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Alamat Kantor
                          </label>
                          <div class="col-7">
                              <textarea class="form-control m-input" id="exampleTextarea" rows="3" placeholder="Alamat Lengkap Kantor"></textarea>
                          </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Nomor Telepon Kantor
                          </label>
                          <div class="col-7">

                                    <div class="col-xlg-7 m-form__group-sub">
                                      <div class="input-group date">
                                        <input type="number" class="m-input--pill form-control m-input" type="number" placeholder="Telepon Kantor" name="no_tlp_kantor1" id="example-text-input">
                                        <div class="input-group-append">
                                          <span class="input-group-text">
                                            <i class="la la-phone"></i>
                                          </span>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-xlg-7 m-form__group-sub">
                                      <div class="input-group date">
                                        <input type="number" class="m-input--pill form-control m-input" type="number" placeholder="Telepon Kantor" name="no_tlp_kantor2" id="example-text-input">
                                        <div class="input-group-append">
                                          <span class="input-group-text">
                                            <i class="la la-phone"></i>
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                          </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Nomor Telepon User
                          </label>
                            <div class="col-7">
                              <div class="col-xlg-7 m-form__group-sub">
                                <div class="input-group">
                                  <input type="number" class="m-input--pill form-control m-input" type="number" placeholder="Telepon Pengguna" name="no_telp_pengguna" id="example-text-input">
                                  <div class="input-group-append">
                                    <span class="input-group-text">
                                      <i class="la la-phone"></i>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Nomor Induk Pegawai (NIP) / NRP
                          </label>
                          <div class="col-7">
                            <input class="m-input--pill form-control m-input" type="number" placeholder="000000000000000000" name="nip" id="example-text-input">
                          </div>
                        </div>
                     </div>
                    <div class="col-md-6">
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Nama Lengkap
                          </label>
                          <div class="col-7">
                            <div class="col-xlg-7 m-form__group-sub">

                              <input class="m-input--pill form-control m-input" type="text" placeholder="Gelar Awal" name="gelar_awal">
                            </div>
                            <div class="col-xlg-7 m-form__group-sub">

                                <input class="m-input--pill form-control m-input" type="text" placeholder="Nama lengkap" name="nama_lengkap">
                            </div>
                            <div class="col-xlg-7 m-form__group-sub">

                              <input class="m-input--pill form-control m-input" type="text" placeholder="Gelar Akhir" name="gelar_akhir">
                            </div>
                          </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Tempat Lahir
                          </label>
                          <div class="col-7">
                            <input class="m-input--pill form-control m-input" type="text" placeholder="Tempat Lahir" name="tempat_lahir" id="example-text-input">
                          </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Tanggal Lahir
                          </label>
                          <div class="col-7">
                            <div class="input-group date">
                              <input type="datepicker" class="m-input--pill form-control m-input" readonly="" placeholder="hh/bb/tttt" name="tanggal_lahir" id="m_datepicker_atas_1">
                              <div class="input-group-append">
                                <span class="input-group-text">
                                  <i class="la la-calendar"></i>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Umur
                          </label>
                          <div class="col-7">
                            <input class="m-input--pill form-control m-input" type="number" placeholder="00 tahun" name="umur" id="example-text-input">
                          </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Jenis Kelamin
                          </label>
                          <div class="col-7">
                            <div class="col-12 m-radio-inline">
																<label class="col-6 m-radio">
																	<input type="radio" name="jenis_kelamin" value="1">
																	Laki-laki
																	<span></span>
																</label>
																<label class="m-radio">
																	<input type="radio" name="jenis_kelamin" value="2">
																	Perempuan
																	<span></span>
																</label>
									          </div>
                          </div>
                        </div>
                        <div class="form-group m-form__group row">
                          <label for="example-text-input" class="col-5 col-form-label">
                            Unggah Foto Pengguna
                          </label>
                          <div class="col-7">
                            <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" id="m-dropzone-one">
                              <div class="m-dropzone__msg dz-message needsclick">
                                <h3 class="m-dropzone__msg-title">
                                  Klik atau Seret File kesini untuk mengunggah
                                </h3>
                                <span class="m-dropzone__msg-desc">
                                  tipe file dengan ekstensi : PDF, JPG)
                                </span>
                              </div>
                            </div>
                            <span class="m-form__help ">
                              tipe file dengan ekstensi : PDF, JPG)
                            </span>
                          </div>
                        </div>
                      </div>
              </div>
            </div>


            <div class="row">
									<div class="col-lg-6 m--align-left">
										<a href="dataumum" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
											<span>
												<i class="la la-arrow-left"></i>
												&nbsp;&nbsp;
												<span>
													Batal
												</span>
											</span>
										</a>
									</div>

									<div class="col-lg-6 m--align-right">
										<a href="auditor" class="btn btn-info m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
											<span>
												<span>
													Simpan &amp; Lanjutkan
												</span>
												&nbsp;&nbsp;
												<i class="la la-arrow-right"></i>
											</span>
										</a>
									</div>
						</div>
          </form>
  </div>

</div>
<!-- begin: page resource -->
<script>
$('#m_datepicker_atas_1').datepicker({
    orientation: "top left",
    todayHighlight: true,
    templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
});

Dropzone.options.mDropzoneOne = {
    paramName: "file", // The name that will be used to transfer the file
    maxFiles: 1,
    maxFilesize: 5, // MB
    addRemoveLinks: true,
    accept: function(file, done) {
        if (file.name == "justinbieber.jpg") {
            done("Naha, you don't.");
        } else {
            done();
        }
    }
};
</script>
<!-- end: page resaource -->
