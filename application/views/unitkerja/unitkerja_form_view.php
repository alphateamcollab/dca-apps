<div class="m-content">

  <div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                          </span>
          <h3 class="m-portlet__head-text">
                            Input Data Unit Kerja
                          </h3>
        </div>
      </div>
    </div>
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right">
      <div class="m-portlet__body">
        <div class="row">
          <div class="col-md-10">
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      NIP/NRP
                                    </label>
              <div class="col-7">
                <input type="number" class="m-input--pill form-control m-input"placeholder="000000000000000000" name="auditor_nip">
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Nama Lengkap
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Nama Lengkap" name="lengkap_nama">
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Instansi/Unit Kerja
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Nama Unit Kerja" name="unit_kerja_nama">
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Pemerintahan
                                    </label>
              <div class="col-7">
                <div class="col-xlg-7 m-form__group-sub">
                  <select class="m-input--pill form-control m-input" name="option">
                    <option value="">
                      --Pilih--
                    </option>
                    <option>

                    </option>
                  </select>
                </div>
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Jabatan Struktural
                                    </label>
              <div class="col-7">
                <input type="text" class="m-input--pill form-control m-input"placeholder="Jabatan Struktural" name="struktural_jabatan">
              </div>
            </div>
            <div class="form-group m-form__group row">
              <label for="example-text-input" class="col-5 col-form-label">
                                      Status
                                    </label>
              <div class="col-7">
                <div class="col-xlg-7 m-form__group-sub">
                  <select class="m-input--pill form-control m-input" name="option">
                    <option value="">
                      --Pilih--
                    </option>
                    <option>
                      Teraktivasi
                    </option>
                    <option>
                      Belum Aktivasi
                    </option>
                    <option>
                      Ditolak
                    </option>
                  </select>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 m--align-left">
          <a href="<?php echo base_url('unitkerja')?>" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
						<span>
							<i class="la la-arrow-left"></i>
							&nbsp;&nbsp;
							<span>
								Batal
							</span>
						</span>
					</a>
        </div>
        <div class="col-lg-6 m--align-right">
          <a href="<?php echo base_url('unitkerja')?>" class="btn btn-info m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
						<span>
							<span>
								Simpan
							</span>
							&nbsp;&nbsp;

						</span>
					</a>
        </div>
      </div>
    </form>
  </div>


</div>
