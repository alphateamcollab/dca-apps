<div class="m-content">

  <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            Data Pejabat Unit Krja
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href=""  data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" id="m_datatable_reload">
              <i class="la la-refresh"></i>
            </a>
          </li>
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
              <i class="la la-expand"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Search Form -->
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
          <div class="row align-items-center">
            <div class="col-xl-8 order-2 order-xl-1">
              <div class="form-group m-form__group row align-items-center">
                <div class="col-md-4">
                  <div class="m-input-icon m-input-icon--left">
                    <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Cari..." id="generalSearch">
                    <span class="m-input-icon__icon m-input-icon__icon--left">
                      <span>
                        <i class="la la-search"></i>
                      </span>
                    </span>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="m-input-icon m-input-icon--left">
                    <select type="text" class="form-control m-input m-input--air m-input--pill" id="m_form_status">
                      <option value="">
                        --Semua Status Akun--
                      </option>
                      <option value="0">
                        Belum Aktivasi
                      </option>
                      <option value="1">
                        Belum Disetujui
                      </option>
                      <option  value="2">
                        Sudah Aktivasi
                      </option>
                    </select>
                      <span class="m-input-icon__icon m-input-icon__icon--left"></span>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 order-1 order-xl-2 m--align-right m--hide">
              <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" id="m_data_tambah_unitkerja">
                <i class="la la-plus"></i>
              </button>
              <div class="m-separator m-separator--dashed d-xl-none"></div>
            </div>
          </div>
        </div>
        <!--end: Search Form -->

        <!--begin: Datatable -->
        <div class="m_datatable" id="json_data"></div>
        <!--end: Datatable -->
      </div>
  </div>

</div>
<!-- begin: Modal -->
<div class="modal fade" id="modal_form_unitkerja" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
          Tambah Data
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="420">
            <div class="m-portlet__body">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h5 class="m-widget1__title"><i class="m-nav__link-icon flaticon-user"></i>Pejabat Unit Kerja</h5>
                    <span class="m-widget1__desc">
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          NIP/NRP
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input" placeholder="NIP/NRP Auditor" name="NIP">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Nama Lengkap
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input" placeholder="Nama Lengkap" name="NamaLengkap">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Tempat Lahir
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input"placeholder="Tempat Lahir" name="TempatLahir">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Tanggal Lahir
                        </label>
                        <div class="col-7">
                          <div class="input-group date">
                            <input type="datepicker" class="m-input--pill form-control m-input" placeholder="hh/bb/tttt" name="TanggalLahir" id="m_datepicker_atas_1">
                            <div class="input-group-append">
                              <span class="input-group-text">
                                <i class="la la-calendar"></i>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Jenis Kelamin
                        </label>
                        <div class="col-7">
                          <!-- <input type="text" class="m-input--pill form-control m-input"placeholder="Tempat Lahir" name="Auditor_TempatLahir"> -->
                          <div class="m-radio-inline">
														<label class="m-radio">
															<input type="radio" name="Auditor_JenisKelamin" value="L">
															Laki - Laki
															<span></span>
														</label>
														<label class="m-radio">
															<input type="radio" name="Auditor_JenisKelamin" value="P">
															Perempuan
															<span></span>
														</label>
													</div>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Instansi - Unit Kerja
                        </label>
                        <div class="col-7">
                          <select name="JenisInstansi" class="form-control m-input m-input--air m-input--pill" id="JenisInstansi" onchange="getInstansi(this.value)" required>
                              <option value="">Pilih Jenis Instansi</option>
                              <?php foreach ($dd_jenisinstansi as $row): ?>
                                <option value="<?php echo $row->JenisInstansi_Kode; ?>"><?php echo $row->JenisInstansi_Nama; ?></option>
                              <?php endforeach; ?>
                          </select>
                          <br>
                          <select name="Pengguna_Instansi" class="form-control m-input m-input--air m-input--pill" id="KodeInstansi" onchange="getUnitKerja(this.value)" required>
                            <option value="">Silahkan Pilih</option>
                          </select>
                          <br>
                          <select name="Pengguna_UnitKerja" class="form-control m-input m-input--air m-input--pill" id="KodeUnitKerja" class="form-control" required>
                            <option value="">Silahkan Pilih</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Surel Unit Kerja
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input"placeholder="Surel Unit Kerja" name="Surel">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Telepon Unit Kerja
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input"placeholder="Nomor Telepon Unit Kerja" name="NoHP">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Peran Pejabat
                        </label>
                        <div class="col-7">
                          <div class="col-xlg-7 m-form__group-sub">
                            <select class="m-input--pill form-control m-input" name="" value="UnitKerja">
                              <option value="">
                                --Pilih--
                              </option>
                              <option>
                                Admin
                              </option>
                              <option>
                                Ketua Penilai
                              </option>
                              <option>
                                Pejabat Penialai
                              </option>
                              <option>
                                Pejabat Pengusul
                              </option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group m-form__group row m--hide">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Kode Unit Kerja
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input"placeholder="Kode Unit Kerja" name="Pengguna_UnitKerja" value="<?php echo $this->session->userdata('UnitKerja'); ?>" disabled>
                        </div>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan">Simpan</button>
       </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal -->

<script>
//-- BEGIN: dataTable remote
var DatatableJsonRemoteUnitKerja = function () {

	//-- BEGIN: fungsi_data
	var unitkerja = function () {

    //-- BEGIN: tabel data
		var tabel_data = {

      //-- BEGIN: datasource definition
			data: {
				type: 'remote',
				source: {
          read: {
            url: '<?php echo base_url('unitkerja/data')?>',
          },
        },
				pageSize: 5,
        serverPaging: true,
				serverFiltering: false,
				serverSorting: false,
			},
      //-- END: datasource definition

			//-- BEGIN: layout definition
			layout: {
				theme: 'default', // datatable theme
				class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

			//-- BEGIN: column properties
			sortable: true,
			pagination: true,
			search: {
				input: $('#generalSearch')
			},
      //-- END: column properties

			//-- BEGIN: columns definition
      columns: [
         {
          field: "NIP",
          title: "NIP/NRP",
          // width: 60,
          // textAlign: 'center',

          sortable: false
        }, {
          field: "NamaLengkap",
          title: "Nama Lengkap",
          width: 160,
          template: function (row) {
            var gelar = row.GelarDepan + '<br>' + row.NamaLengkap + '<br>' + row.GelarBelakang;
            return gelar;
  				}
        }, {
        //  field: "Role",
        //  title: "Peran Pejabat",
        //  width: 60,
        //  textAlign: 'center',
        //  sortable: false
        // }, {
         field: "NamaInstansi",
         title: "Instansi",
         // width: 60,
         // textAlign: 'center',

         sortable: false
       }, {
        field: "NamaUnitKerja",
        title: "Unit Kerja",
        // width: 60,
        // textAlign: 'center',

        sortable: false
        },{
            field: "Status",
            title: "Status Akun",
            template: function (row) {
              var status = {
                  0: {'title': 'Belum Aktivasi', 'class': 'm-badge--gray'},
                  1: {'title': 'Belum Disetujui', 'class': 'm-badge--danger'},
                  2: {'title': 'Sudah Disetujui', 'class': 'm-badge--info'}
                };
    					// callback function support for column rendering
              // var dibuatoleh = row.Status <= 2 ?
              // '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>' :
              // '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';
              return '<span class="m-badge ' + status[row.Status].class + ' m-badge--wide">' + status[row.Status].title + '</span>';
            },
            sortable: 'asc'
          }, {
           field: "Lampiran",
           title: "Surat Penunjukan",
           width: 60,
           textAlign: 'center',
           template: function (row) {
             // callback function support for column rendering
             var file = row.Lampiran == '' ?
             '<a class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-close"></i></a>' :
             '<a target="_blank" href="<?php echo base_url() ?>' + row.Lampiran + '" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" data="'+row.Lampiran+'" title="Lihat File">\
               <i class="flaticon-attachment"></i>\
             </a>';
             return file;
           }
          }, {
           field: "Aksi",
           width: 120,
           title: "Aksi",
           textAlign: 'center',
           sortable: false,
           overflow: 'visible',
           template: function (row, index, datatable) {
             var status = {
                 1: {'title': 'Setujui', 'class': 'm-btn--hover-success', 'icon': 'la-check-circle'},
                 2: {'title': 'Batalkan Persetujuan', 'class': 'm-btn--hover-warning', 'icon': 'la-times-circle'}
               };
             return '\
               <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn ' + status[row.Status].class + ' m-btn--icon m-btn--icon-only m-btn--pill m_data_persetujuan" status="'+row.Status+'" data="'+row.NIP+'" title="' + status[row.Status].title + '">\
                 <i class="la ' + status[row.Status].icon + '"></i>\
             ';
             // </a>\
             // <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill m_data_detail" data="'+row.NIP+'" title="Lihat Data">\
             // <i class="la la-info-circle "></i>\
             // </a>\
             // <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus" data="'+row.NIP+'" title="Hapus Data">\
             // <i class="la la-trash"></i>\
             // </a>\
           },
         }],
      //-- END: columns definition

  	 };
    //-- END: tabel data

    /*
    | -------------------------------------------------------------------------
    | BOF: ACTION BUTTON DEFINITION
    | -------------------------------------------------------------------------
    | # Reload Table
    | # Save Data:
    |   - Create Data
    |   - Update Data
    | # Delete Data
    */

    //-- BEGIN: variabel global
    var datatable = $('.m_datatable').mDatatable(tabel_data);
    var key = $('[name="UnitKerja_Kode"]');
    var metode_simpan;
    var id;
    //-- END: variabel global

    //-- BEGIN: dropdown filter
    $("#m_form_status").on("change", function() {
     datatable.search($(this).val(), "Status")
    }),

    $("#m_form_type").on("change", function() {
     datatable.search($(this).val(), "Type")
    }),

    $("#m_form_status, #m_form_type").selectpicker()
    //-- END: dropdown filter

    //-- BEGIN: fungsi refresh mDatatable
    $('#m_datatable_reload').on('click', function() {
      $('.m_datatable').mDatatable('reload');
    });
    //-- END: fungsi refresh mDatatable

    //-- BEGIN: fungsi simpan data
    $('#m_data_simpan').on('click', function() {
      // variable untuk menyimpan nilai input
      var nip           = $('[name="NIP"]').val();
      var nama          = $('[name="NamaLengkap"]').val();
      var gelardepan    = $('[name="GelarDepan"]').val();
      var gelarbelakang = $('[name="GelarBelakang"]').val();
      var tempatlahir   = $('[name="TempatLahir"]').val();
      var tanggallahir  = $('[name="TanggalLahir"]').val();
      var jeniskelamin  = $('[name="JenisKelamin"]').val();
      var unitkerja     = $('[name="UnitKerja_Kode"]').val();
      // var teraktivasi = $('[name=""]').val();
      // var status = $('[name=""]').val();

      // ubah teks tombol
      $('#m_data_simpan').text('Menyimpan...');
      // nonaktifkan tombol
      $('#m_data_simpan').attr('disabled',true);
      // variable untuk menyimpan url ajax
      var url;
      if(metode_simpan == 'tambah') {
          url = "<?php echo site_url('unitkerja/tambah')?>";
      } else if (metode_simpan == 'ubah') {
          url = "<?php echo site_url('unitkerja/ubah')?>";
      }
      // menambahkan data ke ajax dengan ajax
      $.ajax({
          type: "POST",
          url : url,
          dataType: "JSON",
          data : {NIP:nip, NamaLengkap:nama, GelarDepan:gelardepan, GelarBelakang:gelarbelakang, TempatLahir:tempatlahir, TanggalLahir:tanggallahir, JenisKelamin:jeniskelamin, JenisInstansi_Nama:jenisunit },
          success: function(data)
          {
            $('#modal_form_unitkerja').modal('hide');
            $('.m_datatable').mDatatable('reload');
            // ubah teks tombol
            $('#m_data_simpan').text('Simpan');
            // aktifkan tombol
            $('#m_data_simpan').attr('disabled',false);
            // pesan penambahan data berhasil
            swal({
                title: 'Berhasil!',
                text: "Data telah tersimpan.",
                type: 'success',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                animation: false,
                customClass: 'animated bounceIn'
            });
          },
          error: function ()
          {
            swal({
                title: 'Proses Simpan Gagal!',
                text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                type: 'error',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                animation: false,
                customClass: 'animated bounceIn'
            });
            // alert('Error adding / update data');
            // ubah teks tombol
            $('#m_data_simpan').text('Simpan');
            // aktifkan tombol
            $('#m_data_simpan').attr('disabled',false);
          }
      });
      return false;
    });
    //-- END: fungsi simpan data

    //-- BEGIN: fungsi tambah data
    $('#m_data_tambah_unitkerja').on('click', function() {
      metode_simpan = 'tambah';
      // aktifkan inputan primary key / foreign key
      key.attr('disabled',false);
      // reset isi form di modal
      $('#form')[0].reset();
      // tampilkan modal
      $('#modal_form_unitkerja').modal('show');
      // menetapkan judul di modal
      $('.modal-title').text('Tambah Data Baru');
    });
    //-- END: fungsi tambah data

    //-- BEGIN: fungsi ubah status persetujuan
    $(document).on('click','.m_data_persetujuan', function() {
      // variable id untuk menyimpan nilai primary key atau foreign key,
      // nilai didapat dari action button
      var id=$(this).attr('data');
      var status=$(this).attr('status');
      swal({
          title: 'Apakah anda yakin?',
          text: "Status persetujuan akan diubah!",
          type: 'question',
          showCancelButton: true,
          confirmButtonText: "<span><i class='la la-check'></i><span>Ya, lanjutkan!</span></span>",
          confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
          cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
          cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
          reverseButtons: true,
          animation: false,
          customClass: 'animated bounceIn'
      }).then(function(result){
          if (result.value) {
            // memuat data dengan ajax
            $.ajax({
              type : "POST",
              url  : "<?php echo base_url('unitkerja/persetujuan')?>",
              dataType : "JSON",
                data : {id: id, status: status},
                success: function(data){
                  $('.m_datatable').mDatatable('reload');
                }
            });
            swal({
                title: 'Status Berubah!',
                text: "Persetujuan berhasil diubah.",
                type: 'success',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                customClass: 'animated bounceIn'
            })
            // result.dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
          } else if (result.dismiss === 'cancel') {
              swal({
                  title: 'Dibatalkan',
                  text: "Proses ubah persetujuan dibatalkan",
                  type: 'error',
                  confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                  confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                  customClass: 'animated bounceIn'
              }
              )
          }
      });
      return false;
    });
    //-- END:  fungsi ubah status persetujuan

    //-- BEGIN: fungsi ubah data
    $(document).on('click','.m_data_ubah', function() {
      metode_simpan = 'ubah';
      var id=$(this).attr('data');
      // reset isi form di modal
      $('#form')[0].reset();
      // memuat data dengan ajax
      $.ajax({
          type: "GET",
          url : "<?php echo site_url('unitkerja/ambil')?>",
          dataType: "JSON",
          data : {id:id},
          success: function(data)
          {
            $.each(data,function(NIP, NamaLengkap, GelarDepan, GelarBelakang, TempatLahir, TanggalLahir, JenisKelamin, UnitKerja_Kode)
            {
              // tampilkan modal
              $('#modal_form_unitkerja').modal('show');
              // menetapkan judul di modal
              $('.modal-title').text('Ubah Data');
              // nonaktifkan inputan primary key / foreign key
              key.attr('disabled',true);
              // ambil nilai input dari parameter JSON
              key.val(data[0].UnitKerja_Kode);
              $('[name="NIP"]').val(data[0].NIP);
              $('[name="NamaLengkap"]').val(data[0].NamaLengkap);
              $('[name="NamaLengkap"]').val(data[0].NamaLengkap);
              $('[name="GelarDepan"]').val(data[0].GelarDepan);
              $('[name="GelarBelakang"]').val(data[0].GelarBelakang);
              $('[name="TempatLahir"]').val(data[0].TempatLahir);
              $('[name="TanggalLahir"]').val(data[0].TanggalLahir);
              $('[name="JenisKelamin"]').val(data[0].JenisKelamin);
              // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
            });
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            // alert('Error get data from ajax');
            swal({
                title: 'Proses Simpan Gagal!',
                text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                type: 'error',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                animation: false,
                customClass: 'animated bounceIn'
            });
          }
      });
      return false;
    });
    //-- END: fungsi ubah data

    //-- BEGIN: fungsi hapus data
    $(document).on('click','.m_data_hapus', function() {
      // variable id untuk menyimpan nilai primary key atau foreign key,
      // nilai didapat dari action button
      var id=$(this).attr('data');
      swal({
          title: 'Apakah anda yakin?',
          text: "Data yang telah dihapus tidak dapat dikembalikan!",
          type: 'question',
          showCancelButton: true,
          confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
          confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
          cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
          cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
          reverseButtons: true,
          animation: false,
          customClass: 'animated bounceIn'
      }).then(function(result){
          if (result.value) {
            // memuat data dengan ajax
            $.ajax({
              type : "POST",
              url  : "<?php echo base_url('unitkerja/hapus')?>",
              dataType : "JSON",
                data : {id: id},
                success: function(data){
                  $('.m_datatable').mDatatable('reload');
                }
            });
            swal({
                title: 'Dihapus!',
                text: "Data telah dihapus.",
                type: 'success',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                customClass: 'animated bounceIn'
            })
            // result.dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
          } else if (result.dismiss === 'cancel') {
              swal({
                  title: 'Dibatalkan',
                  text: "Proses hapus dibatalkan",
                  type: 'error',
                  confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                  confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                  customClass: 'animated bounceIn'
              }
              )
          }
      });
      return false;
    });
    //-- END:  fungsi hapus data

    /*
    | -------------------------------------------------------------------------
    | EOF: ACTION BUTTON DEFINITION
    | -------------------------------------------------------------------------
    */

  };
  //-- END: fungsi_data

	return {
		// public functions
		init: function () {
			unitkerja();
		}
	};
}();
//-- END: dataTable remote

//-- BEGIN: jQuery init
jQuery(document).ready(function () {
	DatatableJsonRemoteUnitKerja.init();
});
//-- END: jQuery init
$('#m_datepicker_atas_1').datepicker({
    orientation: "top left",
    todayHighlight: true,
    templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
});
</script>
