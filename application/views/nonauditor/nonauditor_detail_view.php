<!-- begin::Content -->
<div class="m-content">
    <div class="row">
      <div class="col-xl-4 col-lg-4">
        <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
          <div class="m-portlet__body">
            <div class="m--align-right">
              <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Ubah Status Auditor" id="m_data_ubah_status">
                <i class="flaticon-user"></i>
              </button>
              <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Atur Ulang Kata Sandi" id="m_data_aturulang_katasandi">
                <i class="flaticon-lock"></i>
              </button>
              <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Ubah Data Non-Kedinasan" id="m_data_ubah_profil">
                <i class="flaticon-edit-1"></i>
              </button>
            </div>
            <div class="m-card-profile">
              <div class="m-card-profile__title m--hide">
                Profile Saya
              </div>
              <div class="m-card-profile__pic">
                <div class="m-card-profile__pic-wrapper">
                  <img src="<?php echo base_url('assets/app/media/img/users/default.jpg')?>" alt="">
                </div>
              </div>
              <div class="m-card-profile__details">
                <span class="m-card-profile__name" > <h4> <span class="Pendidikan_GelarDepan"></span> <span class="Auditor_NamaLengkap"></span> <span class="Pendidikan_GelarBelakang"></span> </h4> </span>
                <a href="#" class="m-card-profile__email m-link">NIP. <span class="Auditor_NIP"></span></a>
              </div>
            </div>
            <div class="m-widget1 m-widget1--paddingless">
              <div class="m-portlet__body">
                <div class="m-widget1__item">
                  <div class="row m-row--no-padding align-items-center">
                    <div class="col">
                      <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-user">  </i> Status Auditor</h3>
                      <span class="m-widget1__desc isAuditor"></span>
                    </div>
                  </div>
                </div>
                <div class="m-widget1__item">
                  <div class="row m-row--no-padding align-items-center">
                    <div class="col">
                      <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-suitcase">  </i>Peran / Hak Akses</h3>
                      <span class="m-widget1__desc Role"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-8 col-lg-8 ">
        <div class="m-portlet m-portlet--full-height m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
          <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    Instansi & Unit Kerja
                  </h3>
                </div>
              </div>
              <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                    <a href="" data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Reload">
                      <i class="la la-refresh"></i>
                    </a>
                  </li>
                  <li class="m-portlet__nav-item">
                    <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                      <i class="la la-expand"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          <div class="m-portlet__body">
            <div class="m-portlet__head-caption">
              <ul class="m-portlet__nav m-portlet__head-tools pull-right">
                  <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air" data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Ubah Data" id="m_data_ubah_unit">
                    <i class="flaticon-edit-1"></i>
                  </button>
              </ul>
              <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon">
                </span>
                <h4 class="m-portlet__head-text">
                  Detail Unit Kerja
                </h4>
              </div>
            </div>
            <br>
            <div class="m-widget13">
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">
                  Jenis Lembaga :
                </span>
                <span class="m-widget13__text m-widget13__text-bolder JenisInstansi_Nama">
                </span>
              </div>
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">
                  Nama Instansi :
                </span>
                <span class="m-widget13__text m-widget13__text-bolder Instansi_Nama">
                </span>
              </div>
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">
                  Nama Unit Kerja :
                </span>
                <span class="m-widget13__text m-widget13__text-bolder UnitKerja_Nama">
                </span>
              </div>
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">
                  Alamat Kantor :
                </span>
                <span class="m-widget13__text UnitKerja_Alamat">
                </span>
              </div>
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">
                  Surat Elektronik :
                </span>
                <span class="m-widget13__text UnitKerja_Surel">
                </span>
              </div>
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">
                  Nomor Telepon Kantor :
                </span>
                <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoTlp">
                </span>
              </div>
              <div class="m-widget13__item">
                <span class="m-widget13__desc m--align-right">
                  Nomor Faximili Kantor :
                </span>
                <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoFax">
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<!-- begin: Modal Profil -->
<div class="modal fade" id="modal_form_profil" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
          Tambah Data
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_profil">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="420">
            <div class="m-portlet__body">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <span class="m-widget1__desc">
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          NIP/NRP
                        </label>
                        <div class="col-7">
                          <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="NIP/NRP Auditor" name="Auditor_NIP">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Nama Lengkap
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input" placeholder="Nama Lengkap" name="Auditor_NamaLengkap">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Surat Elektronik
                        </label>
                        <div class="col-7">
                          <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Surat Elektronik (email)" name="Pengguna_Surel">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Nomor Kontak Telp. / HP
                        </label>
                        <div class="col-7">
                          <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Kontak Telp. / HP" name="Pengguna_NoHP">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Alamat Tempat Tinggal
                        </label>
                        <div class="col-7">
                          <textarea class="form-control m-input m-input--air m-input--pill" placeholder="Alamat tinggal saat ini" name="Pengguna_Alamat" rows="4"></textarea>
                        </div>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_profil">Simpan</button>
       </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal Profil -->

<!-- begin: Modal unitkerja-->
<div class="modal fade" id="modal_form_unit" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">
          Ubah Data Unit Kerja
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form_unitkerja" action="#">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="260">
            <div class="m-portlet__body">
              <div class="form-group m-form__group row m--hide">
                <label for="example-text-input" class="col-5 col-form-label">
                  NIP / NRP
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" readonly="" placeholder="000000000000000000" name="Auditor_NIP" value="<?php echo $this->input->get('id')?>">
                </div>
              </div>
              <h4>Unit Kerja Sebelumnya</h4>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nama Jenis Instansi
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Unit Kerja" name="JenisInstansi_Nama" disabled>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nama Instansi
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Instansi" name="Instansi_Nama" disabled>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-5 col-form-label">
                  Nama Unit Kerja
                </label>
                <div class="col-7">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Unit Kerja" name="UnitKerja_Nama" disabled>
                </div>
              </div>
              <div class="m-form__heading">
                <h3 class="m-form__heading-title">
                  Pindah Ke :
                  <small class="m--font-danger">
                    <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon la la-asterisk" title="Wajib Diisi"></i>Wajib Diisi
                  </small>
                </h3>
              </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-12">
                    <!-- <div class="m-select2 m-select2--pill"> -->
                      <select name="JenisInstansi" class="form-control m-input m-input--air m-input--pill" id="JenisInstansi" onchange="getInstansi(this.value)" required >
                          <option value="NULL">Pilih Jenis Instansi</option>
                          <?php foreach ($dd_jenisinstansi as $row): ?>
                            <option value="<?php echo $row->JenisInstansi_Kode; ?>"><?php echo $row->JenisInstansi_Nama; ?></option>
                          <?php endforeach; ?>
                      </select>
                    <!-- </div> -->
                </div>
              </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-12">
                  <div class="m-select2 m-select2--pill m-select2--air">
                    <select name="Pengguna_Instansi" class="form-control  m-select2" id="KodeInstansi" onchange="getUnitKerja(this.value)" required>
                      <option value="">Silahkan Pilih</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group m-form__group row">
                <div class="col-lg-12">
                  <div class="m-select2 m-select2--pill m-select2--air">
                    <select name="UnitKerja_Kode" class="form-control m-select2" id="KodeUnitKerja" class="form-control" >
                      <option value="">Silahkan Pilih</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_unit">Simpan</button>
        </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal unitkerja-->


<script>
/* Ajax Dropdown Instansi */
function getInstansi(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_instansi');?>",
    data: {value},
    success: function(data) {
      $("#KodeUnitKerja option:gt(0)").remove();
      $("#KodeInstansi").html(data);
      $("#KodeInstansi").select2();
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
};

/* Ajax Dropdown Unit Kerja */
function getUnitKerja(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auditor/get_unitkerja');?>",
    data:{value},
    success: function(data) {
      $("#KodeUnitKerja").html(data);
      $("#KodeUnitKerja").select2();
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
}

//-- BEGIN: load data Ringkasan Profil & UnitKerja
$(document).ready(function(){
  getProfil();
});

function getProfil(){
  var id='<?php echo $this->input->get('id');?>';
  // memuat data dengan ajax
  $.ajax({
      type: "GET",
      url : "<?php echo site_url('auditor/ambil')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
          $.each(data,function(Auditor_NIP, isAuditor, Auditor_NamaLengkap, Pendidikan_GelarDepan, Pendidikan_GelarBelakang, Pangkat_Nama, Golongan_Kode, JenjangJabatan_Nama, JenisInstansi_Nama, Instansi_Nama, UnitKerja_Nama, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax, UnitKerja_Web)
        {
          // ambil nilai input dari parameter JSON
          $('.Auditor_NIP').text(data[0].Auditor_NIP);
          if (data[0].IsAuditor == 'true') {
            $('.isAuditor').text('Aktif');
          }else {
            $('.isAuditor').text('Non-Aktif');
          }
          $('.Auditor_NamaLengkap').text(data[0].Auditor_NamaLengkap);
          $('.Pendidikan_GelarDepan').text(data[0].Pendidikan_GelarDepan);
          $('.Pendidikan_GelarBelakang').text(data[0].Pendidikan_GelarBelakang);
          $('.Role').text(data[0].Role);
          $('.Pangkat_Nama').text(data[0].Pangkat_Nama);
          $('.Golongan_Kode').text(data[0].Golongan_Kode);
          $('.JenjangJabatan_Nama').text(data[0].JenjangJabatan_Nama);
          $('.JenisInstansi_Nama').text(data[0].JenisInstansi_Nama);
          $('.Instansi_Nama').text(data[0].Instansi_Nama);
          $('.UnitKerja_Nama').text(data[0].UnitKerja_Nama);
          $('.UnitKerja_Alamat').text(data[0].UnitKerja_Alamat);
          $('.UnitKerja_Surel').text(data[0].UnitKerja_Surel);
          $('.UnitKerja_NoTlp').text(data[0].UnitKerja_NoTlp);
          $('.UnitKerja_NoFax').text(data[0].UnitKerja_NoFax);
          $('.UnitKerja_Web').text(data[0].UnitKerja_Web);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
  });
  return false;
};
//-- END: load data Ringkasan Profil & UnitKerja

//-- BEGIN: variabel global
var id;
//-- END: variabel global

//-- BEGIN: fungsi simpan data Ringkasan Profil
$('#m_data_simpan_profil').on('click', function() {
  // variable untuk menyimpan nilai input
  var nip         = $('[name="Auditor_NIP"]').val();
  var nama        = $('[name="Auditor_NamaLengkap"]').val();
  var surel       = $('[name="Pengguna_Surel"]').val();
  var nohp        = $('[name="Pengguna_NoHP"]').val();
  var alamat      = $('[name="Pengguna_Alamat"]').val();
  // ubah teks tombol
  $('#m_data_simpan_profil').text('Menyimpan...');
  // nonaktifkan tombol
  $('#m_data_simpan_profil').attr('disabled',true);
  // variable untuk menyimpan url ajax
  var url = "<?php echo site_url('auditor/auditorprofil_ubah')?>";
  // menambahkan data ke ajax dengan ajax
  $.ajax({
      type: "POST",
      url : url,
      dataType: "JSON",
      data : {Auditor_NIP:nip, Auditor_NamaLengkap:nama, Pengguna_Surel:surel, Pengguna_NoHP:nohp, Pengguna_Alamat:alamat},
      success: function(data)
      {
        $('#modal_form_profil').modal('hide');
        // ubah teks tombol
        $('#m_data_simpan_profil').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan_profil').attr('disabled',false);
        // panggil fungsi getProfil
        getProfil();
        // pesan penambahan data berhasil
        swal({
            title: 'Berhasil!',
            text: "Data telah tersimpan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      },
      error: function ()
      {
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
        // alert('Error adding / update data');
        // ubah teks tombol
        $('#m_data_simpan_profil').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan_profil').attr('disabled',false);
      }
  });
  return false;
});
//-- END: fungsi simpan data Ringkasan Profil

//-- BEGIN: fungsi ubah data Ringkasan Profil
$('#m_data_ubah_profil').on('click', function() {
  metode_simpan = 'ubah';
  var id = '<?php echo $this->input->get('id');?>';
  // reset isi form di modal
  $('#form_profil')[0].reset();
  // memuat data dengan ajax
  $.ajax({
      type: "GET",
      url : "<?php echo site_url('auditor/auditorprofil_ambil')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
        $.each(data,function(Auditor_NIP, Auditor_NamaLengkap, Auditor_Surel, isAuditor, Auditor_NoHP, Auditor_Alamat, Instansi_Nama, UnitKerja_Nama, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax, UnitKerja_Web)
        {
          // tampilkan modal
          $('#modal_form_profil').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Ubah Data Non-Kedinasan');
          // nonaktifkan inputan primary key / foreign key
          $('[name="Auditor_NIP"]').attr('disabled',true);
          // ambil nilai input dari parameter JSON
          $('[name="Auditor_NIP"]').val(data[0].NIP);
          $('[name="Auditor_NamaLengkap"]').val(data[0].NamaLengkap);
          $('[name="Pengguna_Surel"]').val(data[0].Surel);
          $('[name="Pengguna_NoHP"]').val(data[0].NoHP);
          $('[name="Pengguna_Alamat"]').val(data[0].Alamat);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // alert('Error get data from ajax');
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      }
  });
  return false;
});
//-- END: fungsi ubah data Ringkasan Profil

//-- BEGIN: fungsi atur ulang katasandi
$('#m_data_aturulang_katasandi').on('click', function() {
  // variable id untuk menyimpan nilai primary key atau foreign key,
  // nilai didapat dari action button
  var id = '<?php echo $this->input->get('id');?>'
  swal({
      title: 'Apakah anda yakin mengatur ulang Kata Sandi?',
      text: "Kata Sandi yang telah diubah tidak dapat dikembalikan!, sistem akan mengirimkan Kata Sandi baru ke surat elektronik pemilik akun ini.",
      type: 'question',
      showCancelButton: true,
      confirmButtonText: "<span><i class='la la-check'></i><span>Ya, atur ulang!</span></span>",
      confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
      cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
      cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
      reverseButtons: true,
      animation: false,
      customClass: 'animated bounceIn'
  }).then(function(result){
      if (result.value) {
        // memuat data dengan ajax
        $.ajax({
          type : "POST",
          url  : "<?php echo base_url('auditor/katasandi_ubah')?>",
          dataType : "JSON",
            data : {id: id},
            success: function(data){
            // panggil fungsi getProfil
            getProfil();
            }
        });
        swal({
            title: 'Kata Sandi Di Atur Ulang!',
            text: "Kata Sandi berhasil di atur ulang, surel berhasil di kirimkan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            customClass: 'animated bounceIn'
        })
        // panggil fungsi getProfil
        getProfil();
        // result.dismiss can be 'cancel', 'overlay',
        // 'close', and 'timer'
      } else if (result.dismiss === 'cancel') {
          swal({
              title: 'Dibatalkan',
              text: "Proses atur ulang Kata Sandi dibatalkan",
              type: 'error',
              confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
              confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
              customClass: 'animated bounceIn'
          }
          )
      }
  });
  return false;
});
//-- END:  fungsi atur ulang katasandi

//-- BEGIN: fungsi ubah data Unit Kerja
$('#m_data_ubah_unit').on('click', function() {
  metode_simpan = 'ubah unitkerja';
  var id='<?php echo $this->input->get('id');?>';
  // reset isi form di modal
  $('#form_unitkerja')[0].reset();
  // memuat data dengan ajax
  $.ajax({
      type: "GET",
      url : "<?php echo site_url('auditor/auditorunitkerja_ambil')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
        $.each(data,function(UnitKerja_Kode, UnitKerja_NamaUnitInstansi, Instansi_Kode, Instansi_Nama, JenisInstansi_Kode, JenisInstansi)
        {
          // tampilkan modal
          $('#modal_form_unit').modal('show');
          // menetapkan judul di modal
          $('.modal-title').text('Perpindahan Unit Kerja');
          // nonaktifkan inputan primary key / foreign key
          // key.attr('disabled',true);
          // ambil nilai input dari parameter JSON
          // key.val(data[0].Auditor_Kode);
          $('[name="UnitKerja_Kode"]').val(data[0].UnitKerja_Kode);
          $('[name="UnitKerja_Nama"]').val(data[0].UnitKerja_NamaUnitInstansi);
          $('[name="Instansi_Kode"]').val(data[0].Instansi_Kode);
          $('[name="Instansi_Nama"]').val(data[0].Instansi_Nama);
          $('[name="JenisInstansi_Kode"]').val(data[0].JenisInstansi_Kode);
          $('[name="JenisInstansi_Nama"]').val(data[0].JenisInstansi);


          // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        // alert('Error get data from ajax');
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      }
  });
  return false;
});
//-- END: fungsi ubah data Unit Kerja

//-- BEGIN: fungsi simpan data unitkerja
$('#m_data_simpan_unit').on('click', function() {
  // variable untuk menyimpan nilai input
  var nip    = '<?php echo $this->input->get('id');?>';
  var kodeunit    = $('[name="UnitKerja_Kode"]').val();

  // ubah teks tombol
  $('#m_data_simpan').text('Menyimpan...');
  // nonaktifkan tombol
  $('#m_data_simpan').attr('disabled',true);
  // variable untuk menyimpan url ajax
  url = "<?php echo site_url('auditor/auditorunitkerja_ubah')?>";
  // menambahkan data ke ajax dengan ajax
  $.ajax({
      type: "POST",
      url : url,
      dataType: "JSON",
      data : {Auditor_NIP:nip, UnitKerja_Kode:kodeunit},
      success: function(data)
      {
        $('#modal_form_unit').modal('hide');
        getProfil();
        // $('.m_datatable').mDatatable('reload');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
        // pesan penambahan data berhasil
        swal({
            title: 'Berhasil!',
            text: "Data telah tersimpan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      },
      error: function ()
      {
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
        // alert('Error adding / update data');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
      }
  });
  return false;
});
//-- END: fungsi simpan data unitkerja
</script>
