<div class="m-content">
  <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            Tabel Data Auditor
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" id="m_datatable_reload">
              <i class="la la-refresh"></i>
            </a>
          </li>
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
              <i class="la la-expand"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>    <div class="m-portlet__body">
      <!--begin: Search Form -->
      <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <div class="row align-items-center">
          <div class="col-xl-8 order-2 order-xl-1">
            <div class="form-group m-form__group row align-items-center">
              <div class="col-md-4">
                <div class="m-input-icon m-input-icon--left">
                  <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Cari..." id="generalSearch">
                  <span class="m-input-icon__icon m-input-icon__icon--left">
                    <span>
                      <i class="la la-search"></i>
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 order-1 order-xl-2 m--align-right">
            <button type="button" class="btn btn-outline-info m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air"  data-toggle="m-tooltip" data-placement="top" title="" data-original-title="Tambah Data Baru" id="m_data_tambah">
              <i class="la la-plus"></i>
            </button>
            <div class="m-separator m-separator--dashed d-xl-none"></div>
          </div>
        </div>
      </div>
      <!--end: Search Form -->
      <!--begin: Datatable -->
      <div class="m_datatable" id="json_data"></div>
      <!--end: Datatable -->
    </div>
  </div>
</div>
<!-- end: m-content -->

<!-- begin: Modal -->
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
          Tambah Data
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <!--begin::Form-->
      <form class="m-form m-form--fit m-form--label-align-right" id="form">
        <div class="modal-body">
          <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="420">
            <div class="m-portlet__body">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h5 class="m-widget1__title"><i class="m-nav__link-icon flaticon-user"></i>Data Umum</h5>
                    <span class="m-widget1__desc">
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          NIP/NRP
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input" placeholder="NIP/NRP Auditor" name="Auditor_NIP">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Nama Lengkap
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input" placeholder="Nama Lengkap" name="Auditor_NamaLengkap">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Tempat Lahir
                        </label>
                        <div class="col-7">
                          <input type="text" class="m-input--pill form-control m-input"placeholder="Tempat Lahir" name="Auditor_TempatLahir">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                          Tanggal Lahir
                        </label>
                        <div class="col-7">
                          <div class="input-group date">
                            <input type="text" class="m-input--pill form-control m-input" placeholder="31-12-2018" name="Auditor_TglLahir" id="m_datepicker_atas_1">
                            <div class="input-group-append">
                              <span class="input-group-text">
                                <i class="la la-calendar"></i>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Jenis Kelamin
                        </label>
                        <div class="col-7">
                          <div class="m-radio-inline">
                            <label class="m-radio">
                              <input type="radio" name="Auditor_JenisKelamin" value="L">
                              Laki - Laki
                              <span></span>
                            </label>
                              <span></span>
                            <label class="m-radio m-radio--align-right">
                              <input type="radio" name="Auditor_JenisKelamin" value="P">
                              Perempuan
                              <span></span>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Surel
                        </label>
                        <div class="col-7">
                          <input type="email" class="m-input--pill form-control m-input"placeholder="Surat Elektronik (email)" name="Pengguna_Surel">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Peran / Hak Akses
                        </label>
                        <div class="col-7">
                          <!-- <input class="m-input--pill form-control m--hide" placeholder="Auditor"name="Pengguna_RoleID" value="18" disabled> -->
                          <select class="m-input--pill form-control" placeholder="Auditor"name="Pengguna_RoleID" value="Auditor">
                            <option value="NULL"> Silahkan Pilih </option>
                            <?php foreach ($dd_rolecode as $row): ?>
                              <option value="<?php echo $row->Role_ID; ?>"><?php echo $row->Role; ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Status Auditor
                        </label>
                        <div class="col-7 m-form__group-sub">
                          <select type="text" class="form-control m-input m-input--air m-input--pill" name="Status_Auditor">
                            <option value="3">
                              Pengangkatan Pertama
                            </option>
                            <option value="4">
                              Kenaikan Jabatan
                            </option>
                            <option value="5">
                              Inpassing
                            </option>
                            <option value="2">
                              Pembebasan Sementara
                            </option>
                            <option value="6">
                              Pengangkatan Kembali
                            </option>
                            <option value="0">
                              Pembebasan Tetap (Berhenti/Pensiun)
                            </option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Jenis Instansi
                        </label>
                        <div class="col-7">
                          <div class="m-select2 m-select2--pill">
                            <select name="JenisInstansi" class="form-control m-input m-input--air m-input--pill" id="JenisInstansi" onchange="getInstansi(this.value)" required >
                                <option value="NULL">Pilih Jenis Instansi</option>
                                <?php foreach ($dd_jenisinstansi as $row): ?>
                                  <option value="<?php echo $row->JenisInstansi_Kode; ?>"><?php echo $row->JenisInstansi_Nama; ?></option>
                                <?php endforeach; ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Instansi
                        </label>
                        <div class="col-7">
                          <!-- <input type="text" class="m-input--pill form-control m-input"placeholder="Unit Kerja" name="Pengguna_NamaInstansi" value="<?php echo $this->session->userdata('NamaInstansi'); ?>" disabled> -->
                          <div class="m-select2 m-select2--pill m-select2--air">
                            <select name="Pengguna_Instansi" class="form-control  m-select2" id="KodeInstansi" onchange="getUnitKerja(this.value)" required>
                              <option value="">Silahkan Pilih</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-5 col-form-label">
                        Unit Kerja
                        </label>
                        <div class="col-7">
                          <!-- <input type="text" class="m-input--pill form-control m-input"placeholder="Unit Kerja" name="Pengguna_NamaUnitKerja" value="<?php echo $this->session->userdata('NamaUnitKerja'); ?>" disabled> -->
                          <div class="m-select2 m-select2--pill m-select2--air">
                            <select name="Pengguna_UnitKerja" class="form-control m-select2" id="KodeUnitKerja" class="form-control" >
                              <option value="">Silahkan Pilih</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn m-btn--pill m-btn--air m-btn btn-metal" data-dismiss="modal">Batal</button>
          <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan">Simpan</button>
       </div>
      </form>
      <!-- end::Form -->
    </div>
  </div>
</div>
<!-- end: Modal -->


<script>


$(document).ready(function(){
  $("#JenisInstansi").select2();
});

/* Ajax Dropdown Instansi */
function getInstansi(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auth/get_instansi');?>",
    data: {value},
    success: function(data) {
      $("#KodeUnitKerja option:gt(0)").remove();
      $("#KodeInstansi").html(data);
      $("#KodeInstansi").select2();
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
};

/* Ajax Dropdown Unit Kerja */
function getUnitKerja(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auth/get_unitkerja');?>",
    data:{value},
    success: function(data) {
      $("#KodeUnitKerja").html(data);
      $("#KodeUnitKerja").select2();
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
}

//-- BEGIN: dataTable remote
var DatatableJsonRemoteAuditor = function () {

  //-- BEGIN: fungsi_data
  var auditor = function () {

    //-- BEGIN: tabel data
    var tabel_data = {

      //-- BEGIN: datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            url: '<?php echo base_url('nonauditor/datapusbin')?>',
          },
        },
        pageSize: 20,
        serverPaging: true,
        serverFiltering: false,
        serverSorting: false,
      },
      //-- END: datasource definition

      //-- BEGIN: layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        spinner: {
          state: 'info'
        },
        scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
        footer: false // display/hide footer
      },
      //-- END: layout definition

      //-- BEGIN: Translate definition
      translate:{
        records: {
          processing: 'Memuat data',
          noRecords: 'Data masih kosong'
        },
        toolbar: {
          pagination: {
            items: {
              info: 'Menampilkan {{start}} - {{end}} dari {{total}} baris data'
            }
          }
        }
      },
      //-- END: Translate definition

      //-- BEGIN: Toolbar definition
      toolbar: {
        items: {
          pagination: {
            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },
            pageSizeSelect: [5, 10, 20, 30, 50, 100]
          }
        },
      },
      //-- END: Toolbar definition

      //-- BEGIN: column properties
      sortable: true,
      pagination: true,
      search: {
        input: $('#generalSearch')
      },
      //-- END: column properties

      //-- BEGIN: columns definition
      columns: [
        {
            field: "NIP",
            title: "NIP/NRP",
            width: 160,
            sortable: false
        }, {
            field: "NamaLengkap",
            title: "Nama Lengkap",
            width: 200,
            sortable: 'asc',
            template: function (row, index, datatable) {
              var gelar = row.GelarDepan + row.NamaLengkap + row.GelarBelakang;
              return '\
                <a href="<?php echo base_url("nonauditor/detail?id='+row.NIP+'"); ?>" class="m-nav__link" data="'+row.Auditor_NIP+'" title="Lihat Data">\
                  '+gelar+'</a>\
                  ';
            }
        }, {
            field: "Role",
            title: "Peran",
            width: 80,
            textAlign: 'left',
            sortable: false
        }, {
            field: "NamaInstansi",
            title: "Instansi",
            width: 280,
            textAlign: 'left',
            sortable: false
        },  {
            field: "NamaUnitKerja",
            title: "Unit Kerja",
            width: 280,
            textAlign: 'left',
            sortable: false
        }, {
            field: "Aksi",
            width: 80,
            title: "Aksi",
            textAlign: 'center',
            sortable: false,
            overflow: 'visible',
            template: function (row, index, datatable) {
              return '\
                <a href="<?php echo base_url("nonauditor/detail?id='+row.NIP+'"); ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill m_data_detail" data="'+row.NIP+'" title="Lihat Data">\
                  <i class="la la-info-circle"></i>\
                </a>\
                <a href="javascript:void(0)" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill m_data_hapus" data="'+row.NIP+'" title="Hapus Data">\
                  <i class="la la-trash"></i>\
                </a>\
              ';
            }
        }],
      //-- END: columns definition

    };
    //-- END: tabel data

    /*
    | -------------------------------------------------------------------------
    | BOF: ACTION BUTTON DEFINITION
    | -------------------------------------------------------------------------
    | # Reload Table
    | # Save Data:
    |   - Create Data
    |   - Update Data
    | # Delete Data
    */

    //-- BEGIN: variabel global
    var datatable = $('.m_datatable').mDatatable(tabel_data);
    var key = $('[name="Auditor_Kode"]');
    var metode_simpan;
    var id;
    //-- END: variabel global

    //-- BEGIN: fungsi detail data
    $(document).on('click','.m_data_detail', function() {
      // variable id untuk menyimpan nilai primary key atau foreign key,
      // nilai didapat dari action button
      var id=$(this).attr("data");
      // window.location.href = '<?php echo base_url('nonauditor/detail')?>';
      // memuat data dengan ajax
      $.ajax({
        type : "GET",
        url  : "<?php echo base_url('nonauditor/detail')?>",
        dataType : "JSON",
          data : {id: id},
          success: function(data){
            console.log("data",data)
            // if (response.d == true) {
            //         alert("You will now be redirected.");
            //         window.location = "<?php base_url('nonauditor/detail')?>";
            //     }
            // if (data === "no_errors") {
                // window.location.href = url;
            // }
          }

      });
      // location.href ='<?php echo base_url('nonauditor/detail')?>';
      return true;
    });
    //-- END: fungsi detail data

    //-- BEGIN: fungsi refresh mDatatable
    $('#m_datatable_reload').on('click', function() {
      $('.m_datatable').mDatatable('reload');
    });
    //-- END: fungsi refresh mDatatable

    //-- BEGIN: fungsi simpan data
    $('#m_data_simpan').on('click', function() {
      // variable untuk menyimpan nilai input
      var nip           = $('[name="Auditor_NIP"]').val();
      var nama          = $('[name="Auditor_NamaLengkap"]').val();
      var tempatlahir   = $('[name="Auditor_TempatLahir"]').val();
      var tanggallahir  = $('[name="Auditor_TglLahir"]').val();
      var jeniskelamin  = $('input:radio[name="Auditor_JenisKelamin"]:checked').val();
      var status        = $('[name="Status_Auditor"]').val();
      var unitkerja     = $('[name="Pengguna_UnitKerja"]').val();
      var instansi      = $('[name="Pengguna_Instansi"]').val();
      var surel         = $('[name="Pengguna_Surel"]').val();
      var role          = $('[name="Pengguna_RoleID"]').val();


      // var teraktivasi = $('[name=""]').val();
      // var status = $('[name=""]').val();

      // ubah teks tombol
      $('#m_data_simpan').text('Menyimpan...');
      // nonaktifkan tombol
      $('#m_data_simpan').attr('disabled',true);
      // variable untuk menyimpan url ajax
      var url;
      if(metode_simpan == 'tambah') {
          url = "<?php echo site_url('nonauditor/tambah')?>";
      } else if (metode_simpan == 'ubah') {
          url = "<?php echo site_url('nonauditor/ubah')?>";
      }
      // menambahkan data ke ajax dengan ajax
      $.ajax({
          type: "POST",
          url : url,
          dataType: "JSON",
          data : {Auditor_NIP:nip, Auditor_NamaLengkap:nama, Auditor_TempatLahir:tempatlahir, Auditor_TglLahir:tanggallahir, Auditor_JenisKelamin:jeniskelamin, Status_Auditor:status, UnitKerja_Kode:unitkerja, Pengguna_Surel:surel, Pengguna_RoleID:role},
          success: function(data)
          {
            $('#modal_form').modal('hide');
            $('.m_datatable').mDatatable('reload');
            // ubah teks tombol
            $('#m_data_simpan').text('Simpan');
            // aktifkan tombol
            $('#m_data_simpan').attr('disabled',false);
            // pesan penambahan data berhasil
            swal({
                title: 'Berhasil!',
                text: "Data telah tersimpan.",
                type: 'success',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                animation: false,
                customClass: 'animated bounceIn'
            });
          },
          error: function ()
          {
            swal({
                title: 'Proses Simpan Gagal!',
                text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
                type: 'error',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                animation: false,
                customClass: 'animated bounceIn'
            });
            // alert('Error adding / update data');
            // ubah teks tombol
            $('#m_data_simpan').text('Simpan');
            // aktifkan tombol
            $('#m_data_simpan').attr('disabled',false);
          }
      });
      return false;
    });
    //-- END: fungsi simpan data

    //-- BEGIN: fungsi tambah data
    $('#m_data_tambah').on('click', function() {
      metode_simpan = 'tambah';
      // aktifkan inputan primary key / foreign key
      key.attr('disabled',false);
      // reset isi form di modal
      $('#form')[0].reset();
      // tampilkan modal
      $('#modal_form').modal('show');
      // menetapkan judul di modal
      $('.modal-title').text('Tambah Data Baru');
    });
    //-- END: fungsi tambah data

    //-- BEGIN: fungsi ubah data
    $(document).on('click','.m_data_ubah', function() {
      metode_simpan = 'ubah';
      var id=$(this).attr('data');
      // reset isi form di modal
      $('#form')[0].reset();
      // memuat data dengan ajax
      $.ajax({
          type: "GET",
          url : "<?php echo site_url('nonauditor/ambil')?>",
          dataType: "JSON",
          data : {id:id},
          success: function(data)
          {
            $.each(data,function(Auditor_NIP, Auditor_NamaLengkap, Jabatan_Nama, Pangkat_Nama, Pendidikan_Tingkat)
            {
              // tampilkan modal
              $('#modal_form').modal('show');
              // menetapkan judul di modal
              $('.modal-title').text('Ubah Data');
              // nonaktifkan inputan primary key / foreign key
              key.attr('disabled',true);
              // ambil nilai input dari parameter JSON
              key.val(data[0].Auditor_Kode);
              $('[name="Auditor_NIP"]').val(data[0].Auditor_NIP);
              $('[name="Auditor_NamaLengkap"]').val(data[0].Auditor_NamaLengkap);
              $('[name="Jabatan_Nama"]').val(data[0].Jabatan_Nama);
              $('[name="Pangkat_Nama"]').val(data[0].Pangkat_Nama);
              $('[name="Pendidikan_Tingkat"]').val(data[0].Pendidikan_Tingkat);
              // $('[name="tanggal"]').datepicker('update',data[0].tanggal);
            });
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            // alert('Error get data from ajax');
            swal({
                title: 'Proses Simpan Gagal!',
                text: "Terjadi kesalahan, mohon periksa kembali koneksi anda.",
                type: 'error',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                animation: false,
                customClass: 'animated bounceIn'
            });
          }
      });
      return false;
    });
    //-- END: fungsi ubah data

    //-- BEGIN: fungsi hapus data
    $(document).on('click','.m_data_hapus', function() {
      // variable id untuk menyimpan nilai primary key atau foreign key,
      // nilai didapat dari action button
      var id=$(this).attr('data');
      swal({
          title: 'Apakah anda yakin?',
          text: "Data yang telah dihapus tidak dapat dikembalikan!",
          type: 'question',
          showCancelButton: true,
          confirmButtonText: "<span><i class='la la-check'></i><span>Ya, hapus data!</span></span>",
          confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-danger m-btn--gradient-to-warning m-btn--icon",
          cancelButtonText: "<span><i class='la la-close'></i><span>Tidak, batalkan!</span></span>",
          cancelButtonClass: "btn m-btn--pill m-btn--air m-btn btn-metal m-btn--icon",
          reverseButtons: true,
          animation: false,
          customClass: 'animated bounceIn'
      }).then(function(result){
          if (result.value) {
            // memuat data dengan ajax
            $.ajax({
              type : "POST",
              url  : "<?php echo base_url('nonauditor/hapus')?>",
              dataType : "JSON",
                data : {id: id},
                success: function(data){
                  $('.m_datatable').mDatatable('reload');
                }
            });
            swal({
                title: 'Dihapus!',
                text: "Data telah dihapus.",
                type: 'success',
                confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                customClass: 'animated bounceIn'
            })
            // result.dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
          } else if (result.dismiss === 'cancel') {
              swal({
                  title: 'Dibatalkan',
                  text: "Proses hapus dibatalkan",
                  type: 'error',
                  confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
                  confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
                  customClass: 'animated bounceIn'
              }
              )
          }
      });
      return false;
    });
    //-- END:  fungsi hapus data
    /*
    | -------------------------------------------------------------------------
    | EOF: ACTION BUTTON DEFINITION
    | -------------------------------------------------------------------------
    */

  };
  //-- END: fungsi_data

  return {
    // public functions
    init: function () {
      auditor();
    }
  };
}();
//-- END: dataTable remote

//-- BEGIN: jQuery init
jQuery(document).ready(function () {
  DatatableJsonRemoteAuditor.init();
});
//-- END: jQuery init
$('#m_datepicker_atas_1').datepicker({
    orientation: "top left",
    todayHighlight: true,
    changeMonth: true,
    changeYear: true,
    autoclose: true,
    format: "dd/mm/yyyy",
    templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
});

</script>
