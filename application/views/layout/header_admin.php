<div class="m-container m-container--fluid m-container--full-height">
   <div class="m-stack m-stack--ver m-stack--desktop">
      <!-- BEGIN: Brand -->
        <div class="m-stack__item m-brand m-brand--skin-light">
          <div class="m-stack m-stack--ver m-stack--general">
            <div class="m-stack__item m-stack__item--middle m-stack__item--center m-brand__logo">
              <a href="<?php echo base_url('appportal')?>" class="m-brand__logo-wrapper">
                  <img alt="" src="<?php echo base_url('assets/vendors/media/img/logo/logooo.png')?>"/>
              </a>
            </div>
            <div class="m-stack__item m-stack__item--middle m-brand__tools">
              <!-- BEGIN: Responsive Aside Left Menu Toggler -->
              <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                  <span></span>
                </a>
              <!-- END -->
              <!-- BEGIN: Responsive Header Menu Toggler -->
              <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                  <span></span>
              </a>
              <!-- END -->
              <!-- BEGIN: Topbar Toggler -->
              <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                  <i class="flaticon-more"></i>
                </a>
              <!-- BEGIN: Topbar Toggler -->
            </div>
          </div>
        </div>
        <!-- END: Brand -->
        <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
          <!-- BEGIN: Horizontal Menu -->
          <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn">
              <i class="la la-close"></i>
          </button>
          <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark m--hide">
            <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" data-menu-submenu-toggle="click" data-redirect="true" aria-haspopup="true">
                  <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-settings"></i>
                      <span class="m-menu__link-text">
                        Data Referensi
                      </span>
                      <i class="m-menu__hor-arrow la la-angle-down"></i>
                      <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
                  <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                    <ul class="m-menu__subnav">
                      <li class="m-menu__item m--hide" aria-haspopup="true">
                        <a href="<?php echo base_url('ref-struktural')?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-suitcase"></i>
                            <span class="m-menu__link-text">
                              Struktural
                            </span>
                          </a>
                      </li>
                      <li class="m-menu__item  m-menu__item--submenu" data-menu-submenu-toggle="hover" data-redirect="true" aria-haspopup="true">
                        <a href="#" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon flaticon-suitcase"></i>
                            <span class="m-menu__link-text">
                              Fungsional
                            </span>
                            <i class="m-menu__hor-arrow la la-angle-right"></i>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                          </a>
                        <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
                          <span class="m-menu__arrow "></span>
                          <ul class="m-menu__subnav">
                            <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                              <a href="<?php echo base_url('ref-jabatan')?>" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                  <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                  Jabatan Fungsional
                                </span>
                              </a>
                            </li>
                            <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                              <a href="<?php echo base_url('ref-jenjangjabatan')?>" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                  <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                  Jenjang Jabatan Fungsional
                                </span>
                              </a>
                            </li>
                            <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                              <a href="<?php echo base_url('ref-pangkat')?>" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                  <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                  Jenjang Pangkat Auditor
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </li>
                      <li class="m-menu__item  m-menu__item--submenu" data-menu-submenu-toggle="hover" data-redirect="true" aria-haspopup="true">
                        <a href="#" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon flaticon-network"></i>
                            <span class="m-menu__link-text">
                              Instansi / Unit Kerja
                            </span>
                            <i class="m-menu__hor-arrow la la-angle-right"></i>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                          </a>
                        <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
                          <span class="m-menu__arrow "></span>
                          <ul class="m-menu__subnav">
                            <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                              <a href="<?php echo base_url('ref-jenisinstansi')?>" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                  <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                  Jenis Instansi
                                </span>
                              </a>
                            </li>
                            <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                              <a href="<?php echo base_url('ref-instansi')?>" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                  <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                  Data Instansi
                                </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </li>
                      <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                        <a href="<?php echo base_url('ref-golongan')?>" class="m-menu__link ">
                          <i class="m-menu__link-icon flaticon-map"></i>
                          <span class="m-menu__link-text">
                            Golongan Ruang
                          </span>
                        </a>
                      </li>
                      <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                        <a href="<?php echo base_url('ref-pendidikan')?>" class="m-menu__link ">
                          <i class="m-menu__link-icon flaticon-edit-1"></i>
                          <span class="m-menu__link-text">
                            Pendidikan
                          </span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </li>
              <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" data-menu-submenu-toggle="click" data-redirect="true" aria-haspopup="true">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <span class="m-menu__link-text">
                      Menu 2
                    </span>
                    <i class="m-menu__hor-arrow la la-angle-down"></i>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                  </a>
                <div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left" style="width:500px">
                  <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                  <div class="m-menu__subnav">
                    <ul class="m-menu__content">
                      <li class="m-menu__item">
                        <h3 class="m-menu__heading m-menu__toggle">
                            <span class="m-menu__link-text">
                              HR Reports
                            </span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                          </h3>
                        <ul class="m-menu__inner">
                          <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                  <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                  Staff Directory
                                </span>
                              </a>
                          </li>
                          <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                  <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                  Client Directory
                                </span>
                              </a>
                          </li>
                          <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                            <a href="inner.html" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                  <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                  Salary Reports
                                </span>
                              </a>
                          </li>
                        </ul>
                      </li>
                      <li class="m-menu__item">
                        <h3 class="m-menu__heading m-menu__toggle">
                            <span class="m-menu__link-text">
                              Reporting Apps
                            </span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                          </h3>
                        <ul class="m-menu__inner">
                          <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                            <a href="inner.html" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                  Report Adjusments
                                </span>
                              </a>
                          </li>
                          <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                            <a href="inner.html" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                  Sources & Mediums
                                </span>
                              </a>
                          </li>
                          <li class="m-menu__item " data-redirect="true" aria-haspopup="true">
                            <a href="inner.html" class="m-menu__link ">
                                <span class="m-menu__link-text">
                                  Reporting Settings
                                </span>
                              </a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <!-- END: Horizontal Menu -->
          <!-- BEGIN: Topbar -->
          <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
            <div class="m-stack__item m-topbar__nav-wrapper">
              <ul class="m-topbar__nav m-nav m-nav--inline">
                <li class="m-nav__item m-topbar__notifications m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-center	m-dropdown--mobile-full-width" data-dropdown-toggle="click" data-dropdown-persistent="true">
                  <a href="#" class="m-nav__link m-dropdown__toggle" id="m_topbar_notification_icon">
                      <span class="m-nav__link-badge m-badge m-badge--info">
                        0
                      </span>
                      <span class="m-nav__link-icon">
                        <i class="flaticon-alert-2"></i>
                      </span>
                    </a>
                  <div class="m-dropdown__wrapper">
                    <span class="m-dropdown__arrow m-dropdown__arrow--center"></span>
                    <div class="m-dropdown__inner">
                      <div class="m-dropdown__header m--align-center" style="background: #36a3f7; background-size: cover;">
                        <span class="m-dropdown__header-title">
                            0 Pemberitahuan
                          </span>
                        <span class="m-dropdown__header-subtitle">
                            Tidak ada pemberitahuan baru
                          </span>
                      </div>
                      <div class="m-dropdown__body">
                        <div class="m-dropdown__content">
                          <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
                            <li class="nav-item m-tabs__item">
                              <a class="nav-link m-tabs__link active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab">
                                  Alerts
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                              <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_events" role="tab">
                                  Events
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                              <a class="nav-link m-tabs__link" data-toggle="tab" href="#topbar_notifications_logs" role="tab">
                                  Logs
                                </a>
                            </li>
                          </ul>
                          <div class="tab-content">
                            <div class="tab-pane active" id="topbar_notifications_notifications" role="tabpanel">
                              <div class="m-scrollable" data-scrollable="true" data-max-height="250" data-mobile-max-height="200">
                                <div class="m-list-timeline m-list-timeline--skin-light">
                                  <div class="m-list-timeline__items">
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge -m-list-timeline__badge--state-success"></span>
                                      <span class="m-list-timeline__text">
                                          12 new users registered
                                        </span>
                                      <span class="m-list-timeline__time">
                                          Just now
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge"></span>
                                      <span class="m-list-timeline__text">
                                          System shutdown
                                          <span class="m-badge m-badge--success m-badge--wide">
                                            pending
                                          </span>
                                      </span>
                                      <span class="m-list-timeline__time">
                                          14 mins
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge"></span>
                                      <span class="m-list-timeline__text">
                                          New invoice received
                                        </span>
                                      <span class="m-list-timeline__time">
                                          20 mins
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge"></span>
                                      <span class="m-list-timeline__text">
                                          DB overloaded 80%
                                          <span class="m-badge m-badge--info m-badge--wide">
                                            settled
                                          </span>
                                      </span>
                                      <span class="m-list-timeline__time">
                                          1 hr
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge"></span>
                                      <span class="m-list-timeline__text">
                                          System error -
                                          <a href="#" class="m-link">
                                            Check
                                          </a>
                                        </span>
                                      <span class="m-list-timeline__time">
                                          2 hrs
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item m-list-timeline__item--read">
                                      <span class="m-list-timeline__badge"></span>
                                      <span href="" class="m-list-timeline__text">
                                          New order received
                                          <span class="m-badge m-badge--danger m-badge--wide">
                                            urgent
                                          </span>
                                      </span>
                                      <span class="m-list-timeline__time">
                                          7 hrs
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item m-list-timeline__item--read">
                                      <span class="m-list-timeline__badge"></span>
                                      <span class="m-list-timeline__text">
                                          Production server down
                                        </span>
                                      <span class="m-list-timeline__time">
                                          3 hrs
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge"></span>
                                      <span class="m-list-timeline__text">
                                          Production server up
                                        </span>
                                      <span class="m-list-timeline__time">
                                          5 hrs
                                        </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
                              <div class="m-scrollable" m-scrollabledata-scrollable="true" data-max-height="250" data-mobile-max-height="200">
                                <div class="m-list-timeline m-list-timeline--skin-light">
                                  <div class="m-list-timeline__items">
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                      <a href="" class="m-list-timeline__text">
                                          New order received
                                        </a>
                                      <span class="m-list-timeline__time">
                                          Just now
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge m-list-timeline__badge--state1-danger"></span>
                                      <a href="" class="m-list-timeline__text">
                                          New invoice received
                                        </a>
                                      <span class="m-list-timeline__time">
                                          20 mins
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge m-list-timeline__badge--state1-success"></span>
                                      <a href="" class="m-list-timeline__text">
                                          Production server up
                                        </a>
                                      <span class="m-list-timeline__time">
                                          5 hrs
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                      <a href="" class="m-list-timeline__text">
                                          New order received
                                        </a>
                                      <span class="m-list-timeline__time">
                                          7 hrs
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                      <a href="" class="m-list-timeline__text">
                                          System shutdown
                                        </a>
                                      <span class="m-list-timeline__time">
                                          11 mins
                                        </span>
                                    </div>
                                    <div class="m-list-timeline__item">
                                      <span class="m-list-timeline__badge m-list-timeline__badge--state1-info"></span>
                                      <a href="" class="m-list-timeline__text">
                                          Production server down
                                        </a>
                                      <span class="m-list-timeline__time">
                                          3 hrs
                                        </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                              <div class="m-stack m-stack--ver m-stack--general" style="min-height: 180px;">
                                <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                  <span class="">
                                      All caught up!
                                      <br>
                                      No new logs.
                                    </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <!-- <li class="m-nav__item m-topbar__quick-actions m-topbar__quick-actions--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push m-dropdown--mobile-full-width m-dropdown--skin-light"
                  data-dropdown-toggle="click">
                  <a href="#" class="m-nav__link m-dropdown__toggle">
                      <span class="m-nav__link-badge m-badge m-badge--dot m-badge--info m--hide"></span>
                      <span class="m-nav__link-icon">
                        <i class="flaticon-clipboard"></i>
                      </span>
                    </a>
                  <div class="m-dropdown__wrapper">
                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                    <div class="m-dropdown__inner">
                      <div class="m-dropdown__header m--align-center" style="background: #36a3f7; background-size: cover;">
                        <span class="m-dropdown__header-title">
                            Quick Actions
                          </span>
                        <span class="m-dropdown__header-subtitle">
                            Shortcuts
                          </span>
                      </div>
                      <div class="m-dropdown__body m-dropdown__body--paddingless">
                        <div class="m-dropdown__content">
                          <div class="m-scrollable" data-scrollable="false" data-max-height="380" data-mobile-max-height="200">
                            <div class="m-nav-grid m-nav-grid--skin-light">
                              <div class="m-nav-grid__row">
                                <a href="#" class="m-nav-grid__item">
                                    <i class="m-nav-grid__icon flaticon-file"></i>
                                    <span class="m-nav-grid__text">
                                      Generate Report
                                    </span>
                                  </a>
                                <a href="#" class="m-nav-grid__item">
                                    <i class="m-nav-grid__icon flaticon-time"></i>
                                    <span class="m-nav-grid__text">
                                      Add New Event
                                    </span>
                                  </a>
                              </div>
                              <div class="m-nav-grid__row">
                                <a href="#" class="m-nav-grid__item">
                                    <i class="m-nav-grid__icon flaticon-folder"></i>
                                    <span class="m-nav-grid__text">
                                      Create New Task
                                    </span>
                                  </a>
                                <a href="#" class="m-nav-grid__item">
                                    <i class="m-nav-grid__icon flaticon-clipboard"></i>
                                    <span class="m-nav-grid__text">
                                      Completed Tasks
                                    </span>
                                  </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </li> -->
                <!-- <li class="m-nav__item m-topbar__notifications--img m-dropdown m-dropdown--large m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-left	m-dropdown--mobile-full-width" data-dropdown-toggle="click" data-dropdown-persistent="false">
                  <a href="#" class="m-nav__link ">
                      <span class="m-nav__link-icon">
                        <p class="" style="width: 750px; text-align: right;"> <?php echo $this->session->userdata('NamaLengkap').' ( '.$this->session->userdata('Role').' )'; ?></p>
                      </span>
                    </a>
                </li> -->
                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" data-dropdown-toggle="click">
                    <a href="#" class="m-nav__link m-dropdown__toggle">
                    	<span class="m-topbar__username">
                    		<?php echo $this->session->userdata('NamaLengkap') ?>,&nbsp;
                    	</span>
                    	<span class="m-topbar__role">
                    		<?php echo $this->session->userdata('Role') ?>
                    	</span>
                      <span class="m-topbar__userpic">
                        <?php
                          $data = $this->session->userdata('Foto');

                          if ($data === '' || $data == NULL) {
                              echo '
                              <img src="'.base_url('assets/app/media/img/users/default.jpg').'" alt="" >
                              ';
                          } else {
                              echo '
                              <img src="'.base_url($data).'" alt="" >
                              ';
                          }
                        ?>
                        <!-- <img style="text-align: right;"src="<?php echo base_url('assets/app/media/img/users/default.jpg')?>" alt=""/> -->
                      </span>
                     </a>
                    <div class="m-dropdown__wrapper">
                      <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                      <div class="m-dropdown__inner">
                        <div class="m-dropdown__header m--align-center" style="background: #36a3f7; background-size: cover;">
                          <div class="m-card-user m-card-user--skin-dark">
                            <div class="m-card-user__pic">
                              <?php
                                $data = $this->session->userdata('Foto');

                                if ($data === '' || $data == NULL) {
                                    echo '
                                    <img src="'.base_url('assets/app/media/img/users/default.jpg').'" alt="" >
                                    ';
                                } else {
                                    echo '
                                    <img src="'.base_url($data).'" alt="" >
                                    ';
                                }
                              ?>
                              <!-- <img src="<?php echo base_url('assets/app/media/img/users/default.jpg')?>" alt="" /> -->
                            </div>
                            <div class="m-card-user__details">
                              <span class="m-card-user__name m--font-weight-500">
                                  <?php echo $this->session->userdata('NamaLengkap'); ?>
                              </span>
                              <span class="m-card-user__email m--font-weight-300 m-link">
                                  <?php echo $this->session->userdata('Surel'); ?>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="m-dropdown__body">
                          <div class="m-dropdown__content">
                            <ul class="m-nav m-nav--skin-light">
                              <li class="m-nav__section m--hide">
                                <span class="m-nav__section-text">
                                    Section
                                  </span>
                              </li>
                              <li class="m-nav__item">
                                <a href="<?php echo base_url('profil')?>" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                    <span class="m-nav__link-title">
                                      <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">
                                          Profil Saya
                                        </span>
                                      </span>
                                    </span>
                                  </a>
                              </li>
                              <li class="m-nav__item">
                                <a href="<?php echo base_url('profil/sandi')?>" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-lock"></i>
                                    <span class="m-nav__link-text">
                                      Ubah Kata Sandi
                                    </span>
                                  </a>
                              </li>
                              <li class="m-nav__separator m-nav__separator--fit"></li>
                              <!-- <li class="m-nav__item">
                                <a href="profile.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-info"></i>
                                    <span class="m-nav__link-text">
                                      FAQ
                                    </span>
                                  </a>
                              </li> -->
                              <!-- <li class="m-nav__item">
                                <a href="profile.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                    <span class="m-nav__link-text">
                                      Support
                                    </span>
                                  </a>
                              </li> -->
                              <li class="m-nav__separator m-nav__separator--fit"></li>
                              <li class="m-nav__item">
                                <a href="<?php echo base_url('auth/logout')?>" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                    Keluar
                                  </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                </li>
                <li id="m_quick_sidebar_toggle" class="m-nav__item m--hide">
                  <a href="#" class="m-nav__link m-dropdown__toggle">
                      <span class="m-nav__link-icon">
                        <i class="flaticon-logout"></i>
                      </span>
                    </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END: Topbar -->
        </div>
     </div>
 </div>

<script type="text/javascript">
setInterval(function(){

  // load_unseen_notification();

}, 5000);
</script>
