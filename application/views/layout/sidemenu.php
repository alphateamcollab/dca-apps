<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark m-aside-menu--dropdown " data-menu-vertical="true" data-menu-dropdown="true" data-menu-scrollable="true" data-menu-dropdown-timeout="500">
  <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

    <?php
    // $sql_menu = "select * from r_auth_menu where isAktif='y' and isMainMenu=0";
    $RoleGroup_ID = $this->session->userdata('RoleGroup_ID');
    $sql_menu = "SELECT*FROM r_auth_menu WHERE Menu_ID in(select Menu_ID from r_auth_akses where RoleGroup_ID='$RoleGroup_ID') and isMainMenu=0 and isAktif='y'";


    $main_menu = $this->db->query($sql_menu)->result();

    foreach ($main_menu as $menu) {
        $this->db->where('isMainMenu', $menu->Menu_ID);
        $this->db->where('isAktif', 'y');
        $submenu = $this->db->get('r_auth_menu');
        if ($submenu->num_rows()>0) {
            # menampilkan menu yang memiliki sub-menu
            echo "<li class='m-menu__item  m-menu__item--submenu ".activate_menu($menu->Menu_URL)."' aria-haspopup='true' data-menu-submenu-toggle='hover'>
          <a href='#' class='m-menu__link m-menu__toggle'>
            <span class='m-menu__item-here'></span>
            <i class='$menu->Menu_Icon'></i>
            <span class='m-menu__link-text'>
              ".ucwords($menu->Menu_Title)."
            </span>
            <i class='m-menu__ver-arrow la la-angle-right'></i>
          </a>
          <div class='m-menu__submenu m-menu__submenu--up '>
            <span class='m-menu__arrow'></span>
            <ul class='m-menu__subnav'>";
            foreach ($submenu->result() as $sub) {
                $this->db->where('isMainMenu', $sub->Menu_ID);
                $this->db->where('isAktif', 'y');
                $subsubmenu = $this->db->get('r_auth_menu');
                if ($subsubmenu->num_rows()>0) {
                    # sub-menu
                    echo "<li class='m-menu__item m-menu__item--submenu' aria-haspopup='true' data-menu-submenu-toggle='hover'>
                          <a href='".site_url($sub->Menu_URL)."' class='m-menu__link '>
                              <i class='$sub->Menu_Icon'>
                                <span></span>
                              </i>
                              <span class='m-menu__link-text'>
                               ".ucwords($sub->Menu_Title)."
                              </span>
                              <i class='m-menu__ver-arrow la la-angle-right'></i>
                            </a>
                            <div class='m-menu__submenu m-menu__submenu--up '>
                              <span class='m-menu__arrow'></span>
                              <ul class='m-menu__subnav'>";
                            foreach ($subsubmenu->result() as $subsub) {
                              echo "<li class='m-menu__item' aria-haspopup='true'>
                                    <a href='".site_url($subsub->Menu_URL)."' class='m-menu__link '>
                                        <i class='$subsub->Menu_Icon'>
                                          <span></span>
                                        </i>
                                        <span class='m-menu__link-text'>
                                         ".ucwords($subsub->Menu_Title)."
                                        </span>
                                      </a>
                                  </li>";
                            }
                            echo" </ul>
                          </div>
                        </li>";
                } else {
                    # sub-menu
                    echo "<li class='m-menu__item' aria-haspopup='true'>
                          <a href='".site_url($sub->Menu_URL)."' class='m-menu__link '>
                              <i class='$sub->Menu_Icon'>
                                <span></span>
                              </i>
                              <span class='m-menu__link-text'>
                               ".ucwords($sub->Menu_Title)."
                              </span>
                            </a>
                        </li>";
                }
            }
            echo" </ul>
          </div>
        </li>";
            // end: if
        } else {
            # menampilkan main-menu yang tidak memiliki sub-menu
            echo "<li class='m-menu__item ".activate_menu($menu->Menu_URL)."' aria-haspopup='true'>
                <a href='".site_url($menu->Menu_URL)."' class='m-menu__link '>
                  <span class='m-menu__item-here'></span>
                  <i class='$menu->Menu_Icon'></i>
                  <span class='m-menu__link-text'>
                    ".ucwords($menu->Menu_Title)."
                  </span>
                </a>
            </li>";
        }
    }
    // $ci = get_instance();
    // $modul = $ci->uri->slash_segment(1);
    ?>
  </ul>
</div>
