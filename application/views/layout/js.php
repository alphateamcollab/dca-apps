<!--begin::Base Scripts -->
<script src="<?php echo base_url('assets/vendors/base/vendors.bundle.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/vendors/base/scripts.bundle.js')?>" type="text/javascript"></script>
<!--end::Base Scripts -->

<!--begin::Page Snippets -->
<script src="<?php echo base_url('assets/app/js/dashboard.js')?>" type="text/javascript"></script>
<!--end::Page Snippets -->

<!--begin::Page Vendors -->
<script src="<?php echo base_url('assets/vendors/custom/jqupload/jqupload.js')?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/vendors/custom/jqcircle/jQuery.WCircleMenu.js')?>" type="text/javascript"></script>

<script src="//www.amcharts.com/lib/3/ammap.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/maps/js/indonesiaHigh.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
<script src="//www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/vendors/custom/amchart/themeamchart.js')?>" type="text/javascript"></script>
<!--end::Page Vendors -->
