<meta charset="utf-8">
<title>
 <?php
  if (!empty($title)) {
      echo $title;
  } else {
      echo "404";
  }
  ?>
   - Data Center Auditor
</title>
<meta name="description" content="Latest updates and statistic charts">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--begin::Web font -->
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
  WebFont.load({
    google: {
      "families": ["Montserrat:300,400,500,600,700", "Roboto:300,400,500,600,700"]
    },
    active: function() {
      sessionStorage.fonts = true;
    }
  });
</script>
<!--end::Web font -->
<!--begin::Page Vendors -->
<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors -->
<!--begin::Base Styles -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendors/base/vendors.bundle.css')?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendors/base/style.bundle.css')?>" />
<!--end::Base Styles -->
<link rel="shortcut icon" href="<?php echo base_url('assets/vendors/media/img/logo/bpkp1.png')?>" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
