<!DOCTYPE html>


<style>
.sibijak
 {
   top: 2px;
   height: 165px;
   width: auto;
   position: relative;
   padding: 15px;
  }
</style>
<html lang="id" >
  <!-- begin::Head -->
  <head>
    <meta charset="utf-8">
    <title>
     <?php
      if (!empty($title)) {
          echo $title;
      } else {
          echo "404";
      }
      ?>
    </title>
    <meta name="description" content="SISTEM INFORMASI BINA JABATAN AUDITOR BERKUALITAS - BPKP">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
      WebFont.load({
        google: {
          "families": ["Montserrat:300,400,500,600,700", "Roboto:300,400,500,600,700"]
        },
        active: function() {
          sessionStorage.fonts = true;
        }
      });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendors/base/vendors.bundle.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendors/base/style.bundle.css')?>" />
    <!--end::Base Styles -->
        <link rel="shortcut icon" href="<?php echo base_url('assets/vendors/media/img/logo/bpkp1.png')?>" />
  </head>
  <!-- end::Head -->
    <!-- end::Body -->
  <body class="m--skin--m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
      <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" style="background-image: url(<?php echo base_url('assets/vendors/media/bg-fix.jpg')?>);">
        <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
          <div class="m-login__container">
            <div class="m-login__logo">
              <!-- <a href="#"> -->
                <img src="<?php echo base_url('assets/vendors/media/img/logo/bpkp.png')?>" style="width:150px">
              <!-- </a> -->
              <!-- <div class="m-login__head"> -->
            </div>
            <div class="m-login__logo">
                <img src="<?php echo base_url('assets/vendors/media/img/logo/sibijak-logo.png')?>" style="width:100%">
            </div>
            <div class="m-login__signin">
              <form action="<?php echo base_url('auth/cheklogin'); ?>" class="m-login__form m-form col-md-6" method="post" accept-charset="utf-8">
                <?php
                $status_login = $this->session->userdata('status_login');
                if (empty($status_login)) {
                    $message = "Masukan NIP/NRP dan Kata sandi anda";
                    echo '
                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-info m-btn--gradient-from-info m-btn--gradient-to-accent fade show">
                        <div class="m-alert__icon">
                          <i class="flaticon-lock"></i>
                          <span></span>
                        </div>
                        <div class="m-alert__text">
                          <strong>
                            Selamat datang!
                          </strong>
                            '.$message.'
                        </div>
                    </div>';
                } else {
                    $message = $status_login;
                    echo $message;
                }
                ?>

                <div class="m-input-icon m-input-icon--left m-input-icon--right">
                  <input type="text" class="form-control m-input m-input--pill m-input--air" placeholder="NIP / NRP" name="NIP" autocomplete="off" required="required" autofocus="autofocus">
                  <span class="m-input-icon__icon m-input-icon__icon--left">
                    <span>
                      <i class="flaticon-avatar"></i>
                    </span>
                  </span>
                </div>
                  <br>
                <div class="m-input-icon m-input-icon--left m-input-icon--right">
                  <input type="password" class="form-control m-input m-input--pill m-input--air" placeholder="Kata sandi" name="KataSandi" required="required" autocomplete="current-password">
                  <span class="m-input-icon__icon m-input-icon__icon--left">
                    <span>
                      <i class="flaticon-lock-1"></i>
                    </span>
                  </span>
                </div>
                <div class="row m-login__form-sub  m--align-left">
                  <div class="m-login__form-right m-font m--font-boldest">
                    <a href="javascript:;" id="m_login_forget_password">
                      Lupa kata sandi ?
                    </a>
                  </div>
                </div>
                <div class="m-login__form-action">
                  <!-- <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn ">
                    Sign In
                  </button> -->
                  <button type="submit" class="btn m-btn--pill m-btn--air m-btn btn-info m-login__btn m--font-boldest">
                      Masuk
                  </button>
                </div>
              </form>
            </div>
            <div class="m-portlet__body m-login__signup">
              <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm"  data-portlet="true" id="m_portlet_tools_1">
                <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                      <span class="m-portlet__head-icon">
                        <i class="flaticon-edit"></i>
                      </span>
                      <h3 class="m-portlet__head-text">
                        Registrasi Akun Admin
                        <small>Silahkan membaca petunjuk registrasi sebelum mengisi formulir.</small>
                      </h3>
                    </div>
                  </div>
                  <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                      <li class="m-portlet__nav-item">
                        <a href=""  data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon">
                          <i class="la la-refresh"></i>
                        </a>
                      </li>
                      <li class="m-portlet__nav-item">
                        <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                          <i class="la la-expand"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <!--begin::Form-->
                <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="form">
                  <div class="m-portlet__body">
                    <div class="form-group m-form__group">
                      <div class="m-alert m-alert--outline alert alert-info alert-dismissible fade show" role="alert">
                        <!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button> -->
                        <strong>
                          Petunjuk Registrasi Akun Admin :
                        </strong>
                        <br>
                        <ol>
                            <li style="margin-bottom: 10px">Sebelum mengisi formulir registrasi, Anda wajib membuat <b>Surat Penunjukan Admin</b> yang ditandatangani oleh Pejabat minimal setingkat eselon II.
                              Contoh surat dapat <b><a class="m-link m-link--state m-link--info m--font-boldest" href="<?php echo base_url('assets/file/unduh/Surat_Penunjukan_Admin_SIBIJAK.docx');?>" target="_blank">di unduh di sini</a></b> </li>
                            <li style="margin-bottom: 10px">Isikan data Anda dengan benar, jangan lupa untuk mencentang persetujuan registrasi.</li>
                            <li style="margin-bottom: 10px">Lampirkan scan <b>Surat Penunjukan Admin</b> yang telah ditandatangani pada akhir formulir.</li>
                            <li style="margin-bottom: 10px">Setelah Akun Anda diaktivasi, Anda akan menerima surel (email) dari sistem yang menandakan bahwa Anda telah memiliki akses untuk masuk ke <b>SIBIJAK</b>.</li>
                        </ol>
                      </div>
                    </div>
                    <!-- <div class="m-form__section m-form__section--first"> -->
                      <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                          Instansi - Unit Kerja
                          <small class="m--font-danger">
                            <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon la la-asterisk" title="Wajib Diisi"></i>Wajib Diisi
                          </small>
                        </h3>
                      </div>
                      <div class="form-group m-form__group row">
                        <div class="col-lg-12">
                            <!-- <div class="m-select2 m-select2--pill"> -->
                              <select name="JenisInstansi" class="form-control m-input m-input--air m-input--pill" id="JenisInstansi" onchange="getInstansi(this.value)" required >
                                  <option value="NULL">Pilih Jenis Instansi</option>
                                  <?php foreach ($dd_jenisinstansi as $row): ?>
                                    <option value="<?php echo $row->JenisInstansi_Kode; ?>"><?php echo $row->JenisInstansi_Nama; ?></option>
                                  <?php endforeach; ?>
                              </select>
                            <!-- </div> -->
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <div class="col-lg-12">
                          <div class="m-select2 m-select2--pill m-select2--air">
                            <select name="Pengguna_Instansi" class="form-control  m-select2" id="KodeInstansi" onchange="getUnitKerja(this.value)" required>
                              <option value="">Silahkan Pilih</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <div class="col-lg-12">
                          <div class="m-select2 m-select2--pill m-select2--air">
                            <select name="UnitKerja_Kode" class="form-control m-select2" id="KodeUnitKerja" class="form-control" >
                              <option value="">Silahkan Pilih</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    <!-- </div> -->
                    <div class="m-separator m-separator--dashed m-separator--lg"></div>
                    <div class="m-form__section">
                      <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                          Data Diri
                          <small class="m--font-danger">
                            <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon la la-asterisk" title="Wajib Diisi"></i>Wajib Diisi
                          </small>
                        </h3>
                      </div>
                      <div class="form-group m-form__group row">
                        <div class="col-lg-6 m--hide">
                          <select name="Pengguna_RoleID" class="form-control m-input m-input--air m-input--pill" required>
                              <option value="5">Pilih Peran Akun</option>
                              <!-- <option value="">Pilih Peran Akun</option> -->
                              <?php foreach ($dd_rolecode as $row): ?>
                                <option value="<?php echo $row->Role_ID; ?>"><?php echo $row->Role; ?></option>
                              <?php endforeach; ?>
                          </select>
                        </div>
                        <div class="col-lg-12 m-form__group-sub">
                          <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="Nama Lengkap" name="Auditor_NamaLengkap">
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <div class="col-lg-6 m-form__group-sub">
                          <input type="text" class="form-control m-input m-input--air m-input--pill" placeholder="NIP/NRP Auditor" name="Auditor_NIP" required>
                        </div>
                        <div class="col-lg-6 m-form__group-sub">
                          <div class="m-radio-inline">
                            <label class="m-radio">
                              <input type="radio" name="Auditor_JenisKelamin" value="L">
                              Laki - Laki
                              <span></span>
                            </label>
                            <label class="m-radio">
                              <input type="radio" name="Auditor_JenisKelamin" value="P">
                              Perempuan
                              <span></span>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group m-form__group row">
                        <div class="col-lg-6 m-form__group-sub">
                          <input type="text" class="form-control m-input m-input--air m-input--pill"placeholder="Tempat Lahir" name="Auditor_TempatLahir">
                        </div>
                        <div class="col-lg-6 m-form__group-sub">
                          <input type="datepicker" class="form-control m-input m-input--air m-input--pill" placeholder="Tanggal Lahir" name="Auditor_TglLahir" id="m_datepicker_atas_1">
                        </div>
                      </div>
                    </div>
                    <div class="m-separator m-separator--dashed m-separator--lg"></div>
                    <div class="m-form__section">
                      <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                          Keamanan Akun
                          <small class="m--font-danger">
                            <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon la la-asterisk" title="Wajib Diisi"></i>Wajib Diisi
                          </small>
                        </h3>
                      </div>
                      <div class="form-group m-form__group row">
                        <div class="col-lg-4 m-form__group-sub">
                          <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Surel / Surat Elektronik (Email)" name="Pengguna_Surel" required="required">
                        </div>
                        <div class="col-lg-4 m-form__group-sub">
                          <input class="form-control m-input m-input--air m-input--pill" type="password" placeholder="Kata sandi" name="Pengguna_KataSandi" id="password" required="required" autocomplete="new-password">
                        </div>
                        <div class="col-lg-4 m-form__group-sub">
                          <input class="form-control m-input m-input--air m-input--pill" type="password" placeholder="Konfirmasi Kata sandi" name="rpassword" id="rpassword" required="required" autocomplete="new-password">
                          <span id='message'></span>
                        </div>
                      </div>
                    </div>
                    <div class="m-form__section">
                      <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                          Lampiran Surat Penunjukan
                          <small class="m--font-danger">
                            <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon la la-asterisk" title="Wajib Diisi"></i>Wajib Diisi
                          </small>
                        </h3>
                      </div>
                      <div class="form-group m-form__group row">
                        <div class="col-lg-3">
                        </div>
                        <div class="col-lg-6 ">
                          <input class="custom-file-input m-input m-input--air m-input--pill m--hide" name="Pengguna_Lampiran_Lama" type="text">
                          <div class="custom-file">
                            <input class="custom-file-input m-input m-input--air m-input--pill" id="Pengguna_Lampiran" name="Pengguna_Lampiran" type="file">
                            <label class="custom-file-label Pengguna_Lampiran" for="customFile">
                              Pilih File
                            </label>
                          </div>
                          <span class="m-form__help">
                            tipe file dengan ekstensi : PDF, JPG
                          </span>
                        </div>
                        <div class="col-lg-3">
                        </div>
                      </div>
                      <!-- <div class="form-group m-form__group row">
                        <div class="col-lg-12 ">
                          <div class="m-dropzone dropzone m-dropzone--info dz-clickable" action="inc/api/dropzone/upload.php" id="m-dropzone-one">
                            <div class="m-dropzone__msg dz-message needsclick  m--font-info">
                              <strong>
                                Unggah
                              </strong>
                              <h3 class="m-dropzone__msg-title">
                                Lampiran Surat Penunjukan
                              </h3>
                              <span class="m-dropzone__msg-desc">
                                Klik atau Seret File kesini untuk mengunggah
                                <br>
                                tipe file dengan ekstensi : PDF, JPG)
                              </span>
                            </div>
                          </div>
                        </div>
                      </div> -->
                    </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                      <div class="row text-center">
                        <div class="col-lg-12">
                          <div class="m-login__form-action">
                              <a href="#" id="m_login_signup_cancel" class="btn btn-danger m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                                Batal
                              </a>
                              <button type="submit" id="m_login_signup_submit" class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                                Daftar
                              </button>
                              &nbsp;&nbsp;
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
                <!--end::Form-->
              </div>
            </div>
            <div class="m-login__forget-password">
              <div class="m-login__head">
                <h3 class="m-login__title m--font-light m--font-boldest">
                  Lupa Kata Sandi ?
                </h3>
                <div class="m-login__desc m--font-light m--font-boldest">
                  Masukan surel untuk mengatur ulang kata sandi:
                </div>
              </div>
              <form class="m-login__form m-form col-md-6" action="">
                <div class="m-input-icon m-input-icon--left m-input-icon--right">
                  <input type="text" class="form-control m-input m-input--pill m-input--air" placeholder="Surat elektronik" name="Pengguna_Surel" id="m_email" autocomplete="off">
                  <span class="m-input-icon__icon m-input-icon__icon--left">
                    <span>
                      <i class="flaticon-email"></i>
                    </span>
                  </span>
                </div>
                <div class="m-login__form-action">
                  <button id="m_login_forget_password_submit" class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn ">
                    Kirim
                  </button>
                  &nbsp;&nbsp;
                  <button id="m_login_forget_password_cancel" class="btn btn-danger m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                    Batal
                  </button>
                </div>
              </form>
            </div>
            <div class="m-stack__item m-stack__item--center">
              <div class="m-login__account">
                <span class="m--font-primarry m--font-boldest">
                  Pengguna baru ?
                </span>
                &nbsp;&nbsp;
                <a href="javascript:;" id="m_login_signup" class="m--font-boldest m--font-primarry">
                  Registrasi Akun
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end:: Page -->
      <!--begin::Base Scripts -->
      <script src="<?php echo base_url('assets/vendors/base/vendors.bundle.js')?>" type="text/javascript"></script>
      <script src="<?php echo base_url('assets/vendors/base/scripts.bundle.js')?>" type="text/javascript"></script>
      <script src="<?php echo base_url('assets/vendors/custom/jqupload/jqupload.js')?>" type="text/javascript"></script>
      <!--end::Base Scripts -->
      <!--begin::Page Snippets -->
      <script src="<?php echo base_url('assets/snippets/pages/user/login.js')?>" type="text/javascript"></script>
    <!--end::Page Snippets -->
  </body>
  <!-- end::Body -->
</html>

<script>
$(document).ready(function(){
  // $("#JenisInstansi").select2();
});


$('#m_datepicker_atas_1').datepicker({
    orientation: "top left",
    todayHighlight: true,
    changeMonth: true,
    changeYear: true,
    autoclose: true,
    format: "dd/mm/yyyy",
    templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
});

// Dropzone.options.mDropzoneOne = {
//     paramName: "file", // The name that will be used to transfer the file
//     maxFiles: 1,
//     maxFilesize: 5, // MB
//     addRemoveLinks: true,
//     accept: function(file, done) {
//         if (file.name == "justinbieber.jpg") {
//             done("Naha, you don't.");
//         } else {
//             done();
//         }
//     }
// };

/* Ajax Dropdown Instansi */
function getInstansi(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auth/get_instansi');?>",
    data: {value},
    success: function(data) {
      $("#KodeUnitKerja option:gt(0)").remove();
      $("#KodeInstansi").html(data);
      $("#KodeInstansi").select2();
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
};

/* Ajax Dropdown Unit Kerja */
function getUnitKerja(value) {
  var value = value;
  $.ajax({
    type: "POST",
    url: "<?php echo site_url('auth/get_unitkerja');?>",
    data:{value},
    success: function(data) {
      $("#KodeUnitKerja").html(data);
      $("#KodeUnitKerja").select2();
    },

    error:function(XMLHttpRequest){
      alert(XMLHttpRequest.responseText);
    }
  });
}

var pwd;

$('#password, #rpassword').on('keyup', function () {
  if ($('#password').val() == $('#rpassword').val()) {
    $('#message').html('Kata sandi cocok').css('color', 'green');
    pwd = 'OK';
  } else
    $('#message').html('Kata sandi tidak sama!').css('color', 'red');
    pwd = 'NO';
});

//-- BEGIN: fungsi simpan data
$('#m_login_signup_submit').on('click', function() {
  // variable untuk menyimpan nilai input
  var nip         = $('[name="Auditor_NIP"]').val();
  var nama        = $('[name="Auditor_NamaLengkap"]').val();
  var tempatlahir = $('[name="Auditor_TempatLahir"]').val();
  var tanggallahir= $('[name="Auditor_TglLahir"]').val();
  var jeniskelamin= $('input:radio[name="Auditor_JenisKelamin"]:checked').val();
  var roleid      = $('[name="Pengguna_RoleID"]').val();
  var unitkerja   = $('[name="UnitKerja_Kode"]').val();
  var surel       = $('[name="Pengguna_Surel"]').val();
  var sandi       = $('[name="Pengguna_KataSandi"]').val();


  // var teraktivasi = $('[name=""]').val();
  // var status = $('[name=""]').val();

  // ubah teks tombol
  $('#m_login_signup_submit').text('Menyimpan...');
  // nonaktifkan tombol
  $('#m_login_signup_submit').attr('disabled',true);
  // variable untuk menyimpan url ajax
  var url = "<?php echo site_url('auth/tambah')?>";
  // menambahkan data ke ajax dengan ajax
  $.ajaxFileUpload({
      type: "POST",
      url : url,
      secureuri: true,
      fileElementId : 'Pengguna_Lampiran',
      dataType: "JSON",
      data : {Auditor_NIP:nip, Auditor_NamaLengkap:nama, Auditor_TempatLahir:tempatlahir, Auditor_TglLahir:tanggallahir, Auditor_JenisKelamin:jeniskelamin, UnitKerja_Kode:unitkerja, Pengguna_RoleID:roleid, Pengguna_Surel:surel, Pengguna_KataSandi:sandi},
      success: function(data)
      {
        // $('#modal_form').modal('hide');
        // $('.m_datatable').mDatatable('reload');
        // ubah teks tombol
        $('#m_login_signup_submit').text('Mengirim');
        // aktifkan tombol
        $('#m_login_signup_submit').attr('disabled',false);
        // pesan penambahan data berhasil
        swal({
            title: 'Berhasil!',
            text: "Data telah terkirim, Anda akan menerima surel jika data sudah diverifikasi oleh pusbin.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      },
      error: function ()
      {
        swal({
            title: 'Proses Pendaftaran Gagal!',
            text: "Pastikan semua data terisi dengan benar, mohon periksa kembali data anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
        // alert('Error adding / update data');
        // ubah teks tombol
        $('#m_login_signup_submit').text('Daftar');
        // aktifkan tombol
        $('#m_login_signup_submit').attr('disabled',false);
      }
  });
  // if(pwd == 'OK') {
  //
  // } else if (pwd == 'NO') {
  //     swal({
  //         title: 'Proses Pendaftaran Gagal!',
  //         text: "Kata sandi tidak cocok, mohon periksa kembali.",
  //         type: 'error',
  //         confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
  //         confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
  //         animation: false,
  //         customClass: 'animated bounceIn'
  //     });
  // }
  return false;
});
//-- END: fungsi simpan data
</script>
