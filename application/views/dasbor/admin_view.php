<!-- begin::Content -->
<div class="m-content">
  <div class="m-portlet m-portlet--collapsed m-portlet--head-sm  m-portlet--success  m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_7">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
          Data Sebaran Auditor
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" >
              <i class="la la-plus"></i>
            </a>
          </li>
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
              <i class="la la-expand"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
        <div class="m-portlet__body">
            <div id="m_jqvmap_world" class="m-jqvmap" style="height:350px;"></div>
        </div>
      </div>
  <div class="m-portlet m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
    <div class="m-portlet__body  m-portlet__body--no-padding">
      <div class="row m-row--no-padding m-row--col-separator-xl">
        <div class="col-md-12 col-lg-6 col-xl-3">
          <div class="m-portlet__body">
            <div class="m--font-info" >
              <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i> <span> </span> Auditor Ahli</h4>
            </div>
            <div class="m-widget4 m--font-info" >
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Pusat
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BPKP
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BHMN
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Daerah
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text m--font-boldest">
                        <h3>TOTAL</h3>
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info m--font-boldest">
                        <h3>0</h3>
                      </span>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3">
          <div class="m-portlet__body">
            <div class="m--font-info" >
              <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i> <span> </span> Auditor Terampil</h4>
            </div>
            <div class="m-widget4 m--font-info" >
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Pusat
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BPKP
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BHMN
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Daerah
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text m--font-boldest">
                        <h3>TOTAL</h3>
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info m--font-boldest">
                        <h3>0</h3>
                      </span>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3">
          <div class="m-portlet__body">
            <div class="m--font-info" >
              <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i> <span> </span> Penerap JFA</h4>
            </div>
            <div class="m-widget4 m--font-info" >
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Pusat
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BPKP
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BHMN
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Daerah
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text m--font-boldest">
                        <h3>TOTAL</h3>
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info m--font-boldest">
                        <h3>0</h3>
                      </span>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3">
          <div class="m-portlet__body">
            <div class="m--font-info" >
              <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i> <span> </span> Jumlah</h4>
            </div>
            <div class="m-widget4 m--font-info" >
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        Auditor Ahli
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        Auditor Terampil
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        Penerap JFA
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        0
                      </span>
                    </div>
                  </div>
                  <br>
                  <br>
                  <br>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text m--font-boldest">
                        <h3>TOTAL</h3>
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info m--font-boldest">
                        <h3>0</h3>
                      </span>
                    </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-5">
      <div class="m-portlet m-portlet--full-height m-portlet--tabs m-portlet m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" >
          <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
              <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--info" role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#auditor_nasional" role="tab" aria-selected="true">
                  <i class="flaticon-share m--hide"></i>
                  Data Auditor
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#auditor_ahli" role="tab" aria-selected="false">
                  Data Auditor Ahli
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#auditor_terampil" role="tab" aria-selected="false">
                  Data Auditor Terampil
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="tab-content">
              <div class="tab-pane active show" id="auditor_nasional">
                <div class="m-portlet__body">
                  <div id="a_nasional" style="height: 350px;"></div>
                </div>
              </div>
              <div class="tab-pane" id="auditor_ahli">
                <div class="m-portlet__body">
                  <div id="a_ahli" style="height: 350px;"></div>
                </div>
               </div>
              <div class="tab-pane" id="auditor_terampil">
                <div class="m-portlet__body">
                  <div id="a_terampil" style="height: 350px;"></div>
                </div>
              </div>
          </div>
      </div>
    </div>
    <div class="col-xl-7">
      <div class="m-portlet__head m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered "data-portlet="true" id="m_portlet_tools_1">

            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    Penerap JFA
                  </h3>
                </div>
              </div>
              <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                    <a href=""  data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" id="m_datatable_reload">
                      <i class="la la-refresh"></i>
                    </a>
                  </li>
                  <li class="m-portlet__nav-item">
                    <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                      <i class="la la-expand"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
                <div class="m-portlet__body">
                  <div id="m_amcharts_12" style="height: 350px;"></div>
                </div>
              </div>
		</div>
  </div>
</div>
<!--  end::Content-->
<script>
  var amChartsChartsNasional = function() {
    var nasional = function() {
      var chart = AmCharts.makeChart("a_nasional", {
        "type": "serial",
        "theme": "light",
        "dataProvider": [{
          "country": "APIP Pusat",
          "ahli": 10,
          "terampil": 16
        }, {
          "country": "BPKP",
          "ahli": 27,
          "terampil": 10
        }, {
          "country": "BHMN",
          "ahli":37,
          "terampil": 45
        }, {
          "country": "APIP Daerah",
          "ahli": 105,
          "terampil": 210
        }],
        "valueAxes": [ {
          "stackType": "regular",
          "title": "Auditor Nasional",
          "axisAlpha": 0,
          "gridAlpha": 0
        } ],
        "startDuration": 1,
        "graphs": [{
          "balloonText": "Auditor Ahli  : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Ahli",
          "type": "column",
          "valueField": "ahli"
        }, {
          "balloonText": "Auditor Terampil  : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Terampil",
          "type": "column",
          "valueField": "terampil"
        }],
        "plotAreaFillAlphas": 0.1,
        "depth3D": 10,
        "angle": 35,
        "categoryField": "country",
        "categoryAxis": {
        "gridPosition": "start"
        },
        "legend": {
          "horizontalGap": 10,
          "useGraphSettings": true,
          "markerSize": 10
        },
        "export": {
          "enabled": false
        }
      });
    }

    return {
      // public functions
      init: function() {
        nasional();
      }
    };
  }();
  var amChartsChartsAuditorAhli = function() {
    var ahli = function() {
      var chart = AmCharts.makeChart("a_ahli", {
        "theme": "blac",
        "type": "serial",
        "dataProvider": [{
          "country": "APIP Pusat",
          "utama": 2,
          "madya": 1,
          "muda": 2,
          "pertama": 5
        }, {
          "country": "BPKP",
          "utama": 7,
          "madya": 8,
          "muda": 6,
          "pertama": 4
        }, {
          "country": "BHMN",
          "utama": 5.5,
          "madya": 5.7,
          "muda": 7.0,
          "pertama": 5.9
        }, {
          "country": "APIP Daerah",
          "utama": 3.7,
          "madya": 4.7,
          "muda": 3.0,
          "pertama": 8.0
        }],
        "valueAxes": [{
          "stackType": "regular",
          "title": "Auditor Ahli",
          "axisAlpha": 0,
          "gridAlpha": 0
           }],
        "startDuration": 1,
        "graphs": [{
          "balloonText": "Auditor Ahli : Utama : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Utama",
          "type": "column",
          "valueField": "utama"
        }, {
          "balloonText": "Auditor Ahli : Madya : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Madya",
          "type": "column",
          "valueField": "madya"
        }, {
          "balloonText": "Auditor Ahli : Muda : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Muda",
          "type": "column",
          "valueField": "muda"
        }, {
          "balloonText": "Auditor Ahli  :Pertama : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Pertama",
          "type": "column",
          "valueField": "pertama"
        }],
        "plotAreaFillAlphas": 0.1,
        "depth3D": 10,
        "angle": 35,
        "categoryField": "country",
        "categoryAxis": {
        "gridPosition": "start"
        },
        "legend": {
          "horizontalGap": 10,
          "useGraphSettings": true,
          "markerSize": 10
        },
        "export": {
          "enabled": false
        }
      });
    }

    return {
      // public functions
      init: function() {
        ahli();
      }
    };
  }();
  var amChartsChartsAuditorTerampil = function() {
    var terampil = function() {
      var chart = AmCharts.makeChart("a_terampil", {
        "type": "serial",
        "theme": "light",
        "dataProvider": [{
          "country": "APIP Pusat",
          "penyelia": 3.5,
          "pelaksanalanjutan": 3.7,
          "pelaksana": 4.0,
        }, {
          "country": "BPKP",
          "penyelia": 2.5,
          "pelaksanalanjutan": 5.7,
          "pelaksana": 6.0,
        }, {
          "country": "BHMN",
          "penyelia": 5.5,
          "pelaksanalanjutan": 5.7,
          "pelaksana": 7.0,
        }, {
          "country": "APIP Daerah",
          "penyelia": 5.5,
          "pelaksanalanjutan": 5.7,
          "pelaksana": 7.0,
        } ],
        "valueAxes": [{
          "stackType": "regular",
          "title": "Auditor Terampil",
          "axisAlpha": 0,
          "gridAlpha": 0
        }],
        "startDuration": 1,
        "graphs": [{
          "balloonText": "Auditor Terampil  : Penyelia : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Penyelia",
          "type": "column",
          "valueField": "penyelia"
        }, {
          "balloonText": "Auditor Terampil  : Pelaksana Lanjutan : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": " Pelaksana Lanjutan",
          "type": "column",
          "valueField": "pelaksanalanjutan"
        }, {
          "balloonText": "Auditor Terampil : Pelaksana : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": " Pelaksana",
          "type": "column",
          "valueField": "pelaksana"
        }],
          "plotAreaFillAlphas": 0.1,
          "depth3D": 10,
          "angle": 35,
          "categoryField": "country",
          "categoryAxis": {
          "gridPosition": "start"
        },
        "legend": {
          "horizontalGap": 10,
          "useGraphSettings": true,
          "markerSize": 10
        },
        "export": {
          "enabled": false
        }
      });
    }

    return {
      // public functions
      init: function() {
        terampil();
      }
    };
    }();
  var amChartsChartsPenerapJFA = function() {
      var penerapjfa = function() {
          var chart = AmCharts.makeChart("m_amcharts_12", {
              "type": "pie",
              "theme": "light",
              "dataProvider": [{
                  "country": "APIP Pusat",
                  "litres": 4
              }, {
                  "country": "BPKP",
                  "litres": 5
              }, {
                  "country": "BHMN",
                  "litres": 5
              }, {
                  "country": "APIP Daerah",
                  "litres": 5
              }, {
                  "country": "Belum Diterapkan",
                  "litres": 1
              } ],
                  "valueField": "litres",
                  "titleField": "country",
                  "balloon": {
                  "fixedPosition": true
              },
                "legend": {
                  "horizontalGap": 10,
                  "useGraphSettings": false,
                  "markerSize": 10
              },

                  "export": {
                  "enabled": false
              }
          });
      }
      return {
        // public functions
        init: function() {
          penerapjfa();
        }
      };
      }();
  var jQVMap = function() {
        var sample_data = {
            "af": "16.63",
            "al": "11.58",
            "dz": "158.97",
            "ao": "85.81",
            "ag": "1.1",
            "ar": "351.02",
            "am": "8.83",
            "au": "1219.72",
            "at": "366.26",
            "az": "52.17",
            "bs": "7.54",
            "bh": "21.73",
            "bd": "105.4",
            "bb": "3.96",
            "by": "52.89",
            "be": "461.33",
            "bz": "1.43",
            "bj": "6.49",
            "bt": "1.4",
            "bo": "19.18",
            "ba": "16.2",
            "bw": "12.5",
            "br": "2023.53",
            "bn": "11.96",
            "bg": "44.84",
            "bf": "8.67",
            "bi": "1.47",
            "kh": "11.36",
            "cm": "21.88",
            "ca": "1563.66",
            "cv": "1.57",
            "cf": "2.11",
            "td": "7.59",
            "cl": "199.18",
            "cn": "5745.13",
            "co": "283.11",
            "km": "0.56",
            "cd": "12.6",
            "cg": "11.88",
            "cr": "35.02",
            "ci": "22.38",
            "hr": "59.92",
            "cy": "22.75",
            "cz": "195.23",
            "dk": "304.56",
            "dj": "1.14",
            "dm": "0.38",
            "do": "50.87",
            "ec": "61.49",
            "eg": "216.83",
            "sv": "21.8",
            "gq": "14.55",
            "er": "2.25",
            "ee": "19.22",
            "et": "30.94",
            "fj": "3.15",
            "fi": "231.98",
            "fr": "2555.44",
            "ga": "12.56",
            "gm": "1.04",
            "ge": "11.23",
            "de": "3305.9",
            "gh": "18.06",
            "gr": "305.01",
            "gd": "0.65",
            "gt": "40.77",
            "gn": "4.34",
            "gw": "0.83",
            "gy": "2.2",
            "ht": "6.5",
            "hn": "15.34",
            "hk": "226.49",
            "hu": "132.28",
            "is": "12.77",
            "in": "1430.02",
            "id": "695.06",
            "ir": "337.9",
            "iq": "84.14",
            "ie": "204.14",
            "il": "201.25",
            "it": "2036.69",
            "jm": "13.74",
            "jp": "5390.9",
            "jo": "27.13",
            "kz": "129.76",
            "ke": "32.42",
            "ki": "0.15",
            "kr": "986.26",
            "undefined": "5.73",
            "kw": "117.32",
            "kg": "4.44",
            "la": "6.34",
            "lv": "23.39",
            "lb": "39.15",
            "ls": "1.8",
            "lr": "0.98",
            "ly": "77.91",
            "lt": "35.73",
            "lu": "52.43",
            "mk": "9.58",
            "mg": "8.33",
            "mw": "5.04",
            "my": "218.95",
            "mv": "1.43",
            "ml": "9.08",
            "mt": "7.8",
            "mr": "3.49",
            "mu": "9.43",
            "mx": "1004.04",
            "md": "5.36",
            "mn": "5.81",
            "me": "3.88",
            "ma": "91.7",
            "mz": "10.21",
            "mm": "35.65",
            "na": "11.45",
            "np": "15.11",
            "nl": "770.31",
            "nz": "138",
            "ni": "6.38",
            "ne": "5.6",
            "ng": "206.66",
            "no": "413.51",
            "om": "53.78",
            "pk": "174.79",
            "pa": "27.2",
            "pg": "8.81",
            "py": "17.17",
            "pe": "153.55",
            "ph": "189.06",
            "pl": "438.88",
            "pt": "223.7",
            "qa": "126.52",
            "ro": "158.39",
            "ru": "1476.91",
            "rw": "5.69",
            "ws": "0.55",
            "st": "0.19",
            "sa": "434.44",
            "sn": "12.66",
            "rs": "38.92",
            "sc": "0.92",
            "sl": "1.9",
            "sg": "217.38",
            "sk": "86.26",
            "si": "46.44",
            "sb": "0.67",
            "za": "354.41",
            "es": "1374.78",
            "lk": "48.24",
            "kn": "0.56",
            "lc": "1",
            "vc": "0.58",
            "sd": "65.93",
            "sr": "3.3",
            "sz": "3.17",
            "se": "444.59",
            "ch": "522.44",
            "sy": "59.63",
            "tw": "426.98",
            "tj": "5.58",
            "tz": "22.43",
            "th": "312.61",
            "tl": "0.62",
            "tg": "3.07",
            "to": "0.3",
            "tt": "21.2",
            "tn": "43.86",
            "tr": "729.05",
            "tm": 0,
            "ug": "17.12",
            "ua": "136.56",
            "ae": "239.65",
            "gb": "2258.57",
            "us": "14624.18",
            "uy": "40.71",
            "uz": "37.72",
            "vu": "0.72",
            "ve": "285.21",
            "vn": "101.99",
            "ye": "30.02",
            "zm": "15.69",
            "zw": "5.57"
        };

        //== Private functions

        var setupMap = function(name) {
            var data = {
                map: 'world_en',
                backgroundColor: null,
                color: '#ffffff',
                hoverOpacity: 0.7,
                selectedColor: '#666666',
                enableZoom: true,
                showTooltip: true,
                values: sample_data,
                scaleColors: ['#C8EEFF', '#006491'],
                normalizeFunction: 'polynomial',
                onRegionOver: function(event, code) {
                    //sample to interact with map
                    if (code == 'ca') {
                        event.preventDefault();
                    }
                },
                onRegionClick: function(element, code, region) {
                    //sample to interact with map
                    var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
                    alert(message);
                }
            };

            data.map = name + '_en';

            var map = jQuery('#m_jqvmap_' + name);

            map.width(map.parent().width());
            map.vectorMap(data);
        }

        var setupMaps = function() {
            setupMap("world");
            setupMap("usa");
            setupMap("europe");
            setupMap("russia");
            setupMap("germany");
        }

        return {
            // public functions
            init: function() {
                // default charts
                setupMaps();

                mUtil.addResizeHandler(function() {
                    setupMaps();
                });
            }
        };
    }();
  jQuery(document).ready(function() {
    amChartsChartsNasional.init();
    amChartsChartsAuditorAhli.init();
    amChartsChartsAuditorTerampil.init();
    amChartsChartsPenerapJFA.init();
    jQVMap.init();
  });
</script>
