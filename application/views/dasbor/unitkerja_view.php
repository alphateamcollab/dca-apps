<!-- begin::Content -->
<div class="m-content">
  <div class="m--hide m-portlet m-portlet--collapsed m-portlet--head-sm  m-portlet--success  m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_7">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            Data Sebaran Auditor
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon" >
              <i class="la la-plus"></i>
            </a>
          </li>
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
              <i class="la la-expand"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
      <div class="m-portlet__body">
        <div id="m_jqvmap_world" class="m-jqvmap" style="height:500px;"></div>
      </div>
  </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="m-portlet m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
          <div class="m-portlet__body  m-portlet__body--no-padding ">
          <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-md-4 ">
              <div class="m-portlet__body">
                <div class="m--font-info" >
                  <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i><span></span> Auditor Ahli</h4>
                </div>
                <div class="m-widget4 m--font-info" >
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Utama
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Utama"></span>
                        </div>
                      </div>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Muda
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Muda"></span>
                        </div>
                      </div>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Madya
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Madya"></span>
                        </div>
                      </div>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Pertama
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Pertama"></span>
                        </div>
                      </div>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text m--font-boldest">
                            <h3>TOTAL</h3>
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info m--font-boldest">
                            <h3 class="Auditor_Ahli"></h3>
                          </span>
                        </div>
                      </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 ">
              <div class="m-portlet__body">
                <div class="m--font-info" >
                  <h4 class="m-widget4__title"> <i class="m-nav__link-icon flaticon-users "></i> <span></span>Auditor Terampil</h4>
                </div>
                <div class="m-widget4 m--font-info" >
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Penyelia
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Penyelia"></span>
                        </div>
                      </div>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Pelaksana Lanjutan
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Pelaksana_Lanjutan"></span>
                        </div>
                      </div>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Pelaksana
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Pelaksana"></span>
                        </div>
                      </div>
                      <br>
                      <br>
                      <br>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text m--font-boldest">
                            <h3>TOTAL</h3>
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info m--font-boldest">
                            <h3 class="Auditor_Terampil"></h3>
                          </span>
                        </div>
                      </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 ">
              <div class="m-portlet__body">
                <div class="m--font-info" >
                  <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i><span></span> Jumlah</h4>
                </div>
                <div class="m-widget4 m--font-info" >
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Auditor Terampil
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Terampil"></span>
                        </div>
                      </div>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Auditor Ahli
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Ahli"></span>
                        </div>
                      </div>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text">
                            Belum Diverifikasi
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info Auditor_Pra_Jabatan"></span>
                        </div>
                      </div>
                      <br>
                      <br>
                      <br>
                      <div class="m-widget4__item">
                        <div class="m-widget4__info">
                          <span class="m-widget4__text m--font-boldest">
                            <h3>TOTAL</h3>
                          </span>
                        </div>
                        <div class="m-widget4__ext">
                          <span class="m-widget4__number m--font-info m--font-boldest">
                            <h3 class="Jumlah_Auditor"></h3>
                          </span>
                        </div>
                      </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
      <div class="col-lg-5 col-md-12">
        <div class="m-portlet m-portlet--full-height m-portlet--tabs m-portlet m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" >
              <div class="m-portlet__head">
                <div class="m-portlet__head-tools">
                  <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--info" role="tablist">
                    <li class="nav-item m-tabs__item">
                      <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#auditor_ahli" role="tab" aria-selected="true">
                      Data Auditor Ahli
                      </a>
                    </li>
                    <li class="nav-item m-tabs__item">
                      <a class="nav-link m-tabs__link" data-toggle="tab" href="#auditor_terampil" role="tab" aria-selected="false">
                      Data Auditor Terampil
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="tab-content">
                  <div class="tab-pane active show" id="auditor_ahli">
                    <div class="m-portlet__body">
                      <div id="a_ahli" style="height: 350px;"></div>
                    </div>
                   </div>
                  <div class="tab-pane" id="auditor_terampil">
                    <div class="m-portlet__body">
                      <div id="a_terampil" style="height: 350px;"></div>
                    </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="col-lg-7 col-md-12">
            <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
                <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                      <h3 class="m-portlet__head-text">
                        Data Auditor Unit Kerja
                      </h3>
                    </div>
                  </div>
                  <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                      <li class="m-portlet__nav-item">
                        <a href=""  data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" id="m_datatable_reload">
                          <i class="la la-refresh"></i>
                        </a>
                      </li>
                      <li class="m-portlet__nav-item">
                        <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                          <i class="la la-expand"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="m-portlet__body">
                  <div id="pie" style="height: 350px;"></div>
                </div>
              </div>
      	</div>
    </div>
</div>
<!-- end::Content -->
<script>

//-- BEGIN: load Summary
$(document).ready(function() {

  var id="<?php echo $unitkerja_kode; ?>";

  $.ajax({
      type: "GET",
      url : "<?php echo site_url('dasbor/get_auditorunit')?>",
      dataType: "JSON",
      data : {id:id},
      success: function(data)
      {
          $.each(data,function(Auditor_Ahli, Auditor_Utama, Auditor_Madya, Auditor_Muda, Auditor_Pertama, Auditor_Terampil, Auditor_Penyelia, Auditor_Pelaksana_Lanjutan, Auditor_Pelaksana, Jumlah_auditor, Auditor_Pra_Jabatan)
        {
        // Begin : Data Auditor Ahli
          $('.Auditor_Ahli').text(data[0].Auditor_Ahli);
          $('.Auditor_Utama').text(data[0].Auditor_Utama);
          $('.Auditor_Madya').text(data[0].Auditor_Madya);
          $('.Auditor_Muda').text(data[0].Auditor_Muda);
          $('.Auditor_Pertama').text(data[0].Auditor_Pertama);
        // Data End :Auditor Ahli

        // Begin : Data Auditor Terampil
          $('.Auditor_Terampil').text(data[0].Auditor_Terampil);
          $('.Auditor_Penyelia').text(data[0].Auditor_Penyelia);
          $('.Auditor_Pelaksana_Lanjutan').text(data[0].Auditor_Pelaksana_Lanjutan);
          $('.Auditor_Pelaksana').text(data[0].Auditor_Pelaksana);
        // Data End :Auditor Terampil

          $('.Auditor_Pra_Jabatan').text(data[0].Auditor_Pra_Jabatan);
          $('.Jumlah_Auditor').text(data[0].Jumlah_Auditor);
        });
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error get data from ajax');
      }
  });
  return false;
});
//-- END: load Summary

var amChartsChartsAuditorAhli = function() {
    var ahli = function() {
      var chart = AmCharts.makeChart("a_ahli",
      {
          "type": "serial",
          "theme": "light",
          "dataProvider": [
            {
              "ahli": "Utama",
              "utama": <?php echo $auditor_utama; ?>,
            }, {
              "ahli": "Madya",
              "madya": <?php echo $auditor_madya; ?>,
            }, {
              "ahli": "Pertama",
              "muda": <?php echo $auditor_pertama; ?>,
            }, {
              "ahli": "Muda",
              "pertama": <?php echo $auditor_muda; ?>,
            }],
            "valueAxes": [{
              "stackType": "regular",
              "title": "Auditor Ahli",
              "axisAlpha": 0,
              "gridAlpha": 0
               }],
          "startDuration": 1,
          "graphs": [
            {
              "balloonText": "Auditor  : [[category]] : <b>[[value]]</b>",
              "fillAlphas": 0.9,
              "lineAlpha": 0.2,
              "title": "Utama",
              "type": "column",
              "valueField": "utama"
            }, {
              "balloonText": "Auditor  : [[category]] : <b>[[value]]</b>",
              "fillAlphas": 0.9,
              "lineAlpha": 0.2,
              "title": "Madya",
              "type": "column",
              "valueField": "madya"
            }, {
              "balloonText": "Auditor  : [[category]] : <b>[[value]]</b>",
              "fillAlphas": 0.9,
              "lineAlpha": 0.2,
              "title": "Muda",
              "type": "column",
              "valueField": "muda"
            }, {
              "balloonText": "Auditor  : [[category]] : <b>[[value]]</b>",
              "fillAlphas": 0.9,
              "lineAlpha": 0.2,
              "title": "Pertama",
              "type": "column",
              "valueField": "pertama"
            }
          ],
          "plotAreaFillAlphas": 0.1,
          "depth3D": 10,
          "angle": 35,
          "categoryField": "ahli",
          "categoryAxis": {
            "gridPosition": "start"
          },
          //   "legend": {
          //   "horizontalGap": 10,
          //   "useGraphSettings": true,
          //   "markerSize": 10
          // },

          "export": {
            "enabled": false
          },
      });
    }

    return {
      // public functions
      init: function() {
        ahli();
      }
    };
  }();
var amChartsChartsAuditorTerampil = function() {
    var terampil = function() {
      var chart = AmCharts.makeChart("a_terampil", {
          "type": "serial",
          "theme": "light",
          "dataProvider": [
        {
          "terampil": "Penyelia",
          "penyelia": <?php echo $auditor_penyelia; ?>,
        }, {
          "terampil": "Pelaksana Lanjutan",
          "pelaksanalanjutan": <?php echo $auditor_pelaksana_lanjutan; ?>,
        }, {
          "terampil": "Pelaksana",
          "pelaksana": <?php echo $auditor_pelaksana; ?>,
        } ],
          "valueAxes": [{
          "stackType": "regular",
          "title": "Auditor Terampil",
          "axisAlpha": 0,
          "gridAlpha": 0
        }],
          "startDuration": 1,
          "graphs": [{
          "balloonText": "Auditor Terampil : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Penyelia",
          "type": "column",
          "valueField": "penyelia"
        }, {
          "balloonText": "Auditor Terampil : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Pelaksana Lanjutan",
          "type": "column",
          "valueField": "pelaksanalanjutan"
        }, {
          "balloonText": "Auditor Terampil  : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Pelaksana",
          "type": "column",
          "valueField": "pelaksana"
        }],
          "plotAreaFillAlphas": 0.1,
          "depth3D": 10,
          "angle": 35,
          "categoryField": "terampil",
          "categoryAxis": {
          "gridPosition": "start"
        },
        //   "legend": {
        //   "horizontalGap": 10,
        //   "useGraphSettings": true,
        //   "markerSize": 10
        // },
          "export": {
          "enabled": false
        },
      });
    }

    return {
      // public functions
      init: function() {
        terampil();
      }
    };
    }();
var amChartsChartsDataAuditor = function() {
    var dataauditor = function() {
        var chart = AmCharts.makeChart("pie", {
                "type": "pie",
                "theme": "light",
                "dataProvider": [{
                "auditor": "Auditor Ahli",
                "bagian": <?php echo $auditor_ahli; ?>
            }, {
                "auditor": "Auditor Terampil",
                "bagian": <?php echo $auditor_terampil; ?>
            }, {
                "auditor": "Non Auditor",
                "bagian": <?php echo $non_auditor; ?>
            }, {
                "auditor": "Belum Ditentukan",
                "bagian": <?php echo $auditor_pra_jabatan; ?>
            } ],
                "valueField": "bagian",
                "titleField": "auditor",
                "balloon": {
                "fixedPosition": false
            },
                "legend": {
                "horizontalGap": 10,
                "useGraphSettings": false,
                "markerSize": 10,
                "fixedPosition": true
            },
                "export": {
                "enabled": false
            }
        });
    }
    return {
      // public functions
      init: function() {
        dataauditor();
      }
    };
    }();
jQuery(document).ready(function () {
  amChartsChartsAuditorAhli.init();
  amChartsChartsAuditorTerampil.init();
  amChartsChartsDataAuditor.init();
});
</script>
