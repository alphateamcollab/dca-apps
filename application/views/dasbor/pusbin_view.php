<!-- begin::Content -->
<div class="m-content">
  <div class="m-portlet m-portlet--head-sm m-portlet--success  m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_7">
    <div class="m-portlet__head">
      <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
          <h3 class="m-portlet__head-text">
            Peta Sebaran Auditor
          </h3>
        </div>
      </div>
      <div class="m-portlet__head-tools">
        <ul class="m-portlet__nav">
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon">
              <i class="la la-plus"></i>
            </a>
          </li>
          <li class="m-portlet__nav-item">
            <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
              <i class="la la-expand"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="m-portlet__body">
        <div id="m_amcharts_peta" style="height:360px;"></div>
    </div>
  </div>
  <div class="m-portlet m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
    <div class="m-portlet__body  m-portlet__body--no-padding">
      <div class="row m-row--no-padding m-row--col-separator-xl">
        <div class="col-md-12 col-lg-6 col-xl-3">
          <div class="m-portlet__body">
            <div class="m--font-info" >
              <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i> <span> </span> Auditor Ahli</h4>
            </div>
            <div class="m-widget4 m--font-info" >
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Pusat
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $APIPPusat_JumlahAuditorAhli;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BPKP
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $BPKP_JumlahAuditorAhli;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BHMN
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $BHMN_JumlahAuditorAhli;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Daerah
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $APIPDaerah_JumlahAuditorAhli;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text m--font-boldest">
                        <h3>TOTAL</h3>
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info m--font-boldest">
                        <h3><?php echo $Total_AuditorAhli;?></h3>
                      </span>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3">
          <div class="m-portlet__body">
            <div class="m--font-info" >
              <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i> <span> </span> Auditor Terampil</h4>
            </div>
            <div class="m-widget4 m--font-info" >
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Pusat
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $APIPPusat_JumlahAuditorTerampil; ?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BPKP
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $BPKP_JumlahAuditorTerampil; ?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BHMN
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $BHMN_JumlahAuditorTerampil; ?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Daerah
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $APIPDaerah_JumlahAuditorTerampil; ?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text m--font-boldest">
                        <h3>TOTAL</h3>
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info m--font-boldest">
                        <h3><?php echo $Total_AuditorTerampil; ?></h3>
                      </span>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3">
          <div class="m-portlet__body">
            <div class="m--font-info" >
              <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i> <span> </span> Penerapan JFA</h4>
            </div>
            <div class="m-widget4 m--font-info" >
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Pusat
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $APIPPusat_PenerapJFA;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BPKP
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $BPKP_PenerapJFA;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        BHMN
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $BHMN_PenerapJFA;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        APIP Daerah
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $APIPDaerah_PenerapJFA;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text m--font-boldest">
                        <h3>TOTAL</h3>
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info m--font-boldest">
                        <h3><?php echo $Total_PenerapJFA;?></h3>
                      </span>
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-lg-6 col-xl-3">
          <div class="m-portlet__body">
            <div class="m--font-info" >
              <h4 class="m-widget4__title"><i class="m-nav__link-icon flaticon-users "></i> <span> </span> Jumlah</h4>
            </div>
            <div class="m-widget4 m--font-info" >
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        Auditor Ahli
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $Total_AuditorAhli;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        Auditor Terampil
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $Total_AuditorTerampil;?>
                      </span>
                    </div>
                  </div>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text">
                        Belum Diverifikasi
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info">
                        <?php echo $Auditor_Pra_Jabatan;?>
                      </span>
                    </div>
                  </div>
                  <br>
                  <br>
                  <br>
                  <div class="m-widget4__item">
                    <div class="m-widget4__info">
                      <span class="m-widget4__text m--font-boldest">
                        <h3>TOTAL</h3>
                      </span>
                    </div>
                    <div class="m-widget4__ext">
                      <span class="m-widget4__number m--font-info m--font-boldest">
                        <h3><?php echo $Total_Auditor;?></h3>
                      </span>
                    </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xl-6 col-md-12">
      <div class="m-portlet m-portlet--full-height m-portlet--tabs m-portlet m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" >
          <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
              <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--info" role="tablist">
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link active show" data-toggle="tab" href="#auditor_nasional" role="tab" aria-selected="true">
                  <i class="flaticon-share m--hide"></i>
                  Data Auditor
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#auditor_ahli" role="tab" aria-selected="false">
                  Data Auditor Ahli
                  </a>
                </li>
                <li class="nav-item m-tabs__item">
                  <a class="nav-link m-tabs__link" data-toggle="tab" href="#auditor_terampil" role="tab" aria-selected="false">
                  Data Auditor Terampil
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="tab-content">
              <div class="tab-pane active show" id="auditor_nasional">
                <div class="m-portlet__body">
                  <div id="a_nasional" style="height: 350px;"></div>
                </div>
              </div>
              <div class="tab-pane" id="auditor_ahli">
                <div class="m-portlet__body">
                  <div id="a_ahli" style="height: 350px;"></div>
                </div>
               </div>
              <div class="tab-pane" id="auditor_terampil">
                <div class="m-portlet__body">
                  <div id="a_terampil" style="height: 350px;"></div>
                </div>
              </div>
          </div>
      </div>
    </div>
    <div class="col-xl-6 col-md-12">
      <div class="m-portlet__head m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered "data-portlet="true" id="m_portlet_tools_1">

            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    Penerapan JFA
                  </h3>
                </div>
              </div>
              <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                    <a href=""  data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" id="m_datatable_reload">
                      <i class="la la-refresh"></i>
                    </a>
                  </li>
                  <li class="m-portlet__nav-item">
                    <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                      <i class="la la-expand"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
                <div class="m-portlet__body">
                  <div id="m_amcharts_12" style="height: 350px;"></div>
                </div>
              </div>
		</div>
  </div>
</div>
<!--  end::Content-->
<script>
  var amChartsDasbor = function() {
    var nasional = function() {
      var chart = AmCharts.makeChart("a_nasional", {
        "type": "serial",
        "theme": "light",
        "dataProvider": [{
            "kelompok_instansi": "APIP Pusat",
            "ahli": "<?php echo $APIPPusat_JumlahAuditorAhli;?>",
            "terampil": "<?php echo $APIPPusat_JumlahAuditorTerampil;?>"
          }, {
            "kelompok_instansi": "BPKP",
            "ahli": "<?php echo $BPKP_JumlahAuditorAhli; ?>",
            "terampil": "<?php echo $BPKP_JumlahAuditorTerampil;?>"
          }, {
            "kelompok_instansi": "BHMN",
            "ahli":"<?php echo $BHMN_JumlahAuditorAhli;?>",
            "terampil": "<?php echo $BHMN_JumlahAuditorTerampil;?>"
          }, {
            "kelompok_instansi": "APIP Daerah",
            "ahli": "<?php echo $APIPDaerah_JumlahAuditorAhli;?>",
            "terampil": "<?php echo $APIPDaerah_JumlahAuditorTerampil;?>"
          }],
        "valueAxes": [{
            "stackType": "regular",
            "title": "Auditor Nasional",
            "axisAlpha": 0,
            "gridAlpha": 0.2
          }],
        "startDuration": 1,
        "graphs": [{
            "balloonText": "Auditor Ahli  : <b>[[value]]</b>",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "title": "Ahli",
            "type": "column",
            "valueField": "ahli"
          }, {
            "balloonText": "Auditor Terampil  : <b>[[value]]</b>",
            "fillAlphas": 0.9,
            "lineAlpha": 0.2,
            "title": "Terampil",
            "type": "column",
            "valueField": "terampil"
          }],
        "plotAreaFillAlphas": 0.1,
        "depth3D": 10,
        "angle": 45,
        "categoryField": "kelompok_instansi",
        "categoryAxis": {
              "gridPosition": "start"
            },
        "legend": {
          "horizontalGap": 10,
          "useGraphSettings": true,
          "markerSize": 10
        },
        "export": {
          "enabled": true,
                "position": "top-right"
        }
      });
    }

    var ahli = function() {
      var chart = AmCharts.makeChart("a_ahli", {
        "theme": "blac",
        "type": "serial",
        "dataProvider": [{
          "kelompok_instansi": "APIP Pusat",
          "utama": "<?php echo $APIPPusat_AuditorUtama;?>",
          "madya": "<?php echo $APIPPusat_AuditorMadya;?>",
          "muda": "<?php echo $APIPPusat_AuditorMuda;?>",
          "pertama": "<?php echo $APIPPusat_AuditorPertama;?>"
        }, {
          "kelompok_instansi": "BPKP",
          "utama": "<?php echo $BPKP_AuditorUtama;?>",
          "madya": "<?php echo $BPKP_AuditorMadya;?>",
          "muda": "<?php echo $BPKP_AuditorMuda;?>",
          "pertama": "<?php echo $BPKP_AuditorPertama;?>"
        }, {
          "kelompok_instansi": "BHMN",
          "utama": "<?php echo $BHMN_AuditorUtama;?>",
          "madya": "<?php echo $BHMN_AuditorMadya;?>",
          "muda": "<?php echo $BHMN_AuditorMuda;?>",
          "pertama": "<?php echo $BHMN_AuditorPertama;?>"
        }, {
          "kelompok_instansi": "APIP Daerah",
          "utama": "<?php echo $APIPDaerah_AuditorUtama;?>",
          "madya": "<?php echo $APIPDaerah_AuditorMadya;?>",
          "muda": "<?php echo $APIPDaerah_AuditorMuda;?>",
          "pertama": "<?php echo $APIPDaerah_AuditorPertama;?>"
        }],
        "valueAxes": [{
          "stackType": "regular",
          "title": "Auditor Ahli",
          "axisAlpha": 0,
          "gridAlpha": 0.2
           }],
        "startDuration": 1,
        "graphs": [{
          "balloonText": "Auditor Ahli : Utama : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Utama",
          "type": "column",
          "valueField": "utama"
        }, {
          "balloonText": "Auditor Ahli : Madya : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Madya",
          "type": "column",
          "valueField": "madya"
        }, {
          "balloonText": "Auditor Ahli : Muda : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Muda",
          "type": "column",
          "valueField": "muda"
        }, {
          "balloonText": "Auditor Ahli : Pertama : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Pertama",
          "type": "column",
          "valueField": "pertama"
        }],
        "plotAreaFillAlphas": 0.1,
        "depth3D": 10,
        "angle": 45,
        "categoryField": "kelompok_instansi",
        "categoryAxis": {
        "gridPosition": "start"
        },
        "legend": {
          "horizontalGap": 10,
          "useGraphSettings": true,
          "markerSize": 10
        },
        "export": {
          "enabled": true
        }
      });
    }

    var terampil = function() {
      var chart = AmCharts.makeChart("a_terampil", {
        "type": "serial",
        "theme": "light",
        "dataProvider": [{
          "kelompok_instansi": "APIP Pusat",
          "penyelia": "<?php echo $APIPPusat_AuditorPenyelia;?>",
          "pelaksanalanjutan": "<?php echo $APIPPusat_AuditorPelaksanaLanjutan;?>",
          "pelaksana": "<?php echo $APIPPusat_AuditorPelaksana;?>",
        }, {
          "kelompok_instansi": "BPKP",
          "penyelia": "<?php echo $BPKP_AuditorPenyelia;?>",
          "pelaksanalanjutan": "<?php echo $BPKP_AuditorPelaksanaLanjutan;?>",
          "pelaksana": "<?php echo $BPKP_AuditorPelaksana;?>",
        }, {
          "kelompok_instansi": "BHMN",
          "penyelia": "<?php echo $BHMN_AuditorPenyelia;?>",
          "pelaksanalanjutan": "<?php echo $BHMN_AuditorPelaksanaLanjutan;?>",
          "pelaksana": "<?php echo $BHMN_AuditorPelaksana;?>",
        }, {
          "kelompok_instansi": "APIP Daerah",
          "penyelia": "<?php echo $APIPDaerah_AuditorPenyelia;?>",
          "pelaksanalanjutan": "<?php echo $APIPDaerah_AuditorPelaksanaLanjutan;?>",
          "pelaksana": "<?php echo $APIPDaerah_AuditorPelaksana;?>",
        } ],
        "valueAxes": [{
          "stackType": "regular",
          "title": "Auditor Terampil",
          "axisAlpha": 0,
          "gridAlpha": 0.2
        }],
        "startDuration": 1,
        "graphs": [{
          "balloonText": "Auditor Terampil  : Penyelia : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": "Penyelia",
          "type": "column",
          "valueField": "penyelia"
        }, {
          "balloonText": "Auditor Terampil  : Pelaksana Lanjutan : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": " Pelaksana Lanjutan",
          "type": "column",
          "valueField": "pelaksanalanjutan"
        }, {
          "balloonText": "Auditor Terampil : Pelaksana : <b>[[value]]</b>",
          "fillAlphas": 0.9,
          "lineAlpha": 0.2,
          "title": " Pelaksana",
          "type": "column",
          "valueField": "pelaksana"
        }],
          "plotAreaFillAlphas": 0.1,
          "depth3D": 10,
          "angle": 45,
          "categoryField": "kelompok_instansi",
          "categoryAxis": {
          "gridPosition": "start"
        },
        "legend": {
          "horizontalGap": 10,
          "useGraphSettings": true,
          "markerSize": 10
        },
        "export": {
          "enabled": true
        }
      });
    }

    var penerapjfa = function() {
          var chart = AmCharts.makeChart("m_amcharts_12", {
              "type": "pie",
              "theme": "light",
              "dataProvider": [{
                  "kelompok_instansi": "APIP Pusat",
                  "litres":"<?php echo $APIPPusat_PenerapJFA;?>",
              }, {
                  "kelompok_instansi": "BPKP",
                  "litres": "<?php echo $BPKP_PenerapJFA;?>",
              }, {
                  "kelompok_instansi": "BHMN",
                  "litres": "<?php echo $BHMN_PenerapJFA;?>",
              }, {
                  "kelompok_instansi": "APIP Daerah",
                  "litres": "<?php echo $APIPDaerah_PenerapJFA;?>",
              }, {
                  "kelompok_instansi": "Belum Menerapkan",
                  "litres": 0,
              } ],
                  "valueField": "litres",
                  "titleField": "kelompok_instansi",
                  "balloon": {
                  "fixedPosition": true
              },
                "legend": {
                  "horizontalGap": 10,
                  "useGraphSettings": false,
                  "markerSize": 10
              },

                  "export": {
                  "enabled": true
              }
          });
      }

    var peta_prov = function() {
        var map = AmCharts.makeChart("m_amcharts_peta", {
            "type": "map",
            "theme": "light",
            "colorSteps": 100,

            "dataProvider": {
                "map": "indonesiaHigh",
                "areas": [
                  {
                    // Aceh
                      "id": "ID-AC",
                      "value": "<?php echo $k09020101 ?>"
                  }, {
                    // Sumatera Utara
                      "id": "ID-SU",
                      "value": "<?php echo $k09020102 ?>"
                  }, {
                    // Sumatera Barat
                      "id": "ID-SB",
                      "value": "<?php echo $k09020103 ?>"
                  }, {
                    // Riau
                      "id": "ID-RI",
                      "value": "<?php echo $k09020104 ?>"
                  }, {
                    // Kepulauan Riau
                      "id": "ID-KR",
                      "value": "<?php echo $k09020105 ?>"
                  }, {
                    // Jambi
                      "id": "ID-JA",
                      "value": "<?php echo $k09020106 ?>"
                  }, {
                    // Bengkulu
                      "id": "ID-BE",
                      "value": "<?php echo $k09020107 ?>"
                  }, {
                    // Sumatera Selatan
                      "id": "ID-SS",
                      "value": "<?php echo $k09020108 ?>"
                  }, {
                    // Bangka Belitung
                      "id": "ID-BB",
                      "value": "<?php echo $k09020109 ?>"
                  }, {
                    // Lampung
                      "id": "ID-LA",
                      "value": "<?php echo $k09020110 ?>"
                  }, {
                    // Banten
                      "id": "ID-BT",
                      "value": "<?php echo $k09020201 ?>"
                  }, {
                    // Jakarta Raya
                      "id": "ID-JK",
                      "value": "<?php echo $k09020202 ?>"
                  }, {
                    // Jawa Barat
                      "id": "ID-JB",
                      "value": "<?php echo $k09020203 ?>"
                  }, {
                    // Jawa Tengah
                      "id": "ID-JT",
                      "value": "<?php echo $k09020204 ?>"
                  }, {
                    // Yogyakarta
                      "id": "ID-YO",
                      "value": "<?php echo $k09020205 ?>"
                  }, {
                    // Jawa Timur
                      "id": "ID-JI",
                      "value": "<?php echo $k09020206 ?>"
                  }, {
                    // Bali
                      "id": "ID-BA",
                      "value": "<?php echo $k09020300 ?>"
                  }, {
                    // Nusa Tenggara Barat
                      "id": "ID-NB",
                      "value": "<?php echo $k09020401 ?>"
                  }, {
                    // Nusa Tenggara Timur
                      "id": "ID-NT",
                      "value": "<?php echo $k09020402 ?>"
                  }, {
                    // Kalimantan Barat
                      "id": "ID-KB",
                      "value": "<?php echo $k09020501 ?>"
                  }, {
                    // Kalimantan Tengah
                      "id": "ID-KT",
                      "value": "<?php echo $k09020502 ?>"
                  }, {
                    // Kalimantan Selatan
                      "id": "ID-KS",
                      "value": "<?php echo $k09020503 ?>"
                  }, {
                    // Kalimantan Timur
                      "id": "ID-KI",
                      "value": "<?php echo $k09020504 ?>"
                  }, {
                    // Kalimantan Utara
                      "id": "ID-KU",
                      "value": "<?php echo $k09020504 ?>"
                  }, {
                    // Sulawesi Utara
                      "id": "ID-SA",
                      "value": "<?php echo $k09020601 ?>"
                  }, {
                    // Gorontalo
                      "id": "ID-GO",
                      "value": "<?php echo $k09020602 ?>"
                  }, {
                    // Sulawesi Barat
                      "id": "ID-SR",
                      "value": "<?php echo $k09020603 ?>"
                  }, {
                    // Sulawesi Tengah
                      "id": "ID-ST",
                      "value": "<?php echo $k09020604 ?>"
                  }, {
                    // Sulawesi Tenggara
                      "id": "ID-SG",
                      "value": "<?php echo $k09020605 ?>"
                  }, {
                    // Sulawesi Selatan
                      "id": "ID-SN",
                      "value": "<?php echo $k09020606 ?>"
                  }, {
                    // Maluku Utara
                      "id": "ID-MU",
                      "value": "<?php echo $k09020701 ?>"
                  }, {
                    // Maluku
                      "id": "ID-MA",
                      "value": "<?php echo $k09020702 ?>"
                  }, {
                    // Papua Barat
                      "id": "ID-PB",
                      "value": "<?php echo $k09020801 ?>"
                  }, {
                    // Papua
                      "id": "ID-PA",
                      "value": "<?php echo $k09020802 ?>"
                  }
                ]
            },

            "areasSettings": {
                "autoZoom": true,
                "balloonText": "<h6 class='m-widget4__title'>Provinsi [[title]]</h6> <br> [[value]] Auditor",
                "selectedColor": "#36a3f7"
            },

            "valueLegend": {
                "right": 6,
                "minValue": "0",
                "maxValue": "<?php echo $maks ?>"
            },

            "zoomControl": {
                "gridHeight": 100,
                "draggerAlpha": 1,
                "gridAlpha": 0.2
            },

            "backgroundZoomsToTop": true,
            "linesAboveImages": true,

            "export": {
                "enabled": true
            }

        });
    }

    return {
      // public functions
      init: function() {
        nasional();
        ahli();
        terampil();
        penerapjfa();
        peta_prov();
      }
    };
  }();

  jQuery(document).ready(function() {
    amChartsDasbor.init();
  });
</script>
