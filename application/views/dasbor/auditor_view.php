<!-- begin::Content -->
<div class="m-content">
  <div class="row">
    <div class="col-xl-4 col-lg-4">
      <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
        <div class="m-portlet__body">
          <div class="m-card-profile">
            <div class="m-card-profile__title m--hide">
              Profile Saya
            </div>
            <div class="m-card-profile__pic">
              <div class="m-card-profile__pic-wrapper">
                <?php
                  $data = $this->session->userdata('Foto');

                  if ($data === '' || $data == NULL) {
                      echo '
                      <img src="'.base_url('assets/app/media/img/users/default.jpg').'" alt="" >
                      ';
                  } else {
                      echo '
                      <img src="'.base_url($data).'" alt="" >
                      ';
                  }
                ?>
              </div>
            </div>
            <div class="m-card-profile__details">
              <span class="m-card-profile__name" > <h4> <span class="Pendidikan_GelarDepan"></span> <span class="Auditor_NamaLengkap"></span><span class="Pendidikan_GelarBelakang"></span> </h4> </span>
              <a href="#" class="m-card-profile__email m-link">NIP. <span class="Auditor_NIP"></span></a>
            </div>
          </div>
          <div class="m-widget1 m-widget1--paddingless">
            <div class="m-portlet__body">
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-user"></i> Status Auditor</h3>
                    <span class="m-widget1__desc isAuditor"></span>
                  </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-map"></i> Pangkat & Golongan</h3>
                    <span class="m-widget1__desc"><span class="Pangkat_Nama"></span>, <br> Golongan Ruang <span class="Golongan_Kode"></span></span>
                  </div>
                </div>
              </div>
              <div class="m-widget1__item">
                <div class="row m-row--no-padding align-items-center">
                  <div class="col">
                    <h3 class="m-widget1__title"><i class="m-nav__link-icon flaticon-suitcase"></i> Jabatan</h3>
                    <span class="m-widget1__desc JenjangJabatan_Nama"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-8 col-lg-8 ">
      <div class="m-portlet m-portlet--full-height m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
              <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                  Instansi & Unit Kerja
                </h3>
              </div>
            </div>
            <div class="m-portlet__head-tools">
              <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                  <a href="" data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Reload">
                    <i class="la la-refresh"></i>
                  </a>
                </li>
                <li class="m-portlet__nav-item">
                  <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                    <i class="la la-expand"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        <div class="m-portlet__body">
          <div class="form-group m-form__group m--margin-top-10">
            <div class="alert m-alert m-alert--default" role="alert">
              Data Unit Kerja berikt ini di tetapkan oleh Admin Unit Kerja, jika anda tidak lagi di Unit Kerja berikut atau data tidak sesuai silahkan laporkan kepada Admin Unit Kerja terkait. Terimakasih.
            </div>
          </div>
          <br>
          <div class="m-widget13">
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Jenis Lembaga :
              </span>
              <span class="m-widget13__text m-widget13__text-bolder JenisInstansi_Nama">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Nama Instansi :
              </span>
              <span class="m-widget13__text m-widget13__text-bolder Instansi_Nama">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Nama Unit Kerja :
              </span>
              <span class="m-widget13__text m-widget13__text-bolder UnitKerja_Nama">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Alamat Kantor :
              </span>
              <span class="m-widget13__text UnitKerja_Alamat">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Surat Elektronik :
              </span>
              <span class="m-widget13__text UnitKerja_Surel">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Nomor Telepon Kantor :
              </span>
              <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoTlp">
              </span>
            </div>
            <div class="m-widget13__item">
              <span class="m-widget13__desc m--align-right">
                Nomor Faximili Kantor :
              </span>
              <span class="m-widget13__text m-widget13__number-bolder m--font-info UnitKerja_NoFax">
              </span>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<!-- end::Content -->

<script>
    $(document).ready(function() {

      var id="<?php echo $this->session->userdata('NIP'); ?>";

      $.ajax({
          type: "GET",
          url : "<?php echo site_url('dasbor/profil')?>",
          dataType: "JSON",
          data : {id:id},
          success: function(data)
          {
              $.each(data,function(Auditor_NIP, isAuditor, Auditor_NamaLengkap, Pendidikan_GelarDepan, Pendidikan_GelarBelakang, Pangkat_Nama, Golongan_Kode, JenjangJabatan_Nama, JenisInstansi_Nama, Instansi_Nama, UnitKerja_Nama, UnitKerja_Alamat, UnitKerja_Surel, UnitKerja_NoTlp, UnitKerja_NoFax, UnitKerja_Web)
            {
              // ambil nilai input dari parameter JSON
              $('.Auditor_NIP').text(data[0].Auditor_NIP);
              if (data[0].IsAuditor == 'true') {
                $('.isAuditor').text('Aktif');
              }else {
                $('.isAuditor').text('Non-Aktif');
              }
              $('.Auditor_NamaLengkap').text(data[0].Auditor_NamaLengkap);
              $('.Pendidikan_GelarDepan').text(data[0].Pendidikan_GelarDepan);
              $('.Pendidikan_GelarBelakang').text(data[0].Pendidikan_GelarBelakang);
              $('.Pangkat_Nama').text(data[0].Pangkat_Nama);
              $('.Golongan_Kode').text(data[0].Golongan_Kode);
              $('.JenjangJabatan_Nama').text(data[0].JenjangJabatan_Nama);
              $('.JenisInstansi_Nama').text(data[0].JenisInstansi_Nama);
              $('.Instansi_Nama').text(data[0].Instansi_Nama);
              $('.UnitKerja_Nama').text(data[0].UnitKerja_Nama);
              $('.UnitKerja_Alamat').text(data[0].UnitKerja_Alamat);
              $('.UnitKerja_Surel').text(data[0].UnitKerja_Surel);
              $('.UnitKerja_NoTlp').text(data[0].UnitKerja_NoTlp);
              $('.UnitKerja_NoFax').text(data[0].UnitKerja_NoFax);
              $('.UnitKerja_Web').text(data[0].UnitKerja_Web);
            });
          }
      });
      return false;
    });
</script>
