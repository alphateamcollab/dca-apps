<div class="m-content">

  <div class="row">
    <div class="col-xl-6 col-lg-6">
      <div class="m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered">
        <div class="m-portlet__head">
          <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <span class="m-portlet__head-icon">
                <i class="flaticon-lock"></i>
              </span>
              <h3 class="m-portlet__head-text">
                Ubah Kata Sandi
              </h3>
            </div>
          </div>
          <div class="m-portlet__head-tools">
            <!-- <ul class="m-portlet__nav">
              <li class="m-portlet__nav-item">
                <a href="" data-portlet-tool="reload" class="m-portlet__nav-link m-portlet__nav-link--icon" title="" data-original-title="Reload">
                  <i class="la la-refresh"></i>
                </a>
              </li>
              <li class="m-portlet__nav-item">
                <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                  <i class="la la-expand"></i>
                </a>
              </li>
            </ul> -->
          </div>
        </div>
        <!--begin::Form-->
        <form class="m-form m-form--fit m-form--label-align-right" id="form">
        <div class="m-portlet__body">
          <div class="form-group m-form__group row m--hide">
            <label for="example-text-input" class="col-5 col-form-label">
            NIP / NRP
            </label>
            <div class="col-7">
              <input class="form-control m-input m-input--air m-input--pill" type="text" placeholder="Kata sandi lama" name="Auditor_NIP" value="<?php echo $this->session->userdata('NIP');?>" disabled>
            </div>
          </div>
          <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-5 col-form-label">
            Kata Sandi Saat Ini
            <small class="m--font-danger">
              <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon la la-asterisk" title="Wajib Diisi"></i>Wajib Diisi
            </small>
            </label>
            <div class="col-7">
              <input class="form-control m-input m-input--air m-input--pill" type="password" placeholder="Kata sandi lama" name="Pengguna_KataSandi2" id="opassword" required="required" autocomplete="off">
            </div>
          </div>
          <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-5 col-form-label">
            Kata Sandi Baru
            </label>
            <div class="col-7">
              <input class="form-control m-input m-input--air m-input--pill" type="password" placeholder="Kata sandi" name="Pengguna_KataSandi" id="password" required="required" autocomplete="off">
            </div>
          </div>
          <div class="form-group m-form__group row">
            <label for="example-text-input" class="col-5 col-form-label">
            Konfirmasi Kata Sandi Baru
            </label>
            <div class="col-7">
              <input class="form-control m-input m-input--air m-input--pill" type="password" placeholder="Konfirmasi Kata sandi" name="rpassword" id="rpassword" required="required" autocomplete="off">
              <span id='message'></span>
            </div>
          </div>
          <div class="align-items-center">
            <div class="col-lg-12 m--align-right">
              <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan_password">Simpan</button>
            </div>
          </div>
        </div>
       </form>
       <!-- end::Form -->
      </div>
    </div>
  </div>

</div>

<script>

var pwd;

$('#password, #rpassword').on('keyup', function () {
  if ($('#password').val() == $('#rpassword').val()) {
    $('#message').html('Kata sandi cocok').css('color', 'green');
    pwd = 'OK';
  } else
    $('#message').html('Kata sandi tidak sama!').css('color', 'red');
    pwd = 'NO';
});

//-- BEGIN: fungsi simpan data profil
$('#m_data_simpan_password').on('click', function() {
  // variable untuk menyimpan nilai input
  var nip         = $('[name="Auditor_NIP"]').val();
  var sandilama   = $('[name="Pengguna_KataSandi2"]').val();
  var sandibaru   = $('[name="Pengguna_KataSandi"]').val();
  // var sandi       = $('[name="Pengguna_KataSandi"]').val();

  // ubah teks tombol
  $('#m_data_simpan').text('Menyimpan...');
  // nonaktifkan tombol
  $('#m_data_simpan').attr('disabled',true);
  // variable untuk menyimpan url ajax
  url = "<?php echo site_url('profil/ubahsandi')?>";
  // menambahkan data ke ajax dengan ajax
  $.ajax({
      type: "POST",
      url : url,
      dataType: "JSON",
      data : {Auditor_NIP:nip, Pengguna_KataSandi2:sandilama, Pengguna_KataSandi:sandibaru},
      success: function(data)
      {
        $('#modal_form').modal('hide');
        window.location.reload();
        // $('.m_datatable').mDatatable('reload');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
        // pesan penambahan data berhasil
        swal({
            title: 'Berhasil!',
            text: "Data telah tersimpan.",
            type: 'success',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
      },
      error: function ()
      {
        swal({
            title: 'Proses Simpan Gagal!',
            text: "Terjadi kesamaan data, mohon periksa kembali data anda.",
            type: 'error',
            confirmButtonText: "<span><i class='la la-thumbs-o-up'></i><span>OK</span></span>",
            confirmButtonClass: "btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info m-btn--icon",
            animation: false,
            customClass: 'animated bounceIn'
        });
        // alert('Error adding / update data');
        // ubah teks tombol
        $('#m_data_simpan').text('Simpan');
        // aktifkan tombol
        $('#m_data_simpan').attr('disabled',false);
      }
  });
  return false;
});
//-- END: fungsi simpan data profil
</script>
