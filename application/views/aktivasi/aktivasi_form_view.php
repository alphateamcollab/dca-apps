<?php
echo $nama;
echo $gelardepan;
echo $gelarbelakang;
echo $gelardepan; ?>

<!DOCTYPE html>
<html lang="id">
<!-- begin::Head -->

<head>
  <?php
      $this->load->view('layout/head');
      $this->load->view('layout/js');
  ?>
</head>
<!-- end::Head -->
<!--begin::Body -->

<body class="m-page--wide m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default">

  <!-- begin:: m-Page -->
  <div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- begin:: m-Header -->
    <header class="m-grid__item m-header " data-minimize-offset="200" data-minimize-mobile-offset="200">
        <?php $this->load->view('layout/header'); ?>
    </header>
    <!-- end: m-Header -->

    <!-- begin:: m-Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
      <!-- begin:: m-Wrapper -->
      <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- begin: Content -->
        <div class="m-content">
          <!--Begin::Main Portlet-->
          <div class="m-portlet">
            <!--begin: Portlet Head-->
            <div class="m-portlet__head">
              <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                    <?php echo $title; ?>
                    <small>
                        <?php echo $subtitle; ?>
                    </small>
                  </h3>
                </div>
              </div>
              <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                    <a href="#" class="btn btn-outline-metal m-btn m-btn--icon m-btn--pill m-btn--air" data-toggle="m-tooltip" title="<?php echo $bantuan; ?>" data-placement="left">
                      <span>
                        <i class="fa flaticon-info m--icon-font-size-lg3"></i>
                        <span>
                          Bantuan
                        </span>
                      </span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <!--end: Portlet Head-->
            <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right" id="form">
              <div class="modal-body">
                <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="420">
                  <div class="m-portlet__body">
                    <div class="m-widget1__item">
                      <div class="row m-row--no-padding align-items-center">
                        <div class="col-6">
                          <span class="m-widget1__desc">
                            <div class="form-group m-form__group row">
                              <label for="example-text-input" class="col-3 col-form-label">
                                NIP/NRP :
                              </label>
                              <span for="example-text-input" class="col-9 col-form-label">
                                <?php echo $nip; ?>
                              </span>
                            </div>
                            <div class="form-group m-form__group row m--hide">
                              <label for="example-text-input" class="col-3 col-form-label">
                                Nama Lengkap :
                              </label>
                              <span for="example-text-input" class="col-9 col-form-label">
                                <?php echo $nama; ?>
                              </span>
                            </div>
                            <div class="form-group m-form__group row">
                              <label for="example-text-input" class="col-3 col-form-label">
                                Jenis Instansi :
                              </label>
                              <span for="example-text-input" class="col-9 col-form-label">
                                <?php echo $jenisinstansi; ?>
                              </span>
                            </div>
                            <div class="form-group m-form__group row">
                              <label for="example-text-input" class="col-3 col-form-label">
                                Instansi :
                              </label>
                              <span for="example-text-input" class="col-9 col-form-label">
                                <?php echo $instansi; ?>
                              </span>
                            </div>
                            <div class="form-group m-form__group row">
                              <label for="example-text-input" class="col-3 col-form-label">
                                Unit Kerja :
                              </label>
                              <span for="example-text-input" class="col-9 col-form-label">
                                <?php echo $unitkerja; ?>
                              </span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group m-form__group row">
                              <label for="example-text-input" class="col-3 col-form-label">
                              Surat Elektronik
                              </label>
                              <div class="col-9">
                                <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Surat Elektronik (email)" name="Pengguna_Surel" value="<?php echo $surel; ?>">
                              </div>
                            </div>
                            <div class="form-group m-form__group row">
                              <label for="example-text-input" class="col-3 col-form-label">
                              Nomor Kontak Telp. / HP
                              </label>
                              <div class="col-9">
                                <input type="email" class="form-control m-input m-input--air m-input--pill"placeholder="Kontak Telp. / HP" name="Pengguna_NoHP">
                              </div>
                            </div>
                            <div class="form-group m-form__group row">
                              <label for="example-text-input" class="col-3 col-form-label">
                              Alamat Tempat Tinggal
                              </label>
                              <div class="col-9">
                                <textarea class="form-control m-input m-input--air m-input--pill" placeholder="Alamat tinggal saat ini" name="Pengguna_Alamat" rows="4"></textarea>
                              </div>
                            </div>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info" id="m_data_simpan">Simpan</button>
             </div>
            </form>
            <!-- end::Form -->
          </div>
          <!--End::Main Portlet-->
        </div>
        <!-- end: content -->
      </div>
      <!-- end:: m-Wrapper -->
    </div>
    <!-- end:: m-Body -->

    <!-- begin::Footer -->
    <footer class="m-grid__item  m-footer ">
      <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
          <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
            <span class="m-footer__copyright">
                  Data Center Auditor &copy; 2017 - <?php echo date('Y');?> by
                  <a href="http://pusbinjfa.bpkp.go.id/" class="m-link">
                    Pusbin JFA BPKP
                  </a>
                </span>
          </div>
          <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
            <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
              <li class="m-nav__item">
                <a href="#" class="m-nav__link">
                  <span class="m-nav__link-text">
                    Tentang DCA
                  </span>
                </a>
              </li>
              <li class="m-nav__item m-nav__item">
                <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Panduan Penggunaan" data-placement="left">
                  <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- end::Footer -->
  </div>
  <!-- end:: m-Page -->
</body>
</html>

<script>

</script>
