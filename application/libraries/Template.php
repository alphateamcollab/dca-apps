<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {

	protected $_ci;

	function __construct()
  {
		$this->_ci = &get_instance();
	}

	function display($template, $data = null)
  {
        $CI =& get_instance();

        // $mod= $CI->load->model('sysuser_model');
				$data['head'] = $this->_ci->load->view('layout/head', $data, true);
				$data['header'] = $this->_ci->load->view('layout/header', $data, true);
				$data['header_admin'] = $this->_ci->load->view('layout/header_admin', $data, true);
				$data['sidemenu'] = $this->_ci->load->view('layout/sidemenu', $data, true);
        $data['content'] = $this->_ci->load->view($template, $data, true);
				$data['js'] = $this->_ci->load->view('layout/js', $data, true);
        // $data['menu'] = $CI->sysuser_model->get_menu($this->_ci->access->get_level());
        $this->_ci->load->view('layout/global_template', $data);
    }
}

/* End of file Template.php */
/* Location: ./system/application/libraries/Template.php */
