<?php
/**
 *
 */
class Jabatan_model extends CI_Model
{
    public function jabatan_riwayat($nip)
    {
        $hasil = $this->db->query("call sp_jabatanRiwayat('".$nip."')");

        // foreach ($hasil->result() as $row) {
        //     $data['Jabatan_ID']                 = $row->Jabatan_ID;
        //     $data['Auditor_NIP']                = $row->Auditor_NIP;
        //     $data['Jabatan_Kode']               = $row->Jabatan_Kode;
        //     $data['Jabatan_Nama']               = $row->Jabatan_Nama;
        //     $data['JenjangJabatan_Kode']        = $row->JenjangJabatan_Kode;
        //     $data['JenjangJabatan_Nama']        = $row->JenjangJabatan_Nama;
        //     $data['Jabatan_NoSuratKeputusan']   = $row->Jabatan_NoSuratKeputusan;
        //     $data['Jabatan_TglSuratKeputusan']  = indonesian_shortdate($row->Jabatan_TglSuratKeputusan);
        //     $data['Jabatan_TglMulaiTugas']      = indonesian_shortdate($row->Jabatan_TglMulaiTugas);
        //     $data['Jabatan_SK']                 = $row->Jabatan_SK;
        //     $data['DibuatOleh']                 = $row->DibuatOleh;
        //     $data['DibuatTgl']                  = $row->DibuatTgl;
        //     $data['DiubahOleh']                 = $row->DiubahOleh;
        //     $data['DiubahTgl']                  = $row->DiubahTgl;
        // }
        //
        // return [$data];

        return $hasil->result();
    }

    public function jabatan_list()
    {
        $hasil = $this->db->query("call sp_jabatanTampil()");
        return $hasil->result();
    }

    public function jabatan_ambil($id)
    {
        $hasil = $this->db->query("call sp_jabatanDetail('".$id."')");

        foreach ($hasil->result() as $row) {
            $data['Jabatan_ID']                 = $row->Jabatan_ID;
            $data['Auditor_NIP']                = $row->Auditor_NIP;
            $data['Jabatan_Kode']               = $row->Jabatan_Kode;
            $data['JenjangJabatan_Kode']        = $row->JenjangJabatan_Kode;
            $data['Jabatan_NoSuratKeputusan']   = $row->Jabatan_NoSuratKeputusan;
            $data['Jabatan_TglSuratKeputusan']  = indonesian_shortdate($row->Jabatan_TglSuratKeputusan);
            $data['Jabatan_TglMulaiTugas']      = indonesian_shortdate($row->Jabatan_TglMulaiTugas);
            $data['Jabatan_SK']                 = $row->Jabatan_SK;
            $data['DibuatOleh']                 = $row->DibuatOleh;
            $data['DibuatTgl']                  = $row->DibuatTgl;
            $data['DiubahOleh']                 = $row->DiubahOleh;
            $data['DiubahTgl']                  = $row->DiubahTgl;
        }

        return [$data];
    }

    public function jabatan_tambah($id, $nip, $kode, $jabatan, $noSK, $tglSK, $tmt, $SK, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_jabatanTambah('".$nip."', '".$kode."', '".$jabatan."', '".$noSK."', '".$tglSK."', '".$tmt."', '".$SK."', '".$dibuatoleh."', '".$dibuattgl."')");
    }

    public function jabatan_ubah($id, $nip, $kode, $jabatan, $noSK, $tglSK, $tmt, $SK, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_jabatanUbah('".$id."', '".$nip."', '".$kode."', '".$jabatan."', '".$noSK."', '".$tglSK."', '".$tmt."', '".$SK."', '".$diubaholeh."', '".$diubahtgl."')");
    }

    public function jabatan_ubahfile($id, $nip, $kode, $jabatan, $noSK, $tglSK, $tmt, $SK, $diubaholeh, $diubahtgl)
    {
        $file = $this->jabatan_hapusfile($id);
        if (!$this->db->query("call sp_jabatanUbah('".$id."', '".$nip."', '".$kode."', '".$jabatan."', '".$noSK."', '".$tglSK."', '".$tmt."', '".$SK."', '".$diubaholeh."', '".$diubahtgl."')")) {
            return false;
        }
        unlink('./' .$file->Jabatan_SK);
        return true;
    }

    public function jabatan_hapus($id)
    {
        $file = $this->jabatan_hapusfile($id);
        if (!$this->db->query("call sp_jabatanHapus('".$id."')")) {
            return false;
        }
        unlink('./' .$file->Jabatan_SK);
        return true;
    }

    public function jabatan_hapusfile($id)
    {
        return $this->db->select()
                ->from('t_jabatan')
                ->where('Jabatan_ID', $id)
                ->get()
                ->row();
    }
}
