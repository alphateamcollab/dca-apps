<?php
/**
 *
 */
class Auditor_model extends CI_Model
{
    public function auditor_list_pusbin()
    {
      $hasil = $this->db->query("call sp_auditorTampil()");
      return $hasil->result();
    }

    public function auditor_list_korwas($kode)
    {
      $hasil = $this->db->query("call sp_auditorTampilKorwas('".$kode."')");
      return $hasil->result();
    }

    public function auditor_list_unit($kode)
    {
        $hasil = $this->db->query("call sp_auditorTampilUnit('".$kode."')");
        return $hasil->result();
    }

    public function auditor_ambil($id)
    {
        $hasil = $this->db->query("call sp_auditorDetail('".$id."')");
        return $hasil->result();
    }

    public function auditor_tambah($roleid, $isauditor, $isadmin, $status, $surel, $sandi, $alamat, $nip, $nama, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_auditorTambah('".$nip."','".$nama."','".$sandi."','".$alamat."','".$surel."','".$tempatlahir."','".$tanggallahir."','".$jeniskelamin."','".$roleid."','".$isauditor."',
      '".$isadmin."','".$status."','".$unitkerja."','".$dibuatoleh."','".$dibuattgl."')");
    }

    public function auditor_ubah($roleid, $isauditor, $isadmin, $status, $surel, $sandi, $alamat, $nip, $nama, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_auditorUbah('".$nip."','".$nama."','".$sandi."','".$alamat."','".$surel."','".$tempatlahir."','".$tanggallahir."','".$jeniskelamin."','".$roleid."','".$isauditor."',
      '".$isadmin."','".$status."','".$unitkerja."','".$diubaholeh."', '".$diubahtgl."')");
    }

    public function auditor_hapus($id)
    {
        $hasil = $this->db->query("call sp_auditorHapus('".$id."')");
    }

    public function auditorprofil_ambil($id)
    {
        $hasil = $this->db->query("call sp_penggunaDetail('".$id."')");
        return $hasil->result();
    }

    public function auditorprofil_ubah($nip, $nama, $surel, $nohp, $alamat, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_penggunaUbah('".$nip."','".$nama."','".$surel."','".$nohp."','".$alamat."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function auditorkatasandi_ubah($nip, $sandi, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_penggunasandiUbah('".$nip."','".$sandi."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function auditorunitkerja_ambil($id)
    {
        $hasil = $this->db->query("call sp_auditorDetail('".$id."')");
        return $hasil->result();
    }

    public function auditorunitkerja_ubah($nip, $kode, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_auditorunitUbah('".$nip."','".$kode."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function get_rolecode()
    {
        $RoleGroup_ID = $this->session->userdata('RoleGroup_ID');
        $Role = array($RoleGroup_ID, 4);

        $this->db->select('*');
        $this->db->where_in('RoleGroup_ID', $Role);
        $this->db->from('r_role');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_jenjangpendidikan()
    {
        $this->db->select('*');
        $this->db->from('r_pendidikan');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_pangkatgolongan()
    {
        $this->db->where('BerakhirTgl', null);
        $this->db->where('Pekerjaan_ID', '05');
        $this->db->select('*');
        $this->db->from('r_pangkat');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_pangkat($kode)
    {
        if (isset($kode)) {
            $this->db->where('Pangkat_Kode', $kode);
        }
        $this->db->where('BerakhirTgl', null);
        $this->db->where('Pekerjaan_ID', '05');
        $this->db->select('*');
        $this->db->from('r_pangkat');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_jabatan()
    {
        $this->db->select('*');
        $this->db->from('r_jabatan');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_jenjangjabatan($kode)
    {
        if (isset($kode)) {
            $this->db->where('Jabatan_Kode', $kode);
        }
        // $this->db->where('BerakhirTgl', NULL);
        // $this->db->where('Pekerjaan_ID', '05');
        $this->db->select('*');
        $this->db->from('r_jenjangjabatan');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_jenisinstansi()
    {
        $this->db->select('*');
        $this->db->from('r_jenisinstansi');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_instansi($jenisinstansi)
    {
        if (isset($jenisinstansi)) {
            $this->db->where('JenisInstansi_Kode', $jenisinstansi);
        }

        $this->db->select('Instansi_Kode, Instansi_Nama');
        $this->db->from('r_instansi');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_unitkerja($instansi)
    {
        if (isset($instansi)) {
            $this->db->where('Instansi_Kode', $instansi);
        }

        $this->db->select('UnitKerja_Kode, UnitKerja_NamaUnitInstansi');
        $this->db->from('r_unitkerja');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_diklatjfa()
    {
        $this->db->where('DiklatSubGroup_Kode', 302);

        $this->db->select('*');
        $this->db->from('r_diklat');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_diklatteknissubstantif()
    {
        $kode = array(401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423);
        $this->db->where_in('DiklatSubGroup_Kode', $kode);

        $this->db->select('*');
        $this->db->from('r_diklat');
        $result = $this->db->get();
        return $result->result();
    }
}
