<?php
/**
 *
 */
class DiklatSertifikasiProfesi_model extends CI_Model
{
    public function sertifikasiprofesi_riwayat($nip)
    {
        $hasil = $this->db->query("call sp_sertifikasiprofesiRiwayat('".$nip."')");
        return $hasil->result();
    }

    public function sertifikasiprofesi_list()
    {
        $hasil = $this->db->query("call sp_sertifikasiprofesiTampil()");
        return $hasil->result();
    }

    public function sertifikasiprofesi_ambil($id)
    {
        $hasil = $this->db->query("call sp_sertifikasiprofesiDetail('".$id."')");

        foreach ($hasil->result() as $row) {
            $data['SertifikasiProfesi_ID']        = $row->SertifikasiProfesi_ID;
            $data['Auditor_NIP']                  = $row->Auditor_NIP;
            $data['SertifikasiProfesi_Nama']      = $row->SertifikasiProfesi_Nama;
            $data['SertifikasiProfesi_Lembaga']   = $row->SertifikasiProfesi_Lembaga;
            $data['SertifikasiProfesi_Gelar']     = $row->SertifikasiProfesi_Gelar;
            $data['SertifikasiProfesi_TglMulai']  = indonesian_shortdate($row->SertifikasiProfesi_TglMulai);
            $data['SertifikasiProfesi_TglAkhir']  = indonesian_shortdate($row->SertifikasiProfesi_TglAkhir);
            $data['SertifikasiProfesi_NoSTMPL']   = $row->SertifikasiProfesi_NoSTMPL;
            $data['SertifikasiProfesi_TglSTMPL']  = indonesian_shortdate($row->SertifikasiProfesi_TglSTMPL);
            $data['SertifikasiProfesi_Sertifikat']= $row->SertifikasiProfesi_Sertifikat;
            $data['DibuatOleh']                   = $row->DibuatOleh;
            $data['DibuatTgl']                    = $row->DibuatTgl;
            $data['DiubahOleh']                   = $row->DiubahOleh;
            $data['DiubahTgl']                    = $row->DiubahTgl;
        }

        return [$data];
    }

    public function sertifikasiprofesi_tambah($id, $nip, $nama, $lembaga, $gelar, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_sertifikasiprofesiTambah('".$nip."', '".$nama."', '".$lembaga."', '".$gelar."', '".$mulaidiklat."', '".$akhirdiklat."', '".$nostmpl."', '".$tglstmpl."', '".$sertifikat."',
     '".$dibuatoleh."', '".$dibuattgl."')");
    }

    public function sertifikasiprofesi_ubah($id, $nip, $nama, $lembaga, $gelar, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_sertifikasiprofesiUbah('".$id."', '".$nip."', '".$nama."', '".$lembaga."', '".$gelar."', '".$mulaidiklat."', '".$akhirdiklat."', '".$nostmpl."', '".$tglstmpl."', '".$sertifikat."',
     '".$diubaholeh."', '".$diubahtgl."')");
    }

    public function sertifikasiprofesi_ubahfile($id, $nip, $nama, $lembaga, $gelar, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $diubaholeh, $diubahtgl)
    {
        $file = $this->sertifikasiprofesi_hapusfile($id);
        if (!$this->db->query("call sp_sertifikasiprofesiUbah('".$id."', '".$nip."', '".$nama."', '".$lembaga."', '".$gelar."', '".$mulaidiklat."', '".$akhirdiklat."', '".$nostmpl."', '".$tglstmpl."', '".$sertifikat."',
            '".$diubaholeh."', '".$diubahtgl."')")) {
           return false;
        }
        unlink('./' .$file->SertifikasiProfesi_Sertifikat);
        return true;
    }

    public function sertifikasiprofesi_hapus($id)
    {
        $file = $this->sertifikasiprofesi_hapusfile($id);
        if (!$this->db->query("call sp_sertifikasiprofesiHapus('".$id."')")) {
            return false;
        }
        unlink('./' .$file->SertifikasiProfesi_Sertifikat);
        return true;
    }

    public function sertifikasiprofesi_hapusfile($id)
    {
        return $this->db->select()
                ->from('t_sertifikasiprofesi')
                ->where('SertifikasiProfesi_ID', $id)
                ->get()
                ->row();
    }
}
