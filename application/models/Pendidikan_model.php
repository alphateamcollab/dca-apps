<?php
/**
 * Model Pendidikan get and post data for CRUD,
 * also for history / list data by NIP, data detail by ID
 */
class Pendidikan_model extends CI_Model
{
    public function pendidikan_riwayat($nip)
    {
        $hasil = $this->db->query("call sp_pendidikanRiwayat('".$nip."')");

        // foreach ($hasil->result() as $row) {
        //     $data['Pendidikan_ID']              = $row->Pendidikan_ID;
        //     $data['Auditor_NIP']                = $row->Auditor_NIP;
        //     $data['Pendidikan_Kode']            = $row->Pendidikan_Kode;
        //     $data['Pendidikan_Jenjang']         = $row->Pendidikan_Jenjang;
        //     $data['Pendidikan_Lembaga']         = $row->Pendidikan_Lembaga;
        //     $data['Pendidikan_Jurusan']         = $row->Pendidikan_Jurusan;
        //     $data['Pendidikan_GelarDepan']      = $row->Pendidikan_GelarDepan;
        //     $data['Pendidikan_GelarBelakang']   = $row->Pendidikan_GelarBelakang;
        //     $data['Pendidikan_NoIjazah']        = $row->Pendidikan_NoIjazah;
        //     $data['Pendidikan_TglIjazah']       = indonesian_shortdate($row->Pendidikan_TglIjazah);
        //     $data['Pendidikan_Ijazah']          = $row->Pendidikan_Ijazah;
        //     $data['DibuatOleh']                 = $row->DibuatOleh;
        //     $data['DibuatTgl']                  = $row->DibuatTgl;
        //     $data['DiubahOleh']                 = $row->DiubahOleh;
        //     $data['DiubahTgl']                  = $row->DiubahTgl;
        // }
        //
        // return [$data];

        return $hasil->result();
    }

    public function pendidikan_list()
    {
        $hasil = $this->db->query("call sp_pendidikanTampil()");
        return $hasil->result();
    }

    public function pendidikan_ambil($id)
    {
        $hasil = $this->db->query("call sp_pendidikanDetail('".$id."')");

        foreach ($hasil->result() as $row) {
            $data['Pendidikan_ID']              = $row->Pendidikan_ID;
            $data['Auditor_NIP']                = $row->Auditor_NIP;
            $data['Pendidikan_Kode']            = $row->Pendidikan_Kode;
            $data['Pendidikan_Lembaga']         = $row->Pendidikan_Lembaga;
            $data['Pendidikan_Fakultas']        = $row->Pendidikan_Fakultas;
            $data['Pendidikan_Jurusan']         = $row->Pendidikan_Jurusan;
            $data['Pendidikan_GelarDepan']      = $row->Pendidikan_GelarDepan;
            $data['Pendidikan_GelarBelakang']   = $row->Pendidikan_GelarBelakang;
            $data['Pendidikan_NoIjazah']        = $row->Pendidikan_NoIjazah;
            $data['Pendidikan_TglIjazah']       = indonesian_shortdate($row->Pendidikan_TglIjazah);
            $data['Pendidikan_Ijazah']          = $row->Pendidikan_Ijazah;
            $data['DibuatOleh']                 = $row->DibuatOleh;
            $data['DibuatTgl']                  = $row->DibuatTgl;
            $data['DiubahOleh']                 = $row->DiubahOleh;
            $data['DiubahTgl']                  = $row->DiubahTgl;
        }

        return [$data];
    }

    public function pendidikan_tambah($id, $nip, $tingkat, $lembaga, $fakultas, $jurusan, $gelardepan, $gelarbelakang, $noijazah, $tglijazah, $ijazah, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_pendidikanTambah('".$nip."', '".$tingkat."', '".$lembaga."', '".$fakultas."', '".$jurusan."', '".$gelardepan."', '".$gelarbelakang."', '".$noijazah."', '".$tglijazah."', '".$ijazah."', '".$dibuatoleh."', '".$dibuattgl."')");
    }

    public function pendidikan_ubah($id, $nip, $tingkat, $lembaga, $fakultas, $jurusan, $gelardepan, $gelarbelakang, $noijazah, $tglijazah, $ijazah, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_pendidikanUbah('".$id."', '".$nip."', '".$tingkat."', '".$lembaga."', '".$fakultas."', '".$jurusan."', '".$gelardepan."', '".$gelarbelakang."', '".$noijazah."', '".$tglijazah."', '".$ijazah."', '".$diubaholeh."', '".$diubahtgl."')");
    }

    public function pendidikan_ubahfile($id, $nip, $tingkat, $lembaga, $fakultas, $jurusan, $gelardepan, $gelarbelakang, $noijazah, $tglijazah, $ijazah, $diubaholeh, $diubahtgl)
    {
        $file = $this->pendidikan_hapusfile($id);
        if (!$this->db->query("call sp_pendidikanUbah('".$id."', '".$nip."', '".$tingkat."', '".$lembaga."', '".$fakultas."', '".$jurusan."', '".$gelardepan."', '".$gelarbelakang."', '".$noijazah."', '".$tglijazah."', '".$ijazah."', '".$diubaholeh."', '".$diubahtgl."')")) {
            return false;
        }
        unlink('./' .$file->Pendidikan_Ijazah);
        return true;
    }

    public function pendidikan_hapus($id)
    {
        $file = $this->pendidikan_hapusfile($id);
        if (!$this->db->query("call sp_pendidikanHapus('".$id."')")) {
            return false;
        }
        unlink('./' .$file->Pendidikan_Ijazah);
        return true;
    }

    public function pendidikan_hapusfile($id)
    {
        return $this->db->select()
                ->from('t_pendidikan')
                ->where('Pendidikan_ID', $id)
                ->get()
                ->row();
    }
}
