<?php
/**
 *
 */
class Dasbor_model extends CI_Model
{
    public function profil_ambil($id)
    {
        $hasil = $this->db->query("call sp_auditorDetail('".$id."')");
        return $hasil->result();
    }

    function profilfoto_ambil($id)
    {
       $query = $this->db->query("SELECT * FROM t_auditorfoto where AuditorFoto_ID = '$id'");
       foreach($query->result_array() as $ok){
        //header('Content-type: image');
         $image = $ok['AuditorFoto_Data'];
       }
       //header("Content-type: image/*");
       echo $image;
    }

    public function auditor_tambah($nip, $nama, $gelardepan, $gelarbelakang, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_auditorTambah('', '".$nip."', '".$nama."', '".$gelardepan."', '".$gelarbelakang."', '".$tempatlahir."', '".$tanggallahir."', '".$jeniskelamin."', '', '".$dibuattgl."', '".$dibuattgl."', '".$diubaholeh."','".$diubahtgl."')");
    }

    public function auditor_ubah($id, $nip, $tingkat, $lembaga, $jurusan, $masuk, $lulus, $ijazah, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_auditorUbah('".$id."', '".$nip."', '".$tingkat."', '".$lembaga."', '".$jurusan."', '".$masuk."', '".$lulus."', '".$ijazah."', '".$dibuatoleh."', '".$dibuattgl."', '".$diubaholeh."', '".$diubahtgl."')");
    }

    public function auditor_hapus($id)
    {
        $hasil = $this->db->query("call sp_auditorHapus('".$id."')");
    }

    public function get_auditorunit($unitkerja_kode)
    {
        if (isset($unitkerja_kode)) {
            $this->db->where('UnitKerja_Kode', $unitkerja_kode);
        }
        $this->db->select('*');
        $this->db->from('v_jmlauditorbyuker');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_auditorkoorwas($koorwas_kode)
    {
        if (isset($koorwas_kode)) {
            $this->db->where('UnitKerja_Kode', $koorwas_kode);
        }
        $this->db->select('*');
        $this->db->from('v_jmlauditorbykorwas');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_auditorpusbin()
    {
      // if (isset($kode)) {
      //     $this->db->where('UnitKerja_Kode', $kode);
      // }
      $this->db->select('*');
      $this->db->from('t_sum_komposisiauditor');
      $result = $this->db->get();
      return $result->result();
    }

    public function get_petasebaran()
    {
      $this->db->select('*');
      $this->db->from('v_jmlauditorbykorwas');
      $result = $this->db->get();
      return $result->result();
    }
}
