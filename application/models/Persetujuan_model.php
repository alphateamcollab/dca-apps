<?php
/**
 *
 */
class Persetujuan_model extends CI_Model
{
    /*
    | Pendidikan
    */
    public function auditor_data()
    {
        $this->db->select('*');
        $this->db->from('t_auditor');
        $this->db->where('Approval', 1);
        $result = $this->db->get();
        return $result->result();
    }

    public function auditor_status($id, $status)
    {
        $hasil = $this->db->query("call sp_auditorstatusUbah('".$id."','".$status."')");
    }

    /*
    | Pendidikan
    */
    public function pendidikan_data()
    {
        $this->db->select('*');
        $this->db->from('t_pendidikan');
        $this->db->where('Approval', 1);
        $this->db->join('r_pendidikan', 'r_pendidikan.Pendidikan_Kode = t_pendidikan.Pendidikan_Kode');
        $result = $this->db->get();
        return $result->result();
    }

    public function pendidikan_status($id, $status)
    {
        $hasil = $this->db->query("call sp_pendidikanstatusUbah('".$id."','".$status."')");
    }

    /*
    | Pangkat & Golongan
    */
    public function pangkat_data()
    {
        $this->db->select('*');
        $this->db->from('t_pangkat');
        $this->db->where('Approval', 1);
        $this->db->join('r_pangkat', 'r_pangkat.Pangkat_Kode = t_pangkat.Pangkat_Kode');
        $result = $this->db->get();
        return $result->result();
    }

    public function pangkat_status($id, $status)
    {
        $hasil = $this->db->query("call sp_pangkatstatusUbah('".$id."','".$status."')");
    }

    /*
    | Jabatan
    */
    public function jabatan_data()
    {
        $this->db->select('*');
        $this->db->from('t_jabatan');
        $this->db->where('Approval', 1);
        $this->db->join('r_jabatan', 'r_jabatan.Jabatan_Kode = t_jabatan.Jabatan_Kode');
        $this->db->join('r_jenjangjabatan', 'r_jenjangjabatan.JenjangJabatan_Kode = t_jabatan.JenjangJabatan_Kode');
        $result = $this->db->get();
        return $result->result();
    }

    public function jabatan_status($id, $status)
    {
        $hasil = $this->db->query("call sp_jabatanstatusUbah('".$id."','".$status."')");
    }
}
