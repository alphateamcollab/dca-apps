<?php

/**
 *
 */
class Auth_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function get_jenisinstansi()
    {
        $this->db->select('*');
        $this->db->from('r_jenisinstansi');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_instansi($jenisinstansi)
    {
        if (isset($jenisinstansi)) {
            $this->db->where('JenisInstansi_Kode', $jenisinstansi);
        }

        $this->db->select('Instansi_Kode, Instansi_Nama');
        $this->db->from('r_instansi');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_unitkerja($instansi)
    {
        if (isset($instansi)) {
            $this->db->where('Instansi_Kode', $instansi);
        }

        $this->db->select('UnitKerja_Kode, UnitKerja_NamaUnitInstansi');
        $this->db->from('r_unitkerja');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_rolecode()
    {
        $Role = array(3, 4);

        $this->db->select('*');
        $this->db->where_in('RoleGroup_ID', $Role);
        $this->db->from('r_role');
        $result = $this->db->get();
        return $result->result();
    }

    public function auth_tambah($nip, $nama, $sandi, $alamat, $surel, $tempatlahir, $tanggallahir, $jeniskelamin, $roleid, $isauditor, $isadmin, $status, $unitkerja, $lampiran, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_adminunitTambah('".$nip."','".$nama."','".$sandi."','".$alamat."','".$surel."','".$tempatlahir."','".$tanggallahir."','".$jeniskelamin."','".$roleid."','".$isauditor."',
        '".$isadmin."','".$status."','".$unitkerja."','".$lampiran."','".$dibuatoleh."','".$dibuattgl."')");
    }
}
