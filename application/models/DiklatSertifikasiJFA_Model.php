<?php
/**
 *
 */
class DiklatSertifikasiJFA_model extends CI_Model
{
    public function sertifikasijfa_riwayat($nip)
    {
        $hasil = $this->db->query("call sp_sertifikasijfaRiwayat('".$nip."')");
        return $hasil->result();
    }

    public function sertifikasijfa_list()
    {
        $hasil = $this->db->query("call sp_sertifikasijfaTampil()");
        return $hasil->result();
    }

    public function sertifikasijfa_ambil($id)
    {
        $hasil = $this->db->query("call sp_sertifikasijfaDetail('".$id."')");

        foreach ($hasil->result() as $row) {
            $data['SertifikasiJFA_ID']            = $row->SertifikasiJFA_ID;
            $data['Auditor_NIP']                  = $row->Auditor_NIP;
            $data['Diklat_Kode']                  = $row->Diklat_Kode;
            $data['SertifikasiJFA_TglMulaiDiklat']= indonesian_shortdate($row->SertifikasiJFA_TglMulaiDiklat);
            $data['SertifikasiJFA_TglAkhirDiklat']= indonesian_shortdate($row->SertifikasiJFA_TglAkhirDiklat);
            $data['SertifikasiJFA_NoSTMPL']       = $row->SertifikasiJFA_NoSTMPL;
            $data['SertifikasiJFA_TglSTMPL']      = indonesian_shortdate($row->SertifikasiJFA_TglSTMPL);
            $data['SertifikasiJFA_NoSTTPL']       = $row->SertifikasiJFA_NoSTTPL;
            $data['SertifikasiJFA_TglSTTPL']      = indonesian_shortdate($row->SertifikasiJFA_TglSTTPL);
            $data['SertifikasiJFA_Sertifikat']    = $row->SertifikasiJFA_Sertifikat;
            $data['DibuatOleh']                   = $row->DibuatOleh;
            $data['DibuatTgl']                    = $row->DibuatTgl;
            $data['DiubahOleh']                   = $row->DiubahOleh;
            $data['DiubahTgl']                    = $row->DiubahTgl;
        }

        return [$data];
    }

    public function sertifikasijfa_tambah($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $nosttpl, $tglsttpl, $sertifikat, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_sertifikasijfaTambah('".$nip."', '".$kode."', '".$mulaidiklat."', '".$akhirdiklat."', '".$nostmpl."', '".$tglstmpl."', '".$nosttpl."', '".$tglsttpl."', '".$sertifikat."',
       '".$dibuatoleh."', '".$dibuattgl."')");
    }

    public function sertifikasijfa_ubah($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $nosttpl, $tglsttpl, $sertifikat, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_sertifikasijfaUbah('".$id."', '".$nip."', '".$kode."', '".$mulaidiklat."', '".$akhirdiklat."', '".$nostmpl."', '".$tglstmpl."', '".$nosttpl."', '".$tglsttpl."', '".$sertifikat."',
       '".$diubaholeh."', '".$diubahtgl."')");
    }

    public function sertifikasijfa_ubahfile($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $nosttpl, $tglsttpl, $sertifikat, $diubaholeh, $diubahtgl)
    {
        $file = $this->sertifikasijfa_hapusfile($id);
        if (!$this->db->query("call sp_sertifikasijfaUbah('".$id."', '".$nip."', '".$kode."', '".$mulaidiklat."', '".$akhirdiklat."', '".$nostmpl."', '".$tglstmpl."', '".$nosttpl."', '".$tglsttpl."', '".$sertifikat."',
       '".$diubaholeh."', '".$diubahtgl."')")) {
           return false;
       }
       unlink('./' .$file->SertifikasiJFA_Sertifikat);
       return true;
    }

    public function sertifikasijfa_hapus($id)
    {
        $file = $this->sertifikasijfa_hapusfile($id);
        if (!$this->db->query("call sp_sertifikasijfaHapus('".$id."')")) {
            return false;
        }
        unlink('./' .$file->SertifikasiJFA_Sertifikat);
        return true;
    }

    public function sertifikasijfa_hapusfile($id)
    {
        return $this->db->select()
                ->from('t_sertifikasijfa')
                ->where('SertifikasiJFA_ID', $id)
                ->get()
                ->row();
    }
}
