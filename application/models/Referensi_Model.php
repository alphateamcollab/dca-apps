<?php
class Referensi_model extends CI_Model
{

    // begin: Golongan Ruang
    public function golongan_list()
    {
        $hasil = $this->db->query("call sp_RgolonganTampil()");
        return $hasil->result();
    }

    public function golongan_ambil($id)
    {
        $hasil = $this->db->query("call sp_RgolonganDetail('".$id."')");
        return $hasil->result();
    }

    public function golongan_tambah($kode, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_RgolonganTambah('".$kode."','".$nama."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function golongan_ubah($kode, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_RgolonganUbah('".$kode."','".$nama."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function golongan_hapus($id)
    {
        $hasil = $this->db->query("call sp_RgolonganHapus('".$id."')");
    }
    // end: Golongan Ruang

    // begin: Jenis Instansi
    public function jenisinstansi_list()
    {
        $hasil = $this->db->query("call sp_RjenisinstansiTampil()");
        return $hasil->result();
    }

    public function jenisinstansi_ambil($id)
    {
        $hasil = $this->db->query("call sp_RjenisinstansiDetail('".$id."')");
        return $hasil->result();
    }

    public function jenisinstansi_tambah($kode, $nama, $singkatan, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_RjenisinstansiTambah('".$kode."','".$nama."','".$singkatan."','".$dibuatoleh."','".$dibuattgl."')");
    }

    public function jenisinstansi_ubah($kode, $nama, $singkatan, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_RjenisinstansiUbah('".$kode."','".$nama."','".$singkatan."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function jenisinstansi_hapus($id)
    {
        $hasil = $this->db->query("call sp_RjenisinstansiHapus('".$id."')");
    }
    // end: Jenis Instansi

    // begin: Instansi / Unit Kerja
    public function instansi_list()
    {
        $hasil = $this->db->query("call sp_RinstansiTampil()");
        return $hasil->result();
    }

    public function instansi_ambil($id)
    {
        $hasil = $this->db->query("call sp_RinstansiDetail('".$id."')");
        return $hasil->result();
    }

    public function instansi_tambah($kode, $jenis, $instansi_1, $instansi_2, $nama, $namasingkat, $pimpinan_instansi, $alamat, $kota, $kodepos, $no_telp, $no_fax, $web, $surel, $tgl_berlaku, $tgl_berkahir, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_RinstansiTambah('".$kode."', '".$jenis."', '".$instansi_1."', '".$instansi_2."', '".$nama."', '".$namasingkat."', '".$pimpinan_instansi."', '".$alamat."', '".$kota."', '".$kodepos."', '".$no_telp."','".$no_fax."','".$web."','".$surel."','".$tgl_berlaku."','".$tgl_berkahir."','".$dibuatoleh."','".$dibuattgl."')");
        // return $hasil->result();
    }

    public function instansi_ubah($kode, $jenis, $instansi_1, $instansi_2, $nama, $namasingkat, $pimpinan_instansi, $alamat, $kota, $kodepos, $no_telp, $no_fax, $web, $surel, $tgl_berlaku, $tgl_berkahir, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_RinstansiUbah('".$kode."', '".$jenis."', '".$instansi_1."', '".$instansi_2."', '".$nama."', '".$namasingkat."', '".$pimpinan_instansi."', '".$alamat."', '".$kota."', '".$kodepos."', '".$no_telp."','".$no_fax."','".$web."','".$surel."','".$tgl_berlaku."','".$tgl_berkahir."','".$diubaholeh."', '".$diubahtgl."')");
        // return $hasil->result();
    }

    public function instansi_hapus($id)
    {
        $hasil = $this->db->query("call sp_RunitkerjaHapus('".$id."')");
        // return $hasil->result();
    }

    public function unitkerja_list()
    {
        $hasil = $this->db->query("call sp_RunitkerjaTampil()");
        return $hasil->result();
    }

     public function unitkerja_ambil($id)
    {
        $hasil = $this->db->query("call sp_RunitkerjaDetail('".$id."')");
        return $hasil->result();
    }

    public function unitkerja_tambah($kode, $kode_transl1, $instansi_kode, $kode_daerah, $unitkerja_1, $unitkerja_2, $nama_unit, $nama_singkat, $nama_pimpinan, $alamat, $kodepos, $no_telp, $no_fax, $web, $surel, $pemegang_berkas, $unit, $kode_kota, $no_skt, $tgl_berlaku, $tgl_berakhir, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_RunitkerjaTambah('".$kode."','".$kode_transl1."','".$instansi_kode."','".$kode_daerah."', '".$unitkerja_1."', '".$unitkerja_2."', '".$nama_unit."', '".$nama_singkat."', '".$nama_pimpinan."','".$alamat."', '".$kodepos."', '".$no_telp."','".$no_fax."','".$web."','".$surel."', '".$pemegang_berkas."', '".$unit."', '".$kode_kota."','".$no_skt."','".$tgl_berlaku."','".$tgl_berakhir."','".$dibuatoleh."','".$dibuattgl."')");
        // return $hasil->result();
    }

    public function unitkerja_ubah($kode, $kode_transl1, $instansi_kode, $kode_daerah, $unitkerja_1, $unitkerja_2, $nama_unit, $nama_singkat, $nama_pimpinan, $alamat, $kodepos, $no_telp, $no_fax, $web, $surel, $pemegang_berkas, $unit, $kode_kota, $no_skt, $tgl_berlaku, $tgl_berakhir, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_RunitkerjaUbah('".$kode."','".$kode_transl1."','".$instansi_kode."','".$kode_daerah."', '".$unitkerja_1."', '".$unitkerja_2."', '".$nama_unit."', '".$nama_singkat."', '".$nama_pimpinan."','".$alamat."', '".$kodepos."', '".$no_telp."','".$no_fax."','".$web."','".$surel."', '".$pemegang_berkas."', '".$unit."', '".$kode_kota."','".$no_skt."','".$tgl_berlaku."','".$tgl_berakhir."','".$diubaholeh."', '".$diubahtgl."')");
        // return $hasil->result();
    }

    // end: Instansi / Unit Kerja

    // begin: Jenjang Jabatan
    public function jenjangjabatan_list()
    {
        // $hasil = $this->db->query("call sp_api_auditor_tampil()");
        $hasil = $this->db->query("call sp_RjenjangjabatanTampil()");
        return $hasil->result();
    }
    public function jenjangjabatan_ambil($id)
    {
        $hasil = $this->db->query("call sp_RjenjangjabatanDetail('".$id."')");
        return $hasil->result();
    }
    public function jenjangjabatan_tambah($kode, $refjabatan, $nama, $level, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl)
    {
        // $hasil = $this->db->query("call sp_RjenjangjabatanTambah('".$id."','".$kode."','".$refjabatan."','".$nama."','".$level."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."','".$diubaholeh."','".$diubahtgl."')");
        $hasil = $this->db->query("call sp_RjenjangjabatanTambah('".$kode."','".$refjabatan."','".$nama."','".$level."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."')");
    }

    public function jenjangjabatan_ubah($id, $kode, $refjabatan, $nama, $level, $deskripsi, $diubaholeh, $diubahtgl)
    {
        // $hasil = $this->db->query("call sp_RjenjangjabatanUbah('".$id."','".$kode."','".$refjabatan."','".$nama."','".$level."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."','".$diubaholeh."','".$diubahtgl."')");
        $hasil = $this->db->query("call sp_RjenjangjabatanUbah('".$id."','".$kode."','".$refjabatan."','".$nama."','".$level."','".$deskripsi."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function jenjangjabatan_hapus($id)
    {
        $hasil = $this->db->query("call sp_RjenjangjabatanHapus('".$id."')");
    }
    // end: Jenjang Jabatan

    // begin: Jabatan
    public function jabatan_list()
    {
        // $hasil = $this->db->query("call sp_api_auditor_tampil()");
        $hasil = $this->db->query("call sp_RjabatanTampil()");
        return $hasil->result();
    }
    public function jabatan_ambil($id)
    {
        $hasil = $this->db->query("call sp_RjabatanDetail('".$id."')");
        return $hasil->result();
    }
    public function jabatan_tambah($kode, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl)
    {
        // $hasil = $this->db->query("call sp_RjabatanTambah('".$kode."','".$nama."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."','".$diubaholeh."','".$diubahtgl."')");

        $hasil = $this->db->query("call sp_RjabatanTambah('".$kode."','".$nama."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."')");
    }

    public function jabatan_ubah($kode, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl)
    {
        // $hasil = $this->db->query("call sp_RjabatanUbah('".$kode."','".$nama."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."','".$diubaholeh."','".$diubahtgl."')");

        $hasil = $this->db->query("call sp_RjabatanUbah('".$kode."','".$nama."','".$dibuattgl."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function jabatan_hapus($id)
    {
        $hasil = $this->db->query("call sp_RjabatanHapus('".$id."')");
    }
    // end: Jabatan

    // begin: Pangkat
    public function pangkat_list()
    {
        // $hasil = $this->db->query("call sp_api_auditor_tampil()");
        $hasil = $this->db->query("call sp_RpangkatTampil()");
        return $hasil->result();
    }
    public function pangkat_ambil($id)
    {
        $hasil = $this->db->query("call sp_RpangkatDetail('".$id."')");
        return $hasil->result();
    }
    public function pangkat_tambah($kode, $refgolongan, $refruang, $pekerjaan, $nama, $deskripsi, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_RpangkatTambah('".$kode."','".$refgolongan."','".$refruang."','".$pekerjaan."','".$nama."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."')");
    }

    public function pangkat_ubah($kode, $refgolongan, $refruang, $pekerjaan, $nama, $deskripsi, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_RpangkatUbah('".$kode."','".$refgolongan."','".$refruang."','".$pekerjaan."','".$nama."','".$deskripsi."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function pangkat_hapus($id)
    {
        $hasil = $this->db->query("call sp_RpangkatHapus('".$id."')");
    }
    // end: Pangkat

    // begin: Pendidikan
    public function pendidikan_list()
    {
        // $hasil = $this->db->query("call sp_api_auditor_tampil()");
        $hasil = $this->db->query("call sp_RpendidikanTampil()");
        return $hasil->result();
    }
    public function pendidikan_ambil($id)
    {
        $hasil = $this->db->query("call sp_RpendidikanDetail('".$id."')");
        return $hasil->result();
    }
    public function pendidikan_tambah($kode, $jenjang, $deskripsi, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_RpendidikanTambah('".$kode."','".$jenjang."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."')");
    }

    public function pendidikan_ubah($kode, $jenjang, $deskripsi, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_RpendidikanUbah('".$kode."','".$jenjang."','".$deskripsi."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function pendidikan_hapus($id)
    {
        $hasil = $this->db->query("call sp_RpendidikanHapus('".$id."')");
    }
    // end: Pendidikan

    // begin: Jabatan Struktural
    public function struktural_list()
    {
        // $hasil = $this->db->query("call sp_api_auditor_tampil()");
        $hasil = $this->db->query("call sp_RstrukturalTampil()");
        return $hasil->result();
    }
    public function struktural_ambil($id)
    {
        $hasil = $this->db->query("call sp_RstrukturalDetail('".$id."')");
        return $hasil->result();
    }
    public function struktural_tambah($kode, $jabatan, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_RstrukturalTambah('".$kode."','".$jabatan."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function struktural_ubah($kode, $jabatan, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_RstrukturalUbah('".$kode."','".$jabatan."','".$deskripsi."','".$dibuatoleh."','".$dibuattgl."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function struktural_hapus($id)
    {
        $hasil = $this->db->query("call sp_RstrukturalHapus('".$id."')");
    }
    // end: Jabatan Struktural
}
