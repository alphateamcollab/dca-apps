<?php
/**
 * Model Pendidikan get and post data for CRUD,
 * also for history / list data by NIP, data detail by ID
 */
class Pangkat_model extends CI_Model
{
    public function pangkat_riwayat($nip)
    {
        $hasil = $this->db->query("call sp_pangkatRiwayat('".$nip."')");

        // foreach ($hasil->result() as $row) {
        //     $data['Pangkat_ID']              = $row->Pangkat_ID;
        //     $data['PangkatGol_ID']              = $row->PangkatGol_ID;
        //     $data['Auditor_NIP']                = $row->Auditor_NIP;
        //     $data['Pangkat_Kode']               = $row->Pangkat_Kode;
        //     $data['GolRuang_Kode']              = $row->GolRuang_Kode;
        //     $data['Pangkat_Nama']               = $row->Pangkat_Nama;
        //     $data['Pangkat_NoSuratKeputusan']   = $row->Pangkat_NoSuratKeputusan;
        //     $data['Pangkat_TglSuratKeputusan']  = indonesian_shortdate($row->Pangkat_TglSuratKeputusan);
        //     $data['Pangkat_TglMulaiTugas']      = indonesian_shortdate($row->Pangkat_TglMulaiTugas);
        //     $data['Pangkat_SK']                 = $row->Pangkat_SK;
        //     $data['DibuatOleh']                 = $row->DibuatOleh;
        //     $data['DibuatTgl']                  = $row->DibuatTgl;
        //     $data['DiubahOleh']                 = $row->DiubahOleh;
        //     $data['DiubahTgl']                  = $row->DiubahTgl;
        // }
        //
        // return [$data];

        return $hasil->result();
    }

    public function pangkat_list()
    {
        $hasil = $this->db->query("call sp_pangkatTampil()");
        return $hasil->result();
    }

    public function pangkat_ambil($id)
    {
        $hasil = $this->db->query("call sp_pangkatDetail('".$id."')");

        foreach ($hasil->result() as $row) {
            $data['PangkatGol_ID']              = $row->PangkatGol_ID;
            $data['Auditor_NIP']                = $row->Auditor_NIP;
            $data['Pangkat_Kode']               = $row->Pangkat_Kode;
            $data['GolRuang_Kode']              = $row->GolRuang_Kode;
            $data['Pangkat_Nama']               = $row->Pangkat_Nama;
            $data['Pangkat_NoSuratKeputusan']   = $row->Pangkat_NoSuratKeputusan;
            $data['Pangkat_TglSuratKeputusan']  = indonesian_shortdate($row->Pangkat_TglSuratKeputusan);
            $data['Pangkat_TglMulaiTugas']      = indonesian_shortdate($row->Pangkat_TglMulaiTugas);
            $data['Pangkat_SK']                 = $row->Pangkat_SK;
            $data['DibuatOleh']                 = $row->DibuatOleh;
            $data['DibuatTgl']                  = $row->DibuatTgl;
            $data['DiubahOleh']                 = $row->DiubahOleh;
            $data['DiubahTgl']                  = $row->DiubahTgl;
        }

        return [$data];
    }

    public function pangkat_tambah($id, $nip, $kode, $pangkat, $noSK, $tglSK, $tmt, $SK, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_pangkatTambah('".$nip."', '".$kode."', '".$pangkat."', '".$noSK."', '".$tglSK."', '".$tmt."', '".$SK."', '".$dibuatoleh."', '".$dibuattgl."')");
    }

    public function pangkat_ubah($id, $nip, $kode, $pangkat, $noSK, $tglSK, $tmt, $SK, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_pangkatUbah('".$id."', '".$nip."', '".$kode."', '".$pangkat."', '".$noSK."', '".$tglSK."', '".$tmt."', '".$SK."', '".$diubaholeh."', '".$diubahtgl."')");
    }

    public function pangkat_ubahfile($id, $nip, $kode, $pangkat, $noSK, $tglSK, $tmt, $SK, $diubaholeh, $diubahtgl)
    {
        $file = $this->pangkat_hapusfile($id);
        if (!$this->db->query("call sp_pangkatUbah('".$id."', '".$nip."', '".$kode."', '".$pangkat."', '".$noSK."', '".$tglSK."', '".$tmt."', '".$SK."', '".$diubaholeh."', '".$diubahtgl."')")) {
            return false;
        }
        unlink('./' .$file->Pangkat_SK);
        return true;
    }

    public function pangkat_hapus($id)
    {
        $file = $this->pangkat_hapusfile($id);
        if (!$this->db->query("call sp_pangkatHapus('".$id."')")) {
            return false;
        }
        unlink('./' .$file->Pangkat_SK);
        return true;
    }

    public function pangkat_hapusfile($id)
    {
        return $this->db->select()
                ->from('t_pangkat')
                ->where('PangkatGol_ID', $id)
                ->get()
                ->row();
    }
}
