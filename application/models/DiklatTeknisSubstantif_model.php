<?php
/**
 *
 */
class DiklatTeknisSubstantif_model extends CI_Model
{
    public function teknissubstantif_riwayat($nip)
    {
        $hasil = $this->db->query("call sp_teknissubstantifRiwayat('".$nip."')");
        return $hasil->result();
    }

    public function teknissubstantif_list()
    {
        $hasil = $this->db->query("call sp_teknissubstantifTampil()");
        return $hasil->result();
    }

    public function teknissubstantif_ambil($id)
    {
        $hasil = $this->db->query("call sp_teknissubstantifDetail('".$id."')");

        foreach ($hasil->result() as $row) {
            $data['TeknisSubstantif_ID']        = $row->TeknisSubstantif_ID;
            $data['Auditor_NIP']                = $row->Auditor_NIP;
            $data['Diklat_Kode']                = $row->Diklat_Kode;
            $data['TeknisSubstantif_TglMulai']  = indonesian_shortdate($row->TeknisSubstantif_TglMulai);
            $data['TeknisSubstantif_TglAkhir']  = indonesian_shortdate($row->TeknisSubstantif_TglAkhir);
            $data['TeknisSubstantif_NoSTMPL']   = $row->TeknisSubstantif_NoSTMPL;
            $data['TeknisSubstantif_TglSTMPL']  = indonesian_shortdate($row->TeknisSubstantif_TglSTMPL);
            $data['TeknisSubstantif_Sertifikat']= $row->TeknisSubstantif_Sertifikat;
            $data['DibuatOleh']                 = $row->DibuatOleh;
            $data['DibuatTgl']                  = $row->DibuatTgl;
            $data['DiubahOleh']                 = $row->DiubahOleh;
            $data['DiubahTgl']                  = $row->DiubahTgl;
        }

        return [$data];
    }

    public function teknissubstantif_tambah($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_teknissubstantifTambah('".$nip."', '".$kode."', '".$mulaidiklat."', '".$akhirdiklat."', '".$nostmpl."', '".$tglstmpl."', '".$sertifikat."',
       '".$dibuatoleh."', '".$dibuattgl."')");
    }

    public function teknissubstantif_ubah($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_teknissubstantifUbah('".$id."', '".$nip."', '".$kode."', '".$mulaidiklat."', '".$akhirdiklat."', '".$nostmpl."', '".$tglstmpl."', '".$sertifikat."',
       '".$diubaholeh."', '".$diubahtgl."')");
    }

    public function teknissubstantif_ubahfile($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $diubaholeh, $diubahtgl)
    {
        $file = $this->teknissubstantif_hapusfile($id);
        if (!$this->db->query("call sp_teknissubstantifUbah('".$id."', '".$nip."', '".$kode."', '".$mulaidiklat."', '".$akhirdiklat."', '".$nostmpl."', '".$tglstmpl."', '".$sertifikat."',
       '".$diubaholeh."', '".$diubahtgl."')")) {
           return false;
       }
       unlink('./' .$file->TeknisSubstantif_Sertifikat);
       return true;
    }

    public function teknissubstantif_hapus($id)
    {
        $file = $this->teknissubstantif_hapusfile($id);
        if (!$this->db->query("call sp_teknissubstantifHapus('".$id."')")) {
            return false;
        }
        unlink('./' .$file->TeknisSubstantif_Sertifikat);
        return true;
    }

    public function teknissubstantif_hapusfile($id)
    {
        return $this->db->select()
                ->from('t_teknissubstantif')
                ->where('TeknisSubstantif_ID', $id)
                ->get()
                ->row();
    }
}
 ?>
