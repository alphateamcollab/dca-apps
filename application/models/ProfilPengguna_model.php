<?php
/**
 *
 */
class ProfilPengguna_model extends CI_Model
{
    public function profilpengguna_riwayat($nip)
    {
        $hasil = $this->db->query("call sp_profilpenggunaRiwayat('".$nip."')");
        return $hasil->result();
    }

    public function profilpengguna_list()
    {
        $hasil = $this->db->query("call sp_profilpenggunaTampil()");
        return $hasil->result();
    }

    public function profilpengguna_ambil($id)
    {
        $hasil = $this->db->query("call sp_penggunaDetail('".$id."')");
        return $hasil->result();
    }

    public function profilpengguna_ubah($nip, $nama, $surel, $nohp, $alamat, $diubaholeh, $diubahtgl)
    {
         $hasil = $this->db->query("call sp_penggunaUbah('".$nip."','".$nama."','".$surel."','".$nohp."','".$alamat."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function profilunit_ambil($id)
    {
        $hasil = $this->db->query("call sp_RunitkerjaDetail('".$id."')");
        return $hasil->result();
    }

    public function profilunitkerja_ubah($kodeunit, $surel, $notlp, $nofax, $alamat, $diubaholeh, $diubahtgl)
    {
         $hasil = $this->db->query("call sp_profilunitkerjaUbah('".$kodeunit."','".$surel."','".$notlp."','".$nofax."','".$alamat."','".$diubaholeh."','".$diubahtgl."')");
    }

    public function sandipengguna_ubah($nip, $sandi, $diubaholeh, $diubahtgl)
    {
         $hasil = $this->db->query("call sp_penggunasandiUbah('".$nip."','".$sandi."','".$diubaholeh."','".$diubahtgl."')");
    }
}
