<?php

/**
 *
 */
class UnitKerja_model extends CI_Model
{
    public function pengguna_nasional()
    {
        $hasil = $this->db->query('call sp_penggunanasionalTampil()');
        return $hasil->result();
    }

    public function status_haklogin($nip, $status) 
    {
      $hasil = $this->db->query("call sp_penggunahakloginUbah('".$nip."','".$status."')");
    }

    public function unitkerja_nasional()
    {
        $hasil = $this->db->query('call sp_penggunaadminTampil()');
        return $hasil->result();
    }

    public function unitkerja_perwakilan()
    {
        $hasil = $this->db->query('call sp_unitkerjaTampil()');
        return $hasil->result();
    }

    public function status_persetujuan($nip, $status)
    {
      $hasil = $this->db->query("call sp_penggunastatusUbah('".$nip."','".$status."')");
    }
}
