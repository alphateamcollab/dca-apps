<?php
/**
 *
 */
class AngkaKredit_model extends CI_Model
{
    public function angkakredit_riwayat($nip)
    {
        $hasil = $this->db->query("call sp_angkakreditRiwayat('".$nip."')");
        return $hasil->result();
    }

    public function angkakredit_list()
    {
        $hasil = $this->db->query("call sp_angkakreditTampil()");
        return $hasil->result();
    }

    public function angkakredit_ambil($id)
    {
        $hasil = $this->db->query("call sp_angkakreditDetail('".$id."')");

        foreach ($hasil->result() as $row) {
            $data['AngkaKredit_ID']             = $row->AngkaKredit_ID;
            $data['Auditor_NIP']                = $row->Auditor_NIP;
            $data['AngkaKredit_Nilai']          = $row->AngkaKredit_Nilai;
            $data['AngkaKredit_NoPAK']          = $row->AngkaKredit_NoPAK;
            $data['AngkaKredit_TglPAK']         = indonesian_shortdate($row->AngkaKredit_TglPAK);
            $data['AngkaKredit_TglMulai']       = indonesian_shortdate($row->AngkaKredit_TglMulai);
            $data['AngkaKredit_TglAkhir']       = indonesian_shortdate($row->AngkaKredit_TglAkhir);
            $data['AngkaKredit_PAK']            = $row->AngkaKredit_PAK;
            $data['DibuatOleh']                 = $row->DibuatOleh;
            $data['DibuatTgl']                  = $row->DibuatTgl;
            $data['DiubahOleh']                 = $row->DiubahOleh;
            $data['DiubahTgl']                  = $row->DiubahTgl;
        }

        return [$data];
        // return $hasil->result();
    }

    public function angkakredit_tambah($id, $nip, $nilai, $nopak, $tglpak, $mulaipak, $akhirpak, $pak, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_angkakreditTambah('".$nip."', '".$nilai."', '".$nopak."', '".$tglpak."', '".$mulaipak."', '".$akhirpak."', '".$pak."', '".$dibuatoleh."', '".$dibuattgl."')");
    }

    public function angkakredit_ubah($id, $nip, $nilai, $nopak, $tglpak, $mulaipak, $akhirpak, $pak, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_angkakreditUbah('".$id."', '".$nip."', '".$nilai."', '".$nopak."', '".$tglpak."', '".$mulaipak."', '".$akhirpak."', '".$pak."', '".$diubaholeh."', '".$diubahtgl."')");
    }

    public function angkakredit_ubahfile($id, $nip, $nilai, $nopak, $tglpak, $mulaipak, $akhirpak, $pak, $diubaholeh, $diubahtgl)
    {
        $file = $this->angkakredit_hapusfile($id);
        if (!$this->db->query("call sp_angkakreditUbah('".$id."', '".$nip."', '".$nilai."', '".$nopak."', '".$tglpak."', '".$mulaipak."', '".$akhirpak."', '".$pak."', '".$diubaholeh."', '".$diubahtgl."')")) {
            return false;
        }
        unlink('./' .$file->AngkaKredit_PAK);
        return true;
    }

    public function angkakredit_hapus($id)
    {
        $file = $this->angkakredit_hapusfile($id);
        if (!$this->db->query("call sp_angkakreditHapus('".$id."')")) {
            return false;
        }
        unlink('./' .$file->AngkaKredit_PAK);
        return true;
    }

    public function angkakredit_hapusfile($id)
    {
        return $this->db->select()
                ->from('t_angkakredit')
                ->where('AngkaKredit_ID', $id)
                ->get()
                ->row();
    }
}
