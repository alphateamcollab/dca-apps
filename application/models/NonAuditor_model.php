<?php
/**
 *
 */
class NonAuditor_model extends CI_Model
{
    public function nonauditor_list_pusbin()
    {
        $hasil = $this->db->query("call sp_nonauditorTampil()");
        return $hasil->result();
    }

    public function nonauditor_list_korwas($kode)
    {
      $hasil = $this->db->query("call sp_nonauditorTampilKorwas('".$kode."')");
      return $hasil->result();
    }

    public function nonauditor_list_unit($kode)
    {
        $hasil = $this->db->query("call sp_nonauditorTampilUnit('".$kode."')");
        return $hasil->result();
    }

    public function nonauditor_ambil($id)
    {
        $hasil = $this->db->query("call sp_nonauditorDetail('".$id."')");
        return $hasil->result();
    }

    public function nonauditor_tambah($roleid, $isauditor, $isadmin, $status, $surel, $sandi, $alamat, $nip, $nama, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $dibuatoleh, $dibuattgl)
    {
        $hasil = $this->db->query("call sp_nonauditorTambah('".$nip."','".$nama."','".$sandi."','".$alamat."','".$surel."','".$tempatlahir."','".$tanggallahir."','".$jeniskelamin."','".$roleid."','".$isauditor."',
      '".$isadmin."','".$status."','".$unitkerja."','".$dibuatoleh."','".$dibuattgl."')");
    }
    public function nonauditor_ubah($roleid, $isauditor, $isadmin, $status, $surel, $sandi, $alamat, $nip, $nama, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $diubaholeh, $diubahtgl)
    {
        $hasil = $this->db->query("call sp_nonauditorUbah('".$nip."','".$nama."','".$sandi."','".$alamat."','".$surel."','".$tempatlahir."','".$tanggallahir."','".$jeniskelamin."','".$roleid."','".$isauditor."',
      '".$isadmin."','".$status."','".$unitkerja."','".$diubaholeh."', '".$diubahtgl."')");
    }

    public function nonauditor_hapus($id)
    {
        $hasil = $this->db->query("call sp_nonauditorHapus('".$id."')");
    }

    public function get_rolecode()
    {
        $RoleGroup_ID = $this->session->userdata('RoleGroup_ID');

        $this->db->select('*');
        $this->db->where('RoleGroup_ID', $RoleGroup_ID);
        $this->db->from('r_role');
        $result = $this->db->get();
        return $result->result();
    }

    public function get_rolecodepusbin()
    {
        // $RoleGroup_ID = $this->session->userdata('RoleGroup_ID');

        $this->db->select('*');
        // $this->db->where('RoleGroup_ID', $RoleGroup_ID);
        $this->db->from('r_role');
        $result = $this->db->get();
        return $result->result();
    }
}
