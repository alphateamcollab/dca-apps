<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AppPortal_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
    }

    public function index()
    {
        $data["title"]="Portal Aplikasi Sibijak";
        // $data["subtitle"]="Selamat Datang";

        $this->load->view('appportal_view', $data);
    }
}
