<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jabatan_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('referensi_model', 'referensi');
    }

    public function index()
    {
        $data["title"]="Jabatan Fungsional";
        // $data["subtitle"]="Selamat Datang";
        // $data['jabatan'] = $this->db->get('t_jabatan');

        $this->template->display('ref_jabatan/jabatan_list_view', $data);
    }

    // fungsi untuk menampilkan data ke datatable
    public function data()
    {
        // penguraian data dari di referensi_model
        // dan simpan ke variabel $data
        $data = $this->referensi->jabatan_list();
        // cetak variable $data dalam bentuk JSON
        echo json_encode($data);
    }

    // fungsi untuk mengambil data sebagai primary key
    public function ambil()
    {
        // ambil data dari get "name" yang didapat dari form ke variabel
        $id = $this->input->get('id');
        // penguarian data variable dari referensi_model
        // dan simpan ke variabel $data
        $data = $this->referensi->jabatan_ambil($id);
        // cetak variable $data dalam bentuk JSON
        echo json_encode($data);
    }

    // fungsi untuk menambah data
    public function tambah()
    {
        // ambil data dari post "name" yang didapat dari form ke variabel

        $kode = $this->input->post('Jabatan_Kode');
        $nama = $this->input->post('Jabatan_Nama');
        $deskripsi = $this->input->post('Jabatan_Deskripsi');
        // $dibuatoleh = $this->NIP;
        $dibuatoleh = '1234567890';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '';
        $diubahtgl = '';
        // penguarian data variable dari referensi_model
        // dan simpan ke variabel $data
        $data = $this->referensi->jabatan_tambah($kode, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
        // cetak variable $data dalam bentuk JSON
        echo json_encode($data);
    }

    // fungsi untuk mengubah data
    public function ubah()
    {
        // ambil data dari post "name" yang didapat dari form ke variabel
        $kode = $this->input->post('Jabatan_Kode');
        $nama = $this->input->post('Jabatan_Nama');
        $deskripsi = $this->input->post('Jabatan_Deskripsi');
        // $dibuatoleh = $this->input->post('dibuatoleh');
        // $dibuattgl = $this->input->post('dibuattgl');
        // $diubaholeh = $this->NIP;
        $dibuatoleh = '1234567890';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '1';
        $diubahtgl = date('Y-m-d H:i:s');
        // penguarian data variable dari referensi_model
        // dan simpan ke variabel $data
        $data = $this->referensi->jabatan_ubah($kode, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
        // cetak variable data dalam bentuk JSON
        echo json_encode($data);
    }

    // fungsi untuk menghapus data
    public function hapus()
    {
        // ambil data dari post "name" yang didapat dari form ke variabel
        $id = $this->input->post('id');
        // penguarian data variable dari referensi_model
        // dan simpan ke variabel $data
        $data = $this->referensi->jabatan_hapus($id);
        // cetak variable $data dalam bentuk JSON
        echo json_encode($data);
    }
}
