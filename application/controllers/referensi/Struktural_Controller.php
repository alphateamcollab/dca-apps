<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Struktural_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('referensi_model', 'referensi');
    }

    public function index()
    {
        $data["title"]="Data Referensi Jabatan Struktural";
        // $data["subtitle"]="Selamat Datang";

        $this->template->display('ref_struktural/struktural_list_view', $data);
    }

    public function data()
    {
        $data = $this->referensi->struktural_list();
        echo json_encode($data);
    }

    public function ambil()
    {
        $id = $this->input->get('id');
        $data = $this->referensi->struktural_ambil($id);
        echo json_encode($data);
    }

    public function tambah()
    {
        $kode = $this->input->post('Struktural_Kode');
        $jabatan = $this->input->post('Struktural_Jabatan');
        $deskripsi = $this->input->post('Struktural_Deskripsi');
        // $dibuatoleh = $this->NIP;
        $dibuatoleh = '1234567890';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '';
        $diubahtgl = '';
        $data = $this->referensi->struktural_tambah($kode, $jabatan, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function ubah()
    {
        $kode = $this->input->post('Struktural_Kode');
        $jabatan = $this->input->post('Struktural_Jabatan');
        $deskripsi = $this->input->post('Struktural_Deskripsi');
        // $dibuatoleh = $this->input->post('dibuatoleh');
        // $dibuattgl = $this->input->post('dibuattgl');
        // $diubaholeh = $this->NIP;
        $dibuatoleh = '1234567890';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '1';
        $diubahtgl = date('Y-m-d H:i:s');
        $data = $this->referensi->struktural_ubah($kode, $jabatan, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function hapus()
    {
        $id = $this->input->post('id');
        $data = $this->referensi->struktural_hapus($id);
        echo json_encode($data);
    }
}
