<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instansi_Controller extends CI_Controller{

      function __construct()
      {
        parent:: __construct();
        $this->load->model('referensi_model', 'referensi');
      }

      public function index()
      {
        $data["title"]="Tabel Instansi";
    		// $data["subtitle"]="Selamat Datang";
        $data['jenis_instansi'] =$this->db->get('r_jenisinstansi');

        $this->template->display('ref_instansi/instansi_list_view',$data);
      }

      public function data()
      {
        $data = $this->referensi->instansi_list();
        echo json_encode($data);
      }

      public function ambil()
      {
          $id = $this->input->get('id');
          $data = $this->referensi->instansi_ambil($id);
          echo json_encode($data);
      }

      public function tambah()
      {

        // var kode          = $('[name="Instansi_Kode"]').val();
        // var jenis         = $('[name="JenisInstansi_Kode"]').val();
        // var instansi_no1  = $('[name="Instansi_No1"]').val();
        // var instansi_no2  = $('[name="Instansi_No2"]').val();
        // var nama          = $('[name="Instansi_Nama"]').val();
        // var namasingkat   = $('[name="Instansi_NamaSingkat"]').val();
        // var alamat        = $('[name="Instansi_Alamat"]').val();
        // var kota          = $('[name="Instansi_KodeKota"]').val();
        // var kodepos       = $('[name="Instansi_KodePos"]').val();
        // var no_telp       = $('[name="Instansi_NoTlp"]').val();
        // var no_fax        = $('[name="Instansi_NoFax"]').val();
        // var web           = $('[name="Instansi_Web"]').val();
        // var surel         = $('[name="Instansi_Surel"]').val();

          $jenis_instansi = $this->input->post('JenisInstansi_Kode');
          $instansi_no1   = $this->input->post('Instansi_No1');
          $instansi_no2   = '00';

          $kode = $jenis_instansi.$instansi_no1.$instansi_no2;
          $jenis = $jenis_instansi;
          $instansi_1 = $instansi_no1;
          $instansi_2 = $instansi_no2;
          $nama = $this->input->post('Instansi_Nama');
          $namasingkat = $this->input->post('Instansi_NamaSingkat');
          $pimpinan_instansi = $this->input->post('Instansi_NamaPimpinan');
          $alamat = $this->input->post('Instansi_Alamat');
          $kota = $this->input->post('Instansi_KodeKota');
          $kodepos = $this->input->post('Instansi_KodePos');
          $no_telp = $this->input->post('Instansi_NoTlp');
          $no_fax = $this->input->post('Instansi_NoFax');
          $web = $this->input->post('Instansi_Web');
          $surel = $this->input->post('Instansi_Surel');
          $tgl_berlaku = $this->input->post('Berlaku_Tgl');
          $tgl_berkahir = $this->input->post('Berkahir_Tgl');
          $dibuatoleh = '1234567890';
          $dibuattgl = date("Y-m-d H:i:s");
          $diubaholeh = '';
          $diubahtgl = '';
          $data = $this->referensi->instansi_tambah($kode, $jenis, $instansi_1, $instansi_2, $nama, $namasingkat, $pimpinan_instansi, $alamat, $kota, $kodepos, $no_telp, $no_fax, $web, $surel, $tgl_berlaku, $tgl_berkahir, $dibuatoleh, $dibuattgl);
          echo json_encode($data);
      }

      public function ubah()
      {
          $kode = $this->input->post('Instansi_Kode');
          $jenis = $this->input->post('JenisInstansi_Kode');
          $instansi_1 = $this->input->post('Instansi_No1');
          $instansi_2 = $this->input->post('Instansi_No2');
          $nama = $this->input->post('Instansi_Nama');
          $namasingkat = $this->input->post('Instansi_NamaSingkat');
          $pimpinan_instansi = $this->input->post('Instansi_NamaPimpinan');
          $alamat = $this->input->post('Instansi_Alamat');
          $kota = $this->input->post('Instansi_KodeKota');
          $kodepos = $this->input->post('Instansi_KodePos');
          $no_telp = $this->input->post('Instansi_NoTlp');
          $no_fax = $this->input->post('Instansi_NoFax');
          $web = $this->input->post('Instansi_Web');
          $surel = $this->input->post('Instansi_Surel');
          $tgl_berlaku = $this->input->post('Berlaku_Tgl');
          $tgl_berkahir = $this->input->post('Berkahir_Tgl');
          // $dibuatoleh = '1';
          // $dibuattgl = date("Y-m-d H:i:s");
          $diubaholeh = '1234567890';
          $diubahtgl = date('Y-m-d H:i:s');
          // penguarian data variable dari referensi_model
          // dan simpan ke variabel $data
          $data = $this->referensi->instansi_ubah($kode, $jenis, $instansi_1, $instansi_2, $nama, $namasingkat, $pimpinan_instansi, $alamat, $kota, $kodepos, $no_telp, $no_fax, $web, $surel, $tgl_berlaku, $tgl_berkahir, $diubaholeh, $diubahtgl);
          // cetak variable data dalam bentuk JSON
          echo json_encode($data);
      }

      // fungsi untuk menghapus data
      public function hapus()
      {
          // ambil data dari post "name" yang didapat dari form ke variabel
          $id = $this->input->post('id');
          // penguarian data variable dari referensi_model
          // dan simpan ke variabel $data
          $data = $this->referensi->instansi_hapus($id);
          // cetak variable $data dalam bentuk JSON
          echo json_encode($data);
      }
}
