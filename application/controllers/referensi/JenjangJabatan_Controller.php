<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenjangJabatan_Controller extends CI_Controller
{
    function __construct()
    {
      parent:: __construct();
      $this->load->model('referensi_model', 'referensi');
    }

    public function index()
    {
      $data["title"]="Jenjang Jabatan Fungsional";
  		// $data["subtitle"]="Selamat Datang";
      $data['jabatan'] = $this->db->get('r_jabatan');

      $this->template->display('ref_jenjangjabatan/jenjangjabatan_list_view',$data);
    }

    public function data()
    {
      $data = $this->referensi->jenjangjabatan_list();
      echo json_encode($data);
    }

    public function ambil()
    {
        $id = $this->input->get('id');
        $data = $this->referensi->jenjangjabatan_ambil($id);
        echo json_encode($data);
    }

    public function tambah()
    {
        // $id = $this->input->post('JenjangJabatan_Id');
        $kode = $this->input->post('JenjangJabatan_Kode');
        $refjabatan = $this->input->post('Jabatan_Kode');
        $nama = $this->input->post('JenjangJabatan_Nama');
        $level = $this->input->post('JenjangJabatan_Level');
        $deskripsi = $this->input->post('JenjangJabatan_Deskripsi');
        // $dibuatoleh = $this->NIP;
        $dibuatoleh = '1234567890';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '';
        $diubahtgl = '';
        $data = $this->referensi->jenjangjabatan_tambah($kode, $refjabatan, $nama, $level, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function ubah()
    {
      $id = $this->input->post('JenjangJabatan_Id');
      $kode = $this->input->post('JenjangJabatan_Kode');
      $refjabatan = $this->input->post('Jabatan_Kode');
      $nama = $this->input->post('JenjangJabatan_Nama');
      $level = $this->input->post('JenjangJabatan_Level');
      $deskripsi = $this->input->post('JenjangJabatan_Deskripsi');// $dibuatoleh = $this->input->post('dibuatoleh');
        // $dibuattgl = $this->input->post('dibuattgl');
        // $diubaholeh = $this->NIP;
        $dibuatoleh = '1';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '1234567890';
        $diubahtgl = date('Y-m-d H:i:s');
        $data = $this->referensi->jenjangjabatan_ubah($id, $kode, $refjabatan, $nama, $level, $deskripsi, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function hapus()
    {
        $id = $this->input->post('id');
        $data = $this->referensi->jenjangjabatan_hapus($id);
        echo json_encode($data);
    }
}
