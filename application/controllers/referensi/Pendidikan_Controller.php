<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendidikan_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('referensi_model', 'referensi');
    }

    public function index()
    {
        $data["title"]="Data Referensi Jenjang Pendidikan";
        // $data["subtitle"]="Selamat Datang";

        $this->template->display('ref_pendidikan/pendidikan_list_view', $data);
    }

    public function data()
    {
        $data = $this->referensi->pendidikan_list();
        echo json_encode($data);
    }

    public function ambil()
    {
        $id = $this->input->get('id');
        $data = $this->referensi->pendidikan_ambil($id);
        echo json_encode($data);
    }

    public function tambah()
    {
        $kode = $this->input->post('Pendidikan_Kode');
        $jenjang = $this->input->post('Pendidikan_Jenjang');
        $deskripsi = $this->input->post('Pendidikan_Deskripsi');
        // $dibuatoleh = $this->NIP;
        $dibuatoleh = '1234567890';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '';
        $diubahtgl = '';
        $data = $this->referensi->pendidikan_tambah($kode, $jenjang, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function ubah()
    {
        $kode = $this->input->post('Pendidikan_Kode');
        $jenjang = $this->input->post('Pendidikan_Jenjang');
        $deskripsi = $this->input->post('Pendidikan_Deskripsi');
        // $dibuatoleh = $this->input->post('dibuatoleh');
        // $dibuattgl = $this->input->post('dibuattgl');
        // $diubaholeh = $this->NIP;
        $dibuatoleh = '1234567890';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '1';
        $diubahtgl = date('Y-m-d H:i:s');
        $data = $this->referensi->pendidikan_ubah($kode, $jenjang, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function hapus()
    {
        $id = $this->input->post('id');
        $data = $this->referensi->pendidikan_hapus($id);
        echo json_encode($data);
    }
}
