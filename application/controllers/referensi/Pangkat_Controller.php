<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pangkat_Controller extends CI_Controller
{
    function __construct()
    {
      parent:: __construct();
      $this->load->model('referensi_model', 'referensi');
    }

    public function index()
    {
      $data["title"]="Tabel Pangkat";
  		// $data["subtitle"]="Selamat Datang";
      $data['golongan'] = $this->db->get('r_golongan');
      $data['ruang'] = $this->db->get('r_ruang');

      $this->template->display('ref_pangkat/pangkat_list_view',$data);
    }

    public function data()
    {
      $data = $this->referensi->pangkat_list();
      echo json_encode($data);
    }

    public function ambil()
    {
        $id = $this->input->get('id');
        $data = $this->referensi->pangkat_ambil($id);
        echo json_encode($data);
    }

    public function tambah()
    {

        $pekerjaan_id = $this->input->post('Pekerjaan_ID');
        $ruang_kode = $this->input->post('Ruang_Kode');
        $golongan_kode = $this->input->post('Golongan_Kode');

        if ($golongan_kode < 10) {
           $golongan = '0'.$golongan_kode;
        } else {
           $golongan = $golongan_kode;
        }

        $kode = $pekerjaan_id.$golongan_kode.$ruang_kode."1";
        $refgolongan = $golongan_kode;
        $refruang = $ruang_kode;
        $pekerjaan = $this->input->post('Pekerjaan_ID');
        $nama = $this->input->post('Pangkat_Nama');
        $deskripsi = $this->input->post('Pangkat_Deskripsi');
        $dibuatoleh = '1234567890';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '';
        $diubahtgl = '';
        $data = $this->referensi->pangkat_tambah($kode, $refgolongan, $refruang, $pekerjaan, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function ubah()
    {
      $kode = $this->input->post('Pangkat_Kode');
      $refgolongan = $this->input->post('Golongan_Kode');
      $refruang = $this->input->post('Ruang_Kode');
      $pekerjaan = $this->input->post('Pekerjaan_ID');
      $nama = $this->input->post('Pangkat_Nama');
      $deskripsi = $this->input->post('Pangkat_Deskripsi');// $dibuatoleh = $this->input->post('dibuatoleh');
        // $dibuattgl = $this->input->post('dibuattgl');
        // $diubaholeh = $this->NIP;
        $dibuatoleh = '1';
        $dibuattgl = date("Y-m-d H:i:s");
        $diubaholeh = '1234567890';
        $diubahtgl = date('Y-m-d H:i:s');
        $data = $this->referensi->pangkat_ubah($kode, $refgolongan, $refruang, $pekerjaan, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function hapus()
    {
        $id = $this->input->post('id');
        $data = $this->referensi->pangkat_hapus($id);
        echo json_encode($data);
    }

    public function getPekerjaanId(){

    $data = array('status'=>'403','msg'=>'method not allowed');
    if($this->input->get()){
      $this->db->where('Ruang_Kode',$this->input->get('Ruang_Kode'));
      $x = $this->db->get('r_ruang');
      $data = array('status'=>'200','data'=>$x->result());
    }
    echo json_encode($data);
  }

}
?>
