<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisInstansi_Controller extends CI_Controller{

      function __construct()
      {
        parent:: __construct();
        $this->load->model('referensi_model', 'referensi');
      }

      public function index()
      {
        $data["title"]="Tabel Jenis Instansi";
    		// $data["subtitle"]="Selamat Datang";

        $this->template->display('ref_jenisinstansi/jenisinstansi_list_view',$data);
      }

      public function data()
      {
        $data = $this->referensi->jenisinstansi_list();
        echo json_encode($data);
      }

      public function ambil()
      {
          $id = $this->input->get('id');
          $data = $this->referensi->jenisinstansi_ambil($id);
          echo json_encode($data);
      }

      public function tambah()
      {
          $kode = $this->input->post('JenisInstansi_Kode');
          $nama = $this->input->post('JenisInstansi_Nama');
          $singkatan = $this->input->post('JenisInstansi_Singkatan');
          // $dibuatoleh = $this->NIP;
          $dibuatoleh = '1234567890';
          $dibuattgl = date("Y-m-d H:i:s");
          $diubaholeh = '';
          $diubahtgl = '';
          $data = $this->referensi->jenisinstansi_tambah($kode, $nama, $singkatan, $dibuatoleh, $dibuattgl);
          echo json_encode($data);
      }

      public function ubah()
      {
          $kode = $this->input->post('JenisInstansi_Kode');
          $nama = $this->input->post('JenisInstansi_Nama');
          $singkatan = $this->input->post('JenisInstansi_Singkatan');
          $dibuatoleh = '1';
          $dibuattgl = date("Y-m-d H:i:s");
          $diubaholeh = '1234567890';
          $diubahtgl = date('Y-m-d H:i:s');
          // penguarian data variable dari referensi_model
          // dan simpan ke variabel $data
          $data = $this->referensi->jenisinstansi_ubah($kode, $nama, $singkatan, $diubaholeh, $diubahtgl);
          // cetak variable data dalam bentuk JSON
          echo json_encode($data);
      }

      // fungsi untuk menghapus data
      public function hapus()
      {
          // ambil data dari post "name" yang didapat dari form ke variabel
          $id = $this->input->post('id');
          // penguarian data variable dari referensi_model
          // dan simpan ke variabel $data
          $data = $this->referensi->jenisinstansi_hapus($id);
          // cetak variable $data dalam bentuk JSON
          echo json_encode($data);
      }
}
?>
