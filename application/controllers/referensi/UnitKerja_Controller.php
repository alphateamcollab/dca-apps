<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UnitKerja_Controller extends CI_Controller{

      function __construct()
      {
        parent:: __construct();
        $this->load->model('referensi_model', 'referensi');
      }

      public function index()
      {
        $data["title"]="Tabel Unit Kerja";
    		// $data["subtitle"]="Selamat Datang";
        $data['instansi'] = $this->db->get('r_instansi');

        $this->template->display('ref_unitkerja/unitkerja_list_view',$data);
      }

      public function data()
      {
        $data = $this->referensi->unitkerja_list();
        echo json_encode($data);
      }

      public function ambil()
      {
          $id = $this->input->get('id');
          $data = $this->referensi->unitkerja_ambil($id);
          echo json_encode($data);
      }

      public function tambah()
      {

          $kode_instansi  = $this->input->post('Instansi_Kode');
          $unitkerja_daerah   = $this->input->post('UnitKerja_DaerahKode');
          $unitkerja_no1   = $this->input->post('UnitKerja_No1');
          $unitkerja_no2   = '00';

          $kode = $kode_instansi.$unitkerja_daerah.$unitkerja_no1.$unitkerja_no2;
          $kode_transl1 = $kode_instansi.$unitkerja_daerah.$unitkerja_no1.$unitkerja_no2;
          $instansi_kode = $kode_instansi;
          $kode_daerah = $this->input->post('UnitKerja_DaerahKode');
          $unitkerja_1 = $unitkerja_no1;
          $unitkerja_2 = $unitkerja_no2;
          $nama_unit = $this->input->post('UnitKerja_NamaUnitInstansi');
          $nama_singkat = $this->input->post('UnitKerja_SngktnUnitInstansi');
          $nama_pimpinan = $this->input->post('UnitKerja_NamaPimpinan');
          $alamat = $this->input->post('UnitKerja_Alamat');
          $kodepos = $this->input->post('UnitKerja_KodePos');
          $no_telp = $this->input->post('UnitKerja_NoTlp');
          $no_fax = $this->input->post('UnitKerja_NoFax');
          $web = $this->input->post('UnitKerja_Web');
          $surel = $this->input->post('UnitKerja_Surel');
          $pemegang_berkas = $this->input->post('UnitKerja_PemegangBerkas');
          $unit = $this->input->post('UnitKerja_Unit');
          $kode_kota = $this->input->post('UnitKerja_KodeKota');
          $no_skt = $this->input->post('UnitKerja_SktNo');
          $tgl_berlaku = $this->input->post('Berlaku_Tgl');
          $tgl_berkahir = $this->input->post('Berakhir_Tgl');
          $dibuatoleh = '1234567890';
          $dibuattgl = date("Y-m-d H:i:s");
          $diubaholeh = '';
          $diubahtgl = '';
          $data = $this->referensi->unitkerja_tambah($kode, $kode_transl1, $instansi_kode, $kode_daerah, $unitkerja_1, $unitkerja_2, $nama_unit, $nama_singkat, $nama_pimpinan, $alamat, $kodepos, $no_telp, $no_fax, $web, $surel, $pemegang_berkas, $unit, $kode_kota, $no_skt, $tgl_berlaku, $tgl_berkahir, $dibuatoleh, $dibuattgl);
          echo json_encode($data);
      }

      public function ubah()
      {
          $kode = $this->input->post('UnitKerja_Kode');
          $kode_transl1 = $this->input->post('UnitKerja_TransL1Kode');
          $instansi_kode = $this->input->post('Instansi_Kode');
          $kode_daerah = $this->input->post('UnitKerja_DaerahKode');
          $unitkerja_1 = $this->input->post('UnitKerja_No1');
          $unitkerja_2 = $this->input->post('UnitKerja_No2');
          $nama_unit = $this->input->post('UnitKerja_NamaUnitInstansi');
          $nama_singkat = $this->input->post('UnitKerja_SngktnUnitInstansi');
          $nama_pimpinan = $this->input->post('UnitKerja_NamaPimpinan');
          $alamat = $this->input->post('UnitKerja_Alamat');
          $kodepos = $this->input->post('UnitKerja_KodePos');
          $no_telp = $this->input->post('UnitKerja_NoTlp');
          $no_fax = $this->input->post('UnitKerja_NoFax');
          $web = $this->input->post('UnitKerja_Web');
          $surel = $this->input->post('UnitKerja_Surel');
          $pemegang_berkas = $this->input->post('UnitKerja_PemegangBerkas');
          $unit = $this->input->post('UnitKerja_Unit');
          $kode_kota = $this->input->post('UnitKerja_KodeKota');
          $no_skt = $this->input->post('UnitKerja_SktNo');
          $tgl_berlaku = $this->input->post('Berlaku_Tgl');
          $tgl_berkahir = $this->input->post('Berakhir_Tgl');
          // $dibuatoleh = '1';
          // $dibuattgl = date("Y-m-d H:i:s");
          $diubaholeh = '1234567890';
          $diubahtgl = date('Y-m-d H:i:s');
          // penguarian data variable dari referensi_model
          // dan simpan ke variabel $data
          $data = $this->referensi->unitkerja_ubah($kode, $kode_transl1, $instansi_kode, $kode_daerah, $unitkerja_1, $unitkerja_2, $nama_unit, $nama_singkat, $nama_pimpinan, $alamat, $kodepos, $no_telp, $no_fax, $web, $surel, $pemegang_berkas, $unit, $kode_kota, $no_skt, $tgl_berlaku, $tgl_berkahir, $diubaholeh, $diubahtgl);
          // cetak variable data dalam bentuk JSON
          echo json_encode($data);
      }

      // fungsi untuk menghapus data
      public function hapus()
      {
          // ambil data dari post "name" yang didapat dari form ke variabel
          $id = $this->input->post('id');
          // penguarian data variable dari referensi_model
          // dan simpan ke variabel $data
          $data = $this->referensi->instansi_hapus($id);
          // cetak variable $data dalam bentuk JSON
          echo json_encode($data);
      }
}
