<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Golongan_Controller extends CI_Controller
{
    function __construct()
    {
      parent:: __construct();
      $this->load->model('Referensi_model', 'referensi');
    }

    public function index()
    {
      $data["title"]="Golongan Ruang";
      $this->template->display('ref_golongan/golongan_list_view',$data);
    }

    // fungsi untuk menampilkan data ke datatable
    public function data()
    {
      // penguraian data dari di referensi_model
      // dan simpan ke variabel $data
      $data = $this->referensi->golongan_list();
      // cetak variable $data dalam bentuk JSON
      echo json_encode($data);
    }

    // fungsi untuk mengambil data sebagai primary key
    function ambil()
    {
      // ambil data dari get "name" yang didapat dari form ke variabel
      $id = $this->input->get('id');
      // penguarian data variable dari referensi_model
      // dan simpan ke variabel $data
      $data = $this->referensi->golongan_ambil($id);
      // cetak variable $data dalam bentuk JSON
      echo json_encode($data);
    }

    // fungsi untuk menambah data
    public function tambah()
    {
      // ambil data dari post "name" yang didapat dari form ke variabel
      $kode = $this->input->post('Golongan_Kode');
      $nama = $this->input->post('Golongan_Nama');
      $deskripsi = $this->input->post('Golongan_Deskripsi');
      // $dibuatoleh = $this->NIP;
      $dibuatoleh = '1234567890';
      $dibuattgl = date("Y-m-d H:i:s");
      $diubaholeh = '';
      $diubahtgl = '';
      // penguarian data variable dari referensi_model
      // dan simpan ke variabel $data
      $data = $this->referensi->golongan_tambah($kode, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
      // cetak variable $data dalam bentuk JSON
      echo json_encode($data);
    }

    // fungsi untuk mengubah data
    public function ubah()
    {
      // ambil data dari post "name" yang didapat dari form ke variabel
      $kode = $this->input->post('Golongan_Kode');
      $nama = $this->input->post('Golongan_Nama');
      $deskripsi = $this->input->post('Golongan_Deskripsi');
      $dibuatoleh = $this->input->post('DibuatOleh');
      $dibuattgl = $this->input->post('DibuatTgl');
      // $diubaholeh = $this->NIP;
      // $diubahtgl = date('Y-m-d H:i:s');
      $dibuatoleh = '1';
      $dibuattgl = date("Y-m-d H:i:s");
      $diubaholeh = '1234567890';
      $diubahtgl = date('Y-m-d H:i:s');
      // penguarian data variable dari referensi_model
      // dan simpan ke variabel $data
      $data = $this->referensi->golongan_ubah($kode, $nama, $deskripsi, $dibuatoleh, $dibuattgl, $diubaholeh, $diubahtgl);
      // cetak variable data dalam bentuk JSON
      echo json_encode($data);
    }

    // fungsi untuk menghapus data
    public function hapus()
    {
      // ambil data dari post "name" yang didapat dari form ke variabel
      $id = $this->input->post('id');
      // penguarian data variable dari referensi_model
      // dan simpan ke variabel $data
      $data = $this->referensi->golongan_hapus($id);
      // cetak variable $data dalam bentuk JSON
      echo json_encode($data);
    }

}
?>
