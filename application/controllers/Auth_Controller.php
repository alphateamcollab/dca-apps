 <?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('Auth_model', 'auth');
    }

    public function index()
    {
        $data = array(
              'title'=>"Login - Data Center Auditor",
              'dd_jenisinstansi' => $this->auth->get_jenisinstansi(),
              'jenisInstansi_selected' => $this->input->post('JenisInstansi') ? $this->input->post('JenisInstansi') : '',
              'instansi_selected' => $this->input->post('Instansi') ? $this->input->post('Instansi') : '',
              'dd_rolecode' => $this->auth->get_rolecode()
        );

        $this->load->view('login_view', $data);
    }

    public function get_instansi()
    {
        $jenisinstansi_kode = $this->input->post('value');
        $jenisinstansi = $this->auth->get_instansi($jenisinstansi_kode);

        echo '<select name="">';
        echo '<option value="">Pilih Instansi</option>';
        foreach ($jenisinstansi as $row) {
            echo '<option value="'.$row->Instansi_Kode.'">'.$row->Instansi_Nama.'</option>';
        }
        echo '</select>';
    }

    public function get_unitkerja()
    {
        $id_jenis = $this->input->post('value');
        $unitkerja = $this->auth->get_unitkerja($id_jenis);

        echo '<select name="">';
        echo '<option value="">Pilih Unit Kerja</option>';
        foreach ($unitkerja as $row) {
            echo '<option value="'.$row->UnitKerja_Kode.'">'.$row->UnitKerja_NamaUnitInstansi.'</option>';
        }
        echo '</select>';
    }

    public function cheklogin()
    {
        $email      = $this->input->post('NIP');
        //$password   = $this->input->post('Pengguna_KataSandi');
        $password = $this->input->post('KataSandi', true);
        $hashPass = password_hash($password, PASSWORD_DEFAULT);
        $test     = password_verify($password, $hashPass);
        // query chek users
        $this->db->where('NIP', $email);
        //$this->db->where('Pengguna_KataSandi',  $test);
        $users    = $this->db->get('v_pengguna');
        if ($users->num_rows()>0) {
            $user = $users->row_array();
            if (password_verify($password, $user['KataSandi'])) {
                // retrive user data to session
                $this->session->set_userdata($user);
                redirect('appportal');
            } else {
                $this->session->set_flashdata('status_login', '<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show">
                      <div class="m-alert__icon">
                        <i class="flaticon-exclamation"></i>
                        <span></span>
                      </div>
                      <div class="m-alert__text">
                        <strong>
                          Peringatan!
                        </strong>
                          NIP/NRP atau Kata sandi yang anda masukan salah
                      </div>
                  </div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('status_login', '<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show">
                    <div class="m-alert__icon">
                      <i class="flaticon-exclamation"></i>
                      <span></span>
                    </div>
                    <div class="m-alert__text">
                      <strong>
                        Peringatan!
                      </strong>
                        NIP/NRP atau Kata sandi salah
                    </div>
                </div>');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('status_login', '<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-success alert-dismissible fade show">
                <div class="m-alert__icon">
                  <i class="flaticon-info"></i>
                  <span></span>
                </div>
                <div class="m-alert__text">
                  <strong>
                    Informasi
                  </strong>
                    Anda sudah berhasil keluar dari aplikasi
                </div>
            </div>');
        redirect('auth');
    }

    // fungsi untuk menambah data
    public function tambah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/surat_penunjukan';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('Pengguna_Lampiran')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $nip          = $this->input->post('Auditor_NIP');
                $nama         = $this->input->post('Auditor_NamaLengkap');
                $tempatlahir  = $this->input->post('Auditor_TempatLahir');
                $tanggallahir = database_date($this->input->post('Auditor_TglLahir'));
                $jeniskelamin = $this->input->post('Auditor_JenisKelamin');

                $password     = $this->input->post('Pengguna_KataSandi', true);
                $options      = array("cost"=>4);
                $sandi        = password_hash($password, PASSWORD_BCRYPT, $options);

                $surel        = $this->input->post('Pengguna_Surel', true);
                $alamat       = '-';
                $instansi     = $this->input->post('Instansi_Kode');
                $unitkerja    = $this->input->post('UnitKerja_Kode');

                $roleid       = $this->input->post('Pengguna_RoleID');

                $isauditor    = 'false';
                $isadmin      = 'true';
                $status       = 1;
                $lampiran     = 'assets/file/surat_penunjukan/' .$file['upload_data']['file_name'];

                $dibuatoleh   = '1';
                $dibuattgl    = date("Y-m-d H:i:s");
                $data = $this->auth->auth_tambah($nip, $nama, $sandi, $alamat, $surel, $tempatlahir, $tanggallahir, $jeniskelamin, $roleid, $isauditor, $isadmin, $status, $unitkerja, $lampiran, $dibuatoleh, $dibuattgl);

                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['Pengguna_Lampiran']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }
}
