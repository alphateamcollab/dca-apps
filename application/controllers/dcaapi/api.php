<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class api extends REST_Controller {
	
	function __construct($config = 'rest') {
		parent::__construct($config);
		$this->load->database();
	}

	function riwayatdc_get() {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, SIMDIKLAT_API.'tables=zdmw_sibijak_riwayat_dc');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: MTIzNDU2Nzg5MDEyMzQ1Nqn748Rgk/vtJ1Ymg2KSZFINmmQniq2l2CV9XMgOtiplA8QS+Te0y6s+DkiSxGY=']
		);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		$dataJson = curl_exec($ch);
		$dataJson = json_decode($dataJson,true);

		if ($dataJson['data']) {
			$size = count($dataJson);
      for($i=0; $i < $size+1; $i++) {
      	$this->db->query("
      		call sp_teknissubstantifTambah(
	      		'".$dataJson['data'][$i]['nip_baru_nospace']."', 
	      		'".$dataJson['data'][$i]['id_kaldik']."', 
	      		'".$dataJson['data'][$i]['RlsTglMulai']."', 
	      		'".$dataJson['data'][$i]['RlsTglSelesai']."', 
	      		'".$dataJson['data'][$i]['NoUrutSTTPM']."', 
	      		'".$dataJson['data'][$i]['TglSTTPM']."', 
	      		'', 'system', '".datetime()."'
	       	)"
	      );
			}
		}
		$this->response($dataJson, $dataJson['status']);
	}

	function regist_get() {
		$nip  = $this->get('nip');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, SIMDIKLAT_API.'tables=zdmw_peserta_sibijak&data[nip_baru_nospace]='.$nip);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: MTIzNDU2Nzg5MDEyMzQ1Nqn748Rgk/vtJ1Ymg2KSZFINmmQniq2l2CV9XMgOtiplA8QS+Te0y6s+DkiSxGY=']
		);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		$dataJson = curl_exec($ch);
		$dataJson = json_decode($dataJson,true);
		$dataJsonFilter = [];
		if ($dataJson) {
			$dataJsonFilter = [
				'RlsTglMulai' => $dataJson['data'][0]['RlsTglMulai'],
				'RlsTglSelesai' => $dataJson['data'][0]['RlsTglSelesai'],
				'NoUrutSTTPM' => $dataJson['data'][0]['NoUrutSTTPM']
			];
		}
		$newDataJson["status"] = 200;
		$newDataJson["data"] = $dataJsonFilter;
		$dataJsonOK = json_decode(json_encode($newDataJson),true);
		
		$diklat = [];
		$queryDiklat = $this->db->query("
			SELECT IsAuditor, Auditor_NIP, 
			Auditor_NamaLengkap, Auditor_TempatLlahir as Auditor_TempatLahir,
			Auditor_TglLahir, Pendidikan_Tingkat,
			UnitKerja_Nama, Jabatan_Nama
			FROM v_auditor 
			WHERE Auditor_NIP = ? and IsAuditor = 'true'"
			, [$nip])->result();

		if ($queryDiklat) {
			$diklat["status"] = $dataJson['status'];
			$diklat["data"] = $queryDiklat;
			$dataDiklat = json_decode(json_encode($diklat),true);
			
			$dataResult = $this->array_merge_recursive_unique($dataJsonOK['data'], $dataDiklat['data'][0]);
			$result["status"] = 200;
			$result["data"] = $dataResult;
			$this->response($result, 200);
		} else {
			$diklat["status"] = 200;
			$diklat["data"] = $queryDiklat;
			$this->response($diklat, 200);
		}
	}

	function fasilitasi_get() {
		$nip  = $this->get('nip');
		$diklat = [];
		$queryDiklat = $this->db->query("
			SELECT IsAuditor FROM v_auditor WHERE Auditor_NIP = ? and IsAuditor = 'false'"
			, [$nip])->result();

		if ($queryDiklat) {
			$diklat["status"] = 200;
			$diklat["data"] = $queryDiklat;
			$this->response($diklat, 200);
		} else {
			$diklat["status"] = 200;
			$diklat["data"] = $queryDiklat;
			$this->response($diklat, 200);
		}
	}

	function fasilitasijfa_post() {
		$nip  = $this->get('nip');
		$kodediklat = $this->get('kodediklat');
		$nosertifikat  = $this->get('nosertifikat');
		$tglsertifikat  = $this->get('tglsertifikat');
		$sertifikat  = $this->get('sertifikat');
		$dibuatoleh = $this->get('olehdibuat');

		$hasil = $this->db->query("
			call sp_UpdateSertifikasiFromJfa(
				'".$nip."', 
				'".$kodediklat."', 
				'".$nosertifikat."', 
				'".$tglsertifikat."', 
				'".$sertifikat."', 
				'".$dibuatoleh."', 
				'".datetime()."'
			)"
		);

		return $hasil->result();
	}

	function pusbin_get() {
		$nip  = $this->get('nip');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, SIMDIKLAT_API.'tables=zdmw_sibijak_riwayat_jfa&data[nip_baru_nospace]='.$nip);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: MTIzNDU2Nzg5MDEyMzQ1Nqn748Rgk/vtJ1Ymg2KSZFINmmQniq2l2CV9XMgOtiplA8QS+Te0y6s+DkiSxGY=']
		);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		$dataJson = curl_exec($ch);
		$dataJson = json_decode($dataJson,true);
		$dataJsonFilter = [];
		$newDataJson["status"] = 200;
		if ($dataJson['data']) {
			$size = count($dataJson);
      for($i=0; $i < $size+1; $i++) {
				$newdataJsonFilter = [
					'id_kaldik' => $dataJson['data'][$i]['id_kaldik'],
					'KodeDiklat' => $dataJson['data'][$i]['KodeDiklat'],
					'UraianDiklat' => $dataJson['data'][$i]['UraianDiklat'],
					'RncPeserta' => $dataJson['data'][$i]['RncPeserta'],
					'NamaLokasi' => $dataJson['data'][$i]['NamaLokasi']
				];
				array_push($dataJsonFilter, $newdataJsonFilter);
			}
		}
		$newDataJson["data"] = $dataJsonFilter;
		$dataJsonOK = json_decode(json_encode($newDataJson),true);

		if ($dataJsonOK) {
			$this->response($dataJsonOK, 200);
		} else {
			$this->response($dataJsonOK, 200);
		}
	}

	function widyaiswara_get() {
		$nip  = $this->get('nip');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, SIMDIKLAT_API.'tables=zdmw_sibijak_peserta_mata_ajar&data[NIP_instruktur]='.$nip);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization: MTIzNDU2Nzg5MDEyMzQ1Nqn748Rgk/vtJ1Ymg2KSZFINmmQniq2l2CV9XMgOtiplA8QS+Te0y6s+DkiSxGY=']
		);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		$dataJson = curl_exec($ch);
		$dataJson = json_decode($dataJson,true);
		$dataJsonFilter = [];
		$newDataJson["status"] = 200;
		if ($dataJson['data']) {
			$size = count($dataJson);
      for($i=0; $i < $size+1; $i++) {
				$newdataJsonFilter = [
					'Nama_Instruktur' => $dataJson['data'][$i]['Nama_Instruktur'],
					'NIP_instruktur' => $dataJson['data'][$i]['NIP_instruktur'],
					'RlsTglMataAjar' => $dataJson['data'][$i]['RlsTglMataAjar'],
					'id_kaldik' => $dataJson['data'][$i]['id_kaldik'],
					'nip_baru_nospace' => $dataJson['data'][$i]['nip_baru_nospace']
				];
				array_push($dataJsonFilter, $newdataJsonFilter);
			}
		}
		$newDataJson["data"] = $dataJsonFilter;
		$dataJsonOK = json_decode(json_encode($newDataJson),true);

		if ($dataJsonOK) {
			$this->response($dataJsonOK, 200);
		} else {
			$this->response($dataJsonOK, 200);
		}
	}

	function array_merge_recursive_unique($array1, $array2) {
	  if (empty($array1)) return $array2;

	  foreach ($array2 as $key => $value) {
	    if (is_array($value) && is_array(@$array1[$key])) {
	      $value = $this->array_merge_recursive_unique($array1[$key], $value);
	    }
	    $array1[$key] = $value;
	  }
	  return $array1;
	}

}