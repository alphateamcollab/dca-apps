<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Persetujuan_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('Persetujuan_model', 'persetujuan');
    }

    public function index()
    {
        $data['title']='Perubahan Data Auditor Nasional Yang Belum di Setujui';
        $this->template->display('persetujuan/persetujuan_detail_perubahan_view', $data);
    }

    /*
    | Begin of Function Script
    | Bagian Riwayat Auditor
    */
    public function auditor_data()
    {
        $data = $this->persetujuan->auditor_data();
        echo json_encode($data);
    }

    public function auditor_setuju()
    {
        $id = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 1) ? 0 : 1 ;

        $data    = $this->persetujuan->auditor_status($id, $status);
        echo json_encode($data);
    }

    public function auditor_tolak()
    {
        $id = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 1) ? 2 : 1 ;

        $data    = $this->persetujuan->auditor_status($id, $status);
        echo json_encode($data);
    }

    /*
    | Begin of Function Script
    | Bagian Riwayat Pendidikan
    */
    public function pendidikan_data()
    {
        $data = $this->persetujuan->pendidikan_data();
        echo json_encode($data);
    }

    public function pendidikan_setuju()
    {
        $id = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 1) ? 0 : 1 ;

        $data    = $this->persetujuan->pendidikan_status($id, $status);
        echo json_encode($data);
    }

    public function pendidikan_tolak()
    {
        $id = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 1) ? 2 : 1 ;

        $data    = $this->persetujuan->pendidikan_status($id, $status);
        echo json_encode($data);
    }

    /*
    | Begin of Function Script
    | Bagian Riwayat Pangkat & Golongan
    */
    public function pangkat_data()
    {
        $data = $this->persetujuan->pangkat_data();
        echo json_encode($data);
    }

    public function pangkat_setuju()
    {
        $id = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 1) ? 0 : 1 ;

        $data    = $this->persetujuan->pangkat_status($id, $status);
        echo json_encode($data);
    }

    public function pangkat_tolak()
    {
        $id = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 1) ? 2 : 1 ;

        $data    = $this->persetujuan->pangkat_status($id, $status);
        echo json_encode($data);
    }

    /*
    | Begin of Function Script
    | Bagian Riwayat Jabatan
    */
    public function jabatan_data()
    {
        $data = $this->persetujuan->jabatan_data();
        echo json_encode($data);
    }

    public function jabatan_setuju()
    {
        $id = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 1) ? 0 : 1 ;

        $data    = $this->persetujuan->jabatan_status($id, $status);
        echo json_encode($data);
    }

    public function jabatan_tolak()
    {
        $id = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 1) ? 2 : 1 ;

        $data    = $this->persetujuan->jabatan_status($id, $status);
        echo json_encode($data);
    }
}
