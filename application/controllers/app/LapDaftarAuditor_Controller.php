<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LapDaftarAuditor_Controller extends CI_Controller
{
  function __construct()
  {
    parent:: __construct();
  }

  public function index()
  {
    $data["title"]="Laporan Daftar Auditor";
    $this->template->display('pelaporan/lap_daftarauditor', $data);
  }
}
