<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LapRekapDetail_Controller extends CI_Controller
{
  function __construct()
  {
      parent:: __construct();
      $this->load->model('Dasbor_model', 'dasbor');
  }

  public function index()
  {
      $RoleGroup = $this->session->userdata('RoleGroup');

      if ($RoleGroup === 'Level 1' || $RoleGroup === 'Pusbin JFA' ) {
          $this->pusbin();
      } elseif ($RoleGroup === 'Level 2' || $RoleGroup === 'Perwakilan BPKP') {
          $this->koorwas();
      } elseif ($RoleGroup === 'Level 3' || $RoleGroup === 'Unit Kerja') {
          $this->unitkerja();
      } else {
          redirect('error');
          exit;
      }
  }

  public function pusbin()
  {
      // Summary Auditor di Dasbor Pusbin
      $auditorpusbin = $this->dasbor->get_auditorpusbin();

      $data = array();
      foreach ($auditorpusbin as $row) {
        $data['APIPPusat_JumlahAuditorAhli'] = $row->APIPPusat_JumlahAuditorAhli;
        $data['APIPPusat_AuditorPertama'] = $row->APIPPusat_AuditorPertama;
        $data['APIPPusat_AuditorMuda'] = $row->APIPPusat_AuditorMuda;
        $data['APIPPusat_AuditorMadya'] = $row->APIPPusat_AuditorMadya;
        $data['APIPPusat_AuditorUtama'] = $row->APIPPusat_AuditorUtama;
        // $data['apippusat_jumlahahli'] = $row->APIPPusat_JumlahAuditorAhli;
        // --------------------------------------------------------------------
        $data['APIPPusat_JumlahAuditorTerampil'] = $row->APIPPusat_JumlahAuditorTerampil;
        $data['APIPPusat_AuditorPenyelia'] = $row->APIPPusat_AuditorPenyelia;
        $data['APIPPusat_AuditorPelaksanaLanjutan'] = $row->APIPPusat_AuditorPelaksanaLanjutan;
        $data['APIPPusat_AuditorPelaksana'] = $row->APIPPusat_AuditorPelaksana;
        // $data['apippusat_jumlahterampil'] = $row->APIPPusat_JumlahAuditorTerampil;

        $data['APIPPusat_TotalAuditor'] = $row->APIPPusat_TotalAuditor;
        // --------------------------------------------------------------------
        $data['BPKP_JumlahAuditorAhli'] = $row->BPKP_JumlahAuditorAhli;
        $data['BPKP_AuditorPertama'] = $row->BPKP_AuditorPertama;
        $data['BPKP_AuditorMuda'] = $row->BPKP_AuditorMuda;
        $data['BPKP_AuditorMadya'] = $row->BPKP_AuditorMadya;
        $data['BPKP_AuditorUtama'] = $row->BPKP_AuditorUtama;
        // $data['bpkp_jumlahahli'] = $row->BPKP_JumlahAuditorAhli;
        // --------------------------------------------------------------------
        $data['BPKP_JumlahAuditorTerampil'] = $row->BPKP_JumlahAuditorTerampil;
        $data['BPKP_AuditorPenyelia'] = $row->BPKP_AuditorPenyelia;
        $data['BPKP_AuditorPelaksanaLanjutan'] = $row->BPKP_AuditorPelaksanaLanjutan;
        $data['BPKP_AuditorPelaksana'] = $row->BPKP_AuditorPelaksana;
        // $data['bpkp_jumlahterampil'] = $row->BPKP_JumlahAuditorTerampil;

        $data['BPKP_TotalAuditor'] = $row->BPKP_TotalAuditor;
        // --------------------------------------------------------------------
        $data['BHMN_JumlahAuditorAhli'] = $row->BHMN_JumlahAuditorAhli;
        $data['BHMN_AuditorPertama'] = $row->BHMN_AuditorPertama;
        $data['BHMN_AuditorMuda'] = $row->BHMN_AuditorMuda;
        $data['BHMN_AuditorMadya'] = $row->BHMN_AuditorMadya;
        $data['BHMN_AuditorUtama'] = $row->BHMN_AuditorUtama;
        // $data['bhmn_jumlahahli'] = $row->BHMN_JumlahAuditorAhli;
        // --------------------------------------------------------------------
        $data['BHMN_JumlahAuditorTerampil'] = $row->BHMN_JumlahAuditorTerampil;
        $data['BHMN_AuditorPenyelia'] = $row->BHMN_AuditorPenyelia;
        $data['BHMN_AuditorPelaksanaLanjutan'] = $row->BHMN_AuditorPelaksanaLanjutan;
        $data['BHMN_AuditorPelaksana'] = $row->BHMN_AuditorPelaksana;
        // $data['bhmn_jumlahterampil'] = $row->BHMN_JumlahAuditorTerampil;

        $data['BHMN_TotalAuditor'] = $row->BHMN_TotalAuditor;
        // --------------------------------------------------------------------
        $data['APIPDaerah_JumlahAuditorAhli'] = $row->APIPDaerah_JumlahAuditorAhli;
        $data['APIPDaerah_AuditorPertama'] = $row->APIPDaerah_AuditorPertama;
        $data['APIPDaerah_AuditorMuda'] = $row->APIPDaerah_AuditorMuda;
        $data['APIPDaerah_AuditorMadya'] = $row->APIPDaerah_AuditorMadya;
        $data['APIPDaerah_AuditorUtama'] = $row->APIPDaerah_AuditorUtama;
        // $data['apipdaerah_jumlahahli'] = $row->APIPDaerah_JumlahAuditorAhli;
        // --------------------------------------------------------------------
        $data['APIPDaerah_JumlahAuditorTerampil'] = $row->APIPDaerah_JumlahAuditorTerampil;
        $data['APIPDaerah_AuditorPenyelia'] = $row->APIPDaerah_AuditorPenyelia;
        $data['APIPDaerah_AuditorPelaksanaLanjutan'] = $row->APIPDaerah_AuditorPelaksanaLanjutan;
        $data['APIPDaerah_AuditorPelaksana'] = $row->APIPDaerah_AuditorPelaksana;
        // $data['apipdaerah_jumlahterampil'] = $row->APIPDaerah_JumlahAuditorTerampil;

        $data['APIPDaerah_TotalAuditor'] = $row->APIPDaerah_TotalAuditor;
        // --------------------------------------------------------------------
        $data['APIPPusat_PenerapJFA'] = $row->APIPPusat_PenerapJFA;
        $data['BPKP_PenerapJFA'] = $row->BPKP_PenerapJFA;
        $data['BHMN_PenerapJFA'] = $row->BHMN_PenerapJFA;
        $data['APIPDaerah_PenerapJFA'] = $row->APIPDaerah_PenerapJFA;
        $data['Total_PenerapJFA'] = $row->APIPPusat_PenerapJFA+$row->BPKP_PenerapJFA+$row->BHMN_PenerapJFA+$row->APIPDaerah_PenerapJFA;
        // --------------------------------------------------------------------
        $data['Total_Pertama'] = $row->APIPPusat_AuditorPertama+$row->BPKP_AuditorPertama+$row->BHMN_AuditorPertama+$row->APIPDaerah_AuditorPertama;
        $data['Total_Muda'] = $row->APIPPusat_AuditorMuda+$row->BPKP_AuditorMuda+$row->BHMN_AuditorMuda+$row->APIPDaerah_AuditorMuda;
        $data['Total_Madya'] = $row->APIPPusat_AuditorMadya+ $row->BPKP_AuditorMadya+$row->BHMN_AuditorMadya+$row->APIPDaerah_AuditorMadya;
        $data['Total_Utama'] = $row->APIPPusat_AuditorUtama+$row->BPKP_AuditorUtama+$row->BHMN_AuditorUtama+$row->APIPDaerah_AuditorUtama;
        $data['Total_Penyelia'] = $row->APIPPusat_AuditorPenyelia+$row->BPKP_AuditorPenyelia+$row->BHMN_AuditorPenyelia+$row->APIPDaerah_AuditorPenyelia;
        $data['Total_PelaksanaLanjutan'] = $row->APIPPusat_AuditorPelaksanaLanjutan+$row->BPKP_AuditorPelaksanaLanjutan+$row->BHMN_AuditorPelaksanaLanjutan+$row->APIPDaerah_AuditorPelaksanaLanjutan;
        $data['Total_Pelaksana'] = $row->APIPPusat_AuditorPelaksana+$row->BPKP_AuditorPelaksana+$row->BHMN_AuditorPelaksana+$row->APIPDaerah_AuditorPelaksana;

        $data['Total_AuditorAhli'] = $row->APIPPusat_JumlahAuditorAhli+$row->BPKP_JumlahAuditorAhli+$row->BHMN_JumlahAuditorAhli+$row->APIPDaerah_JumlahAuditorAhli;
        $data['Total_AuditorTerampil'] = $row->APIPPusat_JumlahAuditorTerampil+$row->BPKP_JumlahAuditorTerampil+$row->BHMN_JumlahAuditorTerampil+$row->APIPDaerah_JumlahAuditorTerampil;
        $data['Total_Auditor'] =$row->APIPPusat_TotalAuditor+$row->BPKP_TotalAuditor+$row->BHMN_TotalAuditor+$row->APIPDaerah_TotalAuditor;
      }


      $data["title"]="Laporan Rekap Detail Auditor";
      $this->template->display('pelaporan/lap_rekapdetail', $data);
  }

  public function koorwas()
  {
    // Summary Auditor di Dasbor Koorwas
    $koorwas_kode = $this->session->userdata('KodeUnitKerja');
    $auditorunit = $this->dasbor->get_auditorkoorwas($koorwas_kode);

    $data = array();
    foreach ($auditorunit as $row) {

        $data['auditor_belum_ditentukan'] = 0;

        $data['unitkerja_terampil'] = $row->UnitKrja_JumlahAuditorTerampil;
        $data['unitkerja_pelaksana'] = $row->UnitKerja_AuditorPelaksana;
        $data['unitkerja_pelaksana_lanjutan'] = $row->UnitKerja_PelaksanaLanjutan;
        $data['unitkerja_penyelia'] = $row->UnitKerja_AuditorPenyelia;

        $data['unitkerja_ahli'] = $row->UnitKrja_JumlahAuditorAhli;
        $data['unitkerja_pertama'] = $row->UnitKerja_AuditorPertama;
        $data['unitkerja_muda'] = $row->UnitKerja_AuditorMuda;
        $data['unitkerja_madya'] = $row->UnitKerja_AuditorMadya;
        $data['unitkerja_utama'] = $row->UnitKerja_AuditorUtama;

        $data['apipdaerah_terampil'] = $row->APIPDaerah_JumlahAuditorTerampil;
        $data['apipdaerah_pelaksana'] = $row->APIPDaerah_AuditorPelaksana;
        $data['apipdaerah_pelaksana_lanjutan'] = $row->APIPDaerah_AuditorPelaksanaLanjutan;
        $data['apipdaerah_penyelia'] = $row->APIPDaerah_AuditorPenyelia;

        $data['apipdaerah_ahli'] = $row->APIPDaerah_JumlahAuditorAhli;
        $data['apipdaerah_pertama'] = $row->APIPDaerah_AuditorPertama;
        $data['apipdaerah_muda'] = $row->APIPDaerah_AuditorMuda;
        $data['apipdaerah_madya'] = $row->APIPDaerah_AuditorMadya;
        $data['apipdaerah_utama'] = $row->APIPDaerah_AuditorUtama;

        $data['unitkerja_penerapjfa'] = $row->UnitKerja_PenerapJFA;
        $data['apipdaerah_penerapjfa'] = $row->APIPDaerah_PenerapJFA;
        $data['apipdaerah_jumlah'] = $row->UnitKerja_PenerapJFA+$row->APIPDaerah_PenerapJFA;


        $data['jumlah_auditor_ahli'] = $row->UnitKrja_JumlahAuditorAhli+$row->APIPDaerah_JumlahAuditorAhli;
        $data['jumlah_auditor_terampil']= $row->UnitKrja_JumlahAuditorTerampil+$row->APIPDaerah_JumlahAuditorTerampil;
        $data['jumlah_auditor_total'] =  $row->UnitKrja_JumlahAuditorAhli+$row->APIPDaerah_JumlahAuditorAhli+$row->UnitKrja_JumlahAuditorTerampil+$row->APIPDaerah_JumlahAuditorTerampil;
    }


    $data["title"]="Laporan Rekap Detail Auditor";
    $this->template->display('pelaporan/lap_rekapdetail', $data);
  }

  function unitkerja()
  {
      // Summary Auditor di Dasbor Unit APIP
      $unitkerja_kode = $this->session->userdata('KodeUnitKerja');
      $auditorunit = $this->dasbor->get_auditorunit($unitkerja_kode);

      $data = array();
      foreach ($auditorunit as $row) {

          $data['non_auditor'] = $row->Non_Auditor;
          $data['auditor_pra_jabatan'] = $row->Auditor_Pra_Jabatan;

          $data['auditor_terampil'] = $row->Auditor_Terampil;
          $data['auditor_pelaksana'] = $row->Auditor_Pelaksana;
          $data['auditor_pelaksana_lanjutan'] = $row->Auditor_Pelaksana_Lanjutan;
          $data['auditor_penyelia'] = $row->Auditor_Penyelia;

          $data['auditor_ahli'] = $row->Auditor_Ahli;
          $data['auditor_pertama'] = $row->Auditor_Pertama;
          $data['auditor_muda'] = $row->Auditor_Muda;
          $data['auditor_madya'] = $row->Auditor_Madya;
          $data['auditor_utama'] = $row->Auditor_Utama;
      }


      $data["title"]="Laporan Rekap Detail Auditor";
      $this->template->display('pelaporan/lap_rekapdetail', $data);
  }
}
