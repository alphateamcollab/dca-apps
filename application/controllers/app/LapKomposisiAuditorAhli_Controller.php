<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LapKomposisiAuditorAhli_Controller extends CI_Controller
{
  function __construct()
  {
    parent:: __construct();
    $this->load->model('Dasbor_model', 'dasbor');

  }

  public function index()
  {
      $RoleGroup = $this->session->userdata('RoleGroup');

      if ($RoleGroup === 'Level 1' || $RoleGroup === 'Pusbin JFA' ) {
          $this->pusbin();
      } elseif ($RoleGroup === 'Level 2' || $RoleGroup === 'Perwakilan BPKP') {
          $this->koorwas();
      } elseif ($RoleGroup === 'Level 3' || $RoleGroup === 'Unit Kerja') {
          $this->unitkerja();
      } else {
          redirect('error');
          exit;
      }
  }

  public function pusbin()
  {
      // Summary Auditor di Dasbor Pusbin
      $auditorpusbin = $this->dasbor->get_auditorpusbin();

      $data = array();
      foreach ($auditorpusbin as $row) {
        $data['APIPPusat_JumlahAuditorAhli'] = $row->APIPPusat_JumlahAuditorAhli;
        $data['APIPPusat_AuditorPertama'] = $row->APIPPusat_AuditorPertama;
        $data['APIPPusat_AuditorMuda'] = $row->APIPPusat_AuditorMuda;
        $data['APIPPusat_AuditorMadya'] = $row->APIPPusat_AuditorMadya;
        $data['APIPPusat_AuditorUtama'] = $row->APIPPusat_AuditorUtama;
        // --------------------------------------------------------------------
        $data['BPKP_JumlahAuditorAhli'] = $row->BPKP_JumlahAuditorAhli;
        $data['BPKP_AuditorPertama'] = $row->BPKP_AuditorPertama;
        $data['BPKP_AuditorMuda'] = $row->BPKP_AuditorMuda;
        $data['BPKP_AuditorMadya'] = $row->BPKP_AuditorMadya;
        $data['BPKP_AuditorUtama'] = $row->BPKP_AuditorUtama;
        // --------------------------------------------------------------------
        $data['BHMN_JumlahAuditorAhli'] = $row->BHMN_JumlahAuditorAhli;
        $data['BHMN_AuditorPertama'] = $row->BHMN_AuditorPertama;
        $data['BHMN_AuditorMuda'] = $row->BHMN_AuditorMuda;
        $data['BHMN_AuditorMadya'] = $row->BHMN_AuditorMadya;
        $data['BHMN_AuditorUtama'] = $row->BHMN_AuditorUtama;
        // --------------------------------------------------------------------
        $data['APIPDaerah_JumlahAuditorAhli'] = $row->APIPDaerah_JumlahAuditorAhli;
        $data['APIPDaerah_AuditorPertama'] = $row->APIPDaerah_AuditorPertama;
        $data['APIPDaerah_AuditorMuda'] = $row->APIPDaerah_AuditorMuda;
        $data['APIPDaerah_AuditorMadya'] = $row->APIPDaerah_AuditorMadya;
        $data['APIPDaerah_AuditorUtama'] = $row->APIPDaerah_AuditorUtama;

        $data['TotalPertama'] = $row->APIPPusat_AuditorPertama+$row->BPKP_AuditorPertama+$row->BHMN_AuditorPertama+$row->APIPDaerah_AuditorPertama;
        $data['TotalMuda'] = $row->APIPPusat_AuditorMuda+$row->BPKP_AuditorMuda+$row->BHMN_AuditorMuda+$row->APIPDaerah_AuditorMuda;
        $data['TotalMadya'] = $row->APIPPusat_AuditorMadya+$row->BPKP_AuditorMadya+$row->BHMN_AuditorMadya+$row->APIPDaerah_AuditorMadya;
        $data['TotalUtama'] = $row->APIPPusat_AuditorUtama+$row->BPKP_AuditorUtama+$row->BHMN_AuditorUtama+$row->APIPDaerah_AuditorUtama;

        $data['Total_JumlahAhli'] = $row->APIPPusat_JumlahAuditorAhli+$row->BPKP_JumlahAuditorAhli+$row->BHMN_JumlahAuditorAhli+$row->APIPDaerah_JumlahAuditorAhli;

        // --------------------------------------------------------------------
      }
      $data["title"]="Laporan Komposisi Auditor Ahli";
      $this->template->display('pelaporan/lap_komposisiauditorahli', $data);
  }

  public function koorwas()
  {
      // Summary Auditor di Dasbor Koorwas
      $koorwas_kode = $this->session->userdata('KodeUnitKerja');
      $auditorunit = $this->dasbor->get_auditorkoorwas($koorwas_kode);

      $data = array();
      foreach ($auditorunit as $row) {
          $data['auditor_utama'] = $row->UnitKerja_AuditorUtama+$row->APIPDaerah_AuditorUtama;
          $data['auditor_madya'] = $row->UnitKerja_AuditorMadya+$row->APIPDaerah_AuditorMadya;
          $data['auditor_muda'] = $row->UnitKerja_AuditorMuda+$row->APIPDaerah_AuditorMuda;
          $data['auditor_pertama'] = $row->UnitKerja_AuditorPertama+$row->APIPDaerah_AuditorPertama;

          $data['jumlah_auditor_ahli'] = $row->UnitKrja_JumlahAuditorAhli+$row->APIPDaerah_JumlahAuditorAhli;
      }
      $data["title"]="Laporan Komposisi Auditor Ahli";
      $this->template->display('pelaporan/lap_komposisiauditorahli_koorwas', $data);
  }

  function unitkerja()
  {
      // Summary Auditor di Dasbor Unit APIP
      $unitkerja_kode = $this->session->userdata('KodeUnitKerja');
      $auditorunit = $this->dasbor->get_auditorunit($unitkerja_kode);

      $data = array();
      foreach ($auditorunit as $row) {
          $data['auditor_pertama'] = $row->Auditor_Pertama;
          $data['auditor_muda'] = $row->Auditor_Muda;
          $data['auditor_madya'] = $row->Auditor_Madya;
          $data['auditor_utama'] = $row->Auditor_Utama;

          $data['auditor_ahli'] = $row->Auditor_Ahli;
      }
      $data["title"]="Laporan Komposisi Auditor Ahli";
      $this->template->display('pelaporan/lap_komposisiauditorahli_unitkerja', $data);
  }
}
