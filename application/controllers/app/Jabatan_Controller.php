<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan_Controller extends CI_Controller{

    function __construct()
    {
        parent:: __construct();
        is_login();
        $this->load->model('jabatan_model', 'jabatan');
    }

    public function index()
    {
        $data["title"]="Riwayat Jabatan";
        $this->template->display('jabatan/jabatan_list_view', $data);
    }

    public function data()
    {
        $nip = $this->session->userdata('NIP');
        $data = $this->jabatan->jabatan_riwayat($nip);
        echo json_encode($data);
    }

    public function form()
    {
        $data["title"]="Form Jabatan";
        $this->template->display('jabatan/jabatan_form_view', $data);
    }
}
