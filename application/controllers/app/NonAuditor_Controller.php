<?php
defined('BASEPATH') or exit('No direct script access allowed');

class NonAuditor_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        is_login();
        $this->load->model('NonAuditor_model', 'nonauditor');
        $this->load->model('Auditor_model', 'auditor');
        $this->load->model('Auth_model', 'auth');
    }

    public function index()
    {
        $RoleGroup = $this->session->userdata('RoleGroup');

        $data = array(
          'dd_jenisinstansi' => $this->auth->get_jenisinstansi(),
          'jenisInstansi_selected' => $this->input->post('JenisInstansi') ? $this->input->post('JenisInstansi') : '',
          'instansi_selected' => $this->input->post('Instansi') ? $this->input->post('Instansi') : '',
          'dd_rolecode' => $this->auditor->get_rolecode(),
          'rolecode_selected' => $this->input->post('RoleCode')
        );

        if ($RoleGroup === 'Level 0' || $RoleGroup === 'Level 1') {
            $data['title']='Data Non Auditor ' .$this->session->userdata('NamaUnitKerja');
            $this->template->display('nonauditor/pusbin_view', $data);
        } elseif ($RoleGroup === 'Level 2') {
            $data['title']='Data Non Auditor ' .$this->session->userdata('NamaUnitKerja');
            $this->template->display('nonauditor/perwakilan_view', $data);
        } elseif ($RoleGroup === 'Level 3') {
            $data['title']='Data Non Auditor ' .$this->session->userdata('NamaUnitKerja');
            $this->template->display('nonauditor/unitkerja_view', $data);
        // } elseif ($RoleGroup === 'Unit Kerja') {
        //     $data['title']='Data Non Auditor ' .$this->session->userdata('NamaUnitKerja');
        //     $this->template->display('nonauditor/unitkerja_view', $data);
        // } elseif ($RoleGroup === 'Perwakilan BPKP') {
        //     $data['title']='Data Non Auditor ' .$this->session->userdata('NamaUnitKerja');
        //     $this->template->display('nonauditor/perwakilan_view', $data);
        // } elseif ($RoleGroup === 'Pusbin JFA') {
        //     $data['title']='Data Non Auditor ' .$this->session->userdata('NamaUnitKerja');
        //     $this->template->display('nonauditor/pusbin_view', $data);
        } else {
            redirect('error');
            exit;
        }
    }

    public function datapusbin()
    {
        $data = $this->nonauditor->nonauditor_list_pusbin();
        echo json_encode($data);
    }

    public function datakorwas()
    {
        $kode = $this->session->userdata('KodeProvUnitKerja');
        $data = $this->nonauditor->nonauditor_list_korwas($kode);
        echo json_encode($data);
    }

    public function dataunit()
    {
        $kode = $this->session->userdata('KodeUnitKerja');
        $data = $this->nonauditor->nonauditor_list_unit($kode);
        echo json_encode($data);
    }

    public function detail()
    {
        $nip = $this->input->get('id');
        $data = array(
            'title' => 'Detail Profil Auditor <span class="Pendidikan_GelarDepan"></span> <span class="Auditor_NamaLengkap"></span><span class="Pendidikan_GelarBelakang"></span>',

            'dd_jenisinstansi' => $this->auditor->get_jenisinstansi(),
            'jenisInstansi_selected' => $this->input->post('JenisInstansi') ? $this->input->post('JenisInstansi') : '',
            'instansi_selected' => $this->input->post('Instansi') ? $this->input->post('Instansi') : '',
            'dd_rolecode' => $this->auditor->get_rolecode()
          );
        $RoleGroup = $this->session->userdata('RoleGroup');
        $isAuditor = ($this->session->userdata('isAuditor') === true) ? 'Aktif' : 'Non-Aktif';

        $this->template->display('nonauditor/nonauditor_detail_view', $data);
    }

    public function dropdown()
    {
        $data = array(
        'dd_rolecode' => $this->nonauditor->get_rolecode(),
        'rolecode_selected' => $this->input->post('RoleCode')
      );
        $this->template->display('nonauditor/nonauditor_list_view', $data);
    }

    public function lihat()
    {
        $data["title"]="Data Pokok Non-Auditor";
        $this->template->display('nonauditor/nonauditor_detail_view', $data);
    }

    // fungsi untuk mengambil data sebagai primary key
    public function ambil()
    {
        // ambil data dari get "name" yang didapat dari form ke variabel
        $id = $this->input->get('id');
        // penguarian data variable dari auditor_model
        // dan simpan ke variabel $data
        $data = $this->nonauditor->nonauditor_ambil($id);
        // cetak variable $data dalam bentuk JSON
        echo json_encode($data);
    }

    // fungsi untuk menambah data
    public function tambah()
    {
        $nip            = $this->input->post('Auditor_NIP');
        $nama           = $this->input->post('Auditor_NamaLengkap');
        $tempatlahir    = $this->input->post('Auditor_TempatLahir');
        $tanggallahir   = database_date($this->input->post('Auditor_TglLahir'));
        $jeniskelamin   = $this->input->post('Auditor_JenisKelamin');

        $roleid         = $this->input->post('Pengguna_RoleID');

        $status         = $this->input->post('Status_Auditor');
        $isauditor      = 'false';
        $isadmin        = 'false';
        $unitkerja      = $this->input->post('UnitKerja_Kode');
        $surel          = $this->input->post('Pengguna_Surel', true);

        $password       = 'password';
        $options        = array("cost"=>4);
        $sandi          = password_hash($password, PASSWORD_BCRYPT, $options);

        $alamat         = $this->input->post('Pengguna_Alamat');

        $dibuatoleh     = $this->session->userdata('NIP');
        $dibuattgl      = date("Y-m-d H:i:s");

        $data = $this->nonauditor->nonauditor_tambah($roleid, $isauditor, $isadmin, $status, $surel, $sandi, $alamat, $nip, $nama, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $dibuatoleh, $dibuattgl);
        echo json_encode($data);
    }

    public function ubah()
    {
        $nip            = $this->input->post('Auditor_NIP');
        $nama           = $this->input->post('Auditor_NamaLengkap');
        $tempatlahir    = $this->input->post('Auditor_TempatLahir');
        $tanggallahir   = database_date($this->input->post('Auditor_TglLahir'));
        $jeniskelamin   = $this->input->post('Auditor_JenisKelamin');

        $rolecode       = 'Auditor';
        $isauditor      = 'true';
        $unitkerja      = $this->input->post('UnitKerja_Kode');

        $diubaholeh     = $this->session->userdata('NIP');
        $diubahtgl      = date("Y-m-d H:i:s");

        $data = $this->auditor->auditor_ubah($rolecode, $isauditor, $unitkerja, $nip, $nama, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function hapus()
    {
        $nip = $this->input->post('id');
        $data = $this->auditor->auditor_hapus($nip);
        echo json_encode($data);
    }
}
