<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DiklatSertifikasiProfesi_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        is_login();
        $this->load->model('DiklatSertifikasiProfesi_model', 'sertifikasiprofesi');
    }

    public function index()
    {
        $data["title"]="Riwayat Diklat Sertifikasi Profesi";
        $this->template->display('diklatsertifikasiprofesi/diklatsertifikasiprofesi_list_view', $data);
    }

    public function data()
    {
        $nip = $this->session->userdata('NIP');
        $data = $this->sertifikasiprofesi->sertifikasiprofesi_riwayat($nip);
        echo json_encode($data);
    }
}
