<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LapDataAuditor_Controller extends CI_Controller
{
  function __construct()
  {
    parent:: __construct();
  }

  public function index()
  {
    $data["title"]="Laporan Data Auditor";
    $this->template->display('pelaporan/lap_dataauditor', $data);
  }
}
