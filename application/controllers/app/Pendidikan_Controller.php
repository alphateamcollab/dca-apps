<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendidikan_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        is_login();
        $this->load->model('Pendidikan_model', 'pendidikan');
    }

    public function index()
    {
        $data["title"]="Riwayat Pendidikan";
        $this->template->display('pendidikan/pendidikan_list_view', $data);
    }

    public function data()
    {
        $nip = $this->session->userdata('NIP');
        $data = $this->pendidikan->pendidikan_riwayat($nip);
        echo json_encode($data);
    }
}
