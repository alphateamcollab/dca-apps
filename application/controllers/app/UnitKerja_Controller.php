<?php
/**
 *
 */
class UnitKerja_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('UnitKerja_model', 'unitkerja');
    }

    public function index()
    {
        $RoleGroup = $this->session->userdata('RoleGroup');

        if ($RoleGroup === 'Level 0' || $RoleGroup === 'Level 1') {
            $data = array(
              'pagetitle' => 'Data Admin Unit Kerja Nasional',
              'title' => 'Data Unit Kerja Nasional - '  .$this->session->userdata('NamaUnitKerja')
            );
            $this->template->display('unitkerja/pusbin_view', $data);
        } elseif ($RoleGroup === 'Level 2') {
            $data = array(
              'pagetitle' => 'Data Unit Kerja Koorwas',
              'title' => 'Data Unit Kerja Koorwas - '  .$this->session->userdata('NamaUnitKerja')
            );
            $this->template->display('unitkerja/perwakilan_view', $data);
        } else {
            redirect('error');
            exit;
        }
    }

    public function haklogin()
    {
        $RoleGroup = $this->session->userdata('RoleGroup');

        if ($RoleGroup === 'Level 0') {
            $data = array(
              'pagetitle' => 'Data Admin Unit Kerja Nasional',
              'title' => 'Data Unit Kerja Nasional'
            );
            $this->template->display('unitkerja/pengguna_view', $data);
        } elseif ($RoleGroup === 'Level 1') {
            $data = array(
              'pagetitle' => 'Data Admin Unit Kerja Nasional',
              'title' => 'Data Unit Kerja Nasional - '  .$this->session->userdata('NamaUnitKerja')
            );
            $this->template->display('unitkerja/pengguna_view', $data);
        } else {
            redirect('error');
            exit;
        }
    }

    public function data()
    {
        $RoleGroup = $this->session->userdata('RoleGroup');

        if ($RoleGroup === 'Level 0') {
        } elseif ($RoleGroup === 'Level 1') {
            $data = $this->unitkerja->unitkerja_nasional();
            echo json_encode($data);
        } elseif ($RoleGroup === 'Level 2') {
            $data = $this->unitkerja->unitkerja_perwakilan();
            echo json_encode($data);
        } else {
            redirect('error');
            exit;
        }
    }

    public function datahaklogin()
    {
        $RoleGroup = $this->session->userdata('RoleGroup');

        if ($RoleGroup === 'Level 0') {
        } elseif ($RoleGroup === 'Level 1') {
            $data = $this->unitkerja->pengguna_nasional();
            echo json_encode($data);
        } else {
            redirect('error');
            exit;
        }
    }

    public function persetujuan()
    {
        $nip = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 1) ? 2 : 1 ;

        $data    = $this->unitkerja->status_persetujuan($nip, $status);
        echo json_encode($data);
    }

    public function haklogin_ubah()
    {
        $nip = $this->input->post('id');
        $stat = $this->input->post('status');

        $status = ($stat <= 2) ? 3 : 2 ;

        $data    = $this->unitkerja->status_hakloginUbah($nip, $status);
        echo json_encode($data);
    }
}
