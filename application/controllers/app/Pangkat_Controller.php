<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pangkat_Controller extends CI_Controller{

    public function __construct()
    {
        parent:: __construct();
        is_login();
        $this->load->model('Pangkat_model', 'pangkat');
    }

    public function index()
    {
        // $data["title"]="Riwayat Pangkat";
        $data["title"]=$this->session->userdata('NIP');
        $this->template->display('pangkat/pangkat_list_view', $data);
    }

    public function data()
    {
        $nip = $this->session->userdata('NIP');
        // $nip = '199909092009102001';
        $data = $this->pangkat->pangkat_riwayat($nip);
        echo json_encode($data);
    }

    public function ambil()
    {
        $id = $this->input->get('id');
        $data = $this->pangkat->pangkat_ambil($id);
        echo json_encode($data);
    }
}
