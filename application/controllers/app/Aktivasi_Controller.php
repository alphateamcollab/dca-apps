<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 */
class Aktivasi_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        // is_login_global();
    }

    public function index()
    {
        // $RoleGroup = $this->session->userdata('RoleGroup');
        $isAuditor = ($this->session->userdata('isAuditor') === true) ? 'Aktif' : 'Non-Aktif';

        $data['nama']         = $this->session->userdata('NamaLengkap');
        $data['gelardepan']   = $this->session->userdata('GelarDepan');
        $data['gelarbelakang']= $this->session->userdata('GelarBelakang');
        $data['nip']          = $this->session->userdata('NIP');
        $data['surel']        = $this->session->userdata('Surel');
        $data['isauditor']    = $isAuditor;
        $data['pegawai']      = $this->session->userdata('Role');
        $data['jenisinstansi']    = $this->session->userdata('NamaJenisInstansi');
        $data['instansi']    = $this->session->userdata('NamaInstansi');
        $data['unitkerja']    = $this->session->userdata('NamaUnitKerja');

        $data['title']='Selamat Datang di Aktivasi Akun';
        $data['subtitle']='Sebelum menyelesaikan proses aktivasi, silakan periksa kembali data anda, jika ada kesalahan data mohon segera laporkan ke admin';
        $data['bantuan']='Isi bantuan';
        $this->load->view('aktivasi/aktivasi_form_view', $data);
        // $this->load->view('aktivasi/persetujuan_form_view', $data);
    }

    public function persetujuan()
    {
        // $RoleGroup = $this->session->userdata('RoleGroup');
        $isAuditor = ($this->session->userdata('isAuditor') === true) ? 'Aktif' : 'Non-Aktif';

        $data['nama']         = $this->session->userdata('NamaLengkap');
        $data['gelardepan']   = $this->session->userdata('GelarDepan');
        $data['gelarbelakang']= $this->session->userdata('GelarBelakang');
        $data['nip']          = $this->session->userdata('NIP');
        $data['surel']        = $this->session->userdata('Surel');
        $data['nohp']        = $this->session->userdata('NoHP');
        $data['alamat']        = $this->session->userdata('Alamat');
        $data['isauditor']    = $isAuditor;
        $data['pegawai']      = $this->session->userdata('Role');
        // $data['pangkat']      = 'Pangkatnya';
        // $data['golongan']     = 'Golongan Ruang IV/A';
        // $data['jabatan']      = 'Jabatannya';
        $data['jenisinstansi']    = $this->session->userdata('NamaJenisInstansi');
        $data['instansi']    = $this->session->userdata('NamaInstansi');
        $data['unitkerja']    = $this->session->userdata('NamaUnitKerja');

        $data['title']='Selamat Datang, ' .$this->session->userdata('NamaLengkap') .' ! ';
        $data['subtitle']='Anda telah menyelesaikan proses aktivasi, mohon menunggu proses persetujuan sebagai pegawai ' .$this->session->userdata('NamaUnitKerja');
        $data['bantuan']='Isi bantuan';
        $this->load->view('aktivasi/persetujuan_form_view', $data);
    }

    public function simpan()
    {
      $nip            = $this->input->post('Auditor_NIP');
      $jeniskelamin   = $this->input->post('Auditor_JenisKelamin');
      $alamat         = $this->input->post('Pengguna_Alamat');
      $alamat         = $this->input->post('Pengguna_Alamat');

      $dibuatoleh     = $this->session->userdata('NIP');
      $dibuattgl      = date("Y-m-d H:i:s");

      $data = $this->auditor->auditor_tambah($roleid, $isauditor, $isadmin, $instansi, $surel, $sandi, $alamat, $nip, $nama, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $dibuatoleh, $dibuattgl);
      echo json_encode($data);
    }
}
