<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ProfilPengguna_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        is_login_global();
        $this->load->model('ProfilPengguna_model', 'profil');
    }

    public function index()
    {
        $nip = $this->session->userdata('NIP');

        $data["title"]="Profil Pengguna";
        $data["nama"] = $this->session->userdata('NamaLengkap');
        $data["nip"] = $this->encryption->encrypt($nip);
        $data["kodeunit"] = $this->session->userdata('KodeUnitKerja');
        // $data["nip"] = $this->session->userdata('NIP');
        $this->template->display('profilpengguna_view', $data);
    }

    public function sandi()
    {
        $data["title"]="Pengaturan Kata Sandi";

        $this->template->display('pengguna/pengguna_sandi_view', $data);
    }

    public function ubahsandi()
    {
        $nip            = $this->input->post('Auditor_NIP');
        $password       = $this->input->post('Pengguna_KataSandi', true);
        $options        = array("cost"=>4);
        $sandi          = password_hash($password, PASSWORD_BCRYPT, $options);

        $diubaholeh     = $this->session->userdata('NIP');
        $diubahtgl      = date('Y-m-d H:i:s');
        // penguarian data variable dari auditor_model
        // dan simpan ke variabel $data
        $data = $this->profil->sandipengguna_ubah($nip, $sandi, $diubaholeh, $diubahtgl);
        // cetak variable $data dalam bentuk JSON
        echo json_encode($data);
    }

    public function ambil()
    {
        $id = $this->input->get('id');
        $nip = $this->encryption->decrypt($id);
        $data = $this->profil->profilpengguna_ambil($nip);
        // $data = $this->profil->profilpengguna_ambil($id);
        echo json_encode($data);
    }

    public function ubah()
    {
        $nip = $this->input->post('Auditor_NIP');
        $nama = $this->input->post('Auditor_NamaLengkap');
        $surel = $this->input->post('Pengguna_Surel');
        $nohp = $this->input->post('Pengguna_NoHP');
        $alamat = $this->input->post('Pengguna_Alamat');
        $diubaholeh = $this->session->userdata('NIP');
        $diubahtgl = date('Y-m-d H:i:s');
        // penguarian data variable dari referensi_model
        // dan simpan ke variabel $data
        $data = $this->profil->profilpengguna_ubah($nip, $nama, $surel, $nohp, $alamat, $diubaholeh, $diubahtgl);
        // cetak variable data dalam bentuk JSON
        echo json_encode($data);
    }

    public function ambil_unit()
    {
        $id = $this->input->get('id');
        $data = $this->profil->profilunit_ambil($id);
        // $data = $this->profil->profilpengguna_ambil($id);
        echo json_encode($data);
    }

    public function ubah_unit()
    {
        $kodeunit = $this->input->post('UnitKerja_Kode');
        $surel = $this->input->post('UnitKerja_Surel');
        $notlp = $this->input->post('UnitKerja_NoTlp');
        $nofax = $this->input->post('UnitKerja_NoFax');
        $alamat = $this->input->post('UnitKerja_Alamat');
        $diubaholeh = $this->session->userdata('NIP');
        $diubahtgl = date('Y-m-d H:i:s');
        // penguarian data variable dari referensi_model
        // dan simpan ke variabel $data
        $data = $this->profil->profilunitkerja_ubah($kodeunit, $surel, $notlp, $nofax, $alamat, $diubaholeh, $diubahtgl);
        // cetak variable data dalam bentuk JSON
        echo json_encode($data);
    }
}
