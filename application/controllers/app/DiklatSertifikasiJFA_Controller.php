<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DiklatSertifikasiJFA_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        is_login();
        $this->load->model('DiklatSertifikasiJFA_model', 'sertifikasijfa');
    }

    public function index()
    {
        $data["title"]="Riwayat Diklat Sertifikasi JFA";
        $this->template->display('diklatsertifikasijfa/diklatsertifikasijfa_list_view', $data);
    }

    public function data()
    {
        $nip = $this->session->userdata('NIP');
        $data = $this->sertifikasijfa->sertifikasijfa_riwayat($nip);
        echo json_encode($data);
    }
}
