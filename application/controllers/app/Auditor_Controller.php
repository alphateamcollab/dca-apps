<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auditor_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        is_login();
        $this->load->model('Auditor_model', 'auditor');
        $this->load->model('Pendidikan_model', 'pendidikan');
        $this->load->model('Pangkat_model', 'pangkat');
        $this->load->model('Jabatan_model', 'jabatan');
        $this->load->model('DiklatSertifikasiJFA_model', 'sertifikasijfa');
        $this->load->model('DiklatSertifikasiProfesi_model', 'sertifikasiprofesi');
        $this->load->model('DiklatTeknisSubstantif_model', 'teknissubstantif');
        $this->load->model('AngkaKredit_model', 'angkakredit');
        $this->load->model('Auth_model', 'auth');
    }

    public function index()
    {
        $RoleGroup = $this->session->userdata('RoleGroup');

        $data = array(
          'dd_jenisinstansi' => $this->auth->get_jenisinstansi(),
          'jenisInstansi_selected' => $this->input->post('JenisInstansi') ? $this->input->post('JenisInstansi') : '',
          'instansi_selected' => $this->input->post('Instansi') ? $this->input->post('Instansi') : '',
          'dd_rolecode' => $this->auditor->get_rolecode(),
          'rolecode_selected' => $this->input->post('RoleCode')
        );

        if ($RoleGroup === 'Level 0' || $RoleGroup === 'Level 1') {
            $data['title']='Data Auditor ' .$this->session->userdata('NamaUnitKerja');
            $this->template->display('auditor/pusbin_view', $data);
        } elseif ($RoleGroup === 'Level 2') {
            $data['title']='Data Auditor ' .$this->session->userdata('NamaUnitKerja');
            $this->template->display('auditor/perwakilan_view', $data);
        } elseif ($RoleGroup === 'Level 3') {
            $data['title']='Data Auditor ' .$this->session->userdata('NamaUnitKerja');
            $this->template->display('auditor/unitkerja_view', $data);
        } elseif ($RoleGroup === 'Unit Kerja') {
            $data['title']='Data Auditor ' .$this->session->userdata('NamaUnitKerja');
            $this->template->display('auditor/unitkerja_view', $data);
        } elseif ($RoleGroup === 'Perwakilan BPKP') {
            $data['title']='Data Auditor ' .$this->session->userdata('NamaUnitKerja');
            $this->template->display('auditor/perwakilan_view', $data);
        } elseif ($RoleGroup === 'Pusbin JFA') {
            $data['title']='Data Auditor ' .$this->session->userdata('NamaUnitKerja');
            $this->template->display('auditor/pusbin_view', $data);
        } else {
            redirect('error');
            exit;
        }
    }

    public function datapusbin()
    {
        $data = $this->auditor->auditor_list_pusbin();
        echo json_encode($data);
    }

    public function datakorwas()
    {
        $kode = $this->session->userdata('KodeProvUnitKerja');
        $data = $this->auditor->auditor_list_korwas($kode);
        echo json_encode($data);
    }

    public function dataunit()
    {
        $kode = $this->session->userdata('KodeUnitKerja');
        $data = $this->auditor->auditor_list_unit($kode);
        echo json_encode($data);
    }

    public function data_perwakilan()
    {
        $unitkerja = $this->session->userdata('KodeUnitKerja');
        $data = $this->auditor->auditor_list($unitkerja);
        echo json_encode($data);
    }

    public function data_nasional()
    {
        $unitkerja = $this->session->userdata('KodeUnitKerja');
        $data = $this->auditor->auditor_list($unitkerja);
        echo json_encode($data);
    }

    public function detail()
    {
        $nip = $this->input->get('id');
        $data = array(
            'title' => 'Detail Profil Auditor <span class="Pendidikan_GelarDepan"></span> <span class="Auditor_NamaLengkap"></span><span class="Pendidikan_GelarBelakang"></span>',

            'dd_jenjangpendidikan' => $this->auditor->get_jenjangpendidikan(),
            'jenjangpendidikan_selected' => $this->input->post('Pendidikan_Kode') ? $this->input->post('Pendidikan_Kode') : '',

            'dd_golongan' => $this->auditor->get_pangkatgolongan(),
            'golongan_selected' => $this->input->post('Pangkat_Kode') ? $this->input->post('Pangkat_Kode') : '',

            'dd_jabatan' => $this->auditor->get_jabatan(),
            'jabatan_selected' => $this->input->post('Jabatan_Kode') ? $this->input->post('Jabatan_Kode') : '',
            // 'dd_jenjangjabatan' => $this->auditor->get_jenjangjabatan($JenjangJabatan_Kode),
            // 'jenjangjabatan_selected' => $this->input->post('JenjangJabatan_Kode') ? $this->input->post('JenjangJabatan_Kode') : '',

            'dd_diklatjfa' => $this->auditor->get_diklatjfa(),
            'diklatjfa_selected' => $this->input->post('Diklat_Kode') ? $this->input->post('Diklat_Kode') : '',

            'dd_diklatteknissubstantif' => $this->auditor->get_diklatteknissubstantif(),
            'diklatteknissubstantif_selected' => $this->input->post('Diklat_Kode') ? $this->input->post('Diklat_Kode') : '',

            'dd_jenisinstansi' => $this->auditor->get_jenisinstansi(),
            'jenisInstansi_selected' => $this->input->post('JenisInstansi') ? $this->input->post('JenisInstansi') : '',
            'instansi_selected' => $this->input->post('Instansi') ? $this->input->post('Instansi') : '',
            'dd_rolecode' => $this->auditor->get_rolecode()
          );

        $RoleGroup = $this->session->userdata('RoleGroup');

        if ($RoleGroup === 'Level 1' || $RoleGroup === 'Pusbin JFA') {
            $this->template->display('auditor/auditor_detail_pusbin_view', $data);
        } else {
            $this->template->display('auditor/auditor_detail_view', $data);
        }
    }

    public function lihat($nip)
    {
        $data_detail = $this->auditor->auditor_detail($nip);
        echo json_encode($data_detail);
    }

    /*
    | Begin Of Function Script
    | Bagian RU Profil Ringkasan Auditor
    */
    public function ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->auditor->auditor_ambil($nip);
        echo json_encode($data);
    }

    public function tambah()
    {
        $nip            = $this->input->post('Auditor_NIP');
        $nama           = $this->input->post('Auditor_NamaLengkap');
        $tempatlahir    = $this->input->post('Auditor_TempatLahir');
        $tanggallahir   = database_date($this->input->post('Auditor_TglLahir'));
        $jeniskelamin   = $this->input->post('Auditor_JenisKelamin');

        $roleid         = $this->input->post('Pengguna_RoleID');

        $status         = $this->input->post('Status_Auditor');
        $isauditor      = ($status >=2) ? 'true' : 'false' ;
        $isadmin        = 'false';
        $unitkerja      = $this->input->post('UnitKerja_Kode');
        $surel          = $this->input->post('Pengguna_Surel', true);

        $password       = 'password';
        $options        = array("cost"=>4);
        $sandi          = password_hash($password, PASSWORD_BCRYPT, $options);

        $alamat         = $this->input->post('Pengguna_Alamat');

        $dibuatoleh     = $this->session->userdata('NIP');
        $dibuattgl      = date("Y-m-d H:i:s");

        $data = $this->auditor->auditor_tambah($roleid, $isauditor, $isadmin, $status, $surel, $sandi, $alamat, $nip, $nama, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $dibuatoleh, $dibuattgl);
        echo json_encode($data);
    }

    public function ubah()
    {
        $nip            = $this->input->post('Auditor_NIP');
        $nama           = $this->input->post('Auditor_NamaLengkap');
        $tempatlahir    = $this->input->post('Auditor_TempatLahir');
        $tanggallahir   = database_date($this->input->post('Auditor_TglLahir'));
        $jeniskelamin   = $this->input->post('Auditor_JenisKelamin');

        $rolecode       = 'Auditor';
        $isauditor      = 'true';
        $unitkerja      = $this->input->post('UnitKerja_Kode');

        $diubaholeh     = $this->session->userdata('NIP');
        $diubahtgl      = date("Y-m-d H:i:s");

        $data = $this->auditor->auditor_ubah($rolecode, $isauditor, $unitkerja, $nip, $nama, $tempatlahir, $tanggallahir, $jeniskelamin, $unitkerja, $diubaholeh, $diubahtgl);
        echo json_encode($data);
    }

    public function hapus()
    {
        $nip = $this->input->post('id');
        $data = $this->auditor->auditor_hapus($nip);
        echo json_encode($data);
    }

    public function ubahunit()
    {
        $nip = $this->input->get('id');
        $data = $this->auditor->auditor_ambil($nip);
        echo json_encode($data);
    }

    public function get_instansi()
    {
        $jenisinstansi_kode = $this->input->post('value');
        $jenisinstansi = $this->auditor->get_instansi($jenisinstansi_kode);

        echo '<select name="">';
        echo '<option value="">Pilih Instansi</option>';
        foreach ($jenisinstansi as $row) {
            echo '<option value="'.$row->Instansi_Kode.'">'.$row->Instansi_Nama.'</option>';
        }
        echo '</select>';
    }

    public function get_unitkerja()
    {
        $unitkerja_kode = $this->input->post('value');
        $unitkerja = $this->auditor->get_unitkerja($unitkerja_kode);

        echo '<select name="">';
        echo '<option value="">Pilih Unit Kerja</option>';
        foreach ($unitkerja as $row) {
            echo '<option value="'.$row->UnitKerja_Kode.'">'.$row->UnitKerja_NamaUnitInstansi.'</option>';
        }
        echo '</select>';
    }

    /*
    | Begin of Function Script
    | Bagian Update Profil & Akun Pengguna
    */
    public function auditorprofil_ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->auditor->auditorprofil_ambil($nip);
        echo json_encode($data);
    }

    public function auditorprofil_ubah()
    {
        $nip = $this->input->post('Auditor_NIP');
        $nama = $this->input->post('Auditor_NamaLengkap');
        $surel = $this->input->post('Pengguna_Surel');
        $nohp = $this->input->post('Pengguna_NoHP');
        $alamat = $this->input->post('Pengguna_Alamat');
        $diubaholeh = $this->session->userdata('NIP');
        $diubahtgl = date('Y-m-d H:i:s');
        // penguarian data variable dari referensi_model
        // dan simpan ke variabel $data
        $data = $this->auditor->auditorprofil_ubah($nip, $nama, $surel, $nohp, $alamat, $diubaholeh, $diubahtgl);
        // cetak variable data dalam bentuk JSON
        echo json_encode($data);
    }

    public function auditorkatasandi_ubah()
    {
        $nip            = $this->input->post('id');
        $password       = '12345';
        $options        = array("cost"=>4);
        $sandi          = password_hash($password, PASSWORD_BCRYPT, $options);

        $diubaholeh     = $this->session->userdata('NIP');
        $diubahtgl      = date('Y-m-d H:i:s');
        // penguarian data variable dari auditor_model
        // dan simpan ke variabel $data
        $data = $this->auditor->auditorkatasandi_ubah($nip, $sandi, $diubaholeh, $diubahtgl);
        // cetak variable $data dalam bentuk JSON
        echo json_encode($data);
    }

    public function auditorunitkerja_ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->auditor->auditorunitkerja_ambil($nip);
        echo json_encode($data);
    }

    public function auditorunitkerja_ubah()
    {
        $nip = $this->input->post('Auditor_NIP');
        $kode = $this->input->post('UnitKerja_Kode');

        $diubaholeh = $this->session->userdata('NIP');
        $diubahtgl = date('Y-m-d H:i:s');
        // penguarian data variable dari referensi_model
        // dan simpan ke variabel $data
        $data = $this->auditor->auditorunitkerja_ubah($nip, $kode, $diubaholeh, $diubahtgl);
        // cetak variable data dalam bentuk JSON
        echo json_encode($data);
    }

    /*
    | Begin Of Function Script
    | Bagian CRUD Riwayat Pendidikan
    */
    public function pendidikan_detail()
    {
        $nip   = $this->input->get('id');
        $data = $this->pendidikan->pendidikan_riwayat($nip);
        echo json_encode($data);
    }

    public function pendidikan_ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->pendidikan->pendidikan_ambil($nip);
        echo json_encode($data);
    }

    public function pendidikan_tambah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/ijazah';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('Pendidikan_Ijazah')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id = $this->input->post('Pendidikan_ID');
                $nip = $this->input->post('Auditor_NIP');
                $tingkat = $this->input->post('Pendidikan_Kode');
                $lembaga = $this->input->post('Pendidikan_Lembaga');
                $fakultas = $this->input->post('Pendidikan_Fakultas');
                $jurusan = $this->input->post('Pendidikan_Jurusan');
                $gelardepan = $this->input->post('Pendidikan_GelarDepan');
                $gelarbelakang = $this->input->post('Pendidikan_GelarBelakang');
                $noijazah = $this->input->post('Pendidikan_NoIjazah');
                $tglijazah = database_date($this->input->post('Pendidikan_TglIjazah'));
                $ijazah = 'assets/file/ijazah/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl = date("Y-m-d H:i:s");
                $data = $this->pendidikan->pendidikan_tambah($id, $nip, $tingkat, $lembaga, $fakultas, $jurusan, $gelardepan, $gelarbelakang, $noijazah, $tglijazah, $ijazah, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['Pendidikan_Ijazah']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function pendidikan_ubah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/ijazah';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('Pendidikan_Ijazah')) {
                // $status = 'error';
                // $msg = $this->upload->display_errors('Test', 'Testing');

                $id = $this->input->post('Pendidikan_ID');
                $nip = $this->input->post('Auditor_NIP');
                $tingkat = $this->input->post('Pendidikan_Kode');
                $lembaga = $this->input->post('Pendidikan_Lembaga');
                $fakultas = $this->input->post('Pendidikan_Fakultas');
                $jurusan = $this->input->post('Pendidikan_Jurusan');
                $gelardepan = $this->input->post('Pendidikan_GelarDepan');
                $gelarbelakang = $this->input->post('Pendidikan_GelarBelakang');
                $noijazah = $this->input->post('Pendidikan_NoIjazah');
                $tglijazah = database_date($this->input->post('Pendidikan_TglIjazah'));
                $ijazah = $this->input->post('Pendidikan_Ijazah_Lama');
                $diubaholeh = $this->session->userdata('NIP');
                $diubahtgl = date('Y-m-d H:i:s');
                $data = $this->pendidikan->pendidikan_ubah($id, $nip, $tingkat, $lembaga, $fakultas, $jurusan, $gelardepan, $gelarbelakang, $noijazah, $tglijazah, $ijazah, $diubaholeh, $diubahtgl);

            } else {
                $file = array('upload_data' => $this->upload->data());

                $id = $this->input->post('Pendidikan_ID');
                $nip = $this->input->post('Auditor_NIP');
                $tingkat = $this->input->post('Pendidikan_Kode');
                $lembaga = $this->input->post('Pendidikan_Lembaga');
                $fakultas = $this->input->post('Pendidikan_Fakultas');
                $jurusan = $this->input->post('Pendidikan_Jurusan');
                $gelardepan = $this->input->post('Pendidikan_GelarDepan');
                $gelarbelakang = $this->input->post('Pendidikan_GelarBelakang');
                $noijazah = $this->input->post('Pendidikan_NoIjazah');
                $tglijazah = database_date($this->input->post('Pendidikan_TglIjazah'));
                $ijazah = 'assets/file/ijazah/' .$file['upload_data']['file_name'];
                $diubaholeh = $this->session->userdata('NIP');
                $diubahtgl = date('Y-m-d H:i:s');
                $data = $this->pendidikan->pendidikan_ubahfile($id, $nip, $tingkat, $lembaga, $fakultas, $jurusan, $gelardepan, $gelarbelakang, $noijazah, $tglijazah, $ijazah, $diubaholeh, $diubahtgl);

                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['Pendidikan_Ijazah']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }
// $config['encrypt_name'] = true;
    public function pendidikan_hapus()
    {
        $id = $this->input->post('id');
        $data = $this->pendidikan->pendidikan_hapus($id);
        echo json_encode($data);
    }

    /*
    | Begin Of Function Script
    | Bagian CRUD Riwayat Pangkat
    */
    public function pangkat_detail()
    {
        $nip   = $this->input->get('id');
        $data = $this->pangkat->pangkat_riwayat($nip);
        echo json_encode($data);
    }

    public function pangkat_ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->pangkat->pangkat_ambil($nip);
        echo json_encode($data);
    }

    public function pangkat_tambah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/pangkat';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('Pangkat_SK')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id = $this->input->post('PangkatGol_ID');
                $nip = $this->input->post('Auditor_NIP');
                $kode = $this->input->post('Pangkat_Kode');
                $pangkat = $this->input->post('Pangkat_Nama');
                $noSK = $this->input->post('Pangkat_NoSuratKeputusan');
                $tglSK = database_date($this->input->post('Pangkat_TglSuratKeputusan'));
                $tmt = database_date($this->input->post('Pangkat_TglMulaiTugas'));
                $SK = 'assets/file/pangkat/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl = date("Y-m-d H:i:s");
                $data = $this->pangkat->pangkat_tambah($id, $nip, $kode, $pangkat, $noSK, $tglSK, $tmt, $SK, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['Pangkat_SK']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function pangkat_ubah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/pangkat';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('Pangkat_SK')) {
                // $status = 'error';
                // $msg = $this->upload->display_errors('', '');

                $id = $this->input->post('PangkatGol_ID');
                $nip = $this->input->post('Auditor_NIP');
                $kode = $this->input->post('Pangkat_Kode');
                $pangkat = $this->input->post('Pangkat_Nama');
                $noSK = $this->input->post('Pangkat_NoSuratKeputusan');
                $tglSK = database_date($this->input->post('Pangkat_TglSuratKeputusan'));
                $tmt = database_date($this->input->post('Pangkat_TglMulaiTugas'));
                $SK = $this->input->post('Pangkat_SK_Lama');
                $diubaholeh = $this->session->userdata('NIP');
                $diubahtgl = date('Y-m-d H:i:s');
                $data = $this->pangkat->pangkat_ubah($id, $nip, $kode, $pangkat, $noSK, $tglSK, $tmt, $SK, $diubaholeh, $diubahtgl);

            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id = $this->input->post('PangkatGol_ID');
                $nip = $this->input->post('Auditor_NIP');
                $kode = $this->input->post('Pangkat_Kode');
                $pangkat = $this->input->post('Pangkat_Nama');
                $noSK = $this->input->post('Pangkat_NoSuratKeputusan');
                $tglSK = database_date($this->input->post('Pangkat_TglSuratKeputusan'));
                $tmt = database_date($this->input->post('Pangkat_TglMulaiTugas'));
                $SK = 'assets/file/pangkat/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl = date("Y-m-d H:i:s");
                $data = $this->pangkat->pangkat_ubahfile($id, $nip, $kode, $pangkat, $noSK, $tglSK, $tmt, $SK, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['Pangkat_SK']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function pangkat_hapus()
    {
        $id = $this->input->post('id');
        $data = $this->pangkat->pangkat_hapus($id);
        echo json_encode($data);
    }

    public function get_pangkat()
    {
        $kode = $this->input->post('value');
        $pangkat = $this->auditor->get_pangkat($kode);

        $data = array();
        foreach ($pangkat as $row) {
            $data['value'] = $row->Pangkat_Nama;
        }
        echo $data['value'];
    }

    /*
    | Begin Of Function Script
    | Bagian CRUD Riwayat Jabatan
    */
    public function jabatan_detail()
    {
        $nip   = $this->input->get('id');
        $data = $this->jabatan->jabatan_riwayat($nip);
        echo json_encode($data);
    }

    public function jabatan_ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->jabatan->jabatan_ambil($nip);
        echo json_encode($data);
    }

    public function jabatan_tambah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/jabatan';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('Jabatan_SK')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id = $this->input->post('Jabatan_ID');
                $nip = $this->input->post('Auditor_NIP');
                $kode = $this->input->post('Jabatan_Kode');
                $jabatan = $this->input->post('JenjangJabatan_Kode');
                $noSK = $this->input->post('Jabatan_NoSuratKeputusan');
                $tglSK = database_date($this->input->post('Jabatan_TglSuratKeputusan'));
                $tmt = database_date($this->input->post('Jabatan_TglMulaiTugas'));
                $SK = 'assets/file/jabatan/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl = date("Y-m-d H:i:s");
                $data = $this->jabatan->jabatan_tambah($id, $nip, $kode, $jabatan, $noSK, $tglSK, $tmt, $SK, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['Jabatan_SK']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function jabatan_ubah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/jabatan';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('Jabatan_SK')) {
                // $status = 'error';
                // $msg = $this->upload->display_errors('', '');

                $id = $this->input->post('Jabatan_ID');
                $nip = $this->input->post('Auditor_NIP');
                $kode = $this->input->post('Jabatan_Kode');
                $jabatan = $this->input->post('JenjangJabatan_Kode');
                $noSK = $this->input->post('Jabatan_NoSuratKeputusan');
                $tglSK = database_date($this->input->post('Jabatan_TglSuratKeputusan'));
                $tmt = database_date($this->input->post('Jabatan_TglMulaiTugas'));
                $SK = $this->input->post('Jabatan_SK_Lama');
                $diubaholeh = $this->session->userdata('NIP');
                $diubahtgl = date('Y-m-d H:i:s');
                $data = $this->jabatan->jabatan_ubah($id, $nip, $kode, $jabatan, $noSK, $tglSK, $tmt, $SK, $diubaholeh, $diubahtgl);

            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id = $this->input->post('Jabatan_ID');
                $nip = $this->input->post('Auditor_NIP');
                $kode = $this->input->post('Jabatan_Kode');
                $jabatan = $this->input->post('JenjangJabatan_Kode');
                $noSK = $this->input->post('Jabatan_NoSuratKeputusan');
                $tglSK = database_date($this->input->post('Jabatan_TglSuratKeputusan'));
                $tmt = database_date($this->input->post('Jabatan_TglMulaiTugas'));
                $SK = 'assets/file/jabatan/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl = date("Y-m-d H:i:s");
                $data = $this->jabatan->jabatan_ubahfile($id, $nip, $kode, $jabatan, $noSK, $tglSK, $tmt, $SK, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['Jabatan_SK']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function jabatan_hapus()
    {
        $id = $this->input->post('id');
        $data = $this->jabatan->jabatan_hapus($id);
        echo json_encode($data);
    }

    public function get_jenjangjabatan()
    {
        $jenjangjabatan_kode = $this->input->post('value');
        $jenjangjabatan = $this->auditor->get_jenjangjabatan($jenjangjabatan_kode);

        echo '<select name="">';
        echo '<option value="">Pilih Jenjang Jabatan</option>';
        foreach ($jenjangjabatan as $row) {
            echo '<option value="'.$row->JenjangJabatan_Kode.'">'.$row->JenjangJabatan_Nama.'</option>';
        }
        echo '</select>';
    }

    /*
    | Begin Of Function Script
    | Bagian CRUD Riwayat Diklat Sertifikasi JFA
    */
    public function sertifikasijfa_detail()
    {
        $nip   = $this->input->get('id');
        $data = $this->sertifikasijfa->sertifikasijfa_riwayat($nip);
        echo json_encode($data);
    }

    public function sertifikasijfa_ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->sertifikasijfa->sertifikasijfa_ambil($nip);
        echo json_encode($data);
    }

    public function sertifikasijfa_tambah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/sert_jfa';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('SertifikasiJFA_Sertifikat')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id = $this->input->post('SertifikasiJFA_ID');
                $nip = $this->input->post('Auditor_NIP');
                $kode = $this->input->post('Diklat_Kode_JFA');
                $mulaidiklat = database_date($this->input->post('SertifikasiJFA_TglMulaiDiklat'));
                $akhirdiklat = database_date($this->input->post('SertifikasiJFA_TglAkhirDiklat'));
                $nostmpl = $this->input->post('SertifikasiJFA_NoSTMPL');
                $tglstmpl = database_date($this->input->post('SertifikasiJFA_TglSTMPL'));
                $nosttpl = $this->input->post('SertifikasiJFA_NoSTTPL');
                $tglsttpl = database_date($this->input->post('SertifikasiJFA_TglSTTPL'));
                $sertifikat = 'assets/file/sert_jfa/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl = date("Y-m-d H:i:s");
                $data = $this->sertifikasijfa->sertifikasijfa_tambah($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $nosttpl, $tglsttpl, $sertifikat, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['SertifikasiJFA_Sertifikat']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function sertifikasijfa_ubah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/sert_jfa';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('SertifikasiJFA_Sertifikat')) {
                // $status = 'error';
                // $msg = $this->upload->display_errors('', '');

                $id = $this->input->post('SertifikasiJFA_ID');
                $nip = $this->input->post('Auditor_NIP');
                $kode = $this->input->post('Diklat_Kode_JFA');
                $mulaidiklat = database_date($this->input->post('SertifikasiJFA_TglMulaiDiklat'));
                $akhirdiklat = database_date($this->input->post('SertifikasiJFA_TglAkhirDiklat'));
                $nostmpl = $this->input->post('SertifikasiJFA_NoSTMPL');
                $tglstmpl = database_date($this->input->post('SertifikasiJFA_TglSTMPL'));
                $nosttpl = $this->input->post('SertifikasiJFA_NoSTTPL');
                $tglsttpl = database_date($this->input->post('SertifikasiJFA_TglSTTPL'));
                $sertifikat = $this->input->post('SertifikasiJFA_Sertifikat_Lama');
                $diubaholeh = $this->session->userdata('NIP');
                $diubahtgl = date('Y-m-d H:i:s');
                $data = $this->sertifikasijfa->sertifikasijfa_ubah($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $nosttpl, $tglsttpl, $sertifikat, $diubaholeh, $diubahtgl);

            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id = $this->input->post('SertifikasiJFA_ID');
                $nip = $this->input->post('Auditor_NIP');
                $kode = $this->input->post('Diklat_Kode_JFA');
                $mulaidiklat = database_date($this->input->post('SertifikasiJFA_TglMulaiDiklat'));
                $akhirdiklat = database_date($this->input->post('SertifikasiJFA_TglAkhirDiklat'));
                $nostmpl = $this->input->post('SertifikasiJFA_NoSTMPL');
                $tglstmpl = database_date($this->input->post('SertifikasiJFA_TglSTMPL'));
                $nosttpl = $this->input->post('SertifikasiJFA_NoSTTPL');
                $tglsttpl = database_date($this->input->post('SertifikasiJFA_TglSTTPL'));
                $sertifikat = 'assets/file/sert_jfa/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl = date("Y-m-d H:i:s");
                $data = $this->sertifikasijfa->sertifikasijfa_ubahfile($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $nosttpl, $tglsttpl, $sertifikat, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['SertifikasiJFA_Sertifikat']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function sertifikasijfa_hapus()
    {
        $id = $this->input->post('id');
        $data = $this->sertifikasijfa->sertifikasijfa_hapus($id);
        echo json_encode($data);
    }

    /*
    | Begin Of Function Script
    | Bagian CRUD Riwayat Diklat Teknis Substantif
    */
    public function teknissubstantif_detail()
    {
        $nip   = $this->input->get('id');
        $data = $this->teknissubstantif->teknissubstantif_riwayat($nip);
        echo json_encode($data);
    }

    public function teknissubstantif_ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->teknissubstantif->teknissubstantif_ambil($nip);
        echo json_encode($data);
    }

    public function teknissubstantif_tambah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/sert_substantif';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('TeknisSubstantif_Sertifikat')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id         = $this->input->post('TeknisSubstantif_ID');
                $nip        = $this->input->post('Auditor_NIP');
                $kode       = $this->input->post('Diklat_Kode_TekSub');
                $mulaidiklat= database_date($this->input->post('TeknisSubstantif_TglMulai'));
                $akhirdiklat= database_date($this->input->post('TeknisSubstantif_TglAkhir'));
                $nostmpl    = $this->input->post('TeknisSubstantif_NoSTMPL');
                $tglstmpl   = database_date($this->input->post('TeknisSubstantif_TglSTMPL'));
                $sertifikat = 'assets/file/sert_substantif/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl  = date("Y-m-d H:i:s");
                $data = $this->teknissubstantif->teknissubstantif_tambah($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['TeknisSubstantif_Sertifikat']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function teknissubstantif_ubah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/sert_substantif';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('TeknisSubstantif_Sertifikat')) {
                // $status = 'error';
                // $msg = $this->upload->display_errors('', '');

                $id         = $this->input->post('TeknisSubstantif_ID');
                $nip        = $this->input->post('Auditor_NIP');
                $kode       = $this->input->post('Diklat_Kode_TekSub');
                $mulaidiklat= database_date($this->input->post('TeknisSubstantif_TglMulai'));
                $akhirdiklat= database_date($this->input->post('TeknisSubstantif_TglAkhir'));
                $nostmpl    = $this->input->post('TeknisSubstantif_NoSTMPL');
                $tglstmpl   = database_date($this->input->post('TeknisSubstantif_TglSTMPL'));
                $sertifikat = $this->input->post('TeknisSubstantif_Sertifikat_Lama');
                $diubaholeh = $this->session->userdata('NIP');
                $diubahtgl  = date('Y-m-d H:i:s');
                $data = $this->teknissubstantif->teknissubstantif_ubah($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $diubaholeh, $diubahtgl);

            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id         = $this->input->post('TeknisSubstantif_ID');
                $nip        = $this->input->post('Auditor_NIP');
                $kode       = $this->input->post('Diklat_Kode_TekSub');
                $mulaidiklat= database_date($this->input->post('TeknisSubstantif_TglMulai'));
                $akhirdiklat= database_date($this->input->post('TeknisSubstantif_TglAkhir'));
                $nostmpl    = $this->input->post('TeknisSubstantif_NoSTMPL');
                $tglstmpl   = database_date($this->input->post('TeknisSubstantif_TglSTMPL'));
                $sertifikat = 'assets/file/sert_substantif/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl  = date("Y-m-d H:i:s");
                $data = $this->teknissubstantif->teknissubstantif_ubahfile($id, $nip, $kode, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $diubaholeh, $diubahtgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['TeknisSubstantif_Sertifikat']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function teknissubstantif_hapus()
    {
        $id = $this->input->post('id');
        $data = $this->teknissubstantif->teknissubstantif_hapus($id);
        echo json_encode($data);
    }

    /*
    | Begin Of Function Script
    | Bagian CRUD Riwayat Diklat Sertifikasi Profesi
    */
    public function sertifikasiprofesi_detail()
    {
        $nip   = $this->input->get('id');
        $data = $this->sertifikasiprofesi->sertifikasiprofesi_riwayat($nip);
        echo json_encode($data);
    }

    public function sertifikasiprofesi_ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->sertifikasiprofesi->sertifikasiprofesi_ambil($nip);
        echo json_encode($data);
    }

    public function sertifikasiprofesi_tambah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/sert_profesi';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('SertifikasiProfesi_Sertifikat')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id = $this->input->post('SertifikasiProfesi_ID');
                $nip = $this->input->post('Auditor_NIP');
                $nama = $this->input->post('SertifikasiProfesi_Nama');
                $lembaga = $this->input->post('SertifikasiProfesi_Lembaga');
                $gelar = $this->input->post('SertifikasiProfesi_Gelar');
                $mulaidiklat = database_date($this->input->post('SertifikasiProfesi_TglMulai'));
                $akhirdiklat = database_date($this->input->post('SertifikasiProfesi_TglAkhir'));
                $nostmpl = $this->input->post('SertifikasiProfesi_NoSTMPL');
                $tglstmpl = database_date($this->input->post('SertifikasiProfesi_TglSTMPL'));
                $sertifikat = 'assets/file/sert_profesi/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl  = date("Y-m-d H:i:s");
                $data = $this->sertifikasiprofesi->sertifikasiprofesi_tambah($id, $nip, $nama, $lembaga, $gelar, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['SertifikasiProfesi_Sertifikat']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function sertifikasiprofesi_ubah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/sert_profesi';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('SertifikasiProfesi_Sertifikat')) {
                // $status = 'error';
                // $msg = $this->upload->display_errors('', '');

                $id = $this->input->post('SertifikasiProfesi_ID');
                $nip = $this->input->post('Auditor_NIP');
                $nama = $this->input->post('SertifikasiProfesi_Nama');
                $lembaga = $this->input->post('SertifikasiProfesi_Lembaga');
                $gelar = $this->input->post('SertifikasiProfesi_Gelar');
                $mulaidiklat = database_date($this->input->post('SertifikasiProfesi_TglMulai'));
                $akhirdiklat = database_date($this->input->post('SertifikasiProfesi_TglAkhir'));
                $nostmpl = $this->input->post('SertifikasiProfesi_NoSTMPL');
                $tglstmpl = database_date($this->input->post('SertifikasiProfesi_TglSTMPL'));
                $sertifikat = $this->input->post('SertifikasiProfesi_Sertifikat_Lama');
                $diubaholeh = $this->session->userdata('NIP');
                $diubahtgl = date('Y-m-d H:i:s');
                $data = $this->sertifikasiprofesi->sertifikasiprofesi_ubah($id, $nip, $nama, $lembaga, $gelar, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $diubaholeh, $diubahtgl);

            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id = $this->input->post('SertifikasiProfesi_ID');
                $nip = $this->input->post('Auditor_NIP');
                $nama = $this->input->post('SertifikasiProfesi_Nama');
                $lembaga = $this->input->post('SertifikasiProfesi_Lembaga');
                $gelar = $this->input->post('SertifikasiProfesi_Gelar');
                $mulaidiklat = database_date($this->input->post('SertifikasiProfesi_TglMulai'));
                $akhirdiklat = database_date($this->input->post('SertifikasiProfesi_TglAkhir'));
                $nostmpl = $this->input->post('SertifikasiProfesi_NoSTMPL');
                $tglstmpl = database_date($this->input->post('SertifikasiProfesi_TglSTMPL'));
                $sertifikat = 'assets/file/sert_profesi/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl  = date("Y-m-d H:i:s");
                $data = $this->sertifikasiprofesi->sertifikasiprofesi_ubahfile($id, $nip, $nama, $lembaga, $gelar, $mulaidiklat, $akhirdiklat, $nostmpl, $tglstmpl, $sertifikat, $diubaholeh, $diubahtgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['SertifikasiProfesi_Sertifikat']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function sertifikasiprofesi_hapus()
    {
        $id = $this->input->post('id');
        $data = $this->sertifikasiprofesi->sertifikasiprofesi_hapus($id);
        echo json_encode($data);
    }

    /*
    | Begin Of Function Script
    | Bagian CRUD Riwayat Angka Kredit
    */
    public function angkakredit_detail()
    {
        $nip   = $this->input->get('id');
        $data = $this->angkakredit->angkakredit_riwayat($nip);
        echo json_encode($data);
    }

    public function angkakredit_ambil()
    {
        $nip = $this->input->get('id');
        $data = $this->angkakredit->angkakredit_ambil($nip);
        echo json_encode($data);
    }

    public function angkakredit_tambah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/pak';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('AngkaKredit_PAK')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id         = $this->input->post('AngkaKredit_ID');
                $nip        = $this->input->post('Auditor_NIP');
                $nilai      = $this->input->post('AngkaKredit_Nilai');
                $nopak      = $this->input->post('AngkaKredit_NoPAK');
                $tglpak     = database_date($this->input->post('AngkaKredit_TglPAK'));
                $mulaipak   = database_date($this->input->post('AngkaKredit_TglMulai'));
                $akhirpak   = database_date($this->input->post('AngkaKredit_TglAkhir'));
                $pak        = 'assets/file/pak/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl  = date("Y-m-d H:i:s");
                $data = $this->angkakredit->angkakredit_tambah($id, $nip, $nilai, $nopak, $tglpak, $mulaipak, $akhirpak, $pak, $dibuatoleh, $dibuattgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['AngkaKredit_PAK']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function angkakredit_ubah()
    {
        if ($status != "error") {
            $config['upload_path'] = './assets/file/pak';
            $config['allowed_types'] = 'pdf|jpg|png|doc|docx';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = true;
            $config['overwrite'] = false;
            $config['max_filename_increment'] = 100;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('AngkaKredit_PAK')) {
                // $status = 'error';
                // $msg = $this->upload->display_errors('', '');

                $id         = $this->input->post('AngkaKredit_ID');
                $nip        = $this->input->post('Auditor_NIP');
                $nilai      = $this->input->post('AngkaKredit_Nilai');
                $nopak      = $this->input->post('AngkaKredit_NoPAK');
                $tglpak     = database_date($this->input->post('AngkaKredit_TglPAK'));
                $mulaipak   = database_date($this->input->post('AngkaKredit_TglMulai'));
                $akhirpak   = database_date($this->input->post('AngkaKredit_TglAkhir'));
                $pak        = $this->input->post('AngkaKredit_PAK_Lama');
                $diubaholeh = $this->session->userdata('NIP');
                $diubahtgl  = date('Y-m-d H:i:s');
                $data = $this->angkakredit->angkakredit_ubah($id, $nip, $nilai, $nopak, $tglpak, $mulaipak, $akhirpak, $pak, $diubaholeh, $diubahtgl);

            } else {

                $file = array('upload_data' => $this->upload->data()); //ambil file name yang diupload

                $id         = $this->input->post('AngkaKredit_ID');
                $nip        = $this->input->post('Auditor_NIP');
                $nilai      = $this->input->post('AngkaKredit_Nilai');
                $nopak      = $this->input->post('AngkaKredit_NoPAK');
                $tglpak     = database_date($this->input->post('AngkaKredit_TglPAK'));
                $mulaipak   = database_date($this->input->post('AngkaKredit_TglMulai'));
                $akhirpak   = database_date($this->input->post('AngkaKredit_TglAkhir'));
                $pak        = 'assets/file/pak/' .$file['upload_data']['file_name'];
                $dibuatoleh = $this->session->userdata('NIP');
                $dibuattgl  = date("Y-m-d H:i:s");
                $data = $this->angkakredit->angkakredit_ubahfile($id, $nip, $nilai, $nopak, $tglpak, $mulaipak, $akhirpak, $pak, $diubaholeh, $diubahtgl);

                // $data = $this->files_model->insert_file($data['file_name'], $_POST['title']);
                if ($data) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                } else {
                    unlink($file['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES['AngkaKredit_PAK']);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
        // echo json_encode($data);
    }

    public function angkakredit_hapus()
    {
        $id = $this->input->post('id');
        $data = $this->angkakredit->angkakredit_hapus($id);
        echo json_encode($data);
    }
}
