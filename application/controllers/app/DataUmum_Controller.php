<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DataUmum_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
    }

    public function index()
    {
        $data["title"]="Data Umum";
        // $data["subtitle"]="Selamat Datang";

        $this->template->display('dataumum_view', $data);
    }
}
