<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LapKomposisiAuditorTerampil_Controller extends CI_Controller
{
  function __construct()
  {
    parent:: __construct();
    $this->load->model('Dasbor_model', 'dasbor');

  }

  public function index()
  {
      $RoleGroup = $this->session->userdata('RoleGroup');

      if ($RoleGroup === 'Level 1' || $RoleGroup === 'Pusbin JFA' ) {
          $this->pusbin();
      } elseif ($RoleGroup === 'Level 2' || $RoleGroup === 'Perwakilan BPKP') {
          $this->koorwas();
      } elseif ($RoleGroup === 'Level 3' || $RoleGroup === 'Unit Kerja') {
          $this->unitkerja();
      } else {
          redirect('error');
          exit;
      }
  }

  public function pusbin()
  {
      // Summary Auditor di Dasbor Pusbin
      $auditorpusbin = $this->dasbor->get_auditorpusbin();

      $data = array();
      foreach ($auditorpusbin as $row) {
				$data['APIPPusat_JumlahAuditorTerampil'] = $row->APIPPusat_JumlahAuditorTerampil;
				$data['APIPPusat_AuditorPenyelia'] = $row->APIPPusat_AuditorPenyelia;
				$data['APIPPusat_AuditorPelaksanaLanjutan'] = $row->APIPPusat_AuditorPelaksanaLanjutan;
				$data['APIPPusat_AuditorPelaksana'] = $row->APIPPusat_AuditorPelaksana;

        $data['APIPPusat_JumlahTerampil'] = $row->APIPPusat_AuditorPenyelia+$row->APIPPusat_AuditorPelaksanaLanjutan+$row->APIPPusat_AuditorPelaksana;

				// --------------------------------------------------------------------
				$data['BPKP_JumlahAuditorTerampil'] = $row->BPKP_JumlahAuditorTerampil;
				$data['BPKP_AuditorPenyelia'] = $row->BPKP_AuditorPenyelia;
				$data['BPKP_AuditorPelaksanaLanjutan'] = $row->BPKP_AuditorPelaksanaLanjutan;
				$data['BPKP_AuditorPelaksana'] = $row->BPKP_AuditorPelaksana;

        $data['BPKP_JumlahTerampil'] = $row->BPKP_AuditorPenyelia+$row->BPKP_AuditorPelaksanaLanjutan+$row->BPKP_AuditorPelaksana;
				// --------------------------------------------------------------------
				$data['BHMN_JumlahAuditorTerampil'] = $row->BHMN_JumlahAuditorTerampil;
				$data['BHMN_AuditorPenyelia'] = $row->BHMN_AuditorPenyelia;
				$data['BHMN_AuditorPelaksanaLanjutan'] = $row->BHMN_AuditorPelaksanaLanjutan;
				$data['BHMN_AuditorPelaksana'] = $row->BHMN_AuditorPelaksana;

        $data['BHMN_JumlahTerampil'] = $row->BHMN_AuditorPenyelia+$row->BHMN_AuditorPelaksanaLanjutan+$row->BHMN_AuditorPelaksana;
				// --------------------------------------------------------------------
				$data['APIPDaerah_JumlahAuditorTerampil'] = $row->APIPDaerah_JumlahAuditorTerampil;
				$data['APIPDaerah_AuditorPenyelia'] = $row->APIPDaerah_AuditorPenyelia;
				$data['APIPDaerah_AuditorPelaksanaLanjutan'] = $row->APIPDaerah_AuditorPelaksanaLanjutan;
				$data['APIPDaerah_AuditorPelaksana'] = $row->APIPDaerah_AuditorPelaksana;

        $data['APIPDaerah_JumlahTerampil'] = $row->APIPDaerah_AuditorPenyelia+$row->APIPDaerah_AuditorPelaksanaLanjutan+$row->APIPDaerah_AuditorPelaksana;
        $data['TotalPenyelia'] = $row->APIPPusat_AuditorPenyelia+$row->BPKP_AuditorPenyelia+ $row->BHMN_AuditorPenyelia+$row->APIPDaerah_AuditorPenyelia;
        $data['TotalPelaksanaLanjutan'] = $row->APIPPusat_AuditorPelaksanaLanjutan+$row->BPKP_AuditorPelaksanaLanjutan+$row->BHMN_AuditorPelaksanaLanjutan+$row->APIPDaerah_AuditorPelaksanaLanjutan;
        $data['TotalPelaksana'] = $row->APIPPusat_AuditorPelaksana+$row->BPKP_AuditorPelaksana+$row->BHMN_AuditorPelaksana+$row->APIPDaerah_AuditorPelaksana;
				$data['Total_JumlahTerampil'] = $row->APIPPusat_JumlahAuditorTerampil+$row->BPKP_JumlahAuditorTerampil+$row->BHMN_JumlahAuditorTerampil+$row->APIPDaerah_JumlahAuditorTerampil;
      }


      $data["title"]="Laporan Komposisi Auditor Terampil";
      $this->template->display('pelaporan/lap_komposisiauditorterampil', $data);
  }

  public function koorwas()
  {
      // Summary Auditor di Dasbor Koorwas
      $koorwas_kode = $this->session->userdata('KodeUnitKerja');
      $auditorunit = $this->dasbor->get_auditorkoorwas($koorwas_kode);

      $data = array();
      foreach ($auditorunit as $row) {
          $data['auditor_penyelia'] = $row->UnitKerja_AuditorPenyelia+$row->APIPDaerah_AuditorPenyelia;
          $data['auditor_pelaksana_lanjutan'] = $row->UnitKerja_PelaksanaLanjutan+$row->APIPDaerah_AuditorPelaksanaLanjutan;
          $data['auditor_pelaksana'] = $row->UnitKerja_AuditorPelaksana+ $row->APIPDaerah_AuditorPelaksana;

          $data['jumlah_auditor_terampil'] = $row->UnitKrja_JumlahAuditorTerampil+$row->APIPDaerah_JumlahAuditorTerampil;
      }


      $data["title"]="Laporan Komposisi Auditor Terampil";
      $this->template->display('pelaporan/lap_komposisiauditorterampil_koorwas', $data);
  }

  function unitkerja()
  {
      // Summary Auditor di Dasbor Unit APIP
      $unitkerja_kode = $this->session->userdata('KodeUnitKerja');
      $auditorunit = $this->dasbor->get_auditorunit($unitkerja_kode);

      $data = array();
      foreach ($auditorunit as $row) {
          $data['auditor_pelaksana'] = $row->Auditor_Pelaksana;
          $data['auditor_pelaksana_lanjutan'] = $row->Auditor_Pelaksana_Lanjutan;
          $data['auditor_penyelia'] = $row->Auditor_Penyelia;

          $data['jumlah_auditor_terampil'] = $row->Auditor_Terampil;
      }


      $data["title"]="Laporan Komposisi Auditor Terampil";
      $this->template->display('pelaporan/lap_komposisiauditorterampil_unitkerja', $data);
  }
}
