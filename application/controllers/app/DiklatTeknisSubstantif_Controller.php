<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DiklatTeknisSubstantif_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        is_login();
        $this->load->model('DiklatTeknisSubstantif_model', 'teknissubstantif');
    }

    public function index()
    {
        $data["title"]="Riwayat Diklat Teknis Substantif";
        $this->template->display('diklatteknissubstantif/diklatteknissubstantif_list_view', $data);
    }

    public function data()
    {
        $nip = $this->session->userdata('NIP');
        $data = $this->teknissubstantif->teknissubstantif_riwayat($nip);
        echo json_encode($data);
    }
}
