<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AngkaKredit_Controller extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        is_login();
        $this->load->model('angkakredit_model', 'angkakredit');
    }

    public function index()
    {
        $data["title"]="Riwayat Angka Kredit";
        $this->template->display('angkakredit/angkakredit_list_view', $data);
    }

    public function data()
    {
        $nip = $this->session->userdata('NIP');
        $data = $this->angkakredit->angkakredit_riwayat($nip);
        echo json_encode($data);
    }

    public function riwayat()
    {
        $nip = $this->session->userdata('NIP');
        $data["title"]="Riwayat Angka Kredit";
        // $this->template->display('angkakredit/angkakredit_list_view', $data);


        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://api.matapanda.online/_api/pak/nip/".$nip,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Auth-Key: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsYXN0X2xvZ2luIjoiMjAxOC0wNC0yMiAxNDozNzoyNyIsImV4cGlyZWRfYXQiOiIyMDE4LTA0LTIzIDAyOjM3OjI3In0.Kvh0FwNAZPETN-CRNRvJpoBLApO_PWmVOUIz3xU7Ah4",
            "User-ID: 1"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }

        $this->template->display('angkakredit/angkakredit_list_view', $data);
    }
}
