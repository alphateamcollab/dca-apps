<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function index(){
	  //load mpdf libray
	  $this->load->library('M_pdf');

	  $mpdf = $this->m_pdf->load([
	    'mode' => 'utf-8',
	    'format' => 'A4'
	  ]);

	  $mpdf->WriteHTML('
		<!--begin::Page Vendors -->
		<link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendors -->
		<!--begin::Base Styles -->
		<link rel="stylesheet" type="text/css" href=" ' .base_url('assets/vendors/base/vendors.bundle.css'). ' " />
		<link rel="stylesheet" type="text/css" href=" ' .base_url('assets/vendors/base/style.bundle.css'). ' " />

		<div class="m-content">
		  <div class="m-portlet m-portlet m-portlet--info m-portlet--head-solid-bg m-portlet--head-sm m-portlet--bordered" id="print-area-2">
		    <div class="m-portlet__body  m-portlet__body--no-padding">
		      <div class="col-xl-12 order-1 order-xl-2 m--align-right">

		        <!-- Tittle Print -->
		          <div class="title">
		            <div class="m-portlet__body">
		              <div class="row">
		                <div class="col-sm-2">
		  								<!--begin::Section-->
		  								<div class="m-section">
		  									<h3 class="m-section__heading">
		  										Default headings
		  									</h3>
		  								</div>
		  								<!--end::Section-->
		                </div>
		                <div class="col-sm-8">
		                  <!--begin::Section-->
		    							<div class="m-section">
		    								<div class="m-section__content">
		    									<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
		    										<div class="m-demo__preview">
		    											<p class="lead">
		    												Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
		    											</p>
		    										</div>
		    									</div>
		    								</div>
		    							</div>
		    							<!--end::Section-->
		                </div>
		                <div class="col-sm-2">
		  								<!--begin::Section-->
		  								<div class="m-section">
		  									<h3 class="m-section__heading">
		  										Default headings
		  									</h3>
		  								</div>
		  								<!--end::Section-->
		                </div>
		              </div>
							</div>
		              <div class="row">
		                <div class="col-md-3">
		                  <div class="m-stack__item m-stack__item--middle m-stack__item--center m-brand__logo">
		                    <a class="m-brand__logo-wrapper">
		                        <img alt="" src=" ' .base_url('assets/vendors/media/img/logo/logooo.png'). ' "/>
		                    </a>
		                    <p>
		                      Pusbin JFA BPKP
		                    </p>
		                  </div>
		                </div>
		                <div class="col-md-6">
		                  <p class="m--font-info">KOMPOSISI AUDITOR TERAMPIL</p>
		                  <p class="m--font-info">Pusat Pembinaan Jabatan Fungsional Auditor</p>
		                </div>
		                <div class="col-md-3">
		                  <div class="m-stack__item m-stack__item--middle m-stack__item--center m-brand__logo">
		                    <a class="m-brand__logo-wrapper">
		                        <img alt="" src="' .base_url('assets/vendors/media/img/logo/logooo.png'). '"/>
		                    </a>
		                    <br>
		                    <p>
		                      Pusbin JFA BPKP
		                    </p>
		                  </div>
		                </div>

		              </div>
		              <hr color="#000000">
		            </div>
		        <!-- Tittle Print -->
		        <div class="m-separator m-separator--dashed d-xl-none"></div>
		      </div>
		      <div class="row m-row--no-padding m-row--col-separator-xl">
		        <!-- <div class="row"> -->
		        <div class="col-md-4"></div>
		        <div class="col-md-4">
		          <div class="m-portlet__body">
		            <div class="m-widget4 m--font-info" >
		              <div id="a_terampil" style="height: 350px;"></div>
		            </div>
		          </div>
		        </div>
		        <div class="col-md-4"></div>
		    <!-- </div> -->
		        <div class="col-12">
		          <div class="m-portlet__body">
		            <table border="1" width="100%" style="border-collapse:collapse;" align="center">
		                <thead class="table-info">
		                  <tr align="center">
		                    <th class="m--icon-font-size-lg2">Auditor Terampil</th>
		                    <th class="m--icon-font-size-lg2">Jumlah</th>
		                  </tr>
		                </thead>
		                <tbody>
		                  <tr align="center">
		                    <td class="m--icon-font-size-lg2">Penyelia</td>
		                    <td class="m--icon-font-size-lg2"><?php echo $auditor_penyelia?></td>
		                  </tr>
		                  <tr align="center">
		                    <td class="m--icon-font-size-lg2">Pelaksana Lanjutan</td>
		                    <td class="m--icon-font-size-lg2"><?php echo $auditor_pelaksana_lanjutan?></td>
		                  </tr>
		                  <tr align="center">
		                    <td class="m--icon-font-size-lg2">Pelaksana</td>
		                    <td class="m--icon-font-size-lg2"><?php echo $auditor_pelaksana?></td>
		                  </tr>
		                </tbody>
		                <tfoot class="table-info">
		                  <tr align="center">
		                    <td class="m--icon-font-size-lg2 m--font-boldest">Total</td>
		                    <td class="m--icon-font-size-lg2 m--font-boldest "><?php echo $jumlah_auditor_terampil?></td>
		                  </tr>
		                </tfoot>
		              </table>
		          </div>
		        </div>
		      </div>
		    </div>
		  </div>
		</div>
');

	  $mpdf->Output();
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

}
