<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Error_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
    }

    public function index()
    {
        $this->template->display('errors/error404');
    }

    public function error403()
    {
        $this->template->display('errors/error403');
    }
}
